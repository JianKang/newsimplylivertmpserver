#pragma once
#include "../../Lib.Base/audioFrame.h"
#include "../../Lib.Base/videoFrame.h"
#include "../Lib.FrameConsumer/IFrameConsumer.h"

struct sFrameConsumer_Parameter;

class IFrameConsumerChannel
{
public:
	virtual ~IFrameConsumerChannel() {}
	virtual int addChannel(DWORD dwCnlID, const sFrameConsumer_Parameter& pCnlParameter) = 0;
	virtual int startChannel(DWORD nChannelID, const sFrameConsumer_StartParameter &_param) = 0;
	virtual int setFormat(DWORD dwCnlID, const FPTVideoFormat fpVideoFormat) = 0;
	virtual int OutputFrames(DWORD nChannelID, unsigned char *pVideoBuffer, DWORD dwVSize, unsigned char *pAudioBuffer, DWORD dwASize) = 0;
	virtual int OutputFrames(DWORD nChannelID, VideoFrame *pVideo, AudioFrame *pAudio, Timecode *tc) = 0;
	virtual bool WaitGenlockInterrupt(DWORD dwCnlID, DWORD _timeout = 50) = 0;
	virtual int stopChannel(DWORD nChannelID) = 0;
	virtual int removeChannel(DWORD dwCnlID) = 0;
};
