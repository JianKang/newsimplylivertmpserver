﻿#include "IFrameConsumer.h"
#include "FrameConsumerImp.h"
#include "../../Lib.Base/locker.h"

static IFrameConsumer* g_frameConsumer = nullptr;
IFrameConsumer* IFrameConsumer::getInstance()
{
	static Locker g_lock;
	LockHolder lock(g_lock);
	if (g_frameConsumer == nullptr)
		g_frameConsumer = new CFrameConsumerImp;
	return g_frameConsumer;
}

void IFrameConsumer::releaseInstance()
{
	if (g_frameConsumer != nullptr)
	{
		delete g_frameConsumer;
		g_frameConsumer = nullptr;
	}
}