﻿#pragma once
#include "../../Lib.Base/audioFrame.h"
#include "../../Lib.Base/videoFrame.h"
#include "../../Lib.base/BMRServiceDefine.h"

class IFrameCacheIn;

enum FrameConsumerType
{
	FCT_AJA,
	FCT_STREAM,
};

struct sFrameConsumer_Parameter
{
	FrameConsumerType fcType;
	FPTVideoFormat fpVideoFormat;
	DWORD dwBoardID;
	DWORD dwSdiID;

	sFrameConsumer_Parameter()
	{
		memset(this, 0, sizeof(*this));
	}

	sFrameConsumer_Parameter& operator =(const sFrameConsumer_Parameter& rhs)
	{
		fpVideoFormat = rhs.fpVideoFormat;
		dwBoardID = rhs.dwBoardID;
		dwSdiID = rhs.dwSdiID;
		return *this;
	}
};

struct sFrameConsumer_StartParameter
{
	BMRSetting	*bmrSetting;
	sFrameConsumer_StartParameter()
	{
		bmrSetting = nullptr;
	}
};

class IFrameConsumer
{
protected:
	IFrameConsumer() {}
	virtual ~IFrameConsumer() {}
public:
	static IFrameConsumer* getInstance();
	static void releaseInstance();
	virtual int addChannel(DWORD nChannelID, const sFrameConsumer_Parameter& pCnlParameter) = 0;
	virtual int startChannel(DWORD nChannelID, const sFrameConsumer_StartParameter &_param) = 0;
	virtual bool OutputFrames(DWORD nChannelID, unsigned char *pVideoBuffer, DWORD dwVSize, unsigned char *pAudioBuffer, DWORD dwASize) = 0;
	virtual int OutputFrames(DWORD nChannelID, VideoFrame *pVideo, AudioFrame *pAudio, Timecode *tc) = 0;
	virtual int stopChannel(DWORD nChannelID) = 0;
	virtual int removeChannel(DWORD dwCnlID) = 0;
	virtual bool WaitGenlockInterrupt() = 0;
};
