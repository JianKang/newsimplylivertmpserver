#pragma once
#include "IFrameConsumer.h"
#include "IFrameConsumerChannel.h"
#include "../../Lib.Base/locker.h"
#include <array>

struct sFCCnlInfo
{
	IFrameConsumerChannel* _pChannel;
	sFrameConsumer_Parameter stCnlParameter;

	sFCCnlInfo()
	{
		_pChannel = nullptr;
	}
};

class CFrameConsumerImp :public IFrameConsumer
{
	Locker						m_mapCnlInfoLock;
	array<sFCCnlInfo*, EM_ConsumerChannel_MAX> m_mapCnlInfo = { nullptr };

public:
	CFrameConsumerImp();
	~CFrameConsumerImp();

	int addChannel(DWORD nChannelID, const sFrameConsumer_Parameter& pCnlParameter) override;
	int startChannel(DWORD nChannelID,const sFrameConsumer_StartParameter &_param) override;
	bool OutputFrames(DWORD nChannelID, unsigned char *pVideoBuffer, DWORD dwVSize, unsigned char *pAudioBuffer, DWORD dwASize) override;
	int OutputFrames(DWORD nChannelID, VideoFrame *pVideo, AudioFrame *pAudio, Timecode *tc) override;
	int stopChannel(DWORD nChannelID) override;
	int removeChannel(DWORD dwCnlID) override;
	bool WaitGenlockInterrupt() override;
};
