#include <WinSock2.h>
#include "FrameConsumerImp.h"
#include "../../Lib.Logger/LogWriter.h"
#include "../Lib.FrameConsumerAJA/FrameConsumerAJA.h"
#include "../Lib.FrameConsumerLocal/FrameConsumerLocalFile.h"
#include "../Lib.FrameConsumerStream/FrameConsumerStream.h"
#include "../lib.FrameConsumerBlackMagic/FrameConsumerBlackMagic.h"
#include "../../Lib.base/Config.h"

#include "../lib.FrameConsumerDeltaCast/FrameConsumerDeltaCast.h"
static TCHAR* LOG_PATH = L"C:\\Logs\\SimplyLiveRTMPServer\\FrameConsumer.log";
CFrameConsumerImp::CFrameConsumerImp()
{
}

CFrameConsumerImp::~CFrameConsumerImp()
{
	LockHolder lock(m_mapCnlInfoLock);
	for (int i = 0; i < m_mapCnlInfo.size(); i++)
		CFrameConsumerImp::removeChannel(i);

	WriteLogW(LOG_PATH, InfoLevel, L"CFrameConsumer exit");
}

int CFrameConsumerImp::addChannel(DWORD nChannelID, const sFrameConsumer_Parameter& pCnlParameter)
{
	if (nChannelID >= EM_ConsumerChannel_MAX)
	{
		WriteLogW(LOG_PATH, InfoLevel, L"Failed.CFrameConsumer add channel %d  nChannelID[%d] >= EM_ConsumerChannel_MAX[%d]", nChannelID, nChannelID, EM_ConsumerChannel_MAX);
		return -10;
	}
	LockHolder lock(m_mapCnlInfoLock);
	sFCCnlInfo* stCnlInfo = new sFCCnlInfo;
	stCnlInfo->stCnlParameter = pCnlParameter;
	switch (pCnlParameter.fcType)
	{
	case FCT_AJA:
	{
		bool isDMAPlayout = false;
		stCnlInfo->_pChannel = new CFrameConsumerAJA(isDMAPlayout);
		break;
	}
	case FCT_STREAM:
	{
		stCnlInfo->_pChannel = new CFrameConsumerStream();
		break;
	}
	default:
	{
		stCnlInfo->_pChannel = nullptr;
		delete stCnlInfo;

		WriteLogW(LOG_PATH, InfoLevel, L"Failed.CFrameConsumer add channel %d  dontfount type=%d ", nChannelID, pCnlParameter.fcType);

		return -1;
	}
	}

	if (stCnlInfo->_pChannel != nullptr)
	{
		int nRes = stCnlInfo->_pChannel->addChannel(nChannelID, pCnlParameter);
		if (nRes != 0)
		{
			delete stCnlInfo->_pChannel;
			delete stCnlInfo;
			WriteLogW(LOG_PATH, InfoLevel, L"Failed.CFrameConsumer add channel %d  channel->addChannel()=%d", nChannelID, nRes);
			return nRes;
		}
		WriteLogW(LOG_PATH, InfoLevel, L"CFrameConsumer add channel %d", nChannelID);
		m_mapCnlInfo[nChannelID] = stCnlInfo;
		return 0;
	}
	WriteLogW(LOG_PATH, InfoLevel, L"Failed.CFrameConsumer add channel %d  dontfount type=%d 33 ", nChannelID, pCnlParameter.fcType);
	return -3;
}

int CFrameConsumerImp::startChannel(DWORD nChannelID, const sFrameConsumer_StartParameter &_param)
{
	/*
	string str = Config::getInstance()->getOutAddress(m_nCnl);
	memset(m_bmrSetting.netdriveSetting[m_nIndex].subNetdrive[0].keyCamera[m_nCnl].pathValue, 0, MAX_PATH);
	memcpy(m_bmrSetting.netdriveSetting[m_nIndex].subNetdrive[0].keyCamera[m_nCnl].pathValue,str.c_str(), str.size());
	*/
	if (nChannelID >= EM_ConsumerChannel_MAX)
		return -10;
	if (m_mapCnlInfo[nChannelID] && m_mapCnlInfo[nChannelID]->_pChannel)
		return m_mapCnlInfo[nChannelID]->_pChannel->startChannel(nChannelID, _param);
	return -1;
}

bool CFrameConsumerImp::OutputFrames(DWORD nChannelID, unsigned char *pVideoBuffer, DWORD dwVSize, unsigned char *pAudioBuffer, DWORD dwASize)
{
	if (nChannelID >= EM_ConsumerChannel_MAX)
		return false;
	if (m_mapCnlInfo[nChannelID] && m_mapCnlInfo[nChannelID]->_pChannel)
		return m_mapCnlInfo[nChannelID]->_pChannel->OutputFrames(nChannelID, pVideoBuffer, dwVSize, pAudioBuffer, dwASize) == 0;
	return false;
}
int CFrameConsumerImp::OutputFrames(DWORD nChannelID, VideoFrame *pVideo, AudioFrame *pAudio, Timecode *tc)
{
	if (nChannelID >= EM_ConsumerChannel_MAX)
		return 0xff;
	if (m_mapCnlInfo[nChannelID] && m_mapCnlInfo[nChannelID]->_pChannel)
		return m_mapCnlInfo[nChannelID]->_pChannel->OutputFrames(nChannelID, pVideo, pAudio, tc);
	return 0xff;
}
int CFrameConsumerImp::stopChannel(DWORD nChannelID)
{
	if (nChannelID >= EM_ConsumerChannel_MAX)
		return -10;
	if (m_mapCnlInfo[nChannelID] && m_mapCnlInfo[nChannelID]->_pChannel)
		return m_mapCnlInfo[nChannelID]->_pChannel->stopChannel(nChannelID);
	return -1;
}

int CFrameConsumerImp::removeChannel(DWORD dwCnlID)
{
	if (dwCnlID >= EM_ConsumerChannel_MAX)
		return -10;
	LockHolder lock(m_mapCnlInfoLock);
	if (m_mapCnlInfo[dwCnlID])
	{
		delete m_mapCnlInfo[dwCnlID]->_pChannel;
		delete m_mapCnlInfo[dwCnlID];
		m_mapCnlInfo[dwCnlID] = nullptr;
		WriteLogW(LOG_PATH, InfoLevel, L"CFrameConsumer remove channel %d", dwCnlID);
		return 0;
	}
	return -1;
}

bool CFrameConsumerImp::WaitGenlockInterrupt()
{
	for (int i = 0; i < m_mapCnlInfo.size(); i++)
	{
		if (m_mapCnlInfo[i] && m_mapCnlInfo[i]->_pChannel)
			return m_mapCnlInfo[i]->_pChannel->WaitGenlockInterrupt(i);
	}
	return false;
}