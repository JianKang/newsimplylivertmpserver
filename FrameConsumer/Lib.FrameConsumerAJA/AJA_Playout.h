#pragma once
#include "IAJA_Playout.h"
#include "AJAHeader.h"
#include "../lib.Base/VideoFrame.h"
#include "../lib.Base/locker.h"

#include "ajaapi/ajastuff/common/circularbuffer.h"

class CAJA_Playout :public IAJA_Playout
{
	Locker				        m_lock_AJA;
	HANDLE						m_mutexAja;
	TCHAR	                    m_szMutexName[MAX_PATH];

	AJAThread *					            mPlayThread;				            ///< @brief	My worker thread object
	bool						            mWithAudio;				                ///< @brief	Capture and playout audio?
	NTV2Channel					            mOutputChannel;			                ///< @brief	The output channel I'm using
	NTV2OutputDestination		            mOutputDestination;		                ///< @brief	The output I'm using
	NTV2VideoFormat							mVideoFormat;			                ///< @brief	My video format
	NTV2FrameBufferFormat					mPixelFormat;			                ///< @brief	My pixel format
	NTV2EveryFrameTaskMode					mSavedTaskMode;			                ///< @brief	Previous task mode to restore
	bool						            mVancEnabled;			                ///< @brief	VANC enabled?
	bool						            mWideVanc;				                ///< @brief	Wide VANC?
	NTV2AudioSystem							mAudioSystem;			                ///< @brief	The audio system I'm using
	NTV2Crosspoint				            mChannelSpec;				            ///	The AutoCirculate channel spec I'm using
	AUTOCIRCULATE_TRANSFER_STRUCT           mOutputTransferStruct;					///	My A/C output transfer info
	AUTOCIRCULATE_TRANSFER_STATUS_STRUCT    mOutputTransferStatusStruct;			///	My A/C output status
	AVDataBuffer							mAVHostBuffer[CIRCULAR_BUFFER_SIZE];	///	My host buffers
	AJACircularBuffer <AVDataBuffer *>		mAVCircularBuffer;						///	My ring buffer
	bool						            mGlobalQuit;			                ///< @brief	Set "true" to gracefully stop
	uint32_t					            mVideoBufferSize;		                ///< @brief	My video buffer size, in bytes
	uint32_t					            mAudioBufferSize;		                ///< @brief	My audio buffer size, in bytes
	uint32_t *					            mpHostVideoBuffer;		                ///< @brief My host video buffer for burning in the timecode
	uint32_t *					            mpHostAudioBuffer;		                ///< @brief My host audio buffer for the samples matching the video buffer
	uint32_t					            mAudioInLastAddress;	                ///< @brief My record of the location of the last audio sample captured
	uint32_t					            mAudioOutLastAddress;	                ///< @brief My record of the location of the last audio sample played
	uint32_t					            mAudioOutWrapAddress;
	uint32_t					            mFramesProcessed;		                ///< @brief My count of the number of burned frames produced
	uint32_t					            mFramesDropped;			                ///< @brief My count of the number of dropped frames
	AUTOCIRCULATE_TRANSFER		            mOutputXferInfo;			            ///< @brief	My A/C output transfer info
	CNTV2Card								mDevice;								///< @brief	My CNTV2Card instance
	NTV2DeviceID							mDeviceID;								///< @brief	My device identifier
	BOOL									mb4KMode;
	BOOL									mFreeRun;
	bool									m_is4kInLevelA;
	bool									m_is4kOutLevelA;
	UINT32									m_ajaOutputBufferThreshold;

	AJAStatus	SetupVideo(void);
	void		readConfig();
	AJAStatus	SetupVideo4K(void);
	AJAStatus	SetupAudio(void);
	void		RouteOutputSignal(void);
	AJAStatus	SetupHostBuffers(void);
	AJAStatus	OutputFramesToAja(UINT *pVideoBuffer, UINT *pAudioBuffer, DWORD dwVSize, DWORD dwASize);
	AJAStatus	OutputFrames4KToAja(UINT *pVideoBuffer, UINT *pAudioBuffer, DWORD dwVSize, DWORD dwASize);
	void		SetUpOutputAutoCirculate();
	void		StartPlayThread(void);
	static void	PlayThreadStatic(AJAThread * pThread, void * pContext);
	void		PlayFrames(void);
	void		getAJAFormat(const sFrameConsumer_Parameter &pCnlParameter);
	void		clearCables(int nBoardID);

public:
	CAJA_Playout();
	virtual ~CAJA_Playout();

	int Initialize(DWORD dwCnlID, const sFrameConsumer_Parameter& pCnlParameter) override;
	void UnInitialize() override;
	bool OutputFrames(DWORD nChannelID, unsigned char* pVideoBuffer, DWORD dwVSize, unsigned char* pAudioBuffer, DWORD dwASize) override;
	bool OutputFrames(DWORD nChannelID, VideoFrame *pVideo, AudioFrame *pAudio, Timecode *tc)override;
	bool WaitGenlockInterrupt() override;
};
