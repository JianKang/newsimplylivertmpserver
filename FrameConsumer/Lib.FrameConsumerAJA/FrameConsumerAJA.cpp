﻿#include "FrameConsumerAJA.h"
#include "AJA_Playout.h"

CFrameConsumerAJA::CFrameConsumerAJA(bool _isDMA)
{
	m_pPlayout = make_shared<CAJA_Playout>();
}

CFrameConsumerAJA::~CFrameConsumerAJA()
{
}

int CFrameConsumerAJA::addChannel(DWORD dwCnlID, const sFrameConsumer_Parameter& pCnlParameter)
{
	if (m_pPlayout)
	{
		int nRes = m_pPlayout->Initialize(dwCnlID, pCnlParameter);
		if (nRes != 0)
			return nRes;
		return 0;
	}
	return 1;
}

int CFrameConsumerAJA::setFormat(DWORD dwCnlID, const FPTVideoFormat fpVideoFormat)
{
	return 0;
}

int CFrameConsumerAJA::removeChannel(DWORD dwCnlID)
{
	if (m_pPlayout)
	{
		m_pPlayout->UnInitialize();
		return 0;
	}
	return 1;
}

int CFrameConsumerAJA::OutputFrames(DWORD nChannelID, unsigned char* pVideoBuffer, DWORD dwVSize, unsigned char* pAudioBuffer, DWORD dwASize)
{
	if (m_pPlayout)
		return m_pPlayout->OutputFrames(nChannelID, pVideoBuffer, dwVSize, pAudioBuffer, dwASize) ? 0 : 1;
	return 1;
}

int CFrameConsumerAJA::OutputFrames(DWORD nChannelID, VideoFrame *pVideo, AudioFrame *pAudio, Timecode *tc)
{
	if (m_pPlayout)
		return m_pPlayout->OutputFrames(nChannelID, pVideo, pAudio, tc) ? 0 : 1;
	return 1;
}

bool CFrameConsumerAJA::WaitGenlockInterrupt(DWORD dwCnlID, DWORD _timeout)
{
	if (m_pPlayout)
		return m_pPlayout->WaitGenlockInterrupt();
	return false;
}

int CFrameConsumerAJA::startChannel(DWORD nChannelID, const sFrameConsumer_StartParameter &_param)
{
	return 0;
}

int CFrameConsumerAJA::stopChannel(DWORD nChannelID)
{
	return 0;
}