#pragma once
#pragma warning (disable:4005)

#include "ntv2enums.h"
#include "ntv2devicefeatures.h"
#include "ntv2devicescanner.h"
#include "ntv2democommon.h"
#include "ntv2utils.h"
#include "ajastuff/common/videotypes.h"
#include "ajastuff/common/timecode.h"
#include "ajastuff/common/timecodeburn.h"
#include "ajastuff/system/thread.h"
#include "ajastuff/system/process.h"
#include "ajastuff/system/systemtime.h"
#include "ajastuff/common/types.h"
#include "ajastuff/system/memory.h"

#include <future>
#include <functional>

#include "../../lib.Logger/LogWriter.h"

#define NTV2_AUDIOSIZE_MAX	(5* 1024*1024)

#define LOGPATH L"C:\\Logs\\SimplyLiveRTMPServer\\AJALog.log"
