/**
	@file		public.h
	@copyright	Copyright (C) 2009-2015 AJA Video Systems, Inc.  All rights reserved.
	@brief		Master header for the ajastuff library.
**/

#ifndef AJA_PUBLIC_H
#define AJA_PUBLIC_H

#include <string>
#include <list>
#include <vector>
#include <map>

#include "ajastuff/common/types.h"
#include "ajastuff/common/export.h"

/**
	@mainpage	The AJAStuff library is an API containing numerous handy utilities used in several AJA software products.
**/

#endif	//	AJA_PUBLIC_H
