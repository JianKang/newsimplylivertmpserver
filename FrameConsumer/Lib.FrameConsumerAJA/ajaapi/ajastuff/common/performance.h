/**
	@file		performance.h
	@copyright	Copyright (C) 2011-2015 AJA Video Systems, Inc.  All rights reserved.
	@brief		Declaration of the AJAPerformance class.
**/

#ifndef __PERFORMANCE_H
#define __PERFORMANCE_H

/////////////////////////////
// Includes
/////////////////////////////
#include "ajastuff/common/timer.h"
#include <string>

using std::string;


/////////////////////////////
// Typedef
/////////////////////////////
typedef struct
{
	string   name;
	uint32_t totalTime;
	uint32_t entries;
	uint32_t minTime;
	uint32_t maxTime;
} TimerStatisticsType;


/////////////////////////////
// Declarations
/////////////////////////////
class AJAPerformance
{
	public:
		AJAPerformance(void);
		~AJAPerformance(void);

		void Initialize(const string& name);
		void Start(void);
		void Stop(void);

		const TimerStatisticsType& GetTimerStatistics(void);


	private:
		AJATimer            mTimer;
		TimerStatisticsType mTimerStatistics;
};


#endif // __PERFORMANCE_H
//////////////////////// End of performance.h ///////////////////////

