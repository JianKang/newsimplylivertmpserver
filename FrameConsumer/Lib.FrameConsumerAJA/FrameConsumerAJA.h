﻿#pragma once
#include "../Lib.FrameConsumer/IFrameConsumerChannel.h"
#include "IAJA_Playout.h"

class CFrameConsumerAJA :public IFrameConsumerChannel
{
	shared_ptr<IAJA_Playout> m_pPlayout;

public:
	CFrameConsumerAJA(bool isDMAPlayout);
	virtual ~CFrameConsumerAJA();

	int addChannel(DWORD dwCnlID, const sFrameConsumer_Parameter& pCnlParameter) override;
	int startChannel(DWORD nChannelID, const sFrameConsumer_StartParameter &_param) override;
	int setFormat(DWORD dwCnlID, const FPTVideoFormat fpVideoFormat) override;
	int OutputFrames(DWORD nChannelID, unsigned char* pVideoBuffer, DWORD dwVSize, unsigned char* pAudioBuffer, DWORD dwASize) override;
	bool WaitGenlockInterrupt(DWORD dwCnlID, DWORD _timeout) override;
	int stopChannel(DWORD nChannelID) override;
	int removeChannel(DWORD dwCnlID) override;
	virtual int OutputFrames(DWORD nChannelID, VideoFrame *pVideo, AudioFrame *pAudio, Timecode *tc) override;

};
