﻿#pragma once
#include "../Lib.FrameConsumer/IFrameConsumer.h"

__interface IAJA_Playout
{
	int Initialize(DWORD dwCnlID, const sFrameConsumer_Parameter& pCnlParameter);
	bool OutputFrames(DWORD nChannelID, unsigned char *pVideoBuffer, DWORD dwVSize, unsigned char *pAudioBuffer, DWORD dwASize);
	bool OutputFrames(DWORD nChannelID, VideoFrame *pVideo, AudioFrame *pAudio, Timecode *tc);
	bool WaitGenlockInterrupt();
	void UnInitialize();
};
