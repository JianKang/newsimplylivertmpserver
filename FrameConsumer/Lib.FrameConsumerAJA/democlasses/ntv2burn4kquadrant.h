/**
	@file		ntv2burn4kquadrant.h
	@brief		Header file for the NTV2Burn4KQuadrant demonstration class.
	@copyright	Copyright (C) 2012-2015 AJA Video Systems, Inc.  All rights reserved.
**/

#ifndef _NTV2BURN4KQUADRANT_H
#define _NTV2BURN4KQUADRANT_H

#include "ntv2enums.h"
#include "ntv2devicefeatures.h"
#include "ntv2devicescanner.h"
#include "ntv2democommon.h"
#include "ntv2task.h"
#include "ntv2utils.h"
#include "ntv2rp188.h"
#include "ajastuff/common/types.h"
#include "ajastuff/common/videotypes.h"
#include "ajastuff/common/timecode.h"
#include "ajastuff/common/timecodeburn.h"
#include "ajastuff/common/circularbuffer.h"
#include "ajastuff/system/thread.h"
#include "ajastuff/system/process.h"
#include "ajastuff/system/systemtime.h"

/**
	@page	ntv2burn4kquadrant		NTV2Burn4KQuadrant Demo

	This command-line program captures 4K/UHD video provided to four SDI inputs of an AJA device and "burns" timecode into the video, then plays it out onto four SDI outputs of a second AJA device (or the same device, if it has at least 8 channels). The program will use the timecode embedded in the input signal if it has any; otherwise it "invents" its own timecode. It also embeds whatever timecode it uses into the output signal.
	The CNTV2Burn4KQuadrant class demonstrates everything that CNTV2Burn does, plus...
	- how to deduce 4K/UHD geometry by looking at four consecutive SDI inputs;
	- how to programmatically perform 4K/UHD signal routing;
	- how to use AutoCirculate to efficiently stream 4K/UHD video and audio, including timecode;
	- how to use an AJACircularBuffer and the producer/consumer model with AutoCirculate.
**/


/**
	@brief	Instances of me can capture 4K/UHD video from one 4-channel AJA device, burn timecode into one quadrant,
			then play it to a second 4-channel AJA device ... in real time.
**/

class NTV2Burn4KQuadrant
{
	//	Public Instance Methods
	public:
		/**
			@brief	Constructs me using the given configuration settings.
			@note	I'm not completely initialized and ready for use until after my Init method has been called.
			@param[in]	inInputDeviceSpec	Specifies the zero-based index number of the AJA device to use as input.
											Defaults to zero, the first device found.
			@param[in]	inOutputDeviceSpec	Specifies the zero-based index number of the AJA device to use as output.
											Defaults to one, the second device found.
			@param[in]	inWithAudio			If true, include audio in the output signal;  otherwise, omit it.
											Defaults to "true".
			@param[in]	inPixelFormat		Specifies the pixel format to use for the device's frame buffers.
											Defaults to 8-bit YUV.
			@param[in]	inTCSource			Specifies the timecode source. Defaults to whatever is found embedded in the input video.
		**/
							NTV2Burn4KQuadrant (const std::string &			inInputDeviceSpec	= "0",
												const std::string &			inOutputDeviceSpec	= "1",
												const bool					inWithAudio			= true,
												const NTV2FrameBufferFormat	inPixelFormat		= NTV2_FBF_8BIT_YCBCR,
												const NTV2TCSource			inTCSource			= NTV2_TCSOURCE_DEFAULT);
		virtual				~NTV2Burn4KQuadrant ();

		/**
			@brief	Initializes me and prepares me to Run.
		**/
		virtual AJAStatus	Init (void);

		/**
			@brief	Runs me.
			@note	Do not call this method without first calling my Init method.
		**/
		virtual AJAStatus	Run (void);

		/**
			@brief	Gracefully stops me from running.
		**/
		virtual void		Quit (void);

		/**
			@brief	Provides status information about my input (capture) and output (playout) processes.
			@param[out]	inputStatus		Receives status information about my input (capture) process.
			@param[out]	outputStatus	Receives status information about my output (playout) process.
		**/
		virtual void		GetACStatus (AUTOCIRCULATE_STATUS_STRUCT & inputStatus, AUTOCIRCULATE_STATUS_STRUCT & outputStatus);


	//	Protected Instance Methods
	protected:
		/**
			@brief	Sets up everything I need for capturing 4K video.
		**/
		virtual AJAStatus	SetupInputVideo (void);

		/**
			@brief	Sets up everything I need for playing 4K video.
		**/
		virtual AJAStatus	SetupOutputVideo (void);

		/**
			@brief	Sets up everything I need for capturing audio.
		**/
		virtual AJAStatus	SetupInputAudio (void);

		/**
			@brief	Sets up everything I need for playing audio.
		**/
		virtual AJAStatus	SetupOutputAudio (void);

		/**
			@brief	Sets up board routing for capture.
		**/
		virtual void		RouteInputSignal (void);

		/**
			@brief	Sets up board routing for playout.
		**/
		virtual void		RouteOutputSignal (void);

		/**
			@brief	Sets up my circular buffers.
		**/
		virtual void		SetupHostBuffers (void);

		/**
			@brief	Initializes AutoCirculate on my capture side.
		**/
		virtual void		SetupInputAutoCirculate (void);

		/**
			@brief	Initializes AutoCirculate on my playout side.
		**/
		virtual void		SetupOutputAutoCirculate (void);

		/**
			@brief	Starts my playout thread.
		**/
		virtual void		StartPlayThread (void);

		/**
			@brief	Repeatedly plays out frames using AutoCirculate (until global quit flag set).
		**/
		virtual void		PlayFrames (void);

		/**
			@brief	Starts my capture thread.
		**/
		virtual void		StartCaptureThread (void);

		/**
			@brief	Repeatedly captures frames using AutoCirculate (until global quit flag set).
		**/
		virtual void		CaptureFrames (void);


		/**
			@brief	Returns true if the current input signal has timecode embedded in it; otherwise returns false.
		**/
		virtual bool		InputSignalHasTimecode (void);


	//	Protected Class Methods
	protected:
		/**
			@brief	This is the playout thread's static callback function that gets called when the playout thread runs.
					This function gets "Attached" to the playout thread's AJAThread instance.
			@param[in]	pThread		A valid pointer to the playout thread's AJAThread instance.
			@param[in]	pContext	Context information to pass to the thread.
									(For this application, this will be set to point to the NTV2Burn4KQuadrant instance.)
		**/
		static void	PlayThreadStatic (AJAThread * pThread, void * pContext);

		/**
			@brief	This is the capture thread's static callback function that gets called when the capture thread runs.
					This function gets "Attached" to the AJAThread instance.
			@param[in]	pThread		Points to the AJAThread instance.
			@param[in]	pContext	Context information to pass to the thread.
									(For this application, this will be set to point to the NTV2Burn4KQuadrant instance.)
		**/
		static void	CaptureThreadStatic (AJAThread * pThread, void * pContext);

		/**
			@brief	Returns the equivalent AJA_PixelFormat for the given NTV2FrameBufferFormat.
			@param[in]	inFBFormat	Specifies the NTV2FrameBufferFormat to be converted into an equivalent AJA_PixelFormat.
		**/
		static AJA_PixelFormat	GetAJAPixelFormat (const NTV2FrameBufferFormat inFBFormat);

		/**
			@brief	Returns the equivalent TimecodeFormat for the given NTV2FrameRate.
			@param[in]	inFrameRate		Specifies the NTV2FrameRate to be converted into an equivalent TimecodeFormat.
		**/
		static TimecodeFormat	NTV2FrameRate2TimecodeFormat (const NTV2FrameRate inFrameRate);

		/**
			@brief	Returns the RP188 DBB register number to use for the given NTV2InputSource.
			@param[in]	inInputSource	Specifies the NTV2InputSource of interest.
			@return	The number of the RP188 DBB register to use for the given input source.
		**/
		static ULWord			GetRP188RegisterForInput (const NTV2InputSource inInputSource);

	//	Private Member Data
	private:
		AJAThread *					mPlayThread;			///	My playout thread object
		AJAThread *					mCaptureThread;			///	My capture thread object

		CNTV2Card					mInputDevice;			///	My CNTV2Card input instance
		CNTV2Card					mOutputDevice;			///	My CNTV2Card output instance
		bool						mSingleDevice;			/// Using single 8-channel device (4K I/O on one device)?
		NTV2DeviceID				mInputDeviceID;			///	My device identifier
		NTV2DeviceID				mOutputDeviceID;		///	My device identifier
		std::string					mInputDeviceSpecifier;	///	Specifies the input device I should use
		std::string					mOutputDeviceSpecifier;	///	Specifies the output device I should use
		bool						mWithAudio;				///	Capture and playout audio?
		bool						mUseTasks;				///	Use autocirculate tasks?
		NTV2Channel					mInputChannel;			///	The channel I'm using
		NTV2Channel					mOutputChannel;			///	The channel I'm using
		NTV2TCSource				mTCSource;				/// The time code source
		NTV2VideoFormat				mVideoFormat;			///	My video format
		NTV2FrameBufferFormat		mPixelFormat;			///	My pixel format
		NTV2EveryFrameTaskMode		mInputSavedTaskMode;	/// We will restore the previous state for input device
		NTV2EveryFrameTaskMode		mOutputSavedTaskMode;	/// We will restore the previous state for output device
		bool						mVancEnabled;			///	VANC enabled?
		bool						mWideVanc;				///	Wide VANC?
		NTV2AudioSystem				mInputAudioSystem;		///	The input audio system I'm using
		NTV2AudioSystem				mOutputAudioSystem;		///	The output audio system I'm using

		bool						mGlobalQuit;			///	Set "true" to gracefully stop
		AJATimeCodeBurn				mTCBurner;				///	My timecode burner
		uint32_t					mQueueSize;				///	My queue size
		uint32_t					mVideoBufferSize;		///	My video buffer size, in bytes
		uint32_t					mAudioBufferSize;		///	My audio buffer size, in bytes

		AVDataBuffer							mAVHostBuffer [CIRCULAR_BUFFER_SIZE];	///	My host buffers
		AJACircularBuffer <AVDataBuffer *>		mAVCircularBuffer;						///	My ring buffer

		AUTOCIRCULATE_TRANSFER_STRUCT			mInputTransferStruct;					///	My A/C input transfer info
		AUTOCIRCULATE_TRANSFER_STATUS_STRUCT	mInputTransferStatusStruct;				///	My A/C input status
		AUTOCIRCULATE_TRANSFER_STRUCT			mOutputTransferStruct;					///	My A/C output transfer info
		AUTOCIRCULATE_TRANSFER_STATUS_STRUCT	mOutputTransferStatusStruct;			///	My A/C output status
		CNTV2Task								mInputTaskList;							/// My task list for frame accurate input timecode 
		CNTV2Task								mOutputTaskList;						/// My task list for frame accurate output timecode 

};	//	NTV2Burn4KQuadrant

#endif	//	_NTV2BURN4KQUADRANT_H
