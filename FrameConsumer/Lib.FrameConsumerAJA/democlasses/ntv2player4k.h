/**
	@file		ntv2player4k.h
	@brief		Header file for NTV2Player4K demonstration class
	@copyright	Copyright (C) 2013-2015 AJA Video Systems, Inc.  All rights reserved.
**/


#ifndef _NTV2PLAYER4K_H
#define _NTV2PLAYER4K_H

#include "ntv2enums.h"
#include "ntv2devicefeatures.h"
#include "ntv2devicescanner.h"
#include "ntv2democommon.h"
#include "ajastuff/common/timecodeburn.h"
#include "ajastuff/common/circularbuffer.h"
#include "ajastuff/system/thread.h"

/**
	@page	ntv2player4k		NTV2Player4K Demo

	This command-line program is identical to "ntv2player" except that it plays 4K/UHD video.
	The NTV2Player4K class demonstrates everything NTV2Player does, plus...
	- how to generate a 4K/UHD test pattern and get it into a 4K frame buffer;
	- how to use AutoCirculate to efficiently stream 4K/UHD video with audio and timecode.
**/


/**
	@brief	I am an object that can play out a test pattern (with timecode) to an output of an AJA device
			with or without audio tone in real time. I make use of the AJACircularBuffer, which simplifies
			implementing a producer/consumer model, in which a "producer" thread generates the test pattern
			frames, and a "consumer" thread (i.e., the "play" thread) sends those frames to the AJA device.
			I demonstrate how to embed timecode into an SDI output signal using AutoCirculate during playout.
**/

class NTV2Player4K
{
	public:
		/**
			@brief Signature of a function call for requesting frames to be played.
		**/
		typedef AJAStatus (NTV2Player4KCallback)(void * pInstance, const AVDataBuffer * const playData);

	//	Public Instance Methods
	public:
		/**
			@brief	Constructs me using the given configuration settings.
			@note	I'm not completely initialized and ready for use until after my Init method has been called.
			@param[in]	inDeviceSpecifier	Specifies the AJA device to use. Defaults to "0", the first device found.
			@param[in]	inWithAudio			If true, include audio tone in the output signal;  otherwise, omit it.
											Defaults to "true".
			@param[in]	inChannel			Specifies the channel to use. Defaults to NTV2_CHANNEL1.
			@param[in]	inPixelFormat		Specifies the pixel format to use for the device's frame buffers.
											Defaults to 8-bit YUV.
			@param[in]	inOutputDestination	Specifies which output to playout to. Defaults to SDI2.
			@param[in]	inVideoFormat		Specifies the video format to use. Defaults to 1080i5994.
			@param[in]	inUseHDMIOut		If true, enables an HDMI output signal;  otherwise, disable it.
											Defaults to "false", which reduces memory bandwidth needs.
			@param[in]	inDoMultiChannel	If true, enables multiple player 4k instances to share a board.
		**/
								NTV2Player4K (const std::string &		inDeviceSpecifier	= "0",
											const bool					inWithAudio			= true,
											const NTV2Channel			inChannel			= NTV2_CHANNEL1,
											const NTV2FrameBufferFormat	inPixelFormat		= NTV2_FBF_8BIT_YCBCR,
											const NTV2OutputDestination	inOutputDestination	= NTV2_OUTPUTDESTINATION_SDI2,
											const NTV2VideoFormat		inVideoFormat		= NTV2_FORMAT_4x1920x1080p_2997,
											const bool					inUseHDMIOut		= false,
											const bool					inDoMultiChannel	= false);

		virtual					~NTV2Player4K (void);

		/**
			@brief	Initializes me and prepares me to Run.
		**/
		AJAStatus		Init (void);

		/**
			@brief	Runs me.
			@note	Do not call this method without first calling my Init method.
		**/
		AJAStatus		Run (void);

		/**
			@brief	Gracefully stops me from running.
		**/
		void			Quit (void);

		/**
			@brief	Provides status information about my input (capture) and output (playout) processes.
			@param[out]	outOutputStatus		Receives status information about my output (playout) process.
		**/
		void			GetACStatus (AUTOCIRCULATE_STATUS_STRUCT & outOutputStatus);

		/**
			@brief	Returns the current callback function for requesting frames to be played.
		**/
		virtual void			GetCallback (void ** const pInstance, NTV2Player4KCallback ** const callback);

		/**
			@brief	Sets a callback function for requesting frames to be played.
		**/
		virtual bool			SetCallback (void * const pInstance, NTV2Player4KCallback * const callback);


	//	Protected Instance Methods
	protected:
		/**
			@brief	Sets up everything I need to play video.
		**/
		AJAStatus		SetUpVideo (void);

		/**
			@brief	Sets up everything I need to play audio.
		**/
		AJAStatus		SetUpAudio (void);

		/**
			@brief	Sets up board routing for playout.
		**/
		void			RouteOutputSignal (void);

		/**
			@brief	Sets up my circular buffers.
		**/
		void			SetUpHostBuffers (void);

		/**
			@brief	Initializes AutoCirculate on my playout side.
		**/
		void			SetUpOutputAutoCirculate (void);

		/**
			@brief	Creates my test pattern buffers.
		**/
		void			SetUpTestPatternVideoBuffers (void);

		/**
			@brief	Starts my playout thread.
		**/
		void			StartPlayThread (void);

		/**
			@brief	Repeatedly plays out frames using AutoCirculate (until global quit flag set).
		**/
		void			PlayFrames (void);

		/**
			@brief	Starts my test pattern producer thread.
		**/
		void			StartProduceFrameThread (void);

		/**
			@brief	Repeatedly produces test pattern frames (until global quit flag set).
		**/
		void			ProduceFrames (void);

		/**
			@brief	Inserts audio tone (based on my current tone frequency) into the given audio buffer.
			@param[out]	audioBuffer		Specifies a valid, non-NULL pointer to the buffer that is to receive
										the audio tone data.
			@return	Total number of bytes written into the buffer.
		**/
		uint32_t		AddTone (ULWord * audioBuffer);

		/**
			@brief	Returns true if my current output destination's RP188 bypass is enabled; otherwise returns false.
		**/
		bool			OutputDestHasRP188BypassEnabled (void);

		/**
			@brief	Disables my current output destination's RP188 bypass.
		**/
		void			DisableRP188Bypass (void);


	//	Protected Class Methods
	protected:
		/**
			@brief	This is the playout thread's static callback function that gets called when the playout thread runs.
					This function gets "Attached" to the playout thread's AJAThread instance.
			@param[in]	pThread		A valid pointer to the playout thread's AJAThread instance.
			@param[in]	pContext	Context information to pass to the thread.
									(For this application, this will be set to point to the NTV2Player4K instance.)
		**/
		static void	PlayThreadStatic (AJAThread * pThread, void * pContext);

		/**
			@brief	This is the test pattern producer thread's static callback function that gets called when the producer thread runs.
					This function gets "Attached" to the producer thread's AJAThread instance.
			@param[in]	pThread		A valid pointer to the producer thread's AJAThread instance.
			@param[in]	pContext	Context information to pass to the thread.
									(For this application, this will be set to point to the NTV2Player4K instance.)
		**/
		static void	ProduceFrameThreadStatic (AJAThread * pThread, void * pContext);

		/**
			@brief	Returns the equivalent AJA_PixelFormat for the given NTV2FrameBufferFormat.
			@param[in]	format		Specifies the NTV2FrameBufferFormat to be converted into an equivalent AJA_PixelFormat.
		**/
		static AJA_PixelFormat	GetAJAPixelFormat (const NTV2FrameBufferFormat format);

		/**
			@brief	Returns the equivalent AJA_FrameRate for the given NTV2FrameRate.
			@param[in]	frameRate	Specifies the NTV2FrameRate to be converted into an equivalent AJA_FrameRate.
		**/
		static AJA_FrameRate	GetAJAFrameRate (const NTV2FrameRate frameRate);

		/**
			@brief	Returns the equivalent TimecodeFormat for the given NTV2FrameRate.
			@param[in]	inFrameRate		Specifies the NTV2FrameRate to be converted into an equivalent TimecodeFormat.
		**/
		static TimecodeFormat	NTV2FrameRate2TimecodeFormat (const NTV2FrameRate inFrameRate);

		/**
			@brief	Returns the RP188 DBB register number to use for the given NTV2OutputDestination.
			@param[in]	inOutputSource	Specifies the NTV2OutputDestination of interest.
			@return	The number of the RP188 DBB register to use for the given output destination.
		**/
		static ULWord			GetRP188RegisterForOutput (const NTV2OutputDestination inOutputSource);


	//	Private Member Data
	private:
		AJAThread *					mPlayThread;				///	My playout (consumer) thread object
		AJAThread *					mProduceFrameThread;		///	My generator (producer) thread object

		uint32_t					mCurrentFrame;				///	My current frame number (used to generate timecode)
		ULWord						mCurrentSample;				///	My current audio sample (maintains audio tone generator state)
		double						mToneFrequency;				///	My current audio tone frequency, in Hertz

		CNTV2Card					mDevice;					///	My CNTV2Card instance
		NTV2DeviceID				mDeviceID;					///	My board (model) identifier
		const std::string			mDeviceSpecifier;			///	Specifies the device I should use
		const bool					mWithAudio;					///	Capture and playout audio?
		const bool					mUseHDMIOut;				///	Enable HDMI output?
		const NTV2Channel			mChannel;					///	The channel I'm using
		const NTV2OutputDestination	mOutputDestination;			///	The output I'm using
		NTV2Crosspoint				mChannelSpec;				///	The AutoCirculate channel spec I'm using
		NTV2VideoFormat				mVideoFormat;				///	My video format
		NTV2FrameBufferFormat		mPixelFormat;				///	My pixel format
		NTV2EveryFrameTaskMode		mPreviousFrameServices;		/// Used to restore the previous task mode
		bool						mVancEnabled;				///	VANC enabled?
		bool						mWideVanc;					///	Wide VANC?
		NTV2AudioSystem				mAudioSystem;				///	The audio system I'm using
		bool						mDoMultiChannel;			/// Allow more than one player 4k to play

		bool						mGlobalQuit;				///	Set "true" to gracefully stop
		AJATimeCodeBurn				mTCBurner;					///	My timecode burner
		uint32_t					mVideoBufferSize;			///	My video buffer size, in bytes
		uint32_t					mAudioBufferSize;			///	My audio buffer size, in bytes

		uint8_t **					mTestPatternVideoBuffers;	///	My test pattern buffers
		int32_t						mNumTestPatterns;			///	Number of test patterns to cycle through

		AVDataBuffer							mAVHostBuffer [CIRCULAR_BUFFER_SIZE];	///	My host buffers
		AJACircularBuffer <AVDataBuffer *>		mAVCircularBuffer;						///	My ring buffer

		AUTOCIRCULATE_TRANSFER_STRUCT           mOutputTransferStruct;					///	My A/C output transfer info
		AUTOCIRCULATE_TRANSFER_STATUS_STRUCT    mOutputTransferStatusStruct;			///	My A/C output status

		void *						mInstance;					/// Instance information for the callback function
		NTV2Player4KCallback *		mPlayerCallback;			/// Address of callback function

};	//	NTV2Player4K

#endif	//	_NTV2PLAYER4K_H
