/**
	@file	ntv2burn.h
	@brief	Header file for the NTV2Burn demonstration class.
	@copyright	Copyright (C) 2012-2015 AJA Video Systems, Inc.  All rights reserved.
**/

#ifndef _NTV2BURN_H
#define _NTV2BURN_H

#include "ntv2enums.h"
#include "ntv2devicefeatures.h"
#include "ntv2devicescanner.h"
#include "ntv2democommon.h"
#include "ntv2task.h"
#include "ntv2utils.h"
#include "ajastuff/common/types.h"
#include "ajastuff/common/videotypes.h"
#include "ajastuff/common/timecode.h"
#include "ajastuff/common/timecodeburn.h"
#include "ajastuff/common/circularbuffer.h"
#include "ajastuff/system/thread.h"
#include "ajastuff/system/process.h"
#include "ajastuff/system/systemtime.h"

/**
	@page	ntv2burn		NTV2Burn Demo

	This command-line program captures SDI video provided to the input of an AJA device and "burns" timecode into the video,
	then plays it out onto an output of that same AJA device. The program will use the timecode embedded in the input signal
	if it has any; otherwise it "invents" its own timecode. It also embeds whatever timecode it uses into the output signal.
	The CNTV2Burn class demonstrates...
	- how to properly acquire and release an AJA device in order to use it exclusively;
	- how to programmatically perform device configuration and signal routing;
	- how to use AutoCirculate to efficiently stream video and audio, with or without timecode.
	- how to use an AJACircularBuffer and the producer/consumer model with AutoCirculate;
	- how to use and manipulate timecode;
	- how to "burn" timecode into a buffer containing a frame of video data.
**/


/**
	@brief	Instances of me can capture frames from a video signal provided to an input of an AJA device,
			burn timecode into those frames, then deliver them to an output of the same AJA device, all in real time.
			I make use of the AJACircularBuffer, which simplifies implementing a producer/consumer model,
			in which a "consumer" thread delivers burned-in frames to the AJA device output, and a "producer"
			thread grabs raw frames from the AJA device input.
			I also demonstrate how to detect if an SDI input has embedded timecode, and if so, how AutoCirculate
			makes it available. I also show how to embed timecode into an SDI output signal using AutoCirculate
			during playout.
**/

class NTV2Burn
{
	//	Public Instance Methods
	public:
		/**
			@brief	Constructs me using the given configuration settings.
			@note	I'm not completely initialized and ready for use until after my Init method has been called.
			@param[in]	inDeviceSpecifier	Specifies the AJA device to use. Defaults to "0", the first device found.
			@param[in]	inWithAudio			If true, include audio in the output signal; otherwise, omit it.
											Defaults to "true".
			@param[in]	inPixelFormat		Specifies the pixel format to use for the device's frame buffers. Defaults to 8-bit YUV.
			@param[in]	inInputSource		Specifies which input to capture video from. Defaults to SDI1.
			@param[in]	inTCSource			Specifies the timecode source. Defaults to whatever is found embedded in the input video.
			@param[in]	inReserveDevice		If true, acquires and releases the device before using it (the default);  otherwise,
											assumes other application(s) may be using the device.
		**/
							NTV2Burn (const std::string &			inDeviceSpecifier	= "0",
										const bool					inWithAudio			= true,
										const NTV2FrameBufferFormat	inPixelFormat		= NTV2_FBF_8BIT_YCBCR,
										const NTV2InputSource		inInputSource		= NTV2_INPUTSOURCE_SDI1,
										const NTV2TCSource			inTCSource			= NTV2_TCSOURCE_DEFAULT,
										const bool					inDoMultiChannel	= false,
										const bool					inWithAnc			= false);
		virtual				~NTV2Burn ();

		/**
			@brief	Initializes me and prepares me to Run.
		**/
		virtual AJAStatus	Init (void);

		/**
			@brief	Runs me.
			@note	Do not call this method without first calling my Init method.
		**/
		virtual AJAStatus	Run (void);

		/**
			@brief	Gracefully stops me from running.
		**/
		virtual void		Quit (void);

		/**
			@brief	Provides status information about my input (capture) and output (playout) processes.
			@param[out]	outFramesProcessed		Receives the number of frames successfully processed.
			@param[out]	outCaptureFramesDropped	Receives the number of dropped capture frames.
			@param[out]	outPlayoutFramesDropped	Receives the number of dropped playout frames.
			@param[out]	outCaptureBufferLevel	Receives the capture driver buffer level.
			@param[out]	outPlayoutBufferLevel	Receives the playout driver buffer level.
		**/
		virtual void		GetStatus (ULWord & outFramesProcessed, ULWord & outCaptureFramesDropped, ULWord & outPlayoutFramesDropped,
										ULWord & outCaptureBufferLevel, ULWord & outPlayoutBufferLevel);


	//	Protected Instance Methods
	protected:
		/**
			@brief	Sets up everything I need for capturing and playing video.
		**/
		virtual AJAStatus	SetupVideo (void);

		/**
			@brief	Sets up everything I need for capturing and playing audio.
		**/
		virtual AJAStatus	SetupAudio (void);

		/**
			@brief	Sets up board routing for capture.
		**/
		virtual void		RouteInputSignal (void);

		/**
			@brief	Sets up board routing for playout.
		**/
		virtual void		RouteOutputSignal (void);

		/**
			@brief	Sets up my circular buffers.
		**/
		virtual AJAStatus	SetupHostBuffers (void);

		/**
			@brief	Starts my playout thread.
		**/
		virtual void		StartPlayThread (void);

		/**
			@brief	Repeatedly plays out frames using AutoCirculate (until global quit flag set).
		**/
		virtual void		PlayFrames (void);

		/**
			@brief	Starts my capture thread.
		**/
		virtual void		StartCaptureThread (void);

		/**
			@brief	Repeatedly captures frames using AutoCirculate (until global quit flag set).
		**/
		virtual void		CaptureFrames (void);


		/**
			@brief	Returns true if the current input signal has timecode embedded in it; otherwise returns false.
		**/
		virtual bool		InputSignalHasTimecode (void);


		/**
			@brief	Returns true if the current input's RP188 bypass is enabled; otherwise returns false.
		**/
		virtual bool		InputSignalHasRP188BypassEnabled (void);


		/**
			@brief	Disables the current input's RP188 bypass.
		**/
		virtual void		DisableRP188Bypass (void);


		/**
			@brief	Returns true if there is a valid LTC signal on my device's primary analog LTC input port; otherwise returns false.
		**/
		virtual bool		AnalogLTCInputHasTimecode (void);


	//	Protected Class Methods
	protected:
		/**
			@brief	This is the playout thread's static callback function that gets called when the playout thread runs.
					This function gets "Attached" to the playout thread's AJAThread instance.
			@param[in]	pThread		A valid pointer to the playout thread's AJAThread instance.
			@param[in]	pContext	Context information to pass to the thread.
									(For this application, this will be set to point to the NTV2Burn instance.)
		**/
		static void				PlayThreadStatic (AJAThread * pThread, void * pContext);

		/**
			@brief	This is the capture thread's static callback function that gets called when the capture thread runs.
					This function gets "Attached" to the AJAThread instance.
			@param[in]	pThread		Points to the AJAThread instance.
			@param[in]	pContext	Context information to pass to the thread.
									(For this application, this will be set to point to the NTV2Burn instance.)
		**/
		static void				CaptureThreadStatic (AJAThread * pThread, void * pContext);

		/**
			@brief	Returns the equivalent AJA_PixelFormat for the given NTV2FrameBufferFormat.
			@param[in]	format		Specifies the NTV2FrameBufferFormat to be converted into an equivalent AJA_PixelFormat.
		**/
		static AJA_PixelFormat	GetAJAPixelFormat (const NTV2FrameBufferFormat format);

		/**
			@brief	Returns the equivalent TimecodeFormat for the given NTV2FrameRate.
			@param[in]	inFrameRate		Specifies the NTV2FrameRate to be converted into an equivalent TimecodeFormat.
		**/
		static TimecodeFormat	NTV2FrameRate2TimecodeFormat (const NTV2FrameRate inFrameRate);

		/**
			@brief	Returns the RP188 DBB register number to use for the given NTV2InputSource.
			@param[in]	inInputSource	Specifies the NTV2InputSource of interest.
			@return	The number of the RP188 DBB register to use for the given input source.
		**/
		static ULWord			GetRP188RegisterForInput (const NTV2InputSource inInputSource);


	//	Private Member Data
	private:
		AJAThread *					mPlayThread;			///< @brief	My playout thread object
		AJAThread *					mCaptureThread;			///< @brief	My capture thread object

		CNTV2Card					mDevice;				///< @brief	My CNTV2Card instance
		NTV2DeviceID				mDeviceID;				///< @brief	My device identifier
		const std::string			mDeviceSpecifier;		///< @brief	Specifies the device I should use
		const bool					mWithAudio;				///< @brief	Capture and playout audio?
		bool						mWithAnc;				///< @brief	Capture and Playout packetized ANC data
		const bool					mUseTasks;				///< @brief	Use autocirculate tasks?
		NTV2Channel					mInputChannel;			///< @brief	The input channel I'm using
		NTV2Channel					mOutputChannel;			///< @brief	The output channel I'm using
		NTV2InputSource				mInputSource;			///< @brief	The input source I'm using
		NTV2TCSource				mTCSource;				///< @brief	The time code source
		NTV2OutputDestination		mOutputDestination;		///< @brief	The output I'm using
		NTV2VideoFormat				mVideoFormat;			///< @brief	My video format
		NTV2FrameBufferFormat		mPixelFormat;			///< @brief	My pixel format
		NTV2EveryFrameTaskMode		mSavedTaskMode;			///< @brief	We will restore the previous state
		bool						mVancEnabled;			///< @brief	VANC enabled?
		bool						mWideVanc;				///< @brief	Wide VANC?
		NTV2AudioSystem				mAudioSystem;			///< @brief	The audio system I'm using
		bool						mDoMultiChannel;		///< @brief	Set the board up for multi-format

		bool						mGlobalQuit;			///< @brief	Set "true" to gracefully stop
		AJATimeCodeBurn				mTCBurner;				///< @brief	My timecode burner
		uint32_t					mQueueSize;				///< @brief	My queue size
		uint32_t					mVideoBufferSize;		///< @brief	My video buffer size, in bytes
		uint32_t					mAudioBufferSize;		///< @brief	My audio buffer size, in bytes

		AVDataBuffer						mAVHostBuffer [CIRCULAR_BUFFER_SIZE];	///< @brief	My host buffers
		AJACircularBuffer <AVDataBuffer *>	mAVCircularBuffer;						///< @brief	My ring buffer

		AUTOCIRCULATE_TRANSFER				mInputXferInfo;							///< @brief	My A/C input transfer info
		AUTOCIRCULATE_TRANSFER				mOutputXferInfo;						///< @brief	My A/C output transfer info

};	//	NTV2Burn

#endif	//	_NTV2BURN_H
