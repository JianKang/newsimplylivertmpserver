/**
	@file		ntv2democommon.h
	@brief		This file contains structures and constants that are used by many of the demo applications.
				There is nothing magical about anything in this file. In your applications you may use a different
				number of circular buffers, or store different data in the AVDataBuffer. What's listed below
				are simply values that work well with the demos.
	@copyright	Copyright (C) 2013-2015 AJA Video Systems, Inc.  All rights reserved.
**/

#ifndef _NTV2DEMOCOMMON_H
#define _NTV2DEMOCOMMON_H


#include "ntv2rp188.h"

/**
	@page	demoapps		Sample Applications

	The SDK includes a number of demonstration applications that are designed to help new users quickly understand how to use the NTV2 SDK
	to solve real-world problems using AJA devices.

	The demonstration programs operate the same on all three supported platforms. Simply choose the target demo you're interested in,
	build it, and run it.

	Some of the demos are simple command-line programs that have no user interaction, while others provide a simple graphical user interface
	based on the Qt cross-platform application framework. For more information about Qt, please visit http://www.qt.io/   The Qt-based demos
	should work with Qt version 5.4 or later.

	All of the demos are "future ready" and will build & run with the "NTV2_DEPRECATE" macro defined.

	@subpage	ntv2enumerateboards		"NTV2EnumerateBoards Demo"

	@subpage	ntv2outputtestpattern	"NTV2OutputTestPattern Demo"

	@subpage	ntv2capture				"NTV2Capture Demo"

	@subpage	ntv2player				"NTV2Player Demo"

	@subpage	ntv2player4k			"NTV2Player4K Demo"

	@subpage	ntv2ccgrabber			"NTV2CCGrabber Demo"

	@subpage	ntv2ccplayer			"NTV2CCPlayer Demo"

	@subpage	ntv2burn				"NTV2Burn Demo"

	@subpage	ntv2burn4kquadrant		"NTV2Burn4KQuadrant Demo"

	@subpage	ntv2fieldburn			"NTV2FieldBurn Demo"

	@subpage	ntv2llburn				"NTV2LLBurn Demo"

	@subpage	ntv2qtpreview			"NTV2QtPreview Demo"

	@subpage	ntv2qtmultiinput		"NTV2QtMultiInput Demo"

	@subpage	ntv2qtrawcapture		"NTV2QtRawCapture Demo"
**/


/**
	@brief	This structure encapsulates the video and audio buffers used by AutoCirculate. The demo programs
			that employ producer/consumer threads use a fixed number (CIRCULAR_BUFFER_SIZE) of these buffers.
			The AJACircularBuffer template class greatly simplifies implementing this approach to efficiently
			process frames.
	@note	The unused fields in this structure are in preparation for adding features to these demo apps,
			e.g., embedding additional timecode (e.g., VITC) to the output video that's sourced from the input video.
**/
typedef struct
{
	uint32_t *		fVideoBuffer;			///< @brief	Pointer to host video buffer
	uint32_t *		fVideoBuffer2;			///< @brief	Pointer to an additional host video buffer, usually field 2
	uint32_t		fVideoBufferSize;		///< @brief	Size of host video buffer, in bytes
	uint32_t *		fAudioBuffer;			///< @brief	Pointer to host audio buffer
	uint32_t		fAudioBufferSize;		///< @brief	Size of host audio buffer, in bytes
	uint32_t *		fAncBuffer;				///< @brief	Pointer to ANC buffer
	uint32_t		fAncBufferSize;			///< @brief	Size of ANC buffer, in bytes
	uint32_t *		fAncF2Buffer;			///< @brief	Pointer to "Field 2" ANC buffer
	uint32_t		fAncF2BufferSize;		///< @brief	Size of "Field 2" ANC buffer, in bytes
	uint32_t		fAudioRecordSize;		///< @brief	For future use -- (see note)
	uint32_t		fAncRecordSize;			///< @brief	For future use -- (see note)
	RP188_STRUCT	fRP188Data;				///< @brief	For future use -- (see note)
	RP188_STRUCT	fRP188Data2;			///< @brief	For future use -- (see note)
	uint8_t *		fVideoBufferUnaligned;	///< @brief	For future use -- (see note)
} AVDataBuffer;


const unsigned int	CIRCULAR_BUFFER_SIZE	(10);		///< @brief	Specifies how many AVDataBuffers constitute the circular buffer


#endif	//	_NTV2DEMOCOMMON_H

