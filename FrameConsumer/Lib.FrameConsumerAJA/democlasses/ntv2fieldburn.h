/**
	@file	ntv2fieldburn.h
	@brief	Header file for the NTV2FieldBurn demonstration class.
	@copyright	Copyright (C) 2013-2015 AJA Video Systems, Inc.  All rights reserved.
**/

#ifndef _NTV2FIELDBURN_H
#define _NTV2FIELDBURN_H

#include "ntv2enums.h"
#include "ntv2devicefeatures.h"
#include "ntv2devicescanner.h"
#include "ntv2democommon.h"
#include "ntv2task.h"
#include "ntv2utils.h"
#include "ntv2rp188.h"
#include "ajastuff/common/types.h"
#include "ajastuff/common/videotypes.h"
#include "ajastuff/common/timecode.h"
#include "ajastuff/common/timecodeburn.h"
#include "ajastuff/common/circularbuffer.h"
#include "ajastuff/system/thread.h"
#include "ajastuff/system/process.h"
#include "ajastuff/system/systemtime.h"

/**
	@page	ntv2fieldburn		NTV2FieldBurn Demo

	This command-line program captures interlaced SDI video into two separate host buffers, one for each field,
	"burns" timecode into each of them, then recombines and plays out the "burned" fields onto an SDI output of
	that same AJA device. The program will use the timecode embedded in the input signal if it has any; otherwise
	it "invents" a timecode of its own. It also embeds whatever timecode it used into the output signal.
	The CNTV2FieldBurn class demonstrates...
	- how to properly acquire and release an AJA device in order to use it exclusively;
	- how to programmatically perform device configuration and signal routing;
	- how to use AutoCirculate to efficiently stream video and audio, with or without timecode;
	- how to use an AJACircularBuffer and the producer/consumer model with AutoCirculate;
	- how to use and manipulate timecode;
	- how to perform segmented DMAs using AutoCirculate;
	- how to "burn" timecode into a buffer containing a frame of video data.
**/


/**
	@brief	Instances of me can capture frames from a video signal provided to an input of an AJA device.
			The frame is captured as two fields stored in non-contiguous system memory buffers.
			I burn timecode into those fields, then deliver both of them to an output of the same AJA device ... in real time.
			I make use of the AJACircularBuffer, which simplifies implementing a producer/consumer model,
			I use a "consumer" thread to deliver burned-in frames to the AJA device output, and a "producer"
			thread to grab raw frames from the AJA device input.
			I also demonstrate how to detect if an SDI input has embedded timecode, and if so, how AutoCirculate
			makes it available. I also show how to embed timecode into an SDI output signal using AutoCirculate
			during playout.
**/

class NTV2FieldBurn
{
	//	Public Instance Methods
	public:
		/**
			@brief	Constructs me using the given configuration settings.
			@note	I'm not completely initialized and ready for use until after my Init method has been called.
			@param[in]	inDeviceSpecifier	Specifies the AJA device to use. Defaults to "0", the first device found.
			@param[in]	inWithAudio			If true, include audio in the output signal;  otherwise, omit it.
											Defaults to "true".
			@param[in]	inChannel			Specifies the channel to use.
											Defaults to NTV2_CHANNEL1.
			@param[in]	inPixelFormat		Specifies the pixel format to use for the device's frame buffers.
											Defaults to 8-bit YUV.
			@param[in]	inInputSource		Specifies which input to capture from.
											Defaults to SDI1.
			@param[in]	inOutputDestination	Specifies which output to playout to.
											Defaults to SDI3.
		**/
						NTV2FieldBurn (const std::string			inDeviceSpecifier	= "0",
										const bool					inWithAudio			= true,
										const NTV2Channel			inChannel			= NTV2_CHANNEL1,
										const NTV2FrameBufferFormat	inPixelFormat		= NTV2_FBF_8BIT_YCBCR,
										const NTV2InputSource		inInputSource		= NTV2_INPUTSOURCE_SDI1,
										const NTV2OutputDestination	inOutputDestination	= NTV2_OUTPUTDESTINATION_SDI3);
						~NTV2FieldBurn ();

		/**
			@brief	Initializes me and prepares me to Run.
		**/
		AJAStatus		Init (void);

		/**
			@brief	Runs me.
			@note	Do not call this method without first calling my Init method.
		**/
		AJAStatus		Run (void);

		/**
			@brief	Gracefully stops me from running.
		**/
		void			Quit (void);

		/**
			@brief	Provides status information about my input (capture) and output (playout) processes.
			@param[out]	outInputStatus	Receives status information about my input (capture) process.
			@param[out]	outOutputStatus	Receives status information about my output (playout) process.
		**/
		void			GetACStatus (AUTOCIRCULATE_STATUS_STRUCT & outInputStatus, AUTOCIRCULATE_STATUS_STRUCT & outOutputStatus);


	//	Protected Instance Methods
	protected:
		/**
			@brief	Sets up everything I need for capturing and playing video.
		**/
		AJAStatus		SetupVideo (void);

		/**
			@brief	Sets up everything I need for capturing and playing audio.
		**/
		AJAStatus		SetupAudio (void);

		/**
			@brief	Sets up board routing for capture.
		**/
		void			RouteInputSignal (void);

		/**
			@brief	Sets up board routing for playout.
		**/
		void			RouteOutputSignal (void);

		/**
			@brief	Sets up my circular buffers.
		**/
		void			SetupHostBuffers (void);

		/**
			@brief	Initializes AutoCirculate on my capture side.
		**/
		void			SetupInputAutoCirculate (void);

		/**
			@brief	Initializes AutoCirculate on my playout side.
		**/
		void			SetupOutputAutoCirculate (void);

		/**
			@brief	Starts my playout thread.
		**/
		void			StartPlayThread (void);

		/**
			@brief	Repeatedly plays out frames using AutoCirculate (until global quit flag set).
		**/
		void			PlayFrames (void);

		/**
			@brief	Starts my capture thread.
		**/
		void			StartCaptureThread (void);

		/**
			@brief	Repeatedly captures frames using AutoCirculate (until global quit flag set).
		**/
		void			CaptureFrames (void);


		/**
			@brief	Returns true if the current input signal has timecode embedded in it; otherwise returns false.
		**/
		bool			InputSignalHasTimecode (void);


		/**
			@brief	Returns true if the current input's RP188 bypass is enabled; otherwise returns false.
		**/
		bool			InputSignalHasRP188BypassEnabled (void);


		/**
			@brief	Disables the current input's RP188 bypass.
		**/
		void			DisableRP188Bypass (void);


	//	Protected Class Methods
	protected:
		/**
			@brief	This is the playout thread's static callback function that gets called when the playout thread runs.
					This function gets "Attached" to the playout thread's AJAThread instance.
			@param[in]	pThread		A valid pointer to the playout thread's AJAThread instance.
			@param[in]	pContext	Context information to pass to the thread.
									(For this application, this will be set to point to the NTV2FieldBurn instance.)
		**/
		static void	PlayThreadStatic (AJAThread * pThread, void * pContext);

		/**
			@brief	This is the capture thread's static callback function that gets called when the capture thread runs.
					This function gets "Attached" to the AJAThread instance.
			@param[in]	pThread		Points to the AJAThread instance.
			@param[in]	pContext	Context information to pass to the thread.
									(For this application, this will be set to point to the NTV2FieldBurn instance.)
		**/
		static void	CaptureThreadStatic (AJAThread * pThread, void * pContext);

		/**
			@brief	Returns the equivalent AJA_PixelFormat for the given NTV2FrameBufferFormat.
			@param[in]	format		Specifies the NTV2FrameBufferFormat to be converted into an equivalent AJA_PixelFormat.
		**/
		static AJA_PixelFormat	GetAJAPixelFormat (const NTV2FrameBufferFormat format);

		/**
			@brief	Returns the equivalent TimecodeFormat for the given NTV2FrameRate.
			@param[in]	inFrameRate		Specifies the NTV2FrameRate to be converted into an equivalent TimecodeFormat.
		**/
		static TimecodeFormat	NTV2FrameRate2TimecodeFormat (const NTV2FrameRate inFrameRate);

		/**
			@brief	Returns the RP188 DBB register number to use for the given NTV2InputSource.
			@param[in]	inInputSource	Specifies the NTV2InputSource of interest.
			@return	The number of the RP188 DBB register to use for the given input source.
		**/
		static ULWord			GetRP188RegisterForInput (const NTV2InputSource inInputSource);


	//	Private Member Data
	private:
		AJAThread *					mPlayThread;			///	My playout thread object
		AJAThread *					mCaptureThread;			///	My capture thread object

		CNTV2Card					mDevice;				///	My CNTV2Card instance
		NTV2DeviceID				mDeviceID;				///	My device identifier
		const std::string			mDeviceSpecifier;		///	Specifies which device I should use
		const bool					mWithAudio;				///	Capture and playout audio?
		const NTV2Channel			mChannel;				///	The channel I'm using
		NTV2InputSource				mInputSource;			///	The input source I'm using
		const NTV2OutputDestination	mOutputDestination;		///	The output I'm using
		NTV2VideoFormat				mVideoFormat;			///	My video format
		NTV2FrameBufferFormat		mPixelFormat;			///	My pixel format
		NTV2FormatDescriptor		mFormatDescriptor;		/// Description of the board's frame geometry
		NTV2EveryFrameTaskMode		mPreviousFrameServices;	/// We will restore the previous state
		bool						mVancEnabled;			///	VANC enabled?
		bool						mWideVanc;				///	Wide VANC?
		NTV2AudioSystem				mAudioSystem;			///	The audio system I'm using

		bool						mGlobalQuit;			///	Set "true" to gracefully stop
		AJATimeCodeBurn				mTCBurner;				///	My timecode burner
		uint32_t					mQueueSize;				///	My queue size
		uint32_t					mVideoBufferSize;		///	My video buffer size, in bytes
		uint32_t					mAudioBufferSize;		///	My audio buffer size, in bytes

		AVDataBuffer							mAVHostBuffer [CIRCULAR_BUFFER_SIZE];	///	My host buffers
		AJACircularBuffer <AVDataBuffer *>		mAVCircularBuffer;						///	My ring buffer

		AUTOCIRCULATE_TRANSFER_STRUCT           mInputTransferStructField1;				///	My field 1 A/C input transfer info
		AUTOCIRCULATE_TRANSFER_STATUS_STRUCT    mInputTransferStatusStructField1;		///	My field 1 A/C input status
		AUTOCIRCULATE_TRANSFER_STRUCT           mOutputTransferStructField1;			///	My field 1 A/C output transfer info
		AUTOCIRCULATE_TRANSFER_STATUS_STRUCT    mOutputTransferStatusStructField1;		///	My field 1 A/C output status

		AUTOCIRCULATE_TRANSFER_STRUCT           mInputTransferStructField2;				///	My field 2 A/C input transfer info
		AUTOCIRCULATE_TRANSFER_STATUS_STRUCT    mInputTransferStatusStructField2;		///	My field 2 A/C input status
		AUTOCIRCULATE_TRANSFER_STRUCT           mOutputTransferStructField2;			///	My field 2 A/C output transfer info
		AUTOCIRCULATE_TRANSFER_STATUS_STRUCT    mOutputTransferStatusStructField2;		///	My field 2 A/C output status

};	//	NTV2FieldBurn

#endif	//	_NTV2FIELDBURN_H
