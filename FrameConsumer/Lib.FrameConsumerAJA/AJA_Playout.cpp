#include "AJA_Playout.h"
#include "../../Lib.Base/CommonHelper.h"
#include <shlwapi.h>
#include "../../lib.base/Config.h"
#include "..\Lib.Logger\LogWriter.h"
#define NTV2_ANCSIZE_MAX	(2048)

static const uint32_t	AUDIOBYTES_MAX_48K(201 * 1024);

AJAStatus CAJA_Playout::SetupVideo()
{
	mOutputDestination = NTV2ChannelToOutputDestination(mOutputChannel);
	mDevice.SetReference(NTV2_REFERENCE_FREERUN);
	m_ajaOutputBufferThreshold = 0;
	mDevice.EnableChannel(mOutputChannel);
	mDevice.SetVideoFormat(mVideoFormat, false, false, mOutputChannel);
	mDevice.SetFrameBufferFormat(NTV2Channel(mOutputChannel), mPixelFormat);

	mDevice.SetSDIOut3GEnable(mOutputChannel, false);
	mDevice.SetSDIOut3GbEnable(mOutputChannel, false);
	mDevice.SetSDITransmitEnable(mOutputChannel, true);
	//	Enable the output frame buffer
	mDevice.SetMultiFormatMode(true);

	mDevice.SetRP188Mode(mOutputChannel, NTV2_RP188_OUTPUT);													//	... and that we can transmit timcode on the output channel
	if (!NTV2_IS_SD_VIDEO_FORMAT(mVideoFormat))
	{
		mDevice.SetEnableVANCData(false, mWideVanc, mOutputChannel);
		if (Is8BitFrameBufferFormat(mPixelFormat))
			mDevice.SetVANCShiftMode(mOutputChannel, NTV2_VANCDATA_8BITSHIFT_ENABLE);
	}																										
	mDevice.SetMode(mOutputChannel, NTV2_MODE_DISPLAY);														//	Set the Frame Store modes
	mDevice.SetSDIOutLevelAtoLevelBConversion(NTV2Channel(mOutputChannel), false);

	return AJA_STATUS_SUCCESS;
}

void CAJA_Playout::readConfig()
{
	m_is4kInLevelA = true;
	m_is4kOutLevelA = true;
}

AJAStatus CAJA_Playout::SetupVideo4K(void)
{
	readConfig();
	mDevice.SetReference(NTV2_REFERENCE_FREERUN);
	mVideoFormat = NTV2_FORMAT_4x1920x1080p_5000;
	mOutputDestination = NTV2ChannelToOutputDestination(mOutputChannel);
	mDevice.SetMultiFormatMode(true);
	mDevice.SetEnableVANCData(false, false);
	for (int i = 0; i < 4; i++)
	{
		mDevice.SetVideoFormat(mVideoFormat, false, false, NTV2Channel(mOutputChannel + i));
		mDevice.SetFrameBufferFormat(NTV2Channel(mOutputChannel + i), mPixelFormat);
		mDevice.EnableChannel(NTV2Channel(mOutputChannel + i));
		if (m_is4kInLevelA)
		{
			//input is A
			mDevice.SetSDIOutLevelAtoLevelBConversion(NTV2Channel(mOutputChannel + i), !m_is4kOutLevelA);
		}
		else
		{
			//input is B ,but inside frametcp it's must A
			mDevice.SetSDIOutLevelAtoLevelBConversion(NTV2Channel(mOutputChannel + i), true);
		}

		mDevice.SetSDIOut3GEnable(NTV2Channel(mOutputChannel + i), m_is4kOutLevelA);
		mDevice.SetSDIOut3GbEnable(NTV2Channel(mOutputChannel + i), !m_is4kOutLevelA);
		mDevice.SetSDITransmitEnable(NTV2Channel(mOutputChannel + i), true);											//	...then enable transmit mode
	}

	//	Enable the output frame buffer
	mDevice.SetRP188Mode(mOutputChannel, NTV2_RP188_OUTPUT);													//	... and that we can transmit timcode on the output channel

	mDevice.SetVANCShiftMode(mOutputChannel, NTV2_VANCDATA_8BITSHIFT_ENABLE);									//	8-bit FBFs require VANC bit shift...

	for (int i = 0; i < 4; i++)
		mDevice.SetMode(NTV2Channel(mOutputChannel + i), NTV2_MODE_DISPLAY);									//	Set the Frame Store modes
	return AJA_STATUS_SUCCESS;
}

AJAStatus CAJA_Playout::SetupAudio(void)
{
	bool bRes = false;

	if (!mb4KMode)
	{
		if (NTV2DeviceGetNumAudioStreams(mDeviceID))
			mAudioSystem = NTV2ChannelToAudioSystem(mOutputChannel);
		else if (!NTV2DeviceCanDoFrameStore1Display(mDeviceID))
			mAudioSystem = NTV2_AUDIOSYSTEM_1;
	}
	else
	{
		mAudioSystem = (mOutputChannel == NTV2_CHANNEL1 ? NTV2_AUDIOSYSTEM_1 : NTV2_AUDIOSYSTEM_5);
	}
	bRes = mDevice.SetAudioOutputReset(mAudioSystem, true);															//	Reset both the input and output sides of the audio system so that the buffer pointers are reset to zero and inhibited from advancing.
	int n = NTV2DeviceGetMaxAudioChannels(mDeviceID);															//	It's best to use all available audio channels...
	bRes = mDevice.SetNumberAudioChannels(n, mAudioSystem);

	int nbAudioStream = NTV2BoardGetNumAudioStreams(mDeviceID);
	bRes = mDevice.SetAudioRate(NTV2_AUDIO_48K, mAudioSystem);														//	Assume 48kHz PCM...

	bRes = mDevice.SetAudioBufferSize(NTV2_AUDIO_BUFFER_BIG, mAudioSystem);											//	4MB device audio buffers work best...

	if (NTV2DeviceGetNumAudioStreams(mDeviceID) > 1)															//	Set up the output audio embedders...
	{
		if (mb4KMode)
		{
			for (int i = 0; i < 4; i++)
				mDevice.SetSDIOutputAudioSystem(NTV2Channel(mOutputChannel + i), mAudioSystem);
		}
		else
		{
			bRes = mDevice.SetSDIOutputAudioSystem(mOutputChannel, mAudioSystem);
			//mDevice.SetSDIOutputDS2AudioSystem(mOutputChannel, mAudioSystem);
		}
	}

	bRes = mDevice.SetAudioLoopBack(NTV2_AUDIO_LOOPBACK_OFF, mAudioSystem);
	bRes = mDevice.GetAudioWrapAddress(mAudioOutWrapAddress, mAudioSystem);

	bRes = mDevice.SetAudioOutputReset(mAudioSystem, false);															//	Reset both the input and output sides of the audio system so that the buffer pointers are reset to zero and inhibited from advancing.
	return bRes ? AJA_STATUS_SUCCESS : AJA_STATUS_FAIL;
}

void CAJA_Playout::RouteOutputSignal(void)
{
	const bool			isRGB(IsRGBFormat(mPixelFormat));
	const NTV2Standard	videoStandard(GetNTV2StandardFromVideoFormat(mVideoFormat));
	CNTV2SignalRouter	router;
	NTV2CrosspointID	fbOutput;
	NTV2RoutingEntry	sdiOutInput;
	const bool					mUseHDMIOut = false;				///	Enable HDMI output
	if (mb4KMode)
	{
		/*
		switch (mOutputChannel)
		{
		case NTV2_CHANNEL1: router.addWithValue(::GetSDIOut1InputSelectEntry(), NTV2_XptFrameBuffer1YUV);
		router.addWithValue(::GetSDIOut2InputSelectEntry(), NTV2_XptFrameBuffer2YUV);
		router.addWithValue(::GetSDIOut3InputSelectEntry(), NTV2_XptFrameBuffer3YUV);
		router.addWithValue(::GetSDIOut4InputSelectEntry(), NTV2_XptFrameBuffer4YUV);
		break;
		case NTV2_CHANNEL5: router.addWithValue(::GetSDIOut5InputSelectEntry(), NTV2_XptFrameBuffer5YUV);
		router.addWithValue(::GetSDIOut6InputSelectEntry(), NTV2_XptFrameBuffer6YUV);
		router.addWithValue(::GetSDIOut7InputSelectEntry(), NTV2_XptFrameBuffer7YUV);
		router.addWithValue(::GetSDIOut8InputSelectEntry(), NTV2_XptFrameBuffer8YUV);
		break;
		}
		*/
		if (isRGB && mOutputChannel == NTV2_CHANNEL1)
		{
			//	For RGB, connect CSCs between the frame buffer outputs ans the SDI outputs
			router.AddConnection(GetCSC1VidInputSelectEntry(), NTV2_XptFrameBuffer1RGB);
			router.AddConnection(GetSDIOut1InputSelectEntry(), NTV2_XptCSC1VidYUV);

			router.AddConnection(GetCSC2VidInputSelectEntry(), NTV2_XptFrameBuffer2RGB);
			router.AddConnection(GetSDIOut2InputSelectEntry(), NTV2_XptCSC2VidYUV);

			router.AddConnection(GetCSC3VidInputSelectEntry(), NTV2_XptFrameBuffer3RGB);
			router.AddConnection(GetSDIOut3InputSelectEntry(), NTV2_XptCSC3VidYUV);

			router.AddConnection(GetCSC4VidInputSelectEntry(), NTV2_XptFrameBuffer4RGB);
			router.AddConnection(GetSDIOut4InputSelectEntry(), NTV2_XptCSC4VidYUV);

			if (NTV2DeviceCanDoWidget(mDeviceID, NTV2_Wgt4KDownConverter) && NTV2DeviceCanDoWidget(mDeviceID, NTV2_WgtSDIMonOut1))
			{
				router.AddConnection(GetXpt4KDCQ1InputSelectEntry(), NTV2_XptFrameBuffer1RGB);
				router.AddConnection(GetXpt4KDCQ2InputSelectEntry(), NTV2_XptFrameBuffer2RGB);
				router.AddConnection(GetXpt4KDCQ3InputSelectEntry(), NTV2_XptFrameBuffer3RGB);
				router.AddConnection(GetXpt4KDCQ4InputSelectEntry(), NTV2_XptFrameBuffer4RGB);

				mDevice.Enable4KDCRGBMode(true);

				router.AddConnection(GetCSC5VidInputSelectEntry(), NTV2_Xpt4KDownConverterOut);
				router.AddConnection(GetSDIOut5InputSelectEntry(), NTV2_XptCSC5VidYUV);
				mDevice.SetSDIOutputStandard(NTV2_CHANNEL1, videoStandard);
			}

			if (mUseHDMIOut && NTV2DeviceCanDoWidget(mDeviceID, NTV2_WgtHDMIOut1v2))
			{
				router.AddConnection(GetHDMIOutInputSelectEntry(), NTV2_XptCSC1VidYUV);
				router.AddConnection(GetHDMIOutQ2InputSelectEntry(), NTV2_XptCSC2VidYUV);
				router.AddConnection(GetHDMIOutQ3InputSelectEntry(), NTV2_XptCSC3VidYUV);
				router.AddConnection(GetHDMIOutQ4InputSelectEntry(), NTV2_XptCSC4VidYUV);

				mDevice.SetHDMIV2TxBypass(false);
				mDevice.SetHDMIV2OutVideoStandard(GetHdmiV2StandardFromVideoFormat(mVideoFormat));
				mDevice.SetHDMIOutVideoFPS(GetNTV2FrameRateFromVideoFormat(mVideoFormat));
				mDevice.SetLHIHDMIOutColorSpace(NTV2_LHIHDMIColorSpaceYCbCr);
				mDevice.SetHDMIV2Mode(NTV2_HDMI_V2_4K_PLAYBACK);
			}
			else
				mDevice.SetHDMIV2Mode(NTV2_HDMI_V2_4K_DISABLE);
		}	//	if isRGB
		else if (mOutputChannel == NTV2_CHANNEL1)
		{
			//	For YCbCr, the frame buffer outputs feed the SDI outputs directly
			router.AddConnection(GetSDIOut1InputSelectEntry(), NTV2_XptFrameBuffer1YUV);
			router.AddConnection(GetSDIOut2InputSelectEntry(), NTV2_XptFrameBuffer2YUV);
			router.AddConnection(GetSDIOut3InputSelectEntry(), NTV2_XptFrameBuffer3YUV);
			router.AddConnection(GetSDIOut4InputSelectEntry(), NTV2_XptFrameBuffer4YUV);

			if (NTV2DeviceCanDoWidget(mDeviceID, NTV2_Wgt4KDownConverter) && NTV2DeviceCanDoWidget(mDeviceID, NTV2_WgtSDIMonOut1) &&
				mOutputChannel == NTV2_CHANNEL1)
			{
				router.AddConnection(GetXpt4KDCQ1InputSelectEntry(), NTV2_XptFrameBuffer1YUV);
				router.AddConnection(GetXpt4KDCQ2InputSelectEntry(), NTV2_XptFrameBuffer2YUV);
				router.AddConnection(GetXpt4KDCQ3InputSelectEntry(), NTV2_XptFrameBuffer3YUV);
				router.AddConnection(GetXpt4KDCQ4InputSelectEntry(), NTV2_XptFrameBuffer4YUV);

				mDevice.Enable4KDCRGBMode(false);

				router.AddConnection(GetSDIOut5InputSelectEntry(), NTV2_Xpt4KDownConverterOut);
				mDevice.SetSDIOutputStandard(NTV2_CHANNEL5, videoStandard);
			}

			if (mUseHDMIOut && NTV2DeviceCanDoWidget(mDeviceID, NTV2_WgtHDMIOut1v2) && mOutputChannel == NTV2_CHANNEL1)
			{
				router.AddConnection(GetHDMIOutInputSelectEntry(), NTV2_XptFrameBuffer1YUV);
				router.AddConnection(GetHDMIOutQ2InputSelectEntry(), NTV2_XptFrameBuffer2YUV);
				router.AddConnection(GetHDMIOutQ3InputSelectEntry(), NTV2_XptFrameBuffer3YUV);
				router.AddConnection(GetHDMIOutQ4InputSelectEntry(), NTV2_XptFrameBuffer4YUV);

				mDevice.SetHDMIV2TxBypass(false);
				mDevice.SetHDMIV2OutVideoStandard(GetHdmiV2StandardFromVideoFormat(mVideoFormat));
				mDevice.SetHDMIOutVideoFPS(GetNTV2FrameRateFromVideoFormat(mVideoFormat));
				mDevice.SetLHIHDMIOutColorSpace(NTV2_LHIHDMIColorSpaceYCbCr);
				mDevice.SetHDMIV2Mode(NTV2_HDMI_V2_4K_PLAYBACK);
			}
			else
				mDevice.SetHDMIV2Mode(NTV2_HDMI_V2_4K_DISABLE);
		}	//	else YUV
		else if (isRGB && mOutputChannel == NTV2_CHANNEL5)
		{
			//	For RGB, connect CSCs between the frame buffer outputs ans the SDI outputs
			router.AddConnection(GetCSC5VidInputSelectEntry(), NTV2_XptFrameBuffer5RGB);
			router.AddConnection(GetSDIOut5InputSelectEntry(), NTV2_XptCSC5VidYUV);

			router.AddConnection(GetCSC6VidInputSelectEntry(), NTV2_XptFrameBuffer6RGB);
			router.AddConnection(GetSDIOut6InputSelectEntry(), NTV2_XptCSC6VidYUV);

			router.AddConnection(GetCSC7VidInputSelectEntry(), NTV2_XptFrameBuffer7RGB);
			router.AddConnection(GetSDIOut7InputSelectEntry(), NTV2_XptCSC7VidYUV);

			router.AddConnection(GetCSC8VidInputSelectEntry(), NTV2_XptFrameBuffer8RGB);
			router.AddConnection(GetSDIOut8InputSelectEntry(), NTV2_XptCSC8VidYUV);

			if (NTV2DeviceCanDoWidget(mDeviceID, NTV2_Wgt4KDownConverter) && NTV2DeviceCanDoWidget(mDeviceID, NTV2_WgtSDIMonOut1))
			{
				router.AddConnection(GetXpt4KDCQ1InputSelectEntry(), NTV2_XptFrameBuffer5RGB);
				router.AddConnection(GetXpt4KDCQ2InputSelectEntry(), NTV2_XptFrameBuffer6RGB);
				router.AddConnection(GetXpt4KDCQ3InputSelectEntry(), NTV2_XptFrameBuffer7RGB);
				router.AddConnection(GetXpt4KDCQ4InputSelectEntry(), NTV2_XptFrameBuffer8RGB);

				mDevice.Enable4KDCRGBMode(true);

				router.AddConnection(GetCSC5VidInputSelectEntry(), NTV2_Xpt4KDownConverterOut);
				router.AddConnection(GetSDIOut5InputSelectEntry(), NTV2_XptCSC5VidYUV);
			}

			if (mUseHDMIOut && NTV2DeviceCanDoWidget(mDeviceID, NTV2_WgtHDMIOut1v2))
			{
				router.AddConnection(GetHDMIOutInputSelectEntry(), NTV2_XptCSC5VidYUV);
				router.AddConnection(GetHDMIOutQ2InputSelectEntry(), NTV2_XptCSC6VidYUV);
				router.AddConnection(GetHDMIOutQ3InputSelectEntry(), NTV2_XptCSC7VidYUV);
				router.AddConnection(GetHDMIOutQ4InputSelectEntry(), NTV2_XptCSC8VidYUV);

				mDevice.SetHDMIV2TxBypass(false);
				mDevice.SetHDMIV2OutVideoStandard(GetHdmiV2StandardFromVideoFormat(mVideoFormat));
				mDevice.SetHDMIOutVideoFPS(GetNTV2FrameRateFromVideoFormat(mVideoFormat));
				mDevice.SetLHIHDMIOutColorSpace(NTV2_LHIHDMIColorSpaceYCbCr);
				mDevice.SetHDMIV2Mode(NTV2_HDMI_V2_4K_PLAYBACK);
			}
			else
				mDevice.SetHDMIV2Mode(NTV2_HDMI_V2_4K_DISABLE);
		}
		else
		{
			//	For YCbCr, the frame buffer outputs feed the SDI outputs directly
			router.AddConnection(GetSDIOut5InputSelectEntry(), NTV2_XptFrameBuffer5YUV);
			router.AddConnection(GetSDIOut6InputSelectEntry(), NTV2_XptFrameBuffer6YUV);
			router.AddConnection(GetSDIOut7InputSelectEntry(), NTV2_XptFrameBuffer7YUV);
			router.AddConnection(GetSDIOut8InputSelectEntry(), NTV2_XptFrameBuffer8YUV);

			if (NTV2DeviceCanDoWidget(mDeviceID, NTV2_Wgt4KDownConverter) && NTV2DeviceCanDoWidget(mDeviceID, NTV2_WgtSDIMonOut1) &&
				mOutputChannel == NTV2_CHANNEL1)
			{
				router.AddConnection(GetXpt4KDCQ1InputSelectEntry(), NTV2_XptFrameBuffer5YUV);
				router.AddConnection(GetXpt4KDCQ2InputSelectEntry(), NTV2_XptFrameBuffer6YUV);
				router.AddConnection(GetXpt4KDCQ3InputSelectEntry(), NTV2_XptFrameBuffer7YUV);
				router.AddConnection(GetXpt4KDCQ4InputSelectEntry(), NTV2_XptFrameBuffer8YUV);

				mDevice.Enable4KDCRGBMode(false);

				router.AddConnection(GetSDIOut5InputSelectEntry(), NTV2_Xpt4KDownConverterOut);
			}

			if (mUseHDMIOut && NTV2DeviceCanDoWidget(mDeviceID, NTV2_WgtHDMIOut1v2) &&
				mOutputChannel == NTV2_CHANNEL1)
			{
				router.AddConnection(GetHDMIOutInputSelectEntry(), NTV2_XptFrameBuffer5YUV);
				router.AddConnection(GetHDMIOutQ2InputSelectEntry(), NTV2_XptFrameBuffer6YUV);
				router.AddConnection(GetHDMIOutQ3InputSelectEntry(), NTV2_XptFrameBuffer7YUV);
				router.AddConnection(GetHDMIOutQ4InputSelectEntry(), NTV2_XptFrameBuffer8YUV);

				mDevice.SetHDMIV2TxBypass(false);
				mDevice.SetHDMIV2OutVideoStandard(GetHdmiV2StandardFromVideoFormat(mVideoFormat));
				mDevice.SetHDMIOutVideoFPS(GetNTV2FrameRateFromVideoFormat(mVideoFormat));
				mDevice.SetLHIHDMIOutColorSpace(NTV2_LHIHDMIColorSpaceYCbCr);
				mDevice.SetHDMIV2Mode(NTV2_HDMI_V2_4K_PLAYBACK);
			}
			else
				mDevice.SetHDMIV2Mode(NTV2_HDMI_V2_4K_DISABLE);
		}

		//	Enable SDI output from all channels,
		//	but only if the device supports bi-directional SDI.
		if (NTV2DeviceHasBiDirectionalSDI(mDeviceID))
		{
			if (mOutputChannel == NTV2_CHANNEL1)
			{
				mDevice.SetSDITransmitEnable(NTV2_CHANNEL1, true);
				mDevice.SetSDITransmitEnable(NTV2_CHANNEL2, true);
				mDevice.SetSDITransmitEnable(NTV2_CHANNEL3, true);
				mDevice.SetSDITransmitEnable(NTV2_CHANNEL4, true);
			}
			else
			{
				mDevice.SetSDITransmitEnable(NTV2_CHANNEL5, true);
				mDevice.SetSDITransmitEnable(NTV2_CHANNEL6, true);
				mDevice.SetSDITransmitEnable(NTV2_CHANNEL7, true);
				mDevice.SetSDITransmitEnable(NTV2_CHANNEL8, true);
			}
		}
	}
	else
	{
		//hd
		switch (mOutputChannel)
		{
		default:
		case NTV2_CHANNEL1:	router.addWithValue(GetSDIOut1InputSelectEntry(), NTV2_XptFrameBuffer1YUV);	break;
		case NTV2_CHANNEL2: router.addWithValue(GetSDIOut2InputSelectEntry(), NTV2_XptFrameBuffer2YUV);	break;
		case NTV2_CHANNEL3:	router.addWithValue(GetSDIOut3InputSelectEntry(), NTV2_XptFrameBuffer3YUV);	break;
		case NTV2_CHANNEL4:	router.addWithValue(GetSDIOut4InputSelectEntry(), NTV2_XptFrameBuffer4YUV);	break;
		case NTV2_CHANNEL5:	router.addWithValue(GetSDIOut5InputSelectEntry(), NTV2_XptFrameBuffer5YUV);	break;
		case NTV2_CHANNEL6:	router.addWithValue(GetSDIOut6InputSelectEntry(), NTV2_XptFrameBuffer6YUV);	break;
		case NTV2_CHANNEL7:	router.addWithValue(GetSDIOut7InputSelectEntry(), NTV2_XptFrameBuffer7YUV);	break;
		case NTV2_CHANNEL8:	router.addWithValue(GetSDIOut8InputSelectEntry(), NTV2_XptFrameBuffer8YUV);	break;
		}
	}
	mDevice.ApplySignalRoute(router, false);
}

AJAStatus CAJA_Playout::SetupHostBuffers(void)
{
	if (!mb4KMode)
	{
		/*
		mAudioBufferSize = NTV2_AUDIOSIZE_MAX;
		mpHostVideoBuffer = reinterpret_cast <uint32_t *> (AJAMemory::AllocateAligned(mVideoBufferSize, AJA_PAGE_SIZE));
		mpHostAudioBuffer = reinterpret_cast <uint32_t *> (AJAMemory::AllocateAligned(mAudioBufferSize, AJA_PAGE_SIZE));
		if (!mpHostAudioBuffer)
		{
		cerr << "## ERROR:  Unable to allocate host video and/or audio buffer " << endl;
		return AJA_STATUS_MEMORY;
		}
		*/

		mAVCircularBuffer.SetAbortFlag(&mGlobalQuit);

		mVideoBufferSize = GetVideoWriteSize(mVideoFormat, mPixelFormat, mVancEnabled, mWideVanc);
		mAudioBufferSize = AUDIOBYTES_MAX_48K;

		for (unsigned int ndx = 0; ndx < CIRCULAR_BUFFER_SIZE; ndx++)
		{
			mAVHostBuffer[ndx].fVideoBuffer = reinterpret_cast <uint32_t *> (AJAMemory::AllocateAligned(mVideoBufferSize, AJA_PAGE_SIZE));;
			mAVHostBuffer[ndx].fVideoBufferSize = mVideoBufferSize;
			mAVHostBuffer[ndx].fAudioBuffer = mWithAudio
				? reinterpret_cast <uint32_t *> (AJAMemory::AllocateAligned(mAudioBufferSize, AJA_PAGE_SIZE))
				: NULL;
			mAVHostBuffer[ndx].fAudioBufferSize = mWithAudio ? mAudioBufferSize : 0;
			memset(mAVHostBuffer[ndx].fAudioBuffer, 0x00, mWithAudio ? mAudioBufferSize : 0);
			mAVHostBuffer[ndx].fAncBuffer = reinterpret_cast <uint32_t *> (new uint8_t[NTV2_ANCSIZE_MAX]);
			mAVHostBuffer[ndx].fAncBufferSize = NTV2_ANCSIZE_MAX;
			if (Config::getInstance()->isProgressive())
			{
				mAVHostBuffer[ndx].fAncF2Buffer = nullptr;
				mAVHostBuffer[ndx].fAncF2BufferSize = 0;
			}
			else
			{
				mAVHostBuffer[ndx].fAncF2Buffer = reinterpret_cast <uint32_t *> (new uint8_t[NTV2_ANCSIZE_MAX]);
				mAVHostBuffer[ndx].fAncF2BufferSize = NTV2_ANCSIZE_MAX;
			}

			mAVCircularBuffer.Add(&mAVHostBuffer[ndx]);
		}
	}
	else
	{
		mAVCircularBuffer.SetAbortFlag(&mGlobalQuit);

		mVideoBufferSize = GetVideoWriteSize(mVideoFormat, mPixelFormat, mVancEnabled, mWideVanc);
		mAudioBufferSize = AUDIOBYTES_MAX_48K;

		for (unsigned int ndx = 0; ndx < CIRCULAR_BUFFER_SIZE; ndx++)
		{
			mAVHostBuffer[ndx].fVideoBuffer = reinterpret_cast <uint32_t *> (AJAMemory::AllocateAligned(mVideoBufferSize, AJA_PAGE_SIZE));;
			mAVHostBuffer[ndx].fVideoBufferSize = mVideoBufferSize;
			mAVHostBuffer[ndx].fAudioBuffer = mWithAudio
				? reinterpret_cast <uint32_t *> (AJAMemory::AllocateAligned(mAudioBufferSize, AJA_PAGE_SIZE))
				: NULL;
			mAVHostBuffer[ndx].fAudioBufferSize = mWithAudio ? mAudioBufferSize : 0;
			memset(mAVHostBuffer[ndx].fAudioBuffer, 0x00, mWithAudio ? mAudioBufferSize : 0);
			mAVHostBuffer[ndx].fAncBuffer = reinterpret_cast <uint32_t *> (new uint8_t[NTV2_ANCSIZE_MAX]);
			mAVHostBuffer[ndx].fAncBufferSize = NTV2_ANCSIZE_MAX;
			if (Config::getInstance()->isProgressive())
			{
				mAVHostBuffer[ndx].fAncF2Buffer = nullptr;
				mAVHostBuffer[ndx].fAncF2BufferSize = 0;
			}
			else
			{
				mAVHostBuffer[ndx].fAncF2Buffer = reinterpret_cast <uint32_t *> (new uint8_t[NTV2_ANCSIZE_MAX]);
				mAVHostBuffer[ndx].fAncF2BufferSize = NTV2_ANCSIZE_MAX;
			}

			mAVCircularBuffer.Add(&mAVHostBuffer[ndx]);
		}
	}

	return AJA_STATUS_SUCCESS;
}

AJAStatus CAJA_Playout::OutputFramesToAja(UINT *pVideoBuffer, UINT *pAudioBuffer, DWORD dwVSize, DWORD dwASize)
{
#ifdef _DEBUG_AUDIO
	static FILE*  fAudio = NULL;
	if (!fAudio)
	{
		fAudio = fopen("d:\\testplayoutAudio.pcm", "wb");
	}
#endif // _DEBUG_AUDIO

	AJAStatus	status(AJA_STATUS_SUCCESS);

	//	Find out which frame is currently being output from the frame store...

	static uint32_t	currentOutputFrame = 2;
	if (!mDevice.GetOutputFrame(mOutputChannel, currentOutputFrame))
	{
		status = AJA_STATUS_FAIL;
	}
	currentOutputFrame ^= 1;

	if (AJA_SUCCESS(status))
	{
		if (!mDevice.DmaWriteFrame(NTV2_DMA_FIRST_AVAILABLE, currentOutputFrame, (ULWord *)pVideoBuffer, dwVSize))
		{
			status = AJA_STATUS_FAIL;
		}
		else
		{
			/*
			//succeed for video ,now output the audio
			if (!mDevice.DmaAudioWrite(NTV2_DMA_FIRST_AVAILABLE, mAudioSystem, reinterpret_cast <uint32_t *>(pAudioBuffer),0, (uint32_t)dwASize,true))
			{
			status = AJA_STATUS_FAIL;
			}
			else
			mDevice.SetOutputFrame(mOutputChannel, currentOutputFrame);
			*/

			//	Calculate where the next audio samples should go in the buffer, taking wraparound into account...

			int audioBytesCaptured = dwASize;
			memcpy(mpHostAudioBuffer, pAudioBuffer, dwASize);
			if ((mAudioOutLastAddress + audioBytesCaptured) > mAudioOutWrapAddress)
			{
#ifdef _DEBUG_AUDIO
				fwrite(pAudioBuffer, 1, dwASize, fAudio);
#endif
				//	The audio will wrap. Transfer enough bytes to fill the buffer to the end...
				mDevice.DmaAudioWrite(NTV2_DMA_FIRST_AVAILABLE,
					mAudioSystem,
					mpHostAudioBuffer,
					mAudioOutLastAddress,
					mAudioOutWrapAddress - mAudioOutLastAddress,
					true);

				//	Now transfer the remaining bytes to the front of the buffer...
				mDevice.DmaAudioWrite(NTV2_DMA_FIRST_AVAILABLE,
					mAudioSystem,
					&mpHostAudioBuffer[(mAudioOutWrapAddress - mAudioOutLastAddress) / sizeof(uint32_t)],
					0,
					audioBytesCaptured - (mAudioOutWrapAddress - mAudioOutLastAddress),
					true);

				mAudioOutLastAddress = audioBytesCaptured - (mAudioOutWrapAddress - mAudioOutLastAddress);
			}
			else
			{
				//	No wrap, so just do a linear DMA from the buffer...
				mDevice.DmaAudioWrite(NTV2_DMA_FIRST_AVAILABLE,
					mAudioSystem,
					mpHostAudioBuffer,
					mAudioOutLastAddress,
					audioBytesCaptured,
					true);

				mAudioOutLastAddress += audioBytesCaptured;
#ifdef _DEBUG_AUDIO
				fwrite(pAudioBuffer, 1, dwASize, fAudio);
#endif // _DEBUG_AUDIO
			}

			mDevice.SetOutputFrame(mOutputChannel, currentOutputFrame);
		}
	}
	return status;
}

AJAStatus CAJA_Playout::OutputFrames4KToAja(UINT *pVideoBuffer, UINT *pAudioBuffer, DWORD dwVSize, DWORD dwASize)
{
	AJAStatus	status(AJA_STATUS_SUCCESS);
	AVDataBuffer *	frameData(mAVCircularBuffer.StartProduceNextBuffer());
	memcpy(frameData->fVideoBuffer, pVideoBuffer, dwVSize);
	memcpy(frameData->fAudioBuffer, pAudioBuffer, dwASize);
	frameData->fAudioBufferSize = dwASize;
	mAVCircularBuffer.EndProduceNextBuffer();
	return status;
}

void CAJA_Playout::SetUpOutputAutoCirculate()
{
	//mDevice.AutoCirculateStop(mOutputChannel);
	//mDevice.AutoCirculateInitForInput(mOutputChannel, 8,mAudioSystem, AUTOCIRCULATE_WITH_RP188);
	//mInputTransfer.acFrameBufferFormat = mPixelFormat;
	/*
	uint32_t	startFrame(0);
	uint32_t	endFrame(7);

	switch (mOutputChannel)
	{
	default:
	case NTV2_CHANNEL1:		mChannelSpec = NTV2CROSSPOINT_CHANNEL1;		startFrame = 0;		endFrame = 5;		break;
	case NTV2_CHANNEL5:		mChannelSpec = NTV2CROSSPOINT_CHANNEL5;		startFrame = 6;		endFrame = 11;		break;
	}
	mDevice.StopAutoCirculate(mChannelSpec);

	::memset(&mOutputTransferStruct, 0, sizeof(mOutputTransferStruct));
	::memset(&mOutputTransferStatusStruct, 0, sizeof(mOutputTransferStatusStruct));

	mOutputTransferStruct.channelSpec = mChannelSpec;
	mOutputTransferStruct.videoBufferSize = mVideoBufferSize;
	mOutputTransferStruct.videoDmaOffset = 0;
	mOutputTransferStruct.audioBufferSize = mAudioBufferSize;
	mOutputTransferStruct.frameRepeatCount = 1;
	mOutputTransferStruct.desiredFrame = -1;
	mOutputTransferStruct.frameBufferFormat = mPixelFormat;
	mOutputTransferStruct.bDisableExtraAudioInfo = true;

	mDevice.InitAutoCirculate(mOutputTransferStruct.channelSpec, startFrame, endFrame,
	1,				//	Number of channels
	mAudioSystem,	//	Which audio system?
	mWithAudio,		//	With audio?
	true,			//	Add timecode!
	false,			//	Allow frame buffer format changes?
	false,			//	With color correction?
	false,			//	With vidProc?
	false,			//	With custom ANC data?
	false,			//	With LTC?
	false);			//	With audio2?

	*/
	mDevice.AutoCirculateStop(mOutputChannel);
	mDevice.WaitForOutputFieldID(Config::getInstance()->isProgressive() ? NTV2_FIELD0 : NTV2_FIELD1, mOutputChannel);
	mDevice.WaitForOutputFieldID(Config::getInstance()->isProgressive() ? NTV2_FIELD0 : NTV2_FIELD1, mOutputChannel);
	ULWord flags = AUTOCIRCULATE_WITH_RP188 | AUTOCIRCULATE_WITH_ANC;
	mDevice.AutoCirculateInitForOutput(mOutputChannel, CIRCULAR_BUFFER_SIZE, mAudioSystem, flags, 1);
}

void CAJA_Playout::StartPlayThread(void)
{
	mPlayThread = new AJAThread();
	mPlayThread->Attach(PlayThreadStatic, this);
	mPlayThread->SetPriority(AJA_ThreadPriority_High);
	mPlayThread->Start();
}

void CAJA_Playout::PlayThreadStatic(AJAThread * pThread, void * pContext)
{
	(void)pThread;
	CAJA_Playout *	pApp(reinterpret_cast <CAJA_Playout *> (pContext));
	pApp->PlayFrames();
}

void CAJA_Playout::PlayFrames(void)
{
	/*
	mDevice.StartAutoCirculate(mOutputTransferStruct.channelSpec);
	while (!mGlobalQuit)
	{
	AUTOCIRCULATE_STATUS_STRUCT	outputStatus;
	mDevice.GetAutoCirculate(mOutputTransferStruct.channelSpec, &outputStatus);
	if ((outputStatus.bufferLevel < (ULWord)(outputStatus.endFrame - outputStatus.startFrame - 1)))
	{
	AVDataBuffer *	playData(mAVCircularBuffer.StartConsumeNextBuffer());
	if (playData!= NULL)
	{
	mOutputTransferStruct.videoBuffer = playData.fVideoBuffer;
	mOutputTransferStruct.videoBufferSize = playData.fVideoBufferSize;
	mOutputTransferStruct.audioBuffer = mWithAudio ? playData.fAudioBuffer : NULL;
	mOutputTransferStruct.audioBufferSize = mWithAudio ? playData.fAudioBufferSize : 0;
	mOutputTransferStruct.rp188 = playData.fRP188Data;	//	Include timecode in output signal

	mDevice.TransferWithAutoCirculate(&mOutputTransferStruct, &mOutputTransferStatusStruct);
	mAVCircularBuffer.EndConsumeNextBuffer();
	}
	}
	else
	mDevice.WaitForOutputVerticalInterrupt(mOutputChannel);
	}
	mDevice.StopAutoCirculate(mOutputTransferStruct.channelSpec);
	*/
	const NTV2TCDestination	tcDest(NTV2ChannelToTimecodeSource(mOutputChannel));	//	TCSources == TCDestinations for now
	mDevice.AutoCirculateStart(mOutputChannel);	//	Start it running

	while (!mGlobalQuit)
	{
		AUTOCIRCULATE_STATUS	outputStatus;
		mDevice.AutoCirculateGetStatus(mOutputChannel, outputStatus);
		if ((outputStatus.acBufferLevel < (ULWord)(outputStatus.acEndFrame - outputStatus.acStartFrame - 1)))
		{
			//	Wait for the next frame to become ready to "consume"...
			AVDataBuffer *	playData = mAVCircularBuffer.StartConsumeNextBuffer();
			if (playData)
			{
				//NTV2TimeCodes	timecodes;
				//timecodes[NTV2_TCDEST_DEFAULT] = timecodes[tcDest] = NTV2_RP188(playData->fRP188Data);
				//mOutputXferInfo.SetOutputTimeCodes(timecodes);	//	Include timecode in output signal

				//	Transfer the timecode-burned frame to the device for playout...
				mOutputXferInfo.SetVideoBuffer(playData->fVideoBuffer, playData->fVideoBufferSize);
				mOutputXferInfo.SetAudioBuffer(mWithAudio ? playData->fAudioBuffer : NULL, mWithAudio ? playData->fAudioBufferSize : 0);
				mOutputXferInfo.SetAncBuffers(playData->fAncBufferSize == 0 ? nullptr : playData->fAncBuffer, playData->fAncBufferSize,
					playData->fAncF2BufferSize == 0 ? nullptr : playData->fAncF2Buffer, playData->fAncF2BufferSize);
				/*
				{
					UINT64 * pData = (UINT64 *)playData->fAncBuffer;
					FILE *fp = nullptr;
					char szName[MAX_PATH];
					sprintf_s(szName, MAX_PATH, "E:\\TEST\\PLAY\\%I64d_%d.anc", *pData, playData->fAncBufferSize);
					fopen_s(&fp, szName, "wb");
					if (fp != nullptr)
					{
						fwrite(pData, playData->fAncBufferSize, 1, fp);
						fclose(fp);
					}
				}
				*/
				mDevice.AutoCirculateTransfer(mOutputChannel, mOutputXferInfo);
				mAVCircularBuffer.EndConsumeNextBuffer();	//	Signal that the frame has been "consumed"
			}
		}
		else
			mDevice.WaitForOutputVerticalInterrupt(mOutputChannel);
	}	//	loop til quit signaled

		//	Stop AutoCirculate...
	mDevice.AutoCirculateStop(mOutputChannel);
}

CAJA_Playout::CAJA_Playout() :
	mPlayThread(nullptr),
	mWithAudio(true),
	mOutputChannel(NTV2_CHANNEL_INVALID),
	mOutputDestination(NTV2_OUTPUTDESTINATION_SDI3),
	mVideoFormat(NTV2_FORMAT_UNKNOWN),
	mPixelFormat(NTV2_FBF_8BIT_YCBCR),
	mSavedTaskMode(NTV2_DISABLE_TASKS),
	mVancEnabled(true),
	mWideVanc(false),
	mAudioSystem(NTV2_AUDIOSYSTEM_5),
	mGlobalQuit(false),
	mAudioBufferSize(0),
	mpHostAudioBuffer(nullptr),
	mFramesProcessed(0),
	mFramesDropped(0),
	mb4KMode(false)
{
	for (unsigned int ndx = 0; ndx < CIRCULAR_BUFFER_SIZE; ndx++)
	{
		mAVHostBuffer[ndx].fVideoBuffer = nullptr;
		mAVHostBuffer[ndx].fAudioBuffer = nullptr;
		mAVHostBuffer[ndx].fAncBuffer = nullptr;
		mAVHostBuffer[ndx].fAncF2Buffer = nullptr;
	}
}

CAJA_Playout::~CAJA_Playout()
{
	CAJA_Playout::UnInitialize();																											//	Stop my capture and playout threads, then destroy them...
	if (mPlayThread)
		delete mPlayThread;

	if (mpHostAudioBuffer)
		AJAMemory::FreeAligned(mpHostAudioBuffer);

	mDevice.SetAudioOutputReset(mAudioSystem, true);
	if (mb4KMode)
		mDevice.UnsubscribeOutputVerticalEvent(NTV2_CHANNEL5);
	else
		mDevice.UnsubscribeOutputVerticalEvent(NTV2_CHANNEL8);
	mDevice.Close();
	//	Free all my buffers...
	for (unsigned bufferNdx = 0; bufferNdx < CIRCULAR_BUFFER_SIZE; bufferNdx++)
	{
		if (mAVHostBuffer[bufferNdx].fVideoBuffer)
		{
			AJAMemory::FreeAligned(mAVHostBuffer[bufferNdx].fVideoBuffer);
			mAVHostBuffer[bufferNdx].fVideoBuffer = nullptr;
		}
		if (mAVHostBuffer[bufferNdx].fAudioBuffer)
		{
			AJAMemory::FreeAligned(mAVHostBuffer[bufferNdx].fAudioBuffer);
			mAVHostBuffer[bufferNdx].fAudioBuffer = nullptr;
		}
		if (mAVHostBuffer[bufferNdx].fAncBuffer)
		{
			delete[]mAVHostBuffer[bufferNdx].fAncBuffer;
			mAVHostBuffer[bufferNdx].fAncBuffer = nullptr;
		}
		if (mAVHostBuffer[bufferNdx].fAncF2Buffer)
		{
			delete[] mAVHostBuffer[bufferNdx].fAncF2Buffer;
			mAVHostBuffer[bufferNdx].fAncF2Buffer = nullptr;
		}
	}	//	for each buffer in the ring
}

int CAJA_Playout::Initialize(DWORD dwCnlID, const sFrameConsumer_Parameter& pCnlParameter)
{
	AJAStatus	status(AJA_STATUS_SUCCESS);

	if (!CNTV2DeviceScanner::GetDeviceAtIndex(pCnlParameter.dwBoardID, mDevice))
	{
		WriteLogA(LOGPATH, ErrorLevel, "CAJA_Capture::Initialize GetDeviceAtIndex Failed.(%d)", pCnlParameter.dwBoardID);
		return -2;
	}
	mDeviceID = mDevice.GetDeviceID();
	mOutputChannel = NTV2Channel(pCnlParameter.dwSdiID);

	getAJAFormat(pCnlParameter);
	clearCables(pCnlParameter.dwBoardID);

	status = mb4KMode ? SetupVideo4K() : SetupVideo();									//	Set up the video and audio...
	bool ret = false;
	if (mb4KMode)
		ret = mDevice.SubscribeOutputVerticalEvent(NTV2_CHANNEL5);
	else
		ret = mDevice.SubscribeOutputVerticalEvent(NTV2_CHANNEL8);
	if (!ret)
		WriteLogW(LOGPATH, ErrorLevel, L"SubscribeOutput Failed at cnl(%d) in CAJA_Playout::Initialize", dwCnlID);
	else
		WriteLogW(LOGPATH, InfoLevel, L"SubscribeOutput success at cnl(%d) in CAJA_Playout::Initialize", dwCnlID);

	if (AJA_FAILURE(status))
		return -1;

	status = SetupAudio();
	if (AJA_FAILURE(status))
		return -2;

	status = SetupHostBuffers();																							//	Set up the circular buffers...
	if (AJA_FAILURE(status))
		return -3;

	RouteOutputSignal();																									//	Set up the device signal routing, and both playout and capture AutoCirculate...
	SetUpOutputAutoCirculate();
	StartPlayThread();
	return 0;
}

void CAJA_Playout::getAJAFormat(const sFrameConsumer_Parameter &pCnlParameter)
{
	switch (pCnlParameter.fpVideoFormat)
	{
	case FP_FORMAT_UNKNOWN:
	{
		mVideoFormat = NTV2_FORMAT_UNKNOWN;
		break;
	}
	case FP_FORMAT_1080i_5000:
	{
		mVideoFormat = NTV2_FORMAT_1080i_5000;
		break;
	}
	case FP_FORMAT_1080i_5994:
	{
		mVideoFormat = NTV2_FORMAT_1080i_5994;
		break;
	}
	case FP_FORMAT_1080i_6000:
	{
		mVideoFormat = NTV2_FORMAT_1080i_6000;
		break;
	}
	case FP_FORMAT_1080p_2400:
	{
		mVideoFormat = NTV2_FORMAT_1080p_2400;
		break;
	}
	case FP_FORMAT_1080p_2500:
	{
		mVideoFormat = NTV2_FORMAT_1080p_2500;
		break;
	}
	case FP_FORMAT_1080p_2997:
	{
		mVideoFormat = NTV2_FORMAT_1080p_2997;
		break;
	}
	case FP_FORMAT_1080p_3000:
	{
		mVideoFormat = NTV2_FORMAT_1080p_3000;
		break;
	}
	case FP_FORMAT_1080p_5000:
	{
		mVideoFormat = NTV2_FORMAT_1080p_5000;
		break;
	}
	case FP_FORMAT_1080p_6000:
	{
		mVideoFormat = NTV2_FORMAT_1080p_6000;
		break;
	}
	case FP_FORMAT_720p_5000:
	{
		mVideoFormat = NTV2_FORMAT_720p_5000;
		break;
	}
	case FP_FORMAT_720p_5994:
	{
		mVideoFormat = NTV2_FORMAT_720p_5994;
		break;
	}
	case FP_FORMAT_720p_6000:
	{
		mVideoFormat = NTV2_FORMAT_720p_6000;
		break;
	}
	case FP_FORMAT_4Kp_2400:
	{
		mb4KMode = true;
		mVideoFormat = NTV2_FORMAT_4x1920x1080p_2400;
		break;
	}
	case FP_FORMAT_4Kp_2500:
	{
		mb4KMode = true;
		mVideoFormat = NTV2_FORMAT_4x1920x1080p_2500;
		break;
	}
	case FP_FORMAT_4Kp_2997:
	{
		mb4KMode = true;
		mVideoFormat = NTV2_FORMAT_4x1920x1080p_2997;
		break;
	}
	case FP_FORMAT_4Kp_3000:
	{
		mVideoFormat = NTV2_FORMAT_4x1920x1080p_3000;
		mb4KMode = true;
		break;
	}
	case FP_FORMAT_4Kp_5000:
	{
		mb4KMode = true;
		mVideoFormat = NTV2_FORMAT_4x1920x1080p_5000;
		break;
	}
	case FP_FORMAT_4Kp_5994:
	{
		mb4KMode = true;
		mVideoFormat = NTV2_FORMAT_4x1920x1080p_5994;
		break;
	}
	case FP_FORMAT_4Kp_6000:
	{
		mb4KMode = true;
		mVideoFormat = NTV2_FORMAT_4x1920x1080p_6000;
		break;
	}
	case FP_FORMAT_MAX: break;
	default: break;
	}
}

void CAJA_Playout::clearCables(int nBoardID)
{
	LockHolder lock(m_lock_AJA);

	swprintf_s(m_szMutexName, MAX_PATH, _T("SimplyLiveRTMPServer_Playout_AJA_%d"), nBoardID);
	m_mutexAja = OpenMutex(MUTEX_ALL_ACCESS, FALSE, m_szMutexName);
	if (m_mutexAja == nullptr)
	{
		mDevice.ClearRouting();
		m_mutexAja = CreateMutex(nullptr, FALSE, m_szMutexName);
		for (int n = 0; n < NTV2_MAX_NUM_CHANNELS; ++n)
		{
			mDevice.SetVideoFormat(mVideoFormat, false, false, NTV2Channel(n));
			mDevice.AutoCirculateStop(NTV2Channel(n), true);
		}
	}
}

void CAJA_Playout::UnInitialize()
{
	mGlobalQuit = true;																										//	Set the global 'quit' flag, and wait for the thread to go inactive...

	if (mPlayThread)
		while (mPlayThread->Active())
			AJATime::Sleep(10);
	LockHolder lock(m_lock_AJA);
	if (m_mutexAja != nullptr)
	{
		CloseHandle(m_mutexAja);
		m_mutexAja = nullptr;
	
		if (mb4KMode)
		{
			switch (mOutputChannel)
			{
			case NTV2_CHANNEL1:
				mDevice.Disconnect(NTV2_XptFrameBuffer1Input);
				mDevice.Disconnect(NTV2_XptFrameBuffer2Input);
				mDevice.Disconnect(NTV2_XptFrameBuffer3Input);
				mDevice.Disconnect(NTV2_XptFrameBuffer4Input);
				break;
			case NTV2_CHANNEL5:
				mDevice.Disconnect(NTV2_XptFrameBuffer5Input);
				mDevice.Disconnect(NTV2_XptFrameBuffer6Input);
				mDevice.Disconnect(NTV2_XptFrameBuffer7Input);
				mDevice.Disconnect(NTV2_XptFrameBuffer8Input);
				break;
			}
		}
		else
		{
			switch (mOutputChannel)
			{
			case NTV2_CHANNEL1: mDevice.Disconnect(NTV2_XptFrameBuffer1Input);
				break;
			case NTV2_CHANNEL2: mDevice.Disconnect(NTV2_XptFrameBuffer2Input);
				break;
			case NTV2_CHANNEL3: mDevice.Disconnect(NTV2_XptFrameBuffer3Input);
				break;
			case NTV2_CHANNEL4: mDevice.Disconnect(NTV2_XptFrameBuffer4Input);
				break;
			case NTV2_CHANNEL5: mDevice.Disconnect(NTV2_XptFrameBuffer5Input);
				break;
			case NTV2_CHANNEL6: mDevice.Disconnect(NTV2_XptFrameBuffer6Input);
				break;
			case NTV2_CHANNEL7: mDevice.Disconnect(NTV2_XptFrameBuffer7Input);
				break;
			case NTV2_CHANNEL8: mDevice.Disconnect(NTV2_XptFrameBuffer8Input);
				break;
			}
		}
	}
}

bool CAJA_Playout::OutputFrames(DWORD nChannelID, unsigned char* pVideoBuffer, DWORD dwVSize, unsigned char* pAudioBuffer, DWORD dwASize)
{
	AJAStatus	status(AJA_STATUS_SUCCESS);
	AVDataBuffer *	frameData(mAVCircularBuffer.StartProduceNextBuffer());
	if (frameData)
	{
		memcpy(frameData->fVideoBuffer, pVideoBuffer, dwVSize);
		memcpy(frameData->fAudioBuffer, pAudioBuffer, dwASize);
		frameData->fVideoBufferSize = dwVSize;
		frameData->fAudioBufferSize = dwASize;
		mAVCircularBuffer.EndProduceNextBuffer();
		return true;
	}
	return false;
}

bool CAJA_Playout::OutputFrames(DWORD nChannelID, VideoFrame *pVideo, AudioFrame *pAudio, Timecode *tc)
{
	AJAStatus	status(AJA_STATUS_SUCCESS);
	AVDataBuffer *	frameData(mAVCircularBuffer.StartProduceNextBuffer());
	if (frameData)
	{
		memcpy(frameData->fVideoBuffer, pVideo->getRaw(), pVideo->getRawSize());
		memcpy(frameData->fAudioBuffer, pAudio->getRaw(), pAudio->getDataSize());
		//pVideo->getAncFieldInfo((BYTE *)frameData->fAncBuffer, frameData->fAncBufferSize, (BYTE *)frameData->fAncF2Buffer, frameData->fAncF2BufferSize);
		if (Config::getInstance()->isProgressive())
		{
			frameData->fAncF2Buffer = nullptr;
			frameData->fAncF2BufferSize = 0;
		}
		frameData->fVideoBufferSize = mVideoBufferSize;// pVideo->getRawSize();
		frameData->fAudioBufferSize = pAudio->getDataSize();
		mAVCircularBuffer.EndProduceNextBuffer();
		return true;
	}
	return false;
}

bool CAJA_Playout::WaitGenlockInterrupt()
{
	if (mb4KMode)
		mDevice.WaitForOutputVerticalInterrupt(NTV2_CHANNEL5);
	else
		mDevice.WaitForOutput8FieldID(Config::getInstance()->isProgressive() ? NTV2_FIELD0 : NTV2_FIELD1);
	return true;
}