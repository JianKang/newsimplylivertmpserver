/////////////////////////////////////////////////////////////////////////////
// NTV2TestPattern.h
//
// Copyright (C) 2004 AJA Video Systems, Inc.  Proprietary and Confidential information.
//
/////////////////////////////////////////////////////////////////////////////
#ifndef NTV2TESTPATTERN_H
#define NTV2TESTPATTERN_H

#include "ajaexport.h"
#include "ntv2status.h"
#include "string.h"
#include <vector>
#include "ntv2utils.h"

typedef std::vector<const char *> TestPatternList;

typedef struct
{
	int startLine;
	int endLine;
	ULWord *data;
}SegmentDescriptor;

const UWord NumTestPatternSegments = 8;

typedef struct
{
	const char *name;
	SegmentDescriptor segmentDescriptor[NTV2_NUM_STANDARDS][NumTestPatternSegments]; 
} SegmentTestPatternData;


class AJAExport CNTV2TestPattern : public CNTV2Status
{

public:
    CNTV2TestPattern();
	CNTV2TestPattern(UWord inDeviceIndex,
		      bool displayErrorMessage=false, 
			  UWord dwCardTypes = DEVICETYPE_NTV2,
			  bool autoRouteOnXena2=false,
			  const char hostname[] = 0, // Non-null: card on remote host
			  NTV2Channel channel = NTV2_CHANNEL1
			);
	virtual ~CNTV2TestPattern();

public:

	AJA_VIRTUAL void		SetChannel( NTV2Channel channel);
    AJA_VIRTUAL NTV2Channel	GetChannel();
	AJA_VIRTUAL void		SetSignalMask( UWord signalMask );
	AJA_VIRTUAL UWord		GetSignalMask();
	AJA_VIRTUAL void		SetTestPatternFrameBufferFormat(NTV2FrameBufferFormat fbFormat);
	AJA_VIRTUAL NTV2FrameBufferFormat	GetTestPatternFrameBufferFormat();
	AJA_VIRTUAL void	SetClientDownloadBuffer(ULWord* buffer) { _clientDownloadBuffer = buffer; }
	AJA_VIRTUAL ULWord*	GetClientDownloadBuffer() { return  _clientDownloadBuffer; }
	AJA_VIRTUAL void	SetDualLinkTestPatternOutputEnable(bool enable);
	AJA_VIRTUAL bool	GetDualLinkTestPatternOutputEnable();
	AJA_VIRTUAL void	SetTestPatternDMAEnable(bool enable) { _bDMAtoBoard = enable; }
	AJA_VIRTUAL bool	GetTestPatternDMAEnable()  { return _bDMAtoBoard; }
	AJA_VIRTUAL void	EnableFlipFlipPage(bool enable) { _flipFlopPage = enable;}
	AJA_VIRTUAL bool	GetEnableFlipFlipPage() { return _flipFlopPage;}
	AJA_VIRTUAL void	SetAutoRouteOnXena2(bool autoRoute) {_autoRouteOnXena2 = autoRoute;}
	AJA_VIRTUAL bool	GetAutoRouteOnXena2() { return _autoRouteOnXena2;}
	AJA_VIRTUAL bool	DownloadTestPattern(UWord testPatternNumber );
	AJA_VIRTUAL void	DownloadTestPattern(char* testPatternName );
	AJA_VIRTUAL void	DownloadBlackTestPattern();
	AJA_VIRTUAL void	DownloadBorderTestPattern();
	AJA_VIRTUAL void	DownloadSlantRampTestPattern();
	AJA_VIRTUAL void	DownloadYCbCrSlantRampTestPattern();
	AJA_VIRTUAL void	Download48BitRGBSlantRampTestPattern();
	AJA_VIRTUAL void	DownloadVerticalSweepTestPattern();
	AJA_VIRTUAL void	DownloadZonePlateTestPattern();
	
	AJA_VIRTUAL void	RenderTestPatternToBuffer(UWord testPatternNumber, ULWord *buffer);
	AJA_VIRTUAL bool	RenderTestPatternBuffer(NTV2Channel channel, UByte *buffer, NTV2VideoFormat videoFormat, NTV2FrameBufferFormat fbFormat, ULWord width, ULWord height, ULWord rowBytes);
	AJA_VIRTUAL void	DownloadTestPatternBuffer(ULWord *buffer, ULWord size=0);
	AJA_VIRTUAL ULWord	GetPatternBufferSize(ULWord *width=0, ULWord *height=0, ULWord *rowBytes=0, ULWord *firstLine=0);
	
	AJA_VIRTUAL int		MakeSineWaveVideo(double radians, bool bChroma);
	AJA_VIRTUAL void	ConvertLinePixelFormat(UWord *unPackedBuffer, ULWord *packedBuffer, int numPixels);

#ifdef AJAMac
	AJA_VIRTUAL void	DownloadRGBPicture(char *pSrc, ULWord srcWidthPixels, ULWord srcHeightPixels, ULWord srcRowBytes);
#endif

	//LocalLoadTestPattern allows the generator to build the pattern independent of global controls to generate independent formats when inconverter is on.
	AJA_VIRTUAL void	LocalLoadBarsTestPattern( UWord testPatternNumber, NTV2Standard standard);

	AJA_VIRTUAL TestPatternList &	GetTestPatternList();

protected:   // Methods
	AJA_VIRTUAL void	Init();
	AJA_VIRTUAL void	DownloadSegmentedTestPattern(SegmentTestPatternData* pTestPatternSegmentData );
	AJA_VIRTUAL void	AdjustFor2048x1080(ULWord& numPixels,ULWord& linePitch);
	AJA_VIRTUAL void	AdjustFor4096x2160(ULWord& numPixels,ULWord& linePitch, ULWord& numLines);
	AJA_VIRTUAL void	AdjustFor3840x2160(ULWord& numPixels,ULWord& linePitch, ULWord& numLines);

protected:   // Data
	NTV2Channel				_channel;
	TestPatternList			_testPatternList;
	UWord					_signalMask;
	NTV2FrameBufferFormat	_fbFormat;
	bool					_dualLinkOutputEnable;
	bool					_bDMAtoBoard;
	bool					_autoRouteOnXena2;
	bool					_flipFlopPage;
	ULWord *				_clientDownloadBuffer;

};	//	CNTV2TestPattern

#endif	//	NTV2TESTPATTERN_H
