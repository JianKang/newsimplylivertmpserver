// basemachinecontrol.h
//
// Copyright (C) 2004 AJA Video Systems, Inc.  Proprietary and Confidential information.
//
// Base class for machine control
// This was adapted from some DDR control code
// and thus has some DDR-centric stuff.

#ifndef _BASEMACHINECONTROL_H
#define _BASEMACHINECONTROL_H

#include "ajaexport.h"
#include "ajatypes.h"
#include "ntv2enums.h"

/////////////////////////////////////////////////////////////////////////////
// CBaseMachineControl
//  This is the base class for Disk Recorder Control
//  Not meant to be used. 

class AJAExport CBaseMachineControl
{
public:
typedef enum {
 CONTROLTYPE_DDR,		// DDREthernetControl
 CONTROLTYPE_XVID,      // Uses NTV2 File to control Xena Board
 CONTROLTYPE_XENA,      // Uses Xena Serial Port
 CONTROLTYPE_SERIAL     // Uses host serial Port
} ControlType;

typedef enum {
 CONTROL_UNIMPLEMENTED=0xFFFFFFFF
} ControUnimplemented;

public: // Construct/Destruct
	CBaseMachineControl() {;}
	virtual ~CBaseMachineControl() {;}

public: // Methods
	virtual bool	Open()=0;
	virtual void    Close()=0;
// These return true,false or CONTROL_UNIMPLEMENTED
	virtual ULWord  Play(){ return CBaseMachineControl::CONTROL_UNIMPLEMENTED; }
	virtual ULWord  ReversePlay(){ return CBaseMachineControl::CONTROL_UNIMPLEMENTED; };
	virtual ULWord  Stop(){ return CBaseMachineControl::CONTROL_UNIMPLEMENTED; }
	virtual ULWord  FastForward(){ return CBaseMachineControl::CONTROL_UNIMPLEMENTED; }
	virtual ULWord  Rewind(){ return CBaseMachineControl::CONTROL_UNIMPLEMENTED; }
	virtual ULWord  AdvanceFrame(){ return CBaseMachineControl::CONTROL_UNIMPLEMENTED; }
	virtual ULWord  BackFrame(){ return CBaseMachineControl::CONTROL_UNIMPLEMENTED; }
	virtual ULWord  GetTimecodeString(SByte *timecodeString)													{ (void) timecodeString;	return CBaseMachineControl::CONTROL_UNIMPLEMENTED; }
	virtual ULWord  GotoFrameByString(SByte *frameString)														{ (void) frameString;		return CBaseMachineControl::CONTROL_UNIMPLEMENTED; }
	virtual ULWord  GotoFrameByInteger(ULWord frameNumber)														{ (void) frameNumber;		return CBaseMachineControl::CONTROL_UNIMPLEMENTED; }
	virtual ULWord  GotoFrameByHoursMinutesSeconds (UByte hours, UByte minutes, UByte seconds, UByte frames)	{ (void) hours;  (void) minutes;  (void) seconds;  (void) frames;	return CBaseMachineControl::CONTROL_UNIMPLEMENTED; }
	virtual ULWord  RecordAtFrame(ULWord frameNumber)															{ (void) frameNumber;		return CBaseMachineControl::CONTROL_UNIMPLEMENTED; }
	virtual ULWord  Loop(ULWord startFrameNumber,ULWord endFrameNumber)											{ (void) startFrameNumber;  (void) endFrameNumber;	return CBaseMachineControl::CONTROL_UNIMPLEMENTED; }

	ControlType GetControlType() {return _controlType;}

protected:
	ControlType _controlType;	
};

/////////////////////////////////////////////////////////////////////////////

#endif
