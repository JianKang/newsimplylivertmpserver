/////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2004 AJA Video Systems, Inc.  Proprietary and Confidential information.
//
/////////////////////////////////////////////////////////////////////////////
#ifndef NTV2UTILS_H
#define NTV2UTILS_H

#include "ajaexport.h"
#include "ajatypes.h"
#include "ntv2enums.h"
#include "videodefines.h"
#include "ntv2publicinterface.h"
#include "ntv2signalrouter.h"
#include <string>
#include <iostream>
#if defined (AJALinux)
	#include <stdint.h>
#endif


//////////////////////////////////////////////////////
//	BEGIN SECTION MOVED FROM 'videoutilities.h'
//////////////////////////////////////////////////////
#define DEFAULT_PATT_GAIN  0.9		// some patterns pay attention to this...
#define HD_NUMCOMPONENTPIXELS_2K  2048
#define HD_NUMCOMPONENTPIXELS_1080_2K  2048
#define HD_NUMCOMPONENTPIXELS_1080  1920
#define CCIR601_10BIT_BLACK  64
#define CCIR601_10BIT_WHITE  940
#define CCIR601_10BIT_CHROMAOFFSET  512

#define CCIR601_8BIT_BLACK  16
#define CCIR601_8BIT_WHITE  235
#define CCIR601_8BIT_CHROMAOFFSET  128

// line pitch is in bytes.
#define FRAME_0_BASE (0x0)
#define FRAME_1080_10BIT_LINEPITCH (1280*4)
#define FRAME_1080_8BIT_LINEPITCH (1920*2)
#define FRAME_QUADHD_10BIT_SIZE (FRAME_1080_10BIT_LINEPITCH*2160)
#define FRAME_QUADHD_8BIT_SIZE (FRAME_1080_8BIT_LINEPITCH*2160)
#define FRAME_BASE(frameNumber,frameSize) (frameNumber*frameSize) 

AJAExport uint32_t CalcRowBytesForFormat (NTV2FrameBufferFormat format, uint32_t width);
AJAExport void UnPack10BitYCbCrBuffer (uint32_t* packedBuffer, uint16_t* ycbcrBuffer, uint32_t numPixels);
AJAExport void PackTo10BitYCbCrBuffer (uint16_t *ycbcrBuffer, uint32_t *packedBuffer,uint32_t numPixels);
AJAExport void MakeUnPacked10BitYCbCrBuffer (uint16_t* buffer, uint16_t Y , uint16_t Cb , uint16_t Cr,uint32_t numPixels);
AJAExport void ConvertLineTo8BitYCbCr (uint16_t * ycbcr10BitBuffer, uint8_t * ycbcr8BitBuffer,	uint32_t numPixels);
AJAExport void ConvertUnpacked10BitYCbCrToPixelFormat (uint16_t *unPackedBuffer, uint32_t *packedBuffer, uint32_t numPixels, NTV2FrameBufferFormat pixelFormat,
														bool bUseSmpteRange=false, bool bAlphaFromLuma=false);
AJAExport void MaskUnPacked10BitYCbCrBuffer (uint16_t* ycbcrUnPackedBuffer, uint16_t signalMask , uint32_t numPixels);
AJAExport void StackQuadrants (uint8_t* pSrc, uint32_t srcWidth, uint32_t srcHeight, uint32_t srcRowBytes, uint8_t* pDst);
AJAExport void CopyFromQuadrant (uint8_t* srcBuffer, uint32_t srcHeight, uint32_t srcRowBytes, uint32_t srcQuadrant, uint8_t* dstBuffer,  uint32_t quad13Offset=0);
AJAExport void CopyToQuadrant (uint8_t* srcBuffer, uint32_t srcHeight, uint32_t srcRowBytes, uint32_t dstQuadrant, uint8_t* dstBuffer, uint32_t quad13Offset=0);
//////////////////////////////////////////////////////
//	END SECTION MOVED FROM 'videoutilities.h'
//////////////////////////////////////////////////////


#if !defined (NTV2_DEPRECATE)
	/**
		@deprecated		Replaced by UnpackLine_10BitYUVto16BitYUV.
	**/
	AJAExport void UnPackLineData (const ULWord * pIn10BitYUVLine, UWord * pOut16BitYUVLine, const ULWord inNumPixels);

	/**
		@deprecated		Replaced by PackLine_16BitYUVto10BitYUV.
	**/
	AJAExport void PackLineData (const UWord * pIn16BitYUVLine, ULWord * pOut10BitYUVLine, const ULWord inNumPixels);
#endif	//	NTV2_DEPRECATE

/**
	@brief	Unpacks a line of 10-bit-per-component YCbCr video into 16-bit-per-component YCbCr data.
	@param[in]	pIn10BitYUVLine		A valid, non-NULL pointer to the input buffer that contains the packed 10-bit-per-component YUV data
									to be converted into 16-bit-per-component YUV.
	@param[out]	pOut16BitYUVLine	A valid, non-NULL pointer to the output buffer to receive the unpacked 16-bit-per-component YUV data.
	@param[in]	inNumPixels			Specifies the width of the line to be converted, in pixels.
**/
AJAExport void UnpackLine_10BitYUVto16BitYUV (const ULWord * pIn10BitYUVLine, UWord * pOut16BitYUVLine, const ULWord inNumPixels);

/**
	@brief	Packs a line of 16-bit-per-component YCbCr video into 10-bit-per-component YCbCr data.
	@param[in]	pIn16BitYUVLine		A valid, non-NULL pointer to the input buffer that contains the 16-bit-per-component YUV data to be
									converted into 10-bit-per-component YUV.
	@param[out]	pOut10BitYUVLine	A valid, non-NULL pointer to the output buffer to receive the packed 10-bit-per-component YUV data.
	@param[in]	inNumPixels			Specifies the width of the line to be converted, in pixels.
**/
AJAExport void PackLine_16BitYUVto10BitYUV (const UWord * pIn16BitYUVLine, ULWord * pOut10BitYUVLine, const ULWord inNumPixels);

AJAExport void RePackLineDataForYCbCrDPX(ULWord *packedycbcrLine, ULWord numULWords);
AJAExport void UnPack10BitDPXtoRGBAlpha10BitPixel(RGBAlpha10BitPixel* rgba10BitBuffer,ULWord* DPXLinebuffer ,ULWord numPixels, bool bigEndian);
AJAExport void UnPack10BitDPXtoForRP215withEndianSwap(UWord* rawrp215Buffer,ULWord* DPXLinebuffer ,ULWord numPixels);
AJAExport void UnPack10BitDPXtoForRP215(UWord* rawrp215Buffer,ULWord* DPXLinebuffer ,ULWord numPixels);
AJAExport void MaskYCbCrLine(UWord* ycbcrLine, UWord signalMask , ULWord numPixels);
AJAExport void Make10BitBlackLine(UWord* lineData,UWord numPixels=1920);
AJAExport void Make10BitWhiteLine(UWord* lineData,UWord numPixels=1920);
AJAExport void Fill10BitYCbCrVideoFrame(PULWord _baseVideoAddress,
										NTV2Standard standard,
										NTV2FrameBufferFormat frameBufferFormat,
										YCbCr10BitPixel color,
										bool vancEnabled=false,
										bool twoKby1080=false,
							 bool wideVANC=false);
AJAExport void Make8BitBlackLine(UByte* lineData,UWord numPixels=1920,NTV2FrameBufferFormat=NTV2_FBF_8BIT_YCBCR);
AJAExport void Make8BitWhiteLine(UByte* lineData,UWord numPixels=1920,NTV2FrameBufferFormat=NTV2_FBF_8BIT_YCBCR);
AJAExport void Make10BitLine(UWord* lineData, UWord Y , UWord Cb , UWord Cr,UWord numPixels=1920);
AJAExport void Make8BitLine(UByte* lineData, UByte Y , UByte Cb , UByte Cr,ULWord numPixels=1920,NTV2FrameBufferFormat=NTV2_FBF_8BIT_YCBCR);
AJAExport void Fill8BitYCbCrVideoFrame(PULWord _baseVideoAddress,
									   NTV2Standard standard,
									   NTV2FrameBufferFormat frameBufferFormat,
									   YCbCrPixel color,
									   bool vancEnabled=false,
									   bool twoKby1080=false,
									   bool wideVANC=false);
AJAExport void Fill4k8BitYCbCrVideoFrame(PULWord _baseVideoAddress,
									   NTV2FrameBufferFormat frameBufferFormat,
									   YCbCrPixel color,
									   bool vancEnabled=false,
									   bool b4k=false,
									   bool wideVANC=false);
AJAExport void CopyRGBAImageToFrame(ULWord* pSrcBuffer, ULWord srcHeight, ULWord srcWidth, 
									ULWord* pDstBuffer, ULWord dstHeight, ULWord dstWidth);
							 
AJAExport NTV2Standard GetNTV2StandardFromScanGeometry(UByte geometry, bool progressiveTransport);
AJAExport NTV2Standard GetNTV2StandardFromVideoFormat(NTV2VideoFormat videoFormat);
AJAExport NTV2V2Standard GetHdmiV2StandardFromVideoFormat(NTV2VideoFormat videoFormat);
AJAExport ULWord GetVideoActiveSize(NTV2VideoFormat videoFormat,NTV2FrameBufferFormat format,bool VANCenabled=false,bool wideVANC = false);
AJAExport ULWord GetVideoWriteSize(NTV2VideoFormat videoFormat, NTV2FrameBufferFormat format,bool VANCenabled=false,bool wideVANC = false);
AJAExport NTV2VideoFormat GetQuarterSizedVideoFormat(NTV2VideoFormat videoFormat);
AJAExport NTV2VideoFormat GetQuadSizedVideoFormat(NTV2VideoFormat videoFormat);
AJAExport NTV2FrameGeometry GetQuarterSizedGeometry(NTV2FrameGeometry geometry);
AJAExport NTV2FrameGeometry Get4xSizedGeometry(NTV2FrameGeometry geometry);

AJAExport double GetFramesPerSecond(NTV2FrameRate frameRate);
AJAExport double GetFrameTime(NTV2FrameRate frameRate);

/**
	@brief	Returns the number of audio samples for a given video frame rate, audio sample rate, and frame number.
			This is useful since AJA devices use fixed audio sample rates (typically 48KHz), and some video frame
			rates will necessarily result in some frames having more audio samples than others.
	@param[in]	frameRate		Specifies the video frame rate.
	@param[in]	audioRate		Specifies the audio sample rate. Must be one of NTV2_AUDIO_48K or NTV2_AUDIO_96K.
	@param[in]	cadenceFrame	Optionally specifies a frame number for maintaining proper cadence in a frame sequence,
								for those video frame rates that don't accommodate an even number of audio samples.
								Defaults to zero.
	@param[in]	smpte372Enabled	Specifies that 1080p60, 1080p5994 or 1080p50 is being used. In this mode, the device's
								framerate might be NTV2_FRAMERATE_3000, but since 2 links are coming out, the video rate
								is effectively NTV2_FRAMERATE_6000. Defaults to false.
	@return	The number of audio samples.
**/
AJAExport ULWord				GetAudioSamplesPerFrame (NTV2FrameRate frameRate, NTV2AudioRate audioRate, ULWord cadenceFrame=0,bool smpte372Enabled=false);
AJAExport LWord64				GetTotalAudioSamplesFromFrameNbrZeroUpToFrameNbr (NTV2FrameRate frameRate, NTV2AudioRate audioRate, ULWord frameNbrNonInclusive);
AJAExport ULWord				GetVaricamRepeatCount (NTV2FrameRate sequenceRate, NTV2FrameRate playRate, ULWord cadenceFrame=0);
AJAExport ULWord				GetScaleFromFrameRate (NTV2FrameRate playFrameRate);
AJAExport NTV2FrameRate			GetFrameRateFromScale (long scale, long duration, NTV2FrameRate playFrameRate);
AJAExport NTV2FrameRate			GetNTV2FrameRateFromVideoFormat (NTV2VideoFormat videoFormat);
AJAExport ULWord				GetNTV2FrameGeometryWidth (NTV2FrameGeometry geometry);
AJAExport ULWord				GetNTV2FrameGeometryHeight (NTV2FrameGeometry geometry);
AJAExport ULWord				GetDisplayWidth (NTV2VideoFormat videoFormat);
AJAExport ULWord				GetDisplayHeight (NTV2VideoFormat videoFormat);
AJAExport NTV2ConversionMode	GetConversionMode (NTV2VideoFormat inFormat, NTV2VideoFormat outFormat);
AJAExport NTV2VideoFormat		GetInputForConversionMode (NTV2ConversionMode conversionMode);
AJAExport NTV2VideoFormat		GetOutputForConversionMode (NTV2ConversionMode conversionMode);

AJAExport NTV2Channel			GetNTV2ChannelForIndex (const ULWord inIndex0);
AJAExport ULWord				GetIndexForNTV2Channel (const NTV2Channel inChannel);

AJAExport NTV2Crosspoint		GetNTV2CrosspointChannelForIndex (const ULWord inIndex0);
AJAExport ULWord				GetIndexForNTV2CrosspointChannel (const NTV2Crosspoint inChannel);
AJAExport NTV2Crosspoint		GetNTV2CrosspointInputForIndex (const ULWord inIndex0);
AJAExport ULWord				GetIndexForNTV2CrosspointInput (const NTV2Crosspoint inChannel);
AJAExport NTV2Crosspoint		GetNTV2CrosspointForIndex (const ULWord inIndex0);
AJAExport ULWord				GetIndexForNTV2Crosspoint (const NTV2Crosspoint inChannel);

AJAExport bool					IsNTV2CrosspointInput (const NTV2Crosspoint inChannel);
AJAExport bool					IsNTV2CrosspointOutput (const NTV2Crosspoint inChannel);
AJAExport std::string			NTV2CrosspointToString (const NTV2Crosspoint inChannel);

AJAExport NTV2VideoFormat		GetTransportCompatibleFormat (const NTV2VideoFormat inFormat, const NTV2VideoFormat inTargetFormat);
AJAExport bool					IsTransportCompatibleFormat (const NTV2VideoFormat inFormat1, const NTV2VideoFormat inFormat2);

AJAExport NTV2InputSource		GetNTV2InputSourceForIndex (const ULWord inIndex0);				//	0-based index
AJAExport ULWord				GetIndexForNTV2InputSource (const NTV2InputSource inValue);		//	0-based index


/**
	@brief		Converts the given NTV2Channel value into the equivalent input NTV2Crosspoint.
	@param[in]	inChannel		Specifies the NTV2Channel to be converted.
	@return		The equivalent input NTV2Crosspoint value.
**/
AJAExport NTV2Crosspoint		NTV2ChannelToInputCrosspoint (const NTV2Channel inChannel);

/**
	@brief		Converts the given NTV2Channel value into the equivalent output NTV2Crosspoint.
	@param[in]	inChannel		Specifies the NTV2Channel to be converted.
	@return		The equivalent output NTV2Crosspoint value.
**/
AJAExport NTV2Crosspoint		NTV2ChannelToOutputCrosspoint (const NTV2Channel inChannel);

/**
	@brief		Converts the given NTV2Channel value into the equivalent input INTERRUPT_ENUMS value.
	@param[in]	inChannel		Specifies the NTV2Channel to be converted.
	@return		The equivalent input INTERRUPT_ENUMS value.
**/
AJAExport INTERRUPT_ENUMS		NTV2ChannelToInputInterrupt (const NTV2Channel inChannel);

/**
	@brief		Converts the given NTV2Channel value into the equivalent output INTERRUPT_ENUMS value.
	@param[in]	inChannel		Specifies the NTV2Channel to be converted.
	@return		The equivalent output INTERRUPT_ENUMS value.
**/
AJAExport INTERRUPT_ENUMS		NTV2ChannelToOutputInterrupt (const NTV2Channel inChannel);

/**
	@brief		Converts the given NTV2Channel value into the equivalent NTV2TCSource value.
	@param[in]	inChannel		Specifies the NTV2Channel to be converted.
	@param[in]	inEmbeddedLTC	Specify true for embedded LTC. Defaults to false.
	@return		The equivalent NTV2TCSource value.
**/
AJAExport NTV2TCSource			NTV2ChannelToTimecodeSource (const NTV2Channel inChannel, const bool inEmbeddedLTC = false);


#define	NTV2ChannelToCaptureCrosspoint	NTV2ChannelToInputCrosspoint
#define	NTV2ChannelToIngestCrosspoint	NTV2ChannelToInputCrosspoint
#define	NTV2ChannelToInputChannelSpec	NTV2ChannelToInputCrosspoint
#define	NTV2ChannelToPlayoutCrosspoint	NTV2ChannelToOutputCrosspoint
#define	NTV2ChannelToOutputChannelSpec	NTV2ChannelToOutputCrosspoint


/**
	@brief		Converts the given NTV2Framesize value into an exact byte count.
	@param[in]	inFrameSize		Specifies the NTV2Framesize to be converted.
	@return		The equivalent number of bytes.
**/
AJAExport ULWord	NTV2FramesizeToByteCount (const NTV2Framesize inFrameSize);

/**
	@brief		Converts the given NTV2Channel value into its equivalent NTV2EmbeddedAudioInput.
	@param[in]	inChannel		Specifies the NTV2Channel to be converted.
	@return		The equivalent NTV2EmbeddedAudioInput value.
**/
AJAExport NTV2EmbeddedAudioInput NTV2ChannelToEmbeddedAudioInput (const NTV2Channel inChannel);

/**
	@brief		Converts a given NTV2InputSource to its equivalent NTV2EmbeddedAudioInput value.
	@param[in]	inInputSource	Specifies the NTV2InputSource to be converted.
	@return		The equivalent NTV2EmbeddedAudioInput value.
**/
AJAExport NTV2EmbeddedAudioInput NTV2InputSourceToEmbeddedAudioInput (const NTV2InputSource inInputSource);

/**
 @brief		Converts a given NTV2InputSource to its equivalent NTV2Crosspoint value.
 @param[in]	inInputSource	Specifies the NTV2InputSource to be converted.
 @return		The equivalent NTV2Crosspoint value.
 **/
AJAExport NTV2Crosspoint NTV2InputSourceToChannelSpec (const NTV2InputSource inInputSource);

/**
	@brief		Converts a given NTV2InputSource to its equivalent NTV2Channel value.
	@param[in]	inInputSource	Specifies the NTV2InputSource to be converted.
	@return		The equivalent NTV2Channel value.
**/
AJAExport NTV2Channel NTV2InputSourceToChannel (const NTV2InputSource inInputSource);

/**
	@brief		Converts a given NTV2InputSource to its equivalent NTV2ReferenceSource value.
	@param[in]	inInputSource	Specifies the NTV2InputSource to be converted.
	@return		The equivalent NTV2ReferenceSource value.
**/
AJAExport NTV2ReferenceSource NTV2InputSourceToReferenceSource (const NTV2InputSource inInputSource);

/**
	@brief		Converts a given NTV2InputSource to its equivalent NTV2AudioSystem value.
	@param[in]	inInputSource	Specifies the NTV2InputSource to be converted.
	@return		The equivalent NTV2AudioSystem value.
**/
AJAExport NTV2AudioSystem NTV2InputSourceToAudioSystem (const NTV2InputSource inInputSource);

/**
	@brief		Converts the given NTV2Channel value into its equivalent NTV2AudioSystem.
	@param[in]	inChannel		Specifies the NTV2Channel to be converted.
	@return		The equivalent NTV2AudioSystem value.
**/
AJAExport NTV2AudioSystem NTV2ChannelToAudioSystem (const NTV2Channel inChannel);

/**
	@brief		Converts the given NTV2Channel value into its ordinary equivalent NTV2InputSource.
	@param[in]	inChannel		Specifies the NTV2Channel to be converted.
	@return		The equivalent NTV2InputSource value.
**/
AJAExport NTV2InputSource NTV2ChannelToInputSource (const NTV2Channel inChannel);

/**
	@brief		Converts the given NTV2Channel value into its ordinary equivalent NTV2OutputDestination.
	@param[in]	inChannel		Specifies the NTV2Channel to be converted.
	@return		The equivalent NTV2OutputDestination value.
**/
AJAExport NTV2OutputDestination NTV2ChannelToOutputDestination (const NTV2Channel inChannel);


/**
	@brief	Compares two frame rates and returns true if they are "compatible" (with respect to a multiformat-capable device).
	@param[in]	inFrameRate1	Specifies one of the frame rates to be compared.
	@param[in]	inFrameRate2	Specifies another frame rate to be compared.
**/
AJAExport bool IsMultiFormatCompatible (const NTV2FrameRate inFrameRate1, const NTV2FrameRate inFrameRate2);

/**
	@brief	Compares two video formats and returns true if they are "compatible" (with respect to a multiformat-capable device).
	@param[in]	inFormat1	Specifies one of the video formats to be compared.
	@param[in]	inFormat2	Specifies another video format to be compared.
**/
AJAExport bool IsMultiFormatCompatible (const NTV2VideoFormat inFormat1, const NTV2VideoFormat inFormat2);

AJAExport bool IsPSF(NTV2VideoFormat format);
AJAExport bool IsProgressivePicture(NTV2VideoFormat format);
AJAExport bool IsProgressiveTransport(NTV2VideoFormat format);
AJAExport bool IsProgressiveTransport(NTV2Standard format);
AJAExport bool IsRGBFormat(NTV2FrameBufferFormat format);
AJAExport bool IsYCbCrFormat(NTV2FrameBufferFormat format);
AJAExport bool IsAlphaChannelFormat(NTV2FrameBufferFormat format);
AJAExport bool Is2KFormat(NTV2VideoFormat format);
AJAExport bool Is4KFormat(NTV2VideoFormat format);
AJAExport bool IsRaw(NTV2FrameBufferFormat format);
AJAExport bool Is8BitFrameBufferFormat(NTV2FrameBufferFormat fbFormat);
AJAExport bool IsVideoFormatA(NTV2VideoFormat format);
AJAExport bool IsVideoFormatB(NTV2VideoFormat format);


AJAExport int  RecordCopyAudio(PULWord pAja, PULWord pSR, int iStartSample, int iNumBytes, int iChan0,
							   int iNumChans, bool bKeepAudio24Bits);

/**
	@brief	Fills the given buffer with 32-bit (ULWord) audio tone samples.
	@param		audioBuffer		If non-NULL, must be a valid pointer to the buffer to be filled with audio samples,
								and must be at least numSamples*4*numChannels bytes in size.
								Callers may specify NULL to have the function return the required size of the buffer.
	@param		currentSample	On entry, specifies the sample where waveform generation is to resume.
								If audioBuffer is non-NULL, on exit, receives the sample number where waveform generation left off.
								Callers should specify zero for the first invocation of this function.
	@param[in]	numSamples		Specifies the number of samples to generate.
	@param[in]	inSampleRate	Specifies the sample rate, in samples per second.
	@param[in]	amplitude		Specifies the amplitude of the generated tone.
	@param[in]	frequency		Specifies the frequency of the generated tone, in cycles per second.
	@param[in]	numBits			Specifies the number of bits per sample. Should be between 8 and 32 (inclusive).
	@param[in]	endianConvert	If true, byte-swaps each 32-bit sample before copying it into the destination buffer.
	@param[in]	numChannels		Specifies the number of audio channels to produce.
	@return		The total number of bytes written into the destination buffer (or if audioBuffer is NULL, the minimum
				required size of the destination buffer, in bytes).
**/
AJAExport 
ULWord	AddAudioTone (ULWord*             audioBuffer,
					  ULWord&             currentSample,
					  ULWord              numSamples,
					  double              inSampleRate,
					  double              amplitude,
					  double              frequency,
					  ULWord              numBits,
					  bool                endianConvert,
					  ULWord   		      numChannels);

// Fill all audio channels up to numChannels with the frequencies specified in frequency[]
AJAExport 
ULWord	AddAudioTone (ULWord*             audioBuffer,
					  ULWord&             currentSample,
					  ULWord              numSamples,
					  double              inSampleRate,
					  double              amplitude,
					  double*             frequency,		// C++ doesn't allow arrays of references.
					  ULWord              numBits,
					  bool                endianConvert,
					  ULWord   		      numChannels);

/**
	@brief	Fills the given buffer with 16-bit (UWord) audio tone samples.
	@param		audioBuffer		If non-NULL, must be a valid pointer to the buffer to be filled with audio samples,
								and must be at least numSamples*2*numChannels bytes in size.
								Callers may specify NULL to have the function return the required size of the buffer.
	@param		currentSample	On entry, specifies the sample where waveform generation is to resume.
								If audioBuffer is non-NULL, on exit, receives the sample number where waveform generation left off.
								Callers should specify zero for the first invocation of this function.
	@param[in]	numSamples		Specifies the number of samples to generate.
	@param[in]	inSampleRate	Specifies the sample rate, in samples per second.
	@param[in]	amplitude		Specifies the amplitude of the generated tone.
	@param[in]	frequency		Specifies the frequency of the generated tone, in cycles per second.
	@param[in]	numBits			Specifies the number of bits per sample. Should be between 8 and 16 (inclusive).
	@param[in]	endianConvert	If true, byte-swaps each 16-bit sample before copying it into the destination buffer.
	@param[in]	numChannels		Specifies the number of audio channels to produce.
	@return		The total number of bytes written into the destination buffer (or if audioBuffer is NULL, the minimum
				required size of the destination buffer, in bytes).
**/
AJAExport
ULWord	AddAudioTone (UWord*             audioBuffer,
					  ULWord&             currentSample,
					  ULWord              numSamples,
					  double              inSampleRate,
					  double              amplitude,
					  double              frequency,
					  ULWord              numBits,
					  bool                endianConvert,
					  ULWord   		      numChannels);

AJAExport 
ULWord	AddAudioTestPattern (ULWord*             audioBuffer,
							 ULWord&            currentSample,
							 ULWord             numSamples,
							 ULWord             modulus,
							 bool               endianConvert,
							 ULWord   		    numChannels);

#if !defined (NTV2_DEPRECATE)
	AJAExport bool BuildRoutingTableForOutput (CNTV2SignalRouter &		outRouter,
												NTV2Channel				channel,
												NTV2FrameBufferFormat	fbf,
												bool					convert		= false,	// ignored
												bool					lut			= false,
												bool					dualLink	= false,
												bool					keyOut		= false);

	AJAExport bool BuildRoutingTableForInput (CNTV2SignalRouter &		outRouter,
											   NTV2Channel				channel,
											   NTV2FrameBufferFormat	fbf,
											   bool						withKey		= false,
											   bool						lut			= false,
											   bool						dualLink	= false,
											   bool						EtoE		= true);

	AJAExport bool BuildRoutingTableForInput (CNTV2SignalRouter &		outRouter,
											   NTV2Channel				channel,
											   NTV2FrameBufferFormat	fbf,
											   bool						convert,  // Turn on the conversion module
											   bool						withKey,  // only supported for NTV2_CHANNEL1 for rgb formats with alpha
											   bool						lut,	  // not supported
											   bool						dualLink, // assume coming in RGB(only checked for NTV2_CHANNEL1
											   bool						EtoE);

	AJAExport bool BuildRoutingTableForInput (CNTV2SignalRouter &		outRouter,
											   NTV2InputSource			inputSource,
											   NTV2Channel				channel,
											   NTV2FrameBufferFormat	fbf,
											   bool						convert,  // Turn on the conversion module
											   bool						withKey,  // only supported for NTV2_CHANNEL1 for rgb formats with alpha
											   bool						lut,	  // not supported
											   bool						dualLink, // assume coming in RGB(only checked for NTV2_CHANNEL1
											   bool						EtoE);

	AJAExport ULWord ConvertFusionAnalogToTempCentigrade (ULWord adc10BitValue);

	AJAExport ULWord ConvertFusionAnalogToMilliVolts (ULWord adc10BitValue, ULWord millivoltsResolution);
#endif	//	!defined (NTV2_DEPRECATE)


/**
	@brief	This provides additional information about a video frame for a given video standard or format and pixel format,
			including the total number of lines, number of pixels per line, line pitch, and which line contains the start
			of active video.
**/
typedef struct
{
	ULWord	numLines;			//	Height -- total number of lines
	ULWord	numPixels;			//	Width -- total number of pixels per line
	ULWord	linePitch;			//	Number of 32-bit words per line
	ULWord	firstActiveLine;	//	First active line of video (0 if vanc not enabled)
} NTV2FormatDescriptor;

AJAExport std::ostream & operator << (std::ostream & inOutStream, const NTV2FormatDescriptor & inFormatDesc);


typedef struct
{
	ULWord smpteFirstActiveLine;		// smpte line number of first (top-most) active line of video
	ULWord smpteSecondActiveLine;		// smpte line number of second active line of video
	bool firstFieldTop;					// is the first field on the wire the top most field in raster (field dominance)
} NTV2SmpteLineNumber;


/**
	@brief		Returns a format descriptor that describes a video frame having the given video standard and pixel format.
	@param[in]	inVideoStandard			Specifies the video standard being used.
	@param[in]	inFrameBufferFormat		Specifies the pixel format of the frame buffer.
	@param[in]	inVANCenabled			Specifies if VANC is enabled or not. Defaults to false.
	@param[in]	in2Kby1080				Specifies if a 2K format is in use or not. Defaults to false.
	@param[in]	inWideVANC				Specifies if "wide VANC" is enabled or not. Defaults to false.
	@return		A format descriptor that describes a video frame having the given video standard and pixel format.
**/
AJAExport NTV2FormatDescriptor GetFormatDescriptor (const NTV2Standard			inVideoStandard,
													const NTV2FrameBufferFormat	inFrameBufferFormat,
													const bool					inVANCenabled	= false,
													const bool					in2Kby1080		= false,
													const bool					inWideVANC		= false);


/**
	@brief		Returns a format descriptor that describes a video frame having the given video format and pixel format.
	@param[in]	inVideoFormat			Specifies the video format being used.
	@param[in]	inFrameBufferFormat		Specifies the pixel format of the frame buffer.
	@param[in]	inVANCenabled			Specifies if VANC is enabled or not. Defaults to false.
	@param[in]	inWideVANC				Specifies if "wide VANC" is enabled or not. Defaults to false.
	@return		A format descriptor that describes a video frame having the given video standard and pixel format.
**/
AJAExport NTV2FormatDescriptor GetFormatDescriptor (const NTV2VideoFormat			inVideoFormat,
													 const NTV2FrameBufferFormat	inFrameBufferFormat,
													 const bool						inVANCenabled	= false,
													 const bool						inWideVANC		= false);

AJAExport NTV2SmpteLineNumber GetSmpteLineNumber(NTV2Standard standard);

#if !defined (NTV2_DEPRECATE)
	extern AJAExport NTV2FormatDescriptor formatDescriptorTable [NTV2_NUM_STANDARDS] [NTV2_FBF_NUMFRAMEBUFFERFORMATS];
#endif	//	!defined (NTV2_DEPRECATE)


typedef std::set <NTV2DeviceID>			NTV2DeviceIDSet;			///<	@brief	A set of NTV2DeviceIDs.
typedef NTV2DeviceIDSet::iterator		NTV2DeviceIDSetIter;		///<	@brief	A convenient non-const iterator for NTV2DeviceIDSet.
typedef NTV2DeviceIDSet::const_iterator	NTV2DeviceIDSetConstIter;	///<	@brief	A convenient const iterator for NTV2DeviceIDSet.

/**
	@brief	Returns an NTV2DeviceIDSet of devices supported by the SDK.
	@return	An NTV2DeviceIDSet of devices supported by the SDK.
**/
AJAExport NTV2DeviceIDSet NTV2GetSupportedDevices		(void);

AJAExport std::ostream &	operator << (std::ostream & inOutStr, const NTV2DeviceIDSet & inSet);	///<	@brief	Handy ostream writer for NTV2DeviceIDSet.


//	These are new with 12.0 SDK...
AJAExport std::string NTV2DeviceIDToString				(const NTV2DeviceID				inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2VideoFormatToString			(const NTV2VideoFormat			inValue);
AJAExport std::string NTV2StandardToString				(const NTV2Standard				inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2V2StandardToString			(const NTV2V2Standard			inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2FrameBufferFormatToString		(const NTV2FrameBufferFormat	inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2FrameGeometryToString			(const NTV2FrameGeometry		inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2FrameRateToString				(const NTV2FrameRate			inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2InputSourceToString			(const NTV2InputSource			inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2OutputDestinationToString		(const NTV2OutputDestination	inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2ReferenceSourceToString		(const NTV2ReferenceSource		inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2RegisterWriteModeToString		(const NTV2RegisterWriteMode	inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2InterruptEnumToString			(const INTERRUPT_ENUMS			inInterruptEnumValue);
AJAExport std::string NTV2ChannelToString				(const NTV2Channel				inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2AudioSystemToString			(const NTV2AudioSystem			inValue,	const bool inCompactDisplay = false);
AJAExport std::string NTV2AudioRateToString				(const NTV2AudioRate			inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2AudioBufferSizeToString		(const NTV2AudioBufferSize		inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2AudioLoopBackToString			(const NTV2AudioLoopBack		inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2AudioMonitorSelectToString	(const NTV2AudioMonitorSelect	inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2EmbeddedAudioClockToString	(const NTV2EmbeddedAudioClock	inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2GetBitfileName				(const NTV2DeviceID				inValue);
AJAExport bool        NTV2IsCompatibleBitfileName		(const std::string & inBitfileName, const NTV2DeviceID inDeviceID);
AJAExport NTV2DeviceID NTV2GetDeviceIDFromBitfileName	(const std::string & inBitfileName);
AJAExport std::string NTV2GetFirmwareFolderPath			(void);
AJAExport std::ostream & operator << (std::ostream & inOutStream, const RP188_STRUCT & inObj);
AJAExport std::string NTV2GetVersionString				(const bool inDetailed = false);
//	New with 12.3 SDK...
AJAExport std::string NTV2RegisterNumberToString		(const NTV2RegisterNumber		inValue);
AJAExport std::string AutoCircVidProcModeToString		(const AutoCircVidProcMode		inValue,	const bool inCompactDisplay = false);
AJAExport std::string NTV2ColorCorrectionModeToString	(const NTV2ColorCorrectionMode	inValue,	const bool inCompactDisplay = false);
AJAExport std::string NTV2InputCrosspointIDToString		(const NTV2InputCrosspointID	inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2OutputCrosspointIDToString	(const NTV2OutputCrosspointID	inValue,	const bool inForRetailDisplay = false);
AJAExport std::string NTV2WidgetIDToString				(const NTV2WidgetID				inValue,	const bool inCompactDisplay = false);
AJAExport std::string NTV2TaskModeToString				(const NTV2EveryFrameTaskMode	inValue,	const bool inCompactDisplay = false);
AJAExport std::string NTV2RegNumSetToString				(const NTV2RegisterNumberSet &	inValue);
AJAExport std::string NTV2TCSourceToString				(const NTV2TCSource				inValue,	const bool inCompactDisplay = false);

//	FUTURE	** THESE WILL BE DISAPPEARING **		Deprecate in favor of the new "NTV2xxxxxxToString" functions...
#define	NTV2CrosspointIDToString	NTV2OutputCrosspointIDToString	///< @deprecated	Use NTV2OutputCrosspointIDToString
#if !defined (NTV2_DEPRECATE)
	extern AJAExport const char *	NTV2VideoFormatStrings		[];	///< @deprecated	Use NTV2VideoFormatToString instead.
	extern AJAExport const char *	NTV2VideoStandardStrings	[];	///< @deprecated	Use NTV2StandardToString instead.
	extern AJAExport const char *	NTV2PixelFormatStrings		[];	///< @deprecated	Use NTV2FrameBufferFormatToString instead.
	extern AJAExport const char *	NTV2FrameRateStrings		[];	///< @deprecated	Use NTV2FrameRateToString instead.
	extern AJAExport const char *	frameBufferFormats			[];	///< @deprecated	Use NTV2FrameBufferFormatToString instead.

	AJAExport std::string	frameBufferFormatString		(NTV2FrameBufferFormat inFrameBufferFormat);		///< @deprecated	Use NTV2FrameBufferFormatToString and pass 'true' for 'inForRetailDisplay'
	AJAExport void			GetNTV2BoardString			(NTV2BoardID inBoardID, std::string & outString);	///< @deprecated	Use NTV2DeviceIDToString and concatenate a space instead

	AJAExport std::string	NTV2BoardIDToString			(const NTV2BoardID inValue, const bool inForRetailDisplay = false);	///< @deprecated	Use NTV2DeviceIDToString(NTV2DeviceID,bool) instead.
	AJAExport void			GetNTV2RetailBoardString	(NTV2BoardID inBoardID, std::string & outString);	///< @deprecated	Use NTV2DeviceIDToString(NTV2DeviceID,bool) instead.
	AJAExport NTV2BoardType GetNTV2BoardTypeForBoardID	(NTV2BoardID inBoardID);							///< @deprecated	This function is obsolete because NTV2BoardType is obsolete.
#endif	//	!defined (NTV2_DEPRECATE)

#endif	//	NTV2UTILS_H
