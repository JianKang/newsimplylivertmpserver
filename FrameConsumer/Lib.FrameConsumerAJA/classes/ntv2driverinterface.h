/*
 *
	ntv2driverinterface.h

	Copyright (C) 2004, 2005, 2006, 2007, 2009, 2011 AJA Video Systems, Inc.
	Proprietary and Confidential information.

	Purpose:

	This file contains the base class for the interfaces to the NTV2
	cards from different platforms...currently Windows,Mac OS X ,and
	Linux.
	See ntv2wininterface.[h,cpp],ntv2macinterface.[h,cpp] and
	ntv2linuxinterface.[h,cpp] for actual implementations.

        Note that most of the interfaces are pure virtual functions.

 */
#ifndef NTV2DRIVERINTERFACE_H
#define NTV2DRIVERINTERFACE_H

#include "ajaexport.h"

#include "ajatypes.h"
#include "ntv2enums.h"
#include "videodefines.h"
#include "audiodefines.h"

#include "ntv2nubtypes.h"
#include "ntv2publicinterface.h"

#include <string>

#if defined(AJALinux ) || defined(AJAMac)
	#include <sys/types.h>
	#include <netinet/in.h>
	#include <unistd.h>
#endif

#ifdef MSWindows
	#include <WinSock.h>
	#include <assert.h>
#endif


#if !defined (AJA_VIRTUAL)
	#define	AJA_VIRTUAL		virtual
#endif


/**
	@brief	I'm the base class that undergirds the platform-specific derived classes (from which CNTV2Card is ultimately derived).
**/
class AJAExport CNTV2DriverInterface
{
public:
	CNTV2DriverInterface ();
	virtual ~CNTV2DriverInterface ();

public:
	/**
		@brief		Opens an AJA device so that it can be monitored and/or controlled.
		@result		True if successful; otherwise false.
		@param[in]	inDeviceIndex		Optionally specifies a zero-based index number of the AJA device to open.
										Defaults to zero, which is the first AJA device found.
		@param[in]	displayError		Optionally specifies if an alert dialog should be displayed if a failure occurs
										when attempting to open the AJA device (on host platforms that support alert dialogs).
										Defaults to false.
		@param[in]	eDeviceType			Optionally specifies the type of AJA device to look for. This parameter is obsolete.
		@param[in]	hostName			Optionally specifies the name of a host machine on the local area network that has
										one or more AJA devices attached to it. Defaults to NULL, which attempts to open AJA
										devices on the local host. If not NULL, must be a valid pointer to a buffer containing
										a zero-terminated character string.
	**/
	virtual bool Open(UWord inDeviceIndex=0, bool displayError = false,
					  NTV2DeviceType eDeviceType = DEVICETYPE_UNKNOWN,
					  const char *hostName = 0) = 0;


	// call this before Open to set the shareable feature of the Card
	virtual bool SetShareMode (bool bShared) = 0;

	// call this before Open to set the overlapped feature of the Card
	virtual bool SetOverlappedMode (bool bOverlapped) = 0;

	/**
		@brief		Closes the AJA device, releasing host resources that may have been allocated in a previous Open call.
		@result		True if successful; otherwise false.
		@details	This function closes the CNTV2Card instance's connection to the AJA device.
					Once closed, the device can no longer be queried or controlled by the CNTV2Card instance.
					The CNTV2Card instance can be "reconnected" to another AJA device by calling its Open member function again.
	**/
	virtual bool Close() = 0;

	/**
		@brief		Updates or replaces all or part of the 32-bit contents of a specific register (real or virtual) on the AJA device.
					Using the optional mask and shift parameters, it's possible to set or clear any number of specific bits in a real
					register without altering any of the register's other bits.
		@result		True if successful; otherwise false.
		@param[in]	inRegisterNumber	Specifies the RegisterNum of the real register, or VirtualRegisterNum of the virtual register
										on the AJA device to be changed.
		@param[in]	inValue				Specifies the desired new register value. If the "inShift" parameter is non-zero, this value
										is shifted left by the designated number of bit positions before being masked and applied to
										the real register contents.
		@param[in]	inMask				Optionally specifies a bit mask to be applied to the new (shifted) value before updating the
										register. Defaults to 0xFFFFFFFF, which does not perform any masking.
										On Windows and MacOS, a zero mask is treated the same as 0xFFFFFFFF.
										On MacOS, "holes" in the mask (i.e., one or more runs of 0-bits lying between more-significant
										and less-significant 1-bits) were not handled correctly.
										This parameter is ignored when writing to a virtual register.
		@param[in]	inShift				Optionally specifies the number of bits to left-shift the specified value before applying
										it to the register. Defaults to zero, which does not perform any shifting.
										On MacOS, this parameter was ignored in SDKs prior to version 12.3.
										On Windows, a shift value of 0xFFFFFFFF is treated the same as a zero shift value.
										This parameter is ignored when writing to a virtual register.
		@note		This function should be used only when there is no higher-level function available to accomplish the desired task.
		@note		The mask and shift parameters are ignored when setting a virtual register.
	**/
	virtual bool	WriteRegister (ULWord inRegisterNumber,  ULWord inValue,  ULWord inMask = 0xFFFFFFFF,  ULWord inShift = 0);

protected:
	virtual bool	ReadRegister (ULWord inRegisterNumber,  ULWord * pOutValue,  ULWord inMask = 0xFFFFFFFF,  ULWord inShift = 0);

public:
	/**
		@brief		Reads all or part of the 32-bit contents of a specific register (real or virtual) on the AJA device.
					Using the optional mask and shift parameters, it's possible to read any number of specific bits in a register
					while ignoring the register's other bits.
		@result		True if successful; otherwise false.
		@param[in]	inRegisterNumber	Specifies the RegisterNum of the real register, or VirtualRegisterNum of the virtual register
										on the AJA device to be read.
		@param[out]	outValue			Specifies a valid, non-NULL address of the ULWord that is to receive the register value obtained
										from the device.
		@param[in]	inMask				Optionally specifies a bit mask to be applied after reading the real device register.
										Defaults to 0xFFFFFFFF, which does not perform any masking.
										A zero mask (0x00000000) is also ignored.
										This parameter is ignored when reading a virtual register.
		@param[in]	inShift				Optionally specifies the number of bits to right-shift the value obtained
										from the device register after any mask has been applied.
										Defaults to zero, which performs no bit shift.
										This parameter is ignored when reading a virtual register.
		@note		This function should be used only when there is no higher-level function available to accomplish the desired task.
		@note		The mask and shift parameters are ignored when reading a virtual register.
	**/
	virtual inline bool	ReadRegister (const ULWord inRegisterNumber,  ULWord & outValue,  const ULWord inMask = 0xFFFFFFFF,  const ULWord inShift = 0)
	{
		return ReadRegister (inRegisterNumber, &outValue, inMask, inShift);
	}

	// Read multiple registers at once.
	virtual bool ReadRegisterMulti(	ULWord numRegs,
									ULWord *whichRegisterFailed,
							  		NTV2ReadWriteRegisterSingle aRegs[]);	///< @deprecated	Use CNTV2Card::ReadRegisters instead.

	virtual bool RestoreHardwareProcampRegisters() = 0;

    virtual bool DmaTransfer (NTV2DMAEngine DMAEngine,
							  bool bRead,
							  ULWord frameNumber,
							  ULWord * pFrameBuffer,
							  ULWord offsetBytes,
							  ULWord bytes,
							  bool bSync = true) = 0;

	virtual bool DmaUnlock   (void)  = 0;

	virtual bool CompleteMemoryForDMA (ULWord * pFrameBuffer)  = 0;


	virtual bool PrepareMemoryForDMA (ULWord * pFrameBuffer, ULWord ulNumBytes)  = 0;


	virtual bool ConfigureInterrupt (bool bEnable,  INTERRUPT_ENUMS eInterruptType) = 0;

	virtual bool MapFrameBuffers (void)  = 0;

	virtual bool UnmapFrameBuffers (void)  = 0;

	virtual bool MapRegisters (void)  = 0;

	virtual bool UnmapRegisters (void)  = 0;

	virtual bool MapXena2Flash (void)  = 0;

	virtual bool UnmapXena2Flash (void)  = 0;

	virtual bool ConfigureSubscription (bool bSubscribe, INTERRUPT_ENUMS eInterruptType, PULWord & hSubcription);

	virtual bool GetInterruptCount (INTERRUPT_ENUMS eInterrupt,
								    ULWord *pCount)  = 0;

	virtual bool WaitForInterrupt (INTERRUPT_ENUMS eInterrupt, ULWord timeOutMs = 68);

	virtual bool	AutoCirculate (AUTOCIRCULATE_DATA &autoCircData);

	virtual inline bool		NTV2Message (NTV2_HEADER * pInMessage)				{ (void) pInMessage;  return false; }

	virtual bool ControlDriverDebugMessages(NTV2_DriverDebugMessageSet msgSet,
		  									bool enable ) = 0;

    virtual bool GetDriverVersion(ULWord* driverVersion) = 0;


    // Utility methods:
	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL NTV2BoardType	GetCompileFlag (void);
		virtual inline bool			BoardOpened (void) const						{ return IsOpen (); }
	#endif	//	!NTV2_DEPRECATE

	/**
		@brief	Returns true if I'm able to communicate with the device I represent.
		@return	True if I'm able to communicate with the device I represent;  otherwise false.
	**/
	virtual inline bool			IsOpen (void) const									{ return _boardOpened; }

	AJA_VIRTUAL void			InitMemberVariablesOnOpen (NTV2FrameGeometry frameGeometry, NTV2FrameBufferFormat frameFormat);

	virtual inline const char *	GetHostName (void) const							{ return _hostname.c_str (); }

	virtual inline ULWord		GetPCISlotNumber (void) const						{ return _pciSlot; }

	virtual inline Word			SleepMs (LWord msec) const							{ (void) msec; return 0; }

	virtual inline ULWord		GetNumFrameBuffers (void) const						{ return _ulNumFrameBuffers; }
	virtual inline ULWord		GetAudioFrameBufferNumber (void) const				{ return (GetNumFrameBuffers () - 1); }
	virtual inline ULWord		GetFrameBufferSize (void) const						{ return _ulFrameBufferSize; }

	virtual bool DriverGetBitFileInformation (BITFILE_INFO_STRUCT & bitFileInfo,  NTV2BitFileType bitFileType);	///< @deprecated	This function is obsolete.

	virtual bool DriverGetBuildInformation (BUILD_INFO_STRUCT & outBuildInfo);

	// Functions for cards that support more than one bitfile

	virtual bool SwitchBitfile (NTV2DeviceID boardID, NTV2BitfileType bitfile)		{ (void) boardID; (void) bitfile; return false; }	///< @deprecated	This function is obsolete.

	virtual inline NTV2NubProtocolVersion	GetNubProtocolVersion (void) const		{return _nubProtocolVersion;}

	virtual inline bool						IsRemote (void) const					{return _remoteHandle != INVALID_NUB_HANDLE;}

protected:
	virtual bool				DisplayNTV2Error (const char * str) { (void) str; return  false;}	///< @deprecated	This function is obsolete.

	virtual bool				OpenRemote (UWord inDeviceIndex,
											bool displayErrorMessage,
											UWord ulBoardType,
											const char * hostname);
	virtual bool				CloseRemote (void);

	AJA_VIRTUAL bool			ParseFlashHeader (BITFILE_INFO_STRUCT &bitFileInfo);

	/**
		@brief	Private method that increments the event count tally for the given interrupt type.
		@param[in]	eInterruptType	Specifies the type of interrupt that occurred, which determines
									the counter to be incremented.
	**/
	virtual void	BumpEventCount (const INTERRUPT_ENUMS eInterruptType);

private:
	/**
		@brief	My assignment operator.
		@note	We have intentionally disabled this capability because it was never implemented
				and is currently broken. If/when it gets fixed, we'll make this a public method.
		@param[in]	inRHS	The rvalue to be assigned to the lvalue.
		@return	A non-constant reference to the lvalue.
	**/
	virtual CNTV2DriverInterface & operator = (const CNTV2DriverInterface & inRHS);

	/**
		@brief	My copy constructor.
		@note	We have intentionally disabled this because it was never implemented and is
				currently broken. If/when it gets fixed, we'll make this a public method.
		@param[in]	inObjToCopy		The object to be copied.
	**/
	CNTV2DriverInterface (const CNTV2DriverInterface & inObjToCopy);

protected:

    UWord					_boardNumber;			///< @brief	My device index number.
    bool					_boardOpened;			///< @brief	True if I'm open and connected to the device.
    NTV2BoardType			_boardType;				///< @brief	This is obsolete.
	NTV2DeviceID			_boardID;				///< @brief	My cached device ID.
    bool					_displayErrorMessage;	///< @brief	This is obsolete.
	ULWord					_pciSlot;				//	FIXFIXFIX	Replace this with a std::string that identifies my location in the host device tree.
	ULWord					_programStatus;

	struct sockaddr_in		_sockAddr;				///< @brief	Socket address (if using nub to talk to remote host)
	AJASocket				_sockfd;				///< @brief	Socket descriptor (if using nub to talk to remote host)
	std::string				_hostname;				///< @brief	Remote host name (if using nub to talk to remote host)
	LWord					_remoteHandle;			///< @brief	Remote host handle (if using nub to talk to remote host)
	NTV2NubProtocolVersion	_nubProtocolVersion;	///< @brief	Protocol version (if using nub to talk to remote host)

	PULWord					mInterruptEventHandles	[eNumInterruptTypes];	///< @brief	For subscribing to each possible event, one for each interrupt type
	ULWord					mEventCounts			[eNumInterruptTypes];	///< @brief	My event tallies, one for each interrupt type. Note that these
																			///<		tallies are different from the interrupt tallies kept by the driver.
	ULWord *				_pFrameBaseAddress;

	// for old KSD and KHD boards
	ULWord *				_pCh1FrameBaseAddress;			//	DEPRECATE!
	ULWord *				_pCh2FrameBaseAddress;			//	DEPRECATE!

	ULWord *				_pRegisterBaseAddress;
	ULWord					_pRegisterBaseAddressLength;
	ULWord *				_pFS1FPGARegisterBaseAddress;	//	DEPRECATE!

	ULWord *				_pXena2FlashBaseAddress;

	ULWord					_ulNumFrameBuffers;
	ULWord					_ulFrameBufferSize;

};	//	CNTV2DriverInterface

#endif	//	NTV2DRIVERINTERFACE_H
