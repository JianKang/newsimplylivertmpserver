////////////////////////////////////////////////////////////////////////////////
// SMPTE RP-215 utility class
//
// Copyright (C) 2006 AJA Video Systems, Inc.  Proprietary and Confidential information.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __NTV2_RP215_
#define __NTV2_RP215_


#include "ajatypes.h"
#ifdef MSWindows
#include "windows.h"
#include "stdio.h"
#define nil NULL
#endif
#ifdef AJALinux
#define nil NULL
#endif

#include "ajatypes.h"
#include "ntv2enums.h"


#define RP215_PAYLOADSIZE 215  


class CNTV2RP215Decoder
{
public:
     CNTV2RP215Decoder(ULWord* pFrameBufferBaseAddress,NTV2VideoFormat videoFormat,NTV2FrameBufferFormat fbFormat);
    ~CNTV2RP215Decoder();

	bool Locate();
	bool Extract();
	
private:
	ULWord*					_frameBufferBasePointer;
	NTV2VideoFormat			_videoFormat;
	NTV2FrameBufferFormat	_fbFormat;
	Word					_lineNumber;
	Word					_pixelNumber;

	UByte _rp215RawBuffer[RP215_PAYLOADSIZE];

};
#if 0
bool TestQTMovieDecode(char* fileName);
#endif
#endif	// __NTV2_RP215_
