/////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2004 AJA Video Systems, Inc.  Proprietary and Confidential information.
//
/////////////////////////////////////////////////////////////////////////////
#ifndef NTV2VIDPROC_H
#define NTV2VIDPROC_H

#include "ajaexport.h"
#include "ntv2testpattern.h"

#ifdef AJALinux
#define HANDLE HANDLE2
//#include "qcolor.h"
typedef unsigned int AJARgb;
const AJARgb  AJA_RGB_MASK    = 0x00ffffff;		// masks RGB values

inline int ajaRed( AJARgb rgb )		// get red part of RGB
{ return (int)((rgb >> 16) & 0xff); }

inline int ajaGreen( AJARgb rgb )		// get green part of RGB
{ return (int)((rgb >> 8) & 0xff); }

inline int ajaBlue( AJARgb rgb )		// get blue part of RGB
{ return (int)(rgb & 0xff); }

inline int ajaAlpha( AJARgb rgb )		// get alpha part of RGBA
{ return (int)((rgb >> 24) & 0xff); }

inline AJARgb ajaRgb( int r, int g, int b )// set RGB value
{ return (0xff << 24) | ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff); }

inline AJARgb ajaRgba( int r, int g, int b, int a )// set RGBA value
{ return ((a & 0xff) << 24) | ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff); }

inline int ajaGray( int r, int g, int b )// convert R,G,B to gray 0..255
{ return (r*11+g*16+b*5)/32; }

inline int ajaGray( AJARgb rgb )		// convert RGB to gray 0..255
{ return ajaGray( ajaRed(rgb), ajaGreen(rgb), ajaBlue(rgb) ); }

#endif

class AJAExport CNTV2VidProc : public CNTV2TestPattern
{
public:  // Constructor - Deconstructor
        CNTV2VidProc() {};
	CNTV2VidProc(UWord inDeviceIndex, bool displayErrorMessage = false,
		UWord ulBoardType = DEVICETYPE_NTV2);

	CNTV2VidProc(UWord inDeviceIndex,
		      bool displayErrorMessage, 
			  UWord dwCardTypes = DEVICETYPE_NTV2,
			  bool autoRouteOnXena2=false,
			  const char hostname[] = 0	// Non-null: card on remote host
			);

	virtual ~CNTV2VidProc();

public: // Methods

	AJA_VIRTUAL void	SetupDefaultVidProc();
	AJA_VIRTUAL void	DisableVidProc();

	AJA_VIRTUAL void	SetCh1VidProcMode(NTV2Ch1VidProcMode vidProcMode);
	AJA_VIRTUAL NTV2Ch1VidProcMode	GetCh1VidProcMode();
	AJA_VIRTUAL void	SetCh2OutputMode(NTV2Ch2OutputMode outputMode);
	AJA_VIRTUAL NTV2Ch2OutputMode	GetCh2OutputMode();

	AJA_VIRTUAL void	SetForegroundVideoCrosspoint(NTV2Crosspoint crosspoint);
	AJA_VIRTUAL void	SetForegroundKeyCrosspoint(NTV2Crosspoint crosspoint);
	AJA_VIRTUAL void	SetBackgroundVideoCrosspoint(NTV2Crosspoint crosspoint);
	AJA_VIRTUAL void	SetBackgroundKeyCrosspoint(NTV2Crosspoint crosspoint);
	AJA_VIRTUAL NTV2Crosspoint	GetForegroundVideoCrosspoint();
	AJA_VIRTUAL NTV2Crosspoint	GetForegroundKeyCrosspoint();
	AJA_VIRTUAL NTV2Crosspoint	GetBackgroundVideoCrosspoint();
	AJA_VIRTUAL NTV2Crosspoint	GetBackgroundKeyCrosspoint();

	AJA_VIRTUAL void	SetSplitMode(NTV2SplitMode splitMode);
	AJA_VIRTUAL NTV2SplitMode	GetSplitMode();
	AJA_VIRTUAL void	SetSplitParameters(Fixed_ position, Fixed_ softness);
	AJA_VIRTUAL void	SetSlitParameters(Fixed_ start, Fixed_ width);

	AJA_VIRTUAL void	SetMixCoefficient(Fixed_ coefficient);
	AJA_VIRTUAL Fixed_	GetMixCoefficient();

	AJA_VIRTUAL void	SetMatteColor(YCbCr10BitPixel ycbcrPixel);
#ifdef MSWindows    
	AJA_VIRTUAL void	SetMatteColor(COLORREF rgbColor);
#endif   

#ifdef AJALinux
	AJA_VIRTUAL void	SetMatteColor(AJARgb rgbColor);
#endif
	
};	//	CNTV2VidProc


#endif	//	NTV2VIDPROC_H
