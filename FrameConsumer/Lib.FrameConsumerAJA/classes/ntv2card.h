/**
	@file		ntv2card.h
	@brief		Declares the CNTV2Card class and the NTV2VideoFormatSet.
	@copyright	(C) 2004-2014 AJA Video Systems, Inc.	Proprietary and confidential information.
**/

#ifndef NTV2CARD_H
#define NTV2CARD_H

#include "ajaexport.h"

#if defined (MSWindows)
	#include "ntv2windriverinterface.h"
#elif defined (AJAMac)
	#include "ntv2macdriverinterface.h"
#elif defined (AJALinux)
	#include "../linuxclasses/ntv2linuxdriverinterface.h"
#endif
#include "ntv2signalrouter.h"

#include <set>
#include <string>
#include <iostream>

/**
	@page	cntv2card		Talking to a Device Using CNTV2Card

	This is the main class that client programs use to control AJA hardware devices. It includes functions to set board parameters,
	access hardware registers, subscribe to and wait for events, perform video/audio I/O, and many other functions.
**/


typedef std::set <NTV2AudioChannelPair>			NTV2AudioChannelPairs;			/// @brief	A set of distinct NTV2AudioChannelPair values.
typedef NTV2AudioChannelPairs::const_iterator	NTV2AudioChannelPairsConstIter;	/// @brief	Handy const iterator to iterate over a set of distinct NTV2AudioChannelPair values.



/**
	@brief	I interrogate and control an AJA video/audio capture/playout device.
**/
#if defined (MSWindows)
	class AJAExport CNTV2Card	: public CNTV2WinDriverInterface
#elif defined (AJAMac)
	class CNTV2Card				: public CNTV2MacDriverInterface
#elif defined (AJALinux)
	class CNTV2Card				: public CNTV2LinuxDriverInterface
#endif
{
public:
	/**
		@name	Construction & Destruction
	**/
	///@{
	/**
		@brief	My default constructor.
	**/
										CNTV2Card ();

	/**
		@brief	Constructs me from the given parameters.
		@param[in]	inDeviceIndex	A zero-based index number that identifies which device to open,
									which should be the number received from the NTV2DeviceScanner.
		@param[in]	inDisplayError	If true, displays a message box if there's a failure while opening.
									This parameter is obsolete and won't be available in the future.
		@param[in]	inDeviceType	Specifies the NTV2DeviceType of the device to open.
									This parameter is obsolete and won't be available in the future.
		@param[in]	pInHostName		If non-NULL, must be a valid pointer to a character buffer that
									contains the name of a host that has one or more AJA devices.
									Defaults to NULL (the local host).
	**/
	explicit							CNTV2Card ( const UWord		inDeviceIndex,
													const bool		inDisplayError	= false,
													const UWord		inDeviceType	= DEVICETYPE_NTV2,
													const char *	pInHostName		= 0);
	/**
		@brief	My destructor.
	**/
	virtual								~CNTV2Card();
	///@}


	/**
		@name	Opening & Closing
	**/
	///@{
	#if !defined (NTV2_DEPRECATE)
		virtual bool					SetBoard (UWord inDeviceIndex);		///< @deprecated	Use CNTV2DeviceScanner or Open(deviceIndex) instead.
	#endif	//	!defined (NTV2_DEPRECATE)
	///@}

	/**
		@name	Inquiry
	**/
	///@{

	/**
		@brief	Answers with a 4-byte value that uniquely identifies the kind of AJA device I'm talking to.
		@return	The 4-byte value that identifies the kind of AJA device this is.
		@note	NTV2DeviceID is being deprecated.
	**/
	AJA_VIRTUAL NTV2DeviceID			GetDeviceID (void);

	/**
		@brief	Answers with this device's zero-based index number (relative to other known devices).
		@return	This device's zero-based index number.
	**/
	AJA_VIRTUAL inline UWord		GetIndexNumber (void) const		{return _boardNumber;}

	/**
		@brief	Answers with this device's display name.
		@return	A string containing this device's display name.
	**/
	AJA_VIRTUAL std::string			GetDisplayName (void);

	/**
		@brief	Answers with this device's version number.
		@return	This device's version number.
	**/
	AJA_VIRTUAL Word				GetDeviceVersion (void);

	/**
		@brief	Answers with this device's version number as a human-readable string.
		@return	A string containing this device's version number as a human-readable string.
	**/
	AJA_VIRTUAL std::string			GetDeviceVersionString (void);

	/**
		@brief	Answers with this device's driver's version as a human-readable string.
		@return	A string containing this device's driver's version as a human-readable string.
	**/
	AJA_VIRTUAL std::string			GetDriverVersionString (void);

	/**
		@brief	Answers with the individual version components of this device's driver.
		@param[out]	outMajor	Receives the driver's major version number.
		@param[out]	outMinor	Receives the driver's minor version number.
		@param[out]	outPoint	Receives the driver's point release number.
		@param[out]	outBuild	Receives the driver's build number.
		@return	True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool				GetDriverVersionComponents (UWord & outMajor, UWord & outMinor, UWord & outPoint, UWord & outBuild);

	/**
		@brief	Answers with my serial number.
		@return	My 64-bit serial number.
		@note	To decode this into a human-readable form, use my SerialNum64ToString class method.
	**/
	AJA_VIRTUAL uint64_t			GetSerialNumber (void);											//	From CNTV2Status

	/**
		@brief	Answers with a string that contains my human-readable serial number.
		@return	True if successful (and valid);  otherwise false.
	**/
	AJA_VIRTUAL bool				GetSerialNumberString (std::string & outSerialNumberString);	//	From CNTV2Status

	/**
		@brief	Answers with my PCI device ID.
		@param[out]		outPCIDeviceID		Receives my PCI device ID.
		@return	True if successful (and valid);  otherwise false.
	**/
	AJA_VIRTUAL bool				GetPCIDeviceID (ULWord & outPCIDeviceID);

	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL inline NTV2BoardID	GetBoardID (void)				{return GetDeviceID ();}		///< @deprecated	Use GetDeviceID instead.
		AJA_VIRTUAL inline UWord		GetBoardNumber (void) const		{return GetIndexNumber ();}		///< @deprecated	Use GetIndexNumber instead.
		AJA_VIRTUAL NTV2BoardType		GetBoardType (void) const;										///< @deprecated	NTV2BoardType is obsolete.
		AJA_VIRTUAL NTV2BoardSubType	GetBoardSubType (void);											///< @deprecated	NTV2BoardSubType is obsolete.
		static UWord					GetNumNTV2Boards (void);										///< @deprecated	Use CNTV2DeviceScanner instead.
	#endif	//	!defined (NTV2_DEPRECATE)
	///@}


	/**
		@name	DMA Transfer
	**/
	///@{
	/**
		@brief		Transfers data from the AJA device to the host.
		@param[in]	inDMAEngine		Specifies the DMA engine to use.
									NTV2_DMA_FIRST_AVAILABLE is recommended for best DMA engine utilization.
		@param[in]	inFrameNumber	Specifies the zero-based frame number of the starting frame to be read from the device.
		@param[in]	pInFrameBuffer	Specifies the non-NULL address of the host buffer that is to receive the frame data.
									The memory it points to must be writeable.
		@param[in]	inOffsetBytes	Specifies the byte offset [into the host buffer? into the device frame buffer?].	FIXFIXFIX	Document	FINISH THIS
		@param[in]	inByteCount		Specifies the total number of bytes to transfer.
		@param[in]	inSynchronous	If true, the function will block until the transfer completes; otherwise, the function
									will return immediately, and the caller will need to determine when the transfer has completed.
									Defaults to true (synchronous).
		@return		True if successful; otherwise false.
		@note		The host buffer should be at least inBytes + inOffsetBytes in size, or host memory will be corrupted.
	**/
	AJA_VIRTUAL bool	DmaRead (const NTV2DMAEngine inDMAEngine, const ULWord inFrameNumber, ULWord * pInFrameBuffer,
									const ULWord inOffsetBytes, const ULWord inByteCount, const bool inSynchronous = true);

	/**
		@brief		Transfers data from the host to the AJA device.
		@param[in]	inDMAEngine		Specifies the DMA engine to use.
									NTV2_DMA_FIRST_AVAILABLE is recommended for best DMA engine utilization.
		@param[in]	inFrameNumber	Specifies the zero-based frame number of the frame to be written on the device.
		@param[in]	pInFrameBuffer	Specifies the non-NULL address of the host buffer that is to supply the frame data.
									The memory it points to must be readable.
		@param[in]	inOffsetBytes	Specifies the byte offset [into the host buffer? into the device frame buffer?].	FIXFIXFIX	Document	FINISH THIS
		@param[in]	inByteCount		Specifies the total number of bytes to transfer.
		@param[in]	inSynchronous	If true, the function will block until the transfer completes; otherwise, the function
									will return immediately, and the caller will need to determine when the transfer has completed.
									Defaults to true (synchronous).
		@return		True if successful; otherwise false.
		@note		The host buffer should be at least inBytes + inOffsetBytes in size, or a host memory access violation may occur.
	**/
	AJA_VIRTUAL bool	DmaWrite (const NTV2DMAEngine inDMAEngine, const ULWord inFrameNumber, ULWord * pInFrameBuffer,
									const ULWord inOffsetBytes, const ULWord inByteCount, const bool inSynchronous = true);

	AJA_VIRTUAL bool	DmaReadFrame (NTV2DMAEngine DMAEngine, ULWord frameNumber, ULWord *pFrameBuffer,
										ULWord bytes, bool bSync = true);

	AJA_VIRTUAL bool	DmaWriteFrame (NTV2DMAEngine DMAEngine, ULWord frameNumber, ULWord *pFrameBuffer,
										ULWord bytes, bool bSync = true);

	AJA_VIRTUAL bool	DmaReadSegment (NTV2DMAEngine DMAEngine, ULWord frameNumber, ULWord * pFrameBuffer,
										 ULWord offsetBytes, ULWord bytes,
										 ULWord numSegments, ULWord segmentHostPitch, ULWord segmentCardPitch,
										 bool bSync = true);

	AJA_VIRTUAL bool	DmaWriteSegment (NTV2DMAEngine DMAEngine, ULWord frameNumber, ULWord * pFrameBuffer,
										ULWord offsetBytes, ULWord bytes,
										ULWord numSegments, ULWord segmentHostPitch, ULWord segmentCardPitch,
										bool bSync = true);

	AJA_VIRTUAL bool	DmaP2PTargetFrame(NTV2Channel channel,					// frame buffer channel output frame to update on completion
											ULWord frameNumber,					// frame number to target
											ULWord frameOffset,					// frame buffer offset (bytes)
											PCHANNEL_P2P_STRUCT pP2PData);		// p2p target data (output)

	AJA_VIRTUAL bool	DmaP2PTransferFrame(NTV2DMAEngine DMAEngine,			// dma engine for transfer
											 ULWord frameNumber,				// source frame number
											 ULWord frameOffset,				// source frame buffer offset (bytes)
											 ULWord transferSize,				// transfer size (bytes)
											 ULWord numSegments,				// number of segments (0 if not a segmented transfer)
											 ULWord segmentTargetPitch,			// target frame pitch (0 if not a segmented transfer)
											 ULWord segmentCardPitch,			// source frame pitch (0 if not a segmented transfer)
											 PCHANNEL_P2P_STRUCT pP2PData);		// p2p target data

	/**
		@brief	Transfers audio data from a given audio system on the AJA device to the host.
		@param[in]	inDMAEngine			Specifies the DMA engine to use. NTV2_DMA_FIRST_AVAILABLE is recommended.
		@param[in]	inAudioEngine		Specifies the audio engine on the device that is to supply the audio data.
		@param		pOutAudioBuffer		Specifies a valid, non-NULL pointer to the host buffer that is to receive the audio data.
										This buffer must be large enough to accommodate "inByteCount" bytes of data specified (below).
		@param[in]	inOffsetBytes		Specifies the offset into the audio engine's capture buffer on the device from which to transfer audio data.
		@param[in]	inByteCount			Specifies the number of audio bytes to transfer.
		@param[in]	inSynchronous		If true (the default), the transfer is handled synchronously (i.e., this function will block until
										the transfer has completed). If false, the transfer is performed asynchronously. Note that this
										parameter is not respected on all platforms.
		@return	True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool	DmaAudioRead (	const NTV2DMAEngine		inDMAEngine,
										const NTV2AudioSystem	inAudioEngine,
										ULWord *				pOutAudioBuffer,
										const ULWord			inOffsetBytes,
										const ULWord			inByteCount,
										const bool				inSynchronous);

	/**
		@brief	Transfers audio data from a given host buffer to a specific audio system's playout buffer on the AJA device.
		@param[in]	inDMAEngine			Specifies the DMA engine to use. NTV2_DMA_FIRST_AVAILABLE is recommended.
		@param[in]	inAudioEngine		Specifies the audio engine on the device that is to receive the audio data.
		@param[in]	pInAudioBuffer		Specifies a valid, non-NULL pointer to the host buffer that is to supply the audio data.
		@param[in]	inOffsetBytes		Specifies the offset into the audio engine's playout buffer on the device to which audio data will be transferred.
		@param[in]	inByteCount			Specifies the number of audio bytes to transfer. Note that this value must not overrun the host
										buffer, nor the device's audio playout buffer.
		@param[in]	inSynchronous		If true (the default), the transfer is handled synchronously (i.e., this function will block until
										the transfer has completed). If false, the transfer is performed asynchronously. Note that this
										parameter is not respected on all platforms.
		@return	True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool	DmaAudioWrite (	const NTV2DMAEngine		inDMAEngine,
										const NTV2AudioSystem	inAudioEngine,
										ULWord *				pInAudioBuffer,
										const ULWord			inOffsetBytes,
										const ULWord			inByteCount,
										const bool				inSynchronous);

	#if !defined (NTV2_DEPRECATE)
		/**
			@deprecated		This function is obsolete, as there's no current AJA devices that use separate (non-interleaved) fields.
		**/
		AJA_VIRTUAL bool	DmaReadField (NTV2DMAEngine DMAEngine, ULWord frameNumber, NTV2FieldID fieldID, ULWord *pFrameBuffer,
											ULWord bytes, bool bSync = true);

		/**
			@deprecated		This function is obsolete, as there's no current AJA devices use separate (non-interleaved) fields.
		**/
		AJA_VIRTUAL bool	DmaWriteField (NTV2DMAEngine DMAEngine, ULWord frameNumber, NTV2FieldID fieldID, ULWord *pFrameBuffer,
											ULWord bytes, bool bSync = true);
	#endif	//	!defined (NTV2_DEPRECATE)
	///@}

//
//	 Set/Get Parameter routines
//
	#if defined (AJAMac)
		#define	AJA_RETAIL_DEFAULT	true
	#else	//	else !defined (AJAMac)
		#define	AJA_RETAIL_DEFAULT	false
	#endif	//	!defined (AJAMac)

	/**
		@brief		Configures the AJA device to handle a specific video format.
		@return		True if successful; otherwise false.
		@param[in]	inNewValue				Specifies the desired video format to be handled by the AJA device.
											It must be a valid NTV2VideoFormat constant.
		@param[in]	inIsAJARetail			Specifies if the user's control panel settings for horizontal and vertical timing
											will be overridden or not. Defaults to true on MacOS, false on other platforms.
		@param[in]	inKeepVancSettings		If true, specifies that the device's current VANC settings are to be preserved;
											otherwise, they will not be preserved. Defaults to false.
		@param[in]	inChannel				Specifies the NTV2Channel of interest. Defaults to NTV2_CHANNEL1.
		@details	This function changes the device configuration to a specific video standard (e.g., 525, 1080, etc.),
					frame geometry (e.g., 1920x1080, 720x486, etc.) and frame rate (e.g., 59.94 fps, 29.97 fps, etc.),
					plus a few other settings (e.g., progressive/interlaced, etc.), all based on the given video format.
	**/
	AJA_VIRTUAL bool	SetVideoFormat (NTV2VideoFormat inNewValue, bool inIsAJARetail = AJA_RETAIL_DEFAULT, bool inKeepVancSettings = false, NTV2Channel inChannel = NTV2_CHANNEL1);

	AJA_VIRTUAL bool	SetFrameGeometry (NTV2FrameGeometry value, bool ajaRetail = AJA_RETAIL_DEFAULT, NTV2Channel channel = NTV2_CHANNEL1);

	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool	SetReferenceSource (NTV2ReferenceSource value, bool ajaRetail = AJA_RETAIL_DEFAULT);	///< @deprecated	Use SetReference instead.
		AJA_VIRTUAL bool	GetReferenceSource (NTV2ReferenceSource* value, bool ajaRetail = AJA_RETAIL_DEFAULT);	///< @deprecated	Use GetReference instead.
	#endif	//	!defined (NTV2_DEPRECATE)

	/**
		@brief		Sets the frame buffer format for the given frame store on the AJA device.
		@return		True if successful; otherwise false.
		@param[in]	inChannel			Specifies the frame store to be affected, which must be one of NTV2_CHANNEL1,
									NTV2_CHANNEL2, NTV2_CHANNEL3, or NTV2_CHANNEL4.
		@param[in]	inNewFormat		Specifies the desired frame buffer format.
									This must be a valid NTV2FrameBufferFormat value.
		@param[in]	inIsAJARetail	Specifies if the AJA retail configuration settings are to be respected or not.
									Defaults to false on all platforms other than MacOS, which defaults to true.
		@details	This function allows client applications to control the format of frame data stored
					in the frame stores on an AJA device. This is important, because when frames are transferred
					between the host and the AJA device, the frame data format is presumed to be identical.
	**/
	AJA_VIRTUAL bool	SetFrameBufferFormat (NTV2Channel inChannel, NTV2FrameBufferFormat inNewFormat, bool inIsAJARetail = AJA_RETAIL_DEFAULT);


	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool			UpdateK2ColorSpaceMatrixSelect (NTV2VideoFormat currFormat = NTV2_FORMAT_UNKNOWN, bool ajaRetail = AJA_RETAIL_DEFAULT);	///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool			UpdateK2LUTSelect (NTV2VideoFormat currFormat = NTV2_FORMAT_UNKNOWN, bool ajaRetail = AJA_RETAIL_DEFAULT);	///< @deprecated	This function is obsolete.
		AJA_VIRTUAL NTV2BitfileType	BitfileSwitchNeeded (NTV2DeviceID deviceID, NTV2VideoFormat value, bool ajaRetail = AJA_RETAIL_DEFAULT);	///< @deprecated	This function is obsolete.
	#endif	//	!defined (NTV2_DEPRECATE)

	AJA_VIRTUAL bool		SetReference (NTV2ReferenceSource value);
	AJA_VIRTUAL bool		GetReference (NTV2ReferenceSource & outValue);
	AJA_VIRTUAL inline bool	GetReference (NTV2ReferenceSource * pOutValue)									{return pOutValue ? GetReference (*pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	/**
		@brief		Retrieves the device's current retail service task mode.
		@return		True if successful; otherwise false.
		@param[out]	outMode		Receives the device's current "every frame task mode" setting. If successful, the
								variable will contain NTV2_DISABLE_TASKS, NTV2_STANDARD_TASKS, or NTV2_OEM_TASKS.
		@details	AJA's retail drivers come with a program that automatically and continuously configures the
					device once-per-frame using settings that are dictated by the AJA Control Panel application.
					The task runs as a service on Windows and as an agent on MacOS. It starts when a host user logs in,
					restores the device configuration to its last known state (as set by that user via the AJA Control Panel),
					then holds that setting while running in the background, until the user logs off the host.
					Some OEM applications cannot assume that the user's Control Panel settings will be valid for their
					proper operation, and thus may want to know if the retail service has control of the device.
	**/
	AJA_VIRTUAL bool		GetEveryFrameServices (NTV2EveryFrameTaskMode & outMode);
	AJA_VIRTUAL inline bool	GetEveryFrameServices (NTV2EveryFrameTaskMode * pOutMode)						{return pOutMode ? GetEveryFrameServices (*pOutMode) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	/**
		@brief		Enables or disables all or part of the retail mode service task that continuously controls
					the "retail mode" device configuration.
		@return		True if successful; otherwise false.
		@param[in]	mode		Specifies the "every frame task mode" the device is to assume,
								and must be one of the following values: NTV2_DISABLE_TASKS, NTV2_STANDARD_TASKS, or NTV2_OEM_TASKS.
		@details	AJA's retail software provides a program that automatically and continuously configures the device once per frame
					using settings that are dictated by the AJA Control Panel application. This task runs as a service on Windows,
					and as an agent on MacOS X. It starts when a host user logs in, restores the device configuration to its last
					known state (as set by the user via the AJA Control Panel), then holds that setting while running in the background,
					until the user logs off the host. Some OEM applications cannot assume that the user's Control Panel settings
					will be valid for their proper operation, and thus will need to disable the service task as long as their
					application is running.
	**/
	AJA_VIRTUAL bool	SetEveryFrameServices (NTV2EveryFrameTaskMode mode);

	/**
		@brief		Answers as to whether or not the host OS audio services for the AJA device (e.g. CoreAudio on MacOS)
					are currently suspended or not.
		@param[out]	outIsSuspended	Receives 'true' if the host OS audio service is currently suspended for the AJA
									device;  otherwise, receives 'false'.
		@return		True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool	GetSuspendHostAudio (bool & outIsSuspended);

	/**
		@brief		Suspends or resumes host OS audio (e.g. CoreAudio on MacOS) for the AJA device.
		@param[in]	inSuspend	If true, suspends host OS audio for the AJA device;  otherwise, resumes it.
		@return		True if successful; otherwise false.
		@note		This function is currently only implemented on MacOS, and is used to suspend or resume CoreAudio
					when an application uses AutoCirculate to capture or play audio, to keep the two audio systems
					from conflicting with each other.
	**/
	AJA_VIRTUAL bool	SetSuspendHostAudio (const bool inSuspend);

	AJA_VIRTUAL bool		SetDefaultVideoOutMode (ULWord mode);
	AJA_VIRTUAL bool		GetDefaultVideoOutMode (ULWord & outMode);
	AJA_VIRTUAL inline bool	GetDefaultVideoOutMode (ULWord * pOutMode)								{return pOutMode ? GetDefaultVideoOutMode (*pOutMode) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	/**
		@brief		Determines if a given frame store on the AJA device will be used to capture or playout video.
		@return		True if successful; otherwise false.
		@param[in]	inChannel		Specifies the NTV2Channel of interest (which corresponds to the Frame Store of interest).
		@param[in]	inNewValue		Specifies the desired mode for the frame store, which must be either NTV2_MODE_DISPLAY
									or NTV2_MODE_CAPTURE.
		@param[in]	inIsAJARetail	Specifies if the AJA retail configuration should be respected or not.
									Defaults to false on all platforms other than MacOS, which defaults to true.
		@note		Applications that acquire exclusive use of the AJA device, set its "every frame services" mode
					to NTV2_OEM_TASKS, and use AutoCirculate won't need to call this function, since AutoCirculate
					sets the frame store's mode automatically.
	**/
	AJA_VIRTUAL bool	SetMode (NTV2Channel inChannel, NTV2Mode inNewValue, bool inIsAJARetail = AJA_RETAIL_DEFAULT);

	/**
		@brief		Returns the current mode (capture or playout) of the given frame store on the AJA device.
		@param[in]	inChannel	Specifies the frame store of interest (NTV2_CHANNEL1 - NTV2_CHANNEL4).
		@param[out]	outValue	Receives the current mode for the channel. If the function result is true,
								it will contain either NTV2_MODE_DISPLAY or NTV2_MODE_CAPTURE.
		@details	A frame store can either be set to record/capture or display/playout.
					This function allows client applications to determine a frame store's mode.
		@return		True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool		GetMode (const NTV2Channel inChannel, NTV2Mode & outValue);
	AJA_VIRTUAL inline bool	GetMode (const NTV2Channel inChannel, NTV2Mode * pOutValue)				{return pOutValue ? GetMode (inChannel, *pOutValue) : false;}

	AJA_VIRTUAL bool	GetFrameGeometry (NTV2FrameGeometry & outValue, NTV2Channel inChannel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool	GetFrameGeometry (NTV2FrameGeometry * pOutValue, NTV2Channel inChannel = NTV2_CHANNEL1)		{return pOutValue ? GetFrameGeometry (*pOutValue, inChannel) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	/**
		@brief		Returns the current frame buffer format for the given frame store on the AJA device.
		@return		True if successful; otherwise false.
		@param[in]	inChannel		Specifies the frame store (channel) of interest.
		@param[out]	outValue		Receives the frame store's current pixel format. If the function result is true,
									the variable will contain a valid NTV2FrameBufferFormat value.
		@details	This function allows client applications to inquire about the current format of frame data
					stored in an AJA device's frame store. This is important because when frames are transferred
					between the host and the AJA device, the frame data format is presumed to be identical.
	**/
	AJA_VIRTUAL bool		GetFrameBufferFormat (NTV2Channel inChannel, NTV2FrameBufferFormat & outValue);
	AJA_VIRTUAL inline bool	GetFrameBufferFormat (NTV2Channel inChannel, NTV2FrameBufferFormat * pOutValue)		{return pOutValue ? GetFrameBufferFormat (inChannel, *pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.


	/**
		@brief		Returns a std::set of NTV2VideoFormat values that I support.
		@param[out]	outFormats	Receives the set of NTV2VideoFormat values.
								This will be empty if the function fails.
		@return		True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	GetSupportedVideoFormats (NTV2VideoFormatSet & outFormats);


	// The rest of the routines
	AJA_VIRTUAL bool		GetVideoFormat (NTV2VideoFormat & outValue, NTV2Channel inChannel = NTV2_CHANNEL1);
	AJA_VIRTUAL inline bool	GetVideoFormat (NTV2VideoFormat * pOutValue, NTV2Channel inChannel = NTV2_CHANNEL1)		{return pOutValue ? GetVideoFormat (*pOutValue, inChannel) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool	GetActiveFramebufferSize (SIZE* frameBufferSize, NTV2Channel channel = NTV2_CHANNEL1);

	/**
		@brief		Sets the frame buffer size on those boards that allow software to select a video buffer size.
		@return		True if successful; otherwise false.
		@param[in]	size			Specifies the size of frame buffer the hardware should use.
		@details	The firmware will use a frame buffer size big enough to accommodate the largest possible frame
					for the frame buffer format and frame buffer geometry in use.  This can be wasteful, for example,
					when using an 8 bit YCbCr format with a "tall" frame geometry so that VANC can be processed.
					These frames will fit in 8MB, but the firmware will use a size of 16MB just in case the pixel
					format is changed to 48 bit RGB.  This function provides a way to force a given frame buffer size.
					Selecting a smaller size than that actually needed by the hardware will compromise video integrity.
	**/
	AJA_VIRTUAL bool		SetFrameBufferSize (NTV2Framesize size);

	AJA_VIRTUAL bool		GetNumberActiveLines (ULWord & outNumActiveLines);
	AJA_VIRTUAL inline bool	GetNumberActiveLines (ULWord * pOutNumActiveLines)			{return pOutNumActiveLines ? GetNumberActiveLines (*pOutNumActiveLines) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetStandard (NTV2Standard inValue, NTV2Channel inChannel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool		GetStandard (NTV2Standard & outValue, NTV2Channel inChannel = NTV2_CHANNEL1);
	AJA_VIRTUAL inline bool	GetStandard (NTV2Standard * pOutValue, NTV2Channel inChannel = NTV2_CHANNEL1)	{return pOutValue ? GetStandard (*pOutValue, inChannel) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		IsProgressiveStandard (bool & outIsProgressive, NTV2Channel inChannel = NTV2_CHANNEL1);
	AJA_VIRTUAL inline bool	IsProgressiveStandard (bool * pOutIsProgressive, NTV2Channel inChannel = NTV2_CHANNEL1)	{return pOutIsProgressive ? IsProgressiveStandard (*pOutIsProgressive, inChannel) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		IsSDStandard (bool & outIsStandardDef, NTV2Channel inChannel = NTV2_CHANNEL1);
	AJA_VIRTUAL inline bool	IsSDStandard (bool * pOutIsStandardDef, NTV2Channel inChannel = NTV2_CHANNEL1)	{return pOutIsStandardDef ? IsSDStandard (*pOutIsStandardDef, inChannel) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.
	#if !defined (NTV2_DEPRECATE)
		static bool	IsSDVideoADCMode (NTV2LSVideoADCMode mode);			///< @deprecated	This function is obsolete.
		static bool	IsHDVideoADCMode (NTV2LSVideoADCMode mode);			///< @deprecated	This function is obsolete.
	#endif	//	!defined (NTV2_DEPRECATE)
	AJA_VIRTUAL bool	IsBufferSizeSetBySW();

	/**
		@brief		Sets the AJA device's frame rate.
		@return		True if successful; otherwise false.
		@param[in]	inNewValue		Specifies the new NTV2FrameRate value the AJA device is to be configured with.
		@param[in]	inChannel		Specifies the NTV2Channel of interest.
		@details	This function changes bits 0, 1, 2 and 22 of the AJA device's Global Control Register (register 0),
					to change the device's frame rate configuration.
	**/
	AJA_VIRTUAL bool	SetFrameRate (NTV2FrameRate inNewValue, NTV2Channel inChannel = NTV2_CHANNEL1);

	/**
		@brief		Returns the AJA device's currently configured frame rate via its "value" parameter.
		@return		True if successful; otherwise false.
		@param[out]	outValue	Receives the device's current NTV2FrameRate value.
		@param[in]	inChannel	Specifies the NTV2Channel of interest.
		@details	This function queries the AJA device's Global Control Register (register 0), and inspects bits 0, 1, 2 and 22,
					to determine the device's current frame rate configuration.
	**/
	AJA_VIRTUAL bool		GetFrameRate (NTV2FrameRate & outValue, NTV2Channel inChannel = NTV2_CHANNEL1);
	AJA_VIRTUAL inline bool	GetFrameRate (NTV2FrameRate * pOutValue, NTV2Channel inChannel = NTV2_CHANNEL1)		{return pOutValue ? GetFrameRate (*pOutValue, inChannel) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetSmpte372 (ULWord inValue, NTV2Channel inChannel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool		GetSmpte372 (ULWord & outValue, NTV2Channel inChannel = NTV2_CHANNEL1);
	AJA_VIRTUAL inline bool	GetSmpte372 (ULWord * pOutValue, NTV2Channel inChannel = NTV2_CHANNEL1)		{return pOutValue ? GetSmpte372 (*pOutValue, inChannel) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetProgressivePicture (ULWord value);
	AJA_VIRTUAL bool		GetProgressivePicture (ULWord & outValue);
	AJA_VIRTUAL inline bool	GetProgressivePicture (ULWord * pOutValue)									{return pOutValue ? GetProgressivePicture (*pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetQuadFrameEnable(ULWord value, NTV2Channel inChannel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool		GetQuadFrameEnable(ULWord & outValue, NTV2Channel inChannel = NTV2_CHANNEL1);
	AJA_VIRTUAL inline bool	GetQuadFrameEnable(ULWord * pOutValue, NTV2Channel inChannel = NTV2_CHANNEL1)										{ return pOutValue ? GetQuadFrameEnable(*pOutValue, inChannel) : false; }	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		Set4kSquaresEnable (bool inIsEnabled, NTV2Channel inChannel);
	AJA_VIRTUAL bool		Get4kSquaresEnable (bool & outIsEnabled, NTV2Channel inChannel);
	AJA_VIRTUAL inline bool	Get4kSquaresEnable (bool * pOutIsEnabled, NTV2Channel inChannel)			{return pOutIsEnabled ? Get4kSquaresEnable (*pOutIsEnabled, inChannel) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		Set425FrameEnable (bool inIsEnabled, NTV2Channel inChannel);
	AJA_VIRTUAL bool		Get425FrameEnable (bool & outIsEnabled, NTV2Channel inChannel);
	AJA_VIRTUAL inline bool	Get425FrameEnable (bool * pOutIsEnabled, NTV2Channel inChannel)				{return pOutIsEnabled ? Get425FrameEnable (*pOutIsEnabled, inChannel) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool	SetReferenceVoltage (NTV2RefVoltage value);			///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	GetReferenceVoltage (NTV2RefVoltage* value);		///< @deprecated	This function is obsolete.
	#endif	//	!defined (NTV2_DEPRECATE)

	AJA_VIRTUAL bool		SetFrameBufferMode (NTV2Channel inChannel, NTV2FrameBufferMode inValue);
	AJA_VIRTUAL bool		GetFrameBufferMode (NTV2Channel inChannel, NTV2FrameBufferMode & outValue);
	AJA_VIRTUAL inline bool	GetFrameBufferMode (NTV2Channel inChannel, NTV2FrameBufferMode * pOutValue)		{return pOutValue ? GetFrameBufferMode (inChannel, *pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetFrameBufferQuarterSizeMode (NTV2Channel inChannel, NTV2QuarterSizeExpandMode inValue);
	AJA_VIRTUAL bool		GetFrameBufferQuarterSizeMode (NTV2Channel inChannel, NTV2QuarterSizeExpandMode & outValue);
	AJA_VIRTUAL inline bool	GetFrameBufferQuarterSizeMode (NTV2Channel inChannel, NTV2QuarterSizeExpandMode * pOutValue)	{return pOutValue ? GetFrameBufferQuarterSizeMode (inChannel, *pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetFrameBufferQuality (NTV2Channel inChannel, NTV2FrameBufferQuality inValue);
	AJA_VIRTUAL bool		GetFrameBufferQuality (NTV2Channel inChannel, NTV2FrameBufferQuality & outValue);
	AJA_VIRTUAL inline bool	GetFrameBufferQuality (NTV2Channel inChannel, NTV2FrameBufferQuality * pOutValue)	{return pOutValue ? GetFrameBufferQuality (inChannel, *pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetEncodeAsPSF (NTV2Channel inChannel, NTV2EncodeAsPSF inValue);
	AJA_VIRTUAL bool		GetEncodeAsPSF (NTV2Channel inChannel, NTV2EncodeAsPSF & outValue);
	AJA_VIRTUAL inline bool	GetEncodeAsPSF (NTV2Channel inChannel, NTV2EncodeAsPSF * pOutValue)					{return pOutValue ? GetEncodeAsPSF (inChannel, *pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetFrameBufferOrientation (NTV2Channel inChannel, NTV2VideoFrameBufferOrientation inValue);
	AJA_VIRTUAL bool		GetFrameBufferOrientation (NTV2Channel inChannel, NTV2VideoFrameBufferOrientation & outValue);
	AJA_VIRTUAL inline bool	GetFrameBufferOrientation (NTV2Channel inChannel, NTV2VideoFrameBufferOrientation * pOutValue)	{return pOutValue ? GetFrameBufferOrientation (inChannel, *pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetAlphaFromInput2Bit (ULWord inValue);
	AJA_VIRTUAL bool		GetAlphaFromInput2Bit (ULWord & outValue);
	AJA_VIRTUAL inline bool	GetAlphaFromInput2Bit (ULWord * pOutValue)										{return pOutValue ? GetAlphaFromInput2Bit (*pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetPCIAccessFrame (NTV2Channel inChannel, ULWord inValue, bool inWaitForVertical = true);
	AJA_VIRTUAL bool		GetPCIAccessFrame (NTV2Channel inChannel, ULWord & outValue);
	AJA_VIRTUAL inline bool	GetPCIAccessFrame (NTV2Channel inChannel, ULWord * pOutValue)					{return pOutValue ? GetPCIAccessFrame (inChannel, *pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetOutputFrame (NTV2Channel inChannel, ULWord value);
	AJA_VIRTUAL bool		GetOutputFrame (NTV2Channel inChannel, ULWord & outValue);
	AJA_VIRTUAL inline bool	GetOutputFrame (NTV2Channel inChannel, ULWord * pOutValue)						{return pOutValue ? GetOutputFrame (inChannel, *pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetInputFrame (NTV2Channel inChannel, ULWord value);
	AJA_VIRTUAL bool		GetInputFrame (NTV2Channel inChannel, ULWord & outValue);
	AJA_VIRTUAL inline bool	GetInputFrame (NTV2Channel inChannel, ULWord * pOutValue)						{return pOutValue ? GetInputFrame (inChannel, *pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetDualLinkOutputEnable (bool inIsEnabled);
	AJA_VIRTUAL bool		GetDualLinkOutputEnable (bool & outIsEnabled);
	AJA_VIRTUAL inline bool	GetDualLinkOutputEnable (bool * pOutIsEnabled)									{return pOutIsEnabled ? GetDualLinkOutputEnable (*pOutIsEnabled) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetDualLinkInputEnable (bool enable);
	AJA_VIRTUAL bool		GetDualLinkInputEnable (bool & outIsEnabled);
	AJA_VIRTUAL inline bool	GetDualLinkInputEnable (bool * pOutIsEnabled)									{return pOutIsEnabled ? GetDualLinkInputEnable (*pOutIsEnabled) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetVideoLimiting (NTV2VideoLimiting inValue);
	AJA_VIRTUAL bool		GetVideoLimiting (NTV2VideoLimiting & outValue);
	AJA_VIRTUAL inline bool	GetVideoLimiting (NTV2VideoLimiting * pOutValue)								{return pOutValue ? GetVideoLimiting (*pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetEnableVANCData (bool enable, bool wideVANC, NTV2Standard standard, NTV2FrameGeometry geometry, NTV2Channel channel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool		SetEnableVANCData (bool enable, bool wideVANC = false, NTV2Channel channel = NTV2_CHANNEL1);

	/**
		@brief		Retrieves the current VANC settings for the AJA device.
		@return		True if successful; otherwise false.
		@param[out]	outIsEnabled			Receives true if VANC is currently enabled on the device.
		@param[out]	outIsWideVANCEnabled	Receives true if "wide" VANC is currently enabled on the device.
		@param[in]	inChannel				Specifies the NTV2Channel of interest.
	**/
	AJA_VIRTUAL bool		GetEnableVANCData (bool & outIsEnabled, bool & outIsWideVANCEnabled, NTV2Channel inChannel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool		GetEnableVANCData (bool * pOutIsEnabled, bool * pOutIsWideVANCEnabled = NULL, NTV2Channel inChannel = NTV2_CHANNEL1);	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetVANCShiftMode (NTV2Channel inChannel, NTV2VANCDataShiftMode value);
	AJA_VIRTUAL bool		GetVANCShiftMode (NTV2Channel inChannel, NTV2VANCDataShiftMode & outValue);
	AJA_VIRTUAL inline bool	GetVANCShiftMode (NTV2Channel inChannel, NTV2VANCDataShiftMode * pOutValue)		{return pOutValue ? GetVANCShiftMode (inChannel, *pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool		SetPulldownMode (NTV2Channel inChannel, bool inValue);
	AJA_VIRTUAL bool		GetPulldownMode (NTV2Channel inChannel, bool & outValue);
	AJA_VIRTUAL inline bool	GetPulldownMode (NTV2Channel inChannel, bool * pOutValue)	{return pOutValue ? GetPulldownMode (inChannel, *pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	/**
		@brief	Swaps the values stored in the PCI access frame and output frame registers for the given frame store (channel).
		@param[in]	inChannel	Specifies the channel (frame store) of interest.
		@return	True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool		FlipFlopPage (NTV2Channel inChannel);


	/**
		@name	Mixer/Keyer & Video Processing
	**/
	///@{

	/**
		@brief		Sets the VANC source for the given mixer/keyer to the foreground video (or not).
		@return		True if successful; otherwise false.
		@param[in]	inWhichMixer			Specifies the mixer/keyer to be affected as a zero-based index number.
		@param[in]	inFromForegroundSource	If true, sets the mixer/keyer's VANC source to its foreground video input;
											otherwise, sets it to its background video input.
	**/
	AJA_VIRTUAL bool	SetMixerVancOutputFromForeground (const UWord inWhichMixer, const bool inFromForegroundSource = true);

	/**
		@brief		Answers whether or not the VANC source for the given mixer/keyer is currently the foreground video.
		@return		True if successful; otherwise false.
		@param[in]	inWhichMixer				Specifies the mixer/keyer of interest as a zero-based index number.
		@param[in]	outIsFromForegroundSource	Receives True if the mixer/keyer's VANC source is its foreground video input;
												otherwise False if it's its background video input.
	**/
	AJA_VIRTUAL bool	GetMixerVancOutputFromForeground (const UWord inWhichMixer, bool & outIsFromForegroundSource);


	/**
		@brief		Sets the foreground input control value for the given mixer/keyer.
		@return		True if successful; otherwise false.
		@param[in]	inWhichMixer		Specifies the mixer/keyer of interest as a zero-based index number.
		@param[in]	inInputControl		Specifies the mixer/keyer's foreground input control value.
	**/
	AJA_VIRTUAL bool	SetMixerFGInputControl (const UWord inWhichMixer, const NTV2MixerKeyerInputControl inInputControl);

	/**
		@brief		Returns the current foreground input control value for the given mixer/keyer.
		@return		True if successful; otherwise false.
		@param[in]	inWhichMixer		Specifies the mixer/keyer of interest as a zero-based index number.
		@param[in]	outInputControl		Receives the mixer/keyer's foreground input control value; otherwise NTV2MIXERINPUTCONTROL_INVALID upon failure.
	**/
	AJA_VIRTUAL bool	GetMixerFGInputControl (const UWord inWhichMixer, NTV2MixerKeyerInputControl & outInputControl);

	/**
		@brief		Sets the background input control value for the given mixer/keyer.
		@return		True if successful; otherwise false.
		@param[in]	inWhichMixer		Specifies the mixer/keyer of interest as a zero-based index number.
		@param[in]	inInputControl		Specifies the mixer/keyer's background input control value.
	**/
	AJA_VIRTUAL bool	SetMixerBGInputControl (const UWord inWhichMixer, const NTV2MixerKeyerInputControl inInputControl);

	/**
		@brief		Returns the current background input control value for the given mixer/keyer.
		@return		True if successful; otherwise false.
		@param[in]	inWhichMixer		Specifies the mixer/keyer of interest as a zero-based index number.
		@param[in]	outInputControl		Receives the mixer/keyer's background input control value; otherwise NTV2MIXERINPUTCONTROL_INVALID upon failure.
	**/
	AJA_VIRTUAL bool	GetMixerBGInputControl (const UWord inWhichMixer, NTV2MixerKeyerInputControl & outInputControl);

	/**
		@brief		Sets the mode for the given mixer/keyer.
		@return		True if successful; otherwise false.
		@param[in]	inWhichMixer		Specifies the mixer/keyer of interest as a zero-based index number.
		@param[in]	inMode				Specifies the mode value.
	**/
	AJA_VIRTUAL bool	SetMixerMode (const UWord inWhichMixer, const NTV2MixerKeyerMode inMode);

	/**
		@brief		Returns the current operating mode of the given mixer/keyer.
		@return		True if successful; otherwise false.
		@param[in]	inWhichMixer		Specifies the mixer/keyer of interest as a zero-based index number.
		@param[in]	outMode				Receives the mode value.
	**/
	AJA_VIRTUAL bool	GetMixerMode (const UWord inWhichMixer, NTV2MixerKeyerMode & outMode);

	/**
		@brief		Returns the current sync state of the given mixer/keyer.
		@return		True if successful; otherwise false.
		@param[in]	inWhichMixer		Specifies the mixer/keyer of interest as a zero-based index number.
		@param[in]	outIsSyncOK			Receives the mixer's current sync state. If true, the mixer is synchronized to its inputs.
										If false, the mixer is not synchronized to its inputs.
	**/
	AJA_VIRTUAL bool	GetMixerSyncStatus (const UWord inWhichMixer, bool & outIsSyncOK);

	AJA_VIRTUAL bool	WriteVideoProcessingControlCrosspoint (ULWord value);
	AJA_VIRTUAL bool	ReadVideoProcessingControlCrosspoint (ULWord *value);
	AJA_VIRTUAL bool	WriteSplitControl (ULWord value);
	AJA_VIRTUAL bool	ReadSplitControl (ULWord *value);
	AJA_VIRTUAL bool	ReadLineCount (ULWord *value);
	AJA_VIRTUAL bool	WritePanControl (ULWord value);
	AJA_VIRTUAL bool	ReadPanControl (ULWord *value);

	AJA_VIRTUAL bool	WriteVideoProcessingControl (ULWord value);
	AJA_VIRTUAL bool	ReadVideoProcessingControl (ULWord *value);
	AJA_VIRTUAL bool	WriteVideoProcessing2Control (ULWord value);
	AJA_VIRTUAL bool	ReadVideoProcessing2Control (ULWord *value);
	AJA_VIRTUAL bool	WriteVideoProcessing3Control (ULWord value);
	AJA_VIRTUAL bool	ReadVideoProcessing3Control (ULWord *value);
	AJA_VIRTUAL bool	WriteVideoProcessing4Control (ULWord value);
	AJA_VIRTUAL bool	ReadVideoProcessing4Control (ULWord *value);

	AJA_VIRTUAL bool	WriteMixerCoefficient (ULWord value);
	AJA_VIRTUAL bool	ReadMixerCoefficient (ULWord *value);
	AJA_VIRTUAL bool	WriteMixer2Coefficient (ULWord value);
	AJA_VIRTUAL bool	ReadMixer2Coefficient (ULWord *value);
	AJA_VIRTUAL bool	WriteMixer3Coefficient (ULWord value);
	AJA_VIRTUAL bool	ReadMixer3Coefficient (ULWord *value);
	AJA_VIRTUAL bool	WriteMixer4Coefficient (ULWord value);
	AJA_VIRTUAL bool	ReadMixer4Coefficient (ULWord *value);

	AJA_VIRTUAL bool	WriteFlatMatteValue (ULWord value);
	AJA_VIRTUAL bool	ReadFlatMatteValue (ULWord *value);
	AJA_VIRTUAL bool	WriteFlatMatte2Value (ULWord value);
	AJA_VIRTUAL bool	ReadFlatMatte2Value (ULWord *value);
	AJA_VIRTUAL bool	WriteFlatMatte3Value (ULWord value);
	AJA_VIRTUAL bool	ReadFlatMatte3Value (ULWord *value);
	AJA_VIRTUAL bool	WriteFlatMatte4Value (ULWord value);
	AJA_VIRTUAL bool	ReadFlatMatte4Value (ULWord *value);
	///@}


	/**
		@name	Audio
	**/
	///@{

	/**
		@brief		Sets the number of audio channels to be concurrently captured or played for a given audio system on the AJA device.
		@return		True if successful; otherwise false.
		@param[in]	inNumChannels		Specifies the number of audio channels the device will record or play to/from a
										given audio system. For most applications, this should always be set to the maximum
										number of audio channels the device is capable of capturing or playing, which can
										be obtained by calling the NTV2BoardGetMaxAudioChannels function (see ntv2devicefeatures.h).
		@param[in]	inAudioSystem	Optionally specifies the audio system of interest. Defaults to NTV2_AUDIOSYSTEM_1.
	**/
	AJA_VIRTUAL bool	SetNumberAudioChannels (const ULWord inNumChannels, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);

	/**
		@brief		Returns the current number of audio channels being captured or played by a given audio system on the AJA device.
		@return		True if successful; otherwise false.
		@param[out]	outNumChannels		Receives the number of audio channels that the AJA device hardware is currently capturing
										or playing for the given audio system. If the function result is true, the variable's
										contents will be valid, and for most AJA devices will be 6, 8, or 16.
		@param[in]	inAudioSystem		Optionally specifies the audio system of interest. Defaults to NTV2_AUDIOSYSTEM_1.
		@details	This function allows client applications to determine how many audio channels the AJA hardware is
					currently capturing/playing into/from the given audio system on the device.
	**/
	AJA_VIRTUAL bool	GetNumberAudioChannels (ULWord & outNumChannels, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);

	/**
		@brief		Sets the current audio sample rate for the given audio system on the AJA device.
		@return		True if successful; otherwise false.
		@param[in]	inRate			Specifies the desired audio sample rate for the given audio system.
									The specified rate value must be NTV2_AUDIO_48K or NTV2_AUDIO_96K.
		@param[in]	inAudioSystem	Optionally specifies the audio system of interest. Defaults to NTV2_AUDIOSYSTEM_1.
		@details	AJA devices generally use a 48 kHz audio sample rate. Many devices also support a 96 kHz sample rate,
					which is useful for "double speed" ingest or playout applications from tape equipment that is capable
					of playing or recording at twice the normal rate. This function call allows the client application to
					change the AJA device's audio sample rate for a given audio system.
	**/
	AJA_VIRTUAL bool	SetAudioRate (const NTV2AudioRate inRate, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);

	/**
		@brief		Retrieves the current audio sample rate for the given audio system on the AJA device.
		@return		True if successful; otherwise false.
		@param[out]	outRate			Receives the current audio sample rate for the given audio system. If the function
									result is true, the variable will contain one of the following values: NTV2_AUDIO_48K,
									or NTV2_AUDIO_96K.
		@param[in]	inAudioSystem	Optionally specifies the audio system of interest. Defaults to NTV2_AUDIOSYSTEM_1.
		@details	AJA devices usually use a 48 kHz audio sample rate, but many also support a 96 kHz sample rate, which
					is useful for "double speed" ingest or playout applications from tape equipment that is capable of
					playing or recording at twice the normal rate. This function call allows the client application to
					discover the AJA device's audio sample rate that's currently being used for a given audio system.
	**/
	AJA_VIRTUAL bool	GetAudioRate (NTV2AudioRate & outRate, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);

	/**
		@brief		Changes the size of the audio buffer that is used for a given audio system in the AJA device.
		@return		True if successful; otherwise false.
		@param[in]	inValue			Specifies the desired size of the capture/playout audio buffer to be used on the AJA device.
									All modern AJA devices use NTV2_AUDIO_BUFFER_BIG (4 MB).
		@param[in]	inAudioSystem	Optionally specifies the audio system of interest. Defaults to NTV2_AUDIOSYSTEM_1.
	**/
	AJA_VIRTUAL bool	SetAudioBufferSize (const NTV2AudioBufferSize inValue, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);

	/**
		@brief		Returns the size of the audio buffer being used for a given audio system on the AJA device.
		@return		True if successful; otherwise false.
		@param[out]	outSize			Receives the size of the capture/playout audio buffer for the given audio system on the AJA device.
		@param[in]	inAudioSystem	Optionally specifies the audio system of interest. Defaults to NTV2_AUDIOSYSTEM_1.
	**/
	AJA_VIRTUAL bool	GetAudioBufferSize (NTV2AudioBufferSize & outSize, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);


	AJA_VIRTUAL bool	SetAudioAnalogLevel (const NTV2AudioLevel value, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);
	AJA_VIRTUAL bool	GetAudioAnalogLevel (NTV2AudioLevel & outValue, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);
	AJA_VIRTUAL bool	SetAudioLoopBack (const NTV2AudioLoopBack value, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);
	AJA_VIRTUAL bool	GetAudioLoopBack (NTV2AudioLoopBack & outValue, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);
	AJA_VIRTUAL bool	SetEncodedAudioMode (const NTV2EncodedAudioMode value, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);
	AJA_VIRTUAL bool	GetEncodedAudioMode (NTV2EncodedAudioMode & outValue, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);
	AJA_VIRTUAL bool	SetEmbeddedAudioInput (const NTV2EmbeddedAudioInput value, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);
	AJA_VIRTUAL bool	GetEmbeddedAudioInput (NTV2EmbeddedAudioInput & outValue, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);
	AJA_VIRTUAL bool	SetEmbeddedAudioClock (const NTV2EmbeddedAudioClock value, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);
	AJA_VIRTUAL bool	GetEmbeddedAudioClock (NTV2EmbeddedAudioClock & outValue, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);
	AJA_VIRTUAL bool	GetAudioWrapAddress (ULWord & outWrapAddress, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);
	AJA_VIRTUAL bool	GetAudioReadOffset (ULWord & outReadOffset, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);

	#if !defined (NTV2_DEPRECATE)
		//	These functions dealt exclusively with audio systems, but unfortunately required channels to be passed into them.
		AJA_VIRTUAL bool	SetNumberAudioChannels(ULWord numChannels, NTV2Channel channel);			///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	GetNumberAudioChannels(ULWord *numChannels, NTV2Channel channel = NTV2_CHANNEL1);	///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	SetAudioRate(NTV2AudioRate value, NTV2Channel channel);						///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	GetAudioRate(NTV2AudioRate *value, NTV2Channel channel);					///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	SetAudioBufferSize(NTV2AudioBufferSize value, NTV2Channel channel);			///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	GetAudioBufferSize(NTV2AudioBufferSize *value, NTV2Channel channel);		///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	SetAudioAnalogLevel(NTV2AudioLevel value, NTV2Channel channel);				///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	GetAudioAnalogLevel(NTV2AudioLevel *value, NTV2Channel channel);			///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	SetAudioLoopBack(NTV2AudioLoopBack value, NTV2Channel channel);				///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	GetAudioLoopBack(NTV2AudioLoopBack *value, NTV2Channel channel);			///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	SetEncodedAudioMode(NTV2EncodedAudioMode value, NTV2Channel channel);		///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	GetEncodedAudioMode(NTV2EncodedAudioMode *value, NTV2Channel channel);		///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	SetEmbeddedAudioInput(NTV2EmbeddedAudioInput value, NTV2Channel channel);	///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	GetEmbeddedAudioInput(NTV2EmbeddedAudioInput *value, NTV2Channel channel);	///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	SetEmbeddedAudioClock(NTV2EmbeddedAudioClock value, NTV2Channel channel);	///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	GetEmbeddedAudioClock(NTV2EmbeddedAudioClock *value, NTV2Channel channel);	///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	GetAudioWrapAddress(ULWord *wrapAddress, NTV2Channel channel);				///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	GetAudioReadOffset(ULWord *readOffset, NTV2Channel channel);				///< @deprecated	Use the equivalent function that accepts an NTV2AudioSystem instead of an NTV2Channel.
		AJA_VIRTUAL bool	GetAverageAudioLevelChan1_2(ULWord *value);									///< @deprecated	This function is obsolete.
	#endif	//	!defined (NTV2_DEPRECATE)

	AJA_VIRTUAL bool	WriteAudioControl (ULWord value, NTV2Channel channel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool	ReadAudioControl (ULWord *value, NTV2Channel channel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool	WriteAudioSource (ULWord value, NTV2Channel channel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool	ReadAudioSource (ULWord *value, NTV2Channel channel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool	WriteAudioLastOut (ULWord value, NTV2Channel channel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool	ReadAudioLastOut (ULWord *value, NTV2Channel channel = NTV2_CHANNEL1);

	AJA_VIRTUAL bool	ReadAudioLastIn (ULWord *value, NTV2Channel channel = NTV2_CHANNEL1);	//	AudioInputLastAddress register represents the address of the last
																								//	byte of the last 128-byte audio sample written by the hardware

	AJA_VIRTUAL bool	SetAudioOutputMonitorSource (NTV2AudioMonitorSelect inValue, NTV2Channel inChannel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool	GetAudioOutputMonitorSource (NTV2AudioMonitorSelect & outValue, NTV2Channel & outChannel);
	AJA_VIRTUAL bool	GetAudioOutputMonitorSource (NTV2AudioMonitorSelect * pOutValue, NTV2Channel * pOutChannel = NULL);	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	/**
		@brief		Enables or disables the output of audio samples by the given audio engine, resetting
					the playback position to the start of the audio buffer.
		@return		True if successful; otherwise false.
		@param[in]	inAudioSystem	Specifies the audio system on the device to be affected.
		@param[in]	inEnable		If true,  audio samples will be produced by the audio system.
									If false, audio sample production is inhibited.
		@note		Calling this funcion with a true parameter has the side effect of resetting
					the current audio buffer pointer (as reported by ReadAudioLastOut) to zero.
					This can be useful for resynchronizing audio and video. If it is desired to
					stop sample production without resetting the pointer, use SetAudioOutputPause instead. 
	**/
	AJA_VIRTUAL bool	SetAudioOutputReset (const NTV2AudioSystem inAudioSystem, const bool inEnable);

	/**
		@brief		Answers whether or not the device's audio system is currently operating in the mode
					in which it is not producing audio output samples and the audio buffer pointer has
					been reset to zero.
		@return		True if successful; otherwise false.
		@param[in]	inAudioSystem		Specifies the audio system of interest.
		@param[in]	outEnable			A boolean variable that is to receive 'true' if the audio system
										is not producing output samples and the buffer pointer is zero,
										or 'false' if the audio system is operating normally.
	**/
	AJA_VIRTUAL bool	GetAudioOutputReset (const NTV2AudioSystem inAudioSystem, bool & outEnable);

	/**
		@brief		Enables or disables the output of audio samples and advancment of the audio buffer
					pointer of the given audio engine.
		@return		True if successful; otherwise false.
		@param[in]	inAudioSystem	Specifies the audio system on the device to be affected.
		@param[in]	inEnable		If true,  audio samples will be produced by the audio system.
									If false, audio sample production is inhibited.
		@note		If it is desired to reset the audio buffer pointer to the beginning of the
					buffer, use SetAudioOutputReset instead.
	**/
	AJA_VIRTUAL bool	SetAudioOutputPause (const NTV2AudioSystem inAudioSystem, const bool inEnable);

	/**
		@brief		Answers whether or not the device's audio system is currently operating in the mode
					in which it is not producing audio output samples and the audio buffer pointer
					is not advancing.
		@return		True if successful; otherwise false.
		@param[in]	inAudioSystem		Specifies the audio system of interest.
		@param[in]	outEnable			A boolean variable that is to receive 'true' if the audio system
										is not producing samples and the buffer pointer is not advancing,
										or 'false' if the audio system is operating normally.
	**/
	AJA_VIRTUAL bool	GetAudioOutputPause (const NTV2AudioSystem inAudioSystem, bool & outEnable);

	/**
		@brief		Enables or disables the input of audio samples by the given audio engine, resetting
					the playback position to the start of the audio buffer.
		@return		True if successful; otherwise false.
		@param[in]	inAudioSystem	Specifies the audio system on the device to be affected.
		@param[in]	inEnable		If true,  audio samples will be captured by the audio system.
									If false, audio sample capture is inhibited.
		@note		Calling this funcion with a true parameter has the side effect of resetting
					the current audio buffer pointer (as reported by ReadAudioLastIn) to zero.
					This can be useful for resynchronizing audio and video.
	**/
	AJA_VIRTUAL bool	SetAudioInputReset (const NTV2AudioSystem inAudioSystem, const bool inEnable);

	/**
		@brief		Answers whether or not the device's audio system is currently operating in the mode
					in which it is not capturing audio output samples and the audio buffer pointer has
					been reset to zero.
		@return		True if successful; otherwise false.
		@param[in]	inAudioSystem		Specifies the audio system of interest.
		@param[in]	outEnable			A boolean variable that is to receive 'true' if the audio system
										is not capturing output samples and the buffer pointer is zero,
										or 'false' if the audio system is operating normally.
	**/
	AJA_VIRTUAL bool	GetAudioInputReset (const NTV2AudioSystem inAudioSystem, bool & outEnable);

	/**
		@brief		Determines if the given audio system on the AJA device will configured to capture audio samples.
		@return		True if successful; otherwise false.
		@param[in]	inAudioSystem	Specifies the audio system of interest.
		@param[in]	inEnable		If true, the audio system will capture samples into memory, if not currently reset.
									If false, the audio system will not capture samples.
		@note		Applications that acquire exclusive use of the AJA device, set its "every frame services" mode
					to NTV2_OEM_TASKS, and use AutoCirculate won't need to call this function, since AutoCirculate
					configures the audio system automatically.
	**/
	AJA_VIRTUAL bool	SetAudioCaptureEnable (const NTV2AudioSystem inAudioSystem, const bool inEnable);

	/**
		@brief		Answers whether the audio system is configured for capturing audio samples
		@return		True if successful; otherwise false.
		@param[in]	inAudioSystem		Specifies the audio system of interest.
		@param[in]	outEnable			A boolean variable that is to receive 'true' if the audio system
										will capture samples to memory when not in reset mode,
										or 'false' if the audio system is inhibited from capturing samples.
	**/
	AJA_VIRTUAL bool	GetAudioCaptureEnable (const NTV2AudioSystem inAudioSystem, bool & outEnable);

	/**
		@brief		Enables or disables a special mode for the given audio system whereby its embedder and
					deembedder both start from the audio base address, instead of operating independently.
		@return		True if successful; otherwise false.
		@param[in]	inAudioSystem		Specifies the audio system on the device to be affected.
		@param[in]	inEnable			If true, de-embedder and embedder both start from the audio base address.
										If false, the audio system operates normally.
	**/
	AJA_VIRTUAL bool	SetAudioPlayCaptureModeEnable (const NTV2AudioSystem inAudioSystem, const bool inEnable);

	/**
		@brief		Answers whether or not the device's audio system is currently operating in a special mode
					in which its embedder and deembedder both start from the audio base address, instead of
					operating independently.
		@return		True if successful; otherwise false.
		@param[in]	inAudioSystem		Specifies the audio system of interest.
		@param[in]	outEnable			A boolean variable that is to receive 'true' if the audio system's
										de-embedder and embedder both start from the audio base address,
										or 'false' if the audio system is operating normally.
	**/
	AJA_VIRTUAL bool	GetAudioPlayCaptureModeEnable (const NTV2AudioSystem inAudioSystem, bool & outEnable);

	/**
		@brief		Sets the audio input delay for the given audio subsystem on the device.
		@param[in]	inAudioSystem	Specifies the audio subsystem whose input delay is to be set.
		@param[in]	inDelay			Specifies the new audio input delay value for the audio subsystem.
									Each increment of this value increases the delay by exactly 512 bytes
									in the audio subsystem's audio buffer, or about 166.7 microseconds.
									Values from 0 thru 8159 are valid, which gives a maximum delay of about
									1.36 seconds.
		@return		True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	SetAudioInputDelay (const NTV2AudioSystem inAudioSystem, const ULWord inDelay);

	/**
		@brief		Answers with the audio input delay for the given audio subsystem on the device.
		@param[in]	inAudioSystem	Specifies the audio subsystem whose input delay is to be retrieved.
		@param[out]	outDelay		A ULWord variable that is to receive the audio subsystem's current input delay
									value, expressed as an integral number of 512 byte chunks in the audio subsystem's
									audio buffer on the device. This can be translated into microseconds by multiplying
									the received value by 166.7.
		@return		True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	GetAudioInputDelay (const NTV2AudioSystem inAudioSystem, ULWord & outDelay);


	/**
		@brief		Sets the audio output delay for the given audio subsystem on the device.
		@param[in]	inAudioSystem	Specifies the audio subsystem whose output delay is to be set.
		@param[in]	inDelay			Specifies the new audio output delay value for the audio subsystem.
									Each increment of this value increases the delay by exactly 512 bytes
									in the audio subsystem's audio buffer, or about 166.7 microseconds.
									Values from 0 thru 8159 are valid, which gives a maximum delay of about
									1.36 seconds.
		@return		True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	SetAudioOutputDelay (const NTV2AudioSystem inAudioSystem, const ULWord inDelay);

	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool	GetAudioPlayCaptureModeEnable (const NTV2AudioSystem inAudioSystem, bool * pOutEnable);		///< @deprecated	Use GetAudioPlayCaptureModeEnable(NTV2AudioSystem,bool&) instead.
		AJA_VIRTUAL bool	GetAudioInputDelay (const NTV2AudioSystem inAudioSystem, ULWord * pOutDelay);		///< @deprecated	Use GetAudioInputDelay(NTV2AudioSystem,ULWord&) instead.
		AJA_VIRTUAL bool	GetAudioOutputDelay (const NTV2AudioSystem inAudioSystem, ULWord * pOutDelay);		///< @deprecated	Use GetAudioOutputDelay(NTV2AudioSystem,ULWord&) instead.
	#endif	//	!defined (NTV2_DEPRECATE)

	/**
		@brief		Answers with the audio output delay for the given audio subsystem on the device.
		@param[in]	inAudioSystem	Specifies the audio subsystem whose output delay is to be retrieved.
		@param[out]	outDelay		A ULWord variable that is to receive the audio subsystem's current output delay
									value, expressed as an integral number of 512 byte chunks in the audio subsystem's
									audio buffer on the device. This can be translated into microseconds by multiplying
									the received value by 166.7.
		@return		True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool		GetAudioOutputDelay (const NTV2AudioSystem inAudioSystem, ULWord & outDelay);


	/**
		@brief		Determines whether the given audio subsystem on the device is treated as normal PCM audio or not.
		@param[in]	inAudioSystem	Specifies the audio subsystem of interest.
		@param[in]	inIsNonPCM		If true, all audio channels in the audio subsystem are treated as non-PCM;
									otherwise, they're treated as normal PCM audio.
		@return		True if successful;  otherwise false.
		@note		This setting, if non-PCM, overrides per-audio-channel-pair PCM control on those devices that support it.
	**/
	AJA_VIRTUAL bool		SetAudioPCMControl (const NTV2AudioSystem inAudioSystem, const bool inIsNonPCM);


	/**
		@brief		Answers whether or not the given audio subsystem on the device is being treated as normal PCM audio.
		@param[in]	inAudioSystem	Specifies the audio subsystem of interest.
		@param[out]	outIsNonPCM		Receives true if all audio channels in the audio subsystem are being treated as non-PCM;
									otherwise false if they're being treated as normal PCM audio.
		@return		True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool		GetAudioPCMControl (const NTV2AudioSystem inAudioSystem, bool & outIsNonPCM);


	/**
		@brief		Determines whether or not the given channel pair in the given audio subsystem on the device is treated as normal PCM audio.
		@param[in]	inAudioSystem	Specifies the audio subsystem of interest.
		@param[in]	inChannelPair	Specifies the channel pair of interest.
		@param[in]	inIsNonPCM		If true, the channel pair is treated as non-PCM;
									otherwise, it's treated as normal PCM audio.
		@return		True if successful;  otherwise false.
		@note		Call ::NTV2DeviceCanDoPCMControl to determine if per-audio-channel-pair PCM control capability is available on this device.
		@note		This function has no effect if the per-audio-subsystem PCM control setting is set to non-PCM.
					(See the overloaded function SetAudioPCMControl(const NTV2AudioSystem, const bool).)
	**/
	AJA_VIRTUAL bool		SetAudioPCMControl (const NTV2AudioSystem inAudioSystem, const NTV2AudioChannelPair inChannelPair, const bool inIsNonPCM);


	/**
		@brief		Answers whether or not the given channel pair in the given audio subsystem on the device is being treated as normal PCM audio.
		@param[in]	inAudioSystem	Specifies the audio subsystem of interest.
		@param[in]	inChannelPair	Specifies the channel pair of interest.
		@param[out]	outIsNonPCM		Receives true if the channel pair is being treated as non-PCM;
									otherwise false if it's being treated as normal PCM audio.
		@return		True if successful; otherwise false.
		@note		Call ::NTV2DeviceCanDoPCMControl to determine if this device supports per-audio-channel-pair PCM control.
		@note		This function's answer is irrelevant if the per-audio-subsystem PCM control setting is set to non-PCM.
					(See the overloaded function SetAudioPCMControl(const NTV2AudioSystem, const bool).)
	**/
	AJA_VIRTUAL bool		GetAudioPCMControl (const NTV2AudioSystem inAudioSystem, const NTV2AudioChannelPair inChannelPair, bool & outIsNonPCM);


	/**
		@brief		Sets the audio source for the given audio system on the device.
		@param[in]	inAudioSystem		Specifies the audio system of interest on the device (e.g., NTV2_AUDIOSYSTEM_1, NTV2_AUDIOSYSTEM_2, etc.).
										(Use the ::NTV2BoardGetNumAudioStreams function to determine how many independent audio systems are available on the device.)
		@param[in]	inAudioSource		Specifies the audio source to use for the given audio system (e.g., NTV2_AUDIO_EMBEDDED, NTV2_AUDIO_AES, NTV2_AUDIO_ANALOG, etc.).
		@param[in]	inEmbeddedInput		If the audio source is set to NTV2_AUDIO_EMBEDDED, and the device has multiple SDI inputs, use inEmbeddedInput
										to specify which NTV2EmbeddedAudioInput to use. This parameter is ignored if the inAudioSource is not NTV2_AUDIO_EMBEDDED.
		@return		True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool	SetAudioSystemInputSource (const NTV2AudioSystem inAudioSystem, const NTV2AudioSource inAudioSource, const NTV2EmbeddedAudioInput inEmbeddedInput);

	#if !defined (NTV2_DEPRECATE_12_3)
		AJA_VIRTUAL bool	SetAudioSystemInputSource (const NTV2AudioSystem inAudioSystem, const NTV2AudioSource inAudioSource);
		AJA_VIRTUAL bool	SetAudioSystemInputSource (const NTV2AudioSystem inAudioSystem, const NTV2InputSource inInputSource);
	#endif	//	!defined (NTV2_DEPRECATE_12_3)

	/**
		@brief		Answers with the current audio source for the given audio system on the device.
		@param[in]	inAudioSystem	Specifies the audio system of interest on the device (e.g., NTV2_AUDIOSYSTEM_1, NTV2_AUDIOSYSTEM_2, etc.).
									(Use the ::NTV2BoardGetNumAudioStreams function to determine how many independent audio systems are available on the device.)
		@param[out]	outAudioSource	Receives the audio source that's currently being used for the given audio system (e.g., NTV2_AUDIO_EMBEDDED, NTV2_AUDIO_AES, NTV2_AUDIO_ANALOG, etc.).
		@param[out]	outEmbeddedSource	If the audio source is NTV2_AUDIO_EMBEDDED, outEmbeddedSource will be the SDI input it is configured for.
		@return		True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool GetAudioSystemInputSource (const NTV2AudioSystem inAudioSystem, NTV2AudioSource & outAudioSource, NTV2EmbeddedAudioInput & outEmbeddedSource);

	#if !defined (NTV2_DEPRECATE_12_3)
		AJA_VIRTUAL bool	GetAudioSystemInputSource (const NTV2AudioSystem inAudioSystem, NTV2AudioSource & outAudioSource);
		AJA_VIRTUAL bool	GetAudioSystemInputSource (const NTV2AudioSystem inAudioSystem, NTV2AudioSource * pOutAudioSource);
	#endif	//	!defined (NTV2_DEPRECATE_12_3)

	/**
		@brief		Sets the device's audio system that will provide audio for the given SDI output's audio embedder.
		@param[in]	inChannel		Specifies the SDI output as an NTV2Channel (e.g., NTV2_CHANNEL1 == SDIOut1, NTV2_CHANNEL2 == SDIOut2, etc.)
		@param[in]	inAudioSystem	Specifies the audio system that is to be used by the SDI output's embedder (e.g., NTV2_AUDIOSYSTEM_1).
		@return		True if successful; otherwise false.
		@note		Use the NTV2BoardGetNumAudioStreams function to determine how many independent audio systems are available on the device.
	**/
	AJA_VIRTUAL bool	SetSDIOutputAudioSystem (const NTV2Channel inChannel, const NTV2AudioSystem inAudioSystem);

	/**
		@brief		Answers with the device's audio system that is currently providing audio for the given SDI output's audio embedder.
		@param[in]	inChannel		Specifies the SDI output of interest as an NTV2Channel (e.g., NTV2_CHANNEL1 == SDIOut1, NTV2_CHANNEL2 == SDIOut2, etc.)
		@param[in]	outAudioSystem	Receives the audio system that is being used by the SDI output's embedder (e.g., NTV2_AUDIOSYSTEM_1).
		@return		True if successful; otherwise false.
		@note		Use the NTV2BoardGetNumAudioStreams function to determine how many independent audio systems are available on the device.
	**/
	AJA_VIRTUAL bool	GetSDIOutputAudioSystem (const NTV2Channel inChannel, NTV2AudioSystem & outAudioSystem);

	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool	SetSDIOutAudioSource (const ULWord inValue, const NTV2Channel channel = NTV2_CHANNEL1);	///< @deprecated	Use SetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetSDIOutAudioSource (ULWord & outValue, const NTV2Channel channel = NTV2_CHANNEL1);	///< @deprecated	Use GetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
		AJA_VIRTUAL bool	SetK2SDI1OutAudioSource(ULWord value);	///< @deprecated	Use SetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetK2SDI1OutAudioSource(ULWord* value);	///< @deprecated	Use GetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
		AJA_VIRTUAL bool	SetK2SDI2OutAudioSource(ULWord value);	///< @deprecated	Use SetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetK2SDI2OutAudioSource(ULWord* value);	///< @deprecated	Use GetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
		AJA_VIRTUAL bool	SetK2SDI3OutAudioSource(ULWord value);	///< @deprecated	Use SetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetK2SDI3OutAudioSource(ULWord* value);	///< @deprecated	Use GetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
		AJA_VIRTUAL bool	SetK2SDI4OutAudioSource(ULWord value);	///< @deprecated	Use SetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetK2SDI4OutAudioSource(ULWord* value);	///< @deprecated	Use GetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
		AJA_VIRTUAL bool	SetK2SDI5OutAudioSource(ULWord value);	///< @deprecated	Use SetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetK2SDI5OutAudioSource(ULWord* value);	///< @deprecated	Use GetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
		AJA_VIRTUAL bool	SetK2SDI6OutAudioSource(ULWord value);	///< @deprecated	Use SetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetK2SDI6OutAudioSource(ULWord* value);	///< @deprecated	Use GetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
		AJA_VIRTUAL bool	SetK2SDI7OutAudioSource(ULWord value);	///< @deprecated	Use SetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetK2SDI7OutAudioSource(ULWord* value);	///< @deprecated	Use GetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
		AJA_VIRTUAL bool	SetK2SDI8OutAudioSource(ULWord value);	///< @deprecated	Use SetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetK2SDI8OutAudioSource(ULWord* value);	///< @deprecated	Use GetSDIOutputAudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
	#endif	//	!defined (NTV2_DEPRECATE)

	/**
		@brief		Sets the device's audio system that will provide audio for the given SDI output's audio embedder for the 2nd data stream on a dual-link output.
		@param[in]	inChannel		Specifies the SDI output as an NTV2Channel (e.g., NTV2_CHANNEL1 == SDIOut1, NTV2_CHANNEL2 == SDIOut2, etc.)
		@param[in]	inAudioSystem	Specifies the audio system that is to be used by the SDI output's embedder (e.g., NTV2_AUDIOSYSTEM_1).
		@return		True if successful; otherwise false.
		@note		Use the NTV2BoardGetNumAudioStreams function to determine how many independent audio systems are available on the device.
	**/
	AJA_VIRTUAL bool	SetSDIOutputDS2AudioSystem (const NTV2Channel inChannel, const NTV2AudioSystem inAudioSystem);

	/**
		@brief		Answers with the device's audio system that is currently providing audio for the given SDI output's audio embedder for the 2nd data stream on a dual-link output.
		@param[in]	inChannel		Specifies the SDI output of interest as an NTV2Channel (e.g., NTV2_CHANNEL1 == SDIOut1, NTV2_CHANNEL2 == SDIOut2, etc.)
		@param[in]	outAudioSystem	Receives the audio system that is being used by the SDI output's embedder (e.g., NTV2_AUDIOSYSTEM_1).
		@return		True if successful; otherwise false.
		@note		Use the NTV2BoardGetNumAudioStreams function to determine how many independent audio systems are available on the device.
	**/
	AJA_VIRTUAL bool	GetSDIOutputDS2AudioSystem (const NTV2Channel inChannel, NTV2AudioSystem & outAudioSystem);

	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool	SetSDIOutDS2AudioSource (const ULWord inValue, const NTV2Channel channel = NTV2_CHANNEL1);	///< @deprecated	Use SetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetSDIOutDS2AudioSource (ULWord & outValue, const NTV2Channel channel = NTV2_CHANNEL1);	///< @deprecated	Use GetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
		AJA_VIRTUAL bool	SetK2SDI1OutDS2AudioSource(ULWord value);	///< @deprecated	Use SetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetK2SDI1OutDS2AudioSource(ULWord* value);	///< @deprecated	Use GetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
		AJA_VIRTUAL bool	SetK2SDI2OutDS2AudioSource(ULWord value);	///< @deprecated	Use SetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetK2SDI2OutDS2AudioSource(ULWord* value);	///< @deprecated	Use GetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
		AJA_VIRTUAL bool	SetK2SDI3OutDS2AudioSource(ULWord value);	///< @deprecated	Use SetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetK2SDI3OutDS2AudioSource(ULWord* value);	///< @deprecated	Use GetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
		AJA_VIRTUAL bool	SetK2SDI4OutDS2AudioSource(ULWord value);	///< @deprecated	Use SetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetK2SDI4OutDS2AudioSource(ULWord* value);	///< @deprecated	Use GetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
		AJA_VIRTUAL bool	SetK2SDI5OutDS2AudioSource(ULWord value);	///< @deprecated	Use SetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetK2SDI5OutDS2AudioSource(ULWord* value);	///< @deprecated	Use GetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
		AJA_VIRTUAL bool	SetK2SDI6OutDS2AudioSource(ULWord value);	///< @deprecated	Use SetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetK2SDI6OutDS2AudioSource(ULWord* value);	///< @deprecated	Use GetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
		AJA_VIRTUAL bool	SetK2SDI7OutDS2AudioSource(ULWord value);	///< @deprecated	Use SetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetK2SDI7OutDS2AudioSource(ULWord* value);	///< @deprecated	Use GetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
		AJA_VIRTUAL bool	SetK2SDI8OutDS2AudioSource(ULWord value);	///< @deprecated	Use SetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem) instead.
		AJA_VIRTUAL bool	GetK2SDI8OutDS2AudioSource(ULWord* value);	///< @deprecated	Use GetSDIOutputDS2AudioSystem(NTV2Channel,NTV2AudioSystem&) instead.
	#endif	//	!defined (NTV2_DEPRECATE)

	#if defined (HAS_PCM_DETECT_SUPPORT_IN_FIRMWARE)
		/**
			@brief		For the given SDI input (specified as a channel number), answers if the specified audio channel pair is currently PCM-encoded or not.
			@param[in]	inSDIInputChannel	Specifies the SDI input of interest.
			@param[in]	inAudioChannelPair	Specifies the audio channel pair of interest.
			@param[out]	outIsPCM			Receives true if the channel pair is currently PCM-encoded;  otherwise false.
			@return		True if successful;  otherwise false.
		**/
		virtual bool InputAudioChannelPairHasPCM (const NTV2Channel inSDIInputChannel, const NTV2AudioChannelPair inAudioChannelPair, bool & outHasPCM);

		/**
			@brief		For the given SDI input (specified as a channel number), returns the set of audio channel pairs that are currently PCM-encoded.
			@param[in]	inSDIInputChannel		Specifies the SDI input of interest.
			@param[out]	outChannelPairs		Receives the channel pairs that are currently PCM-encoded.
			@return		True if successful;  otherwise false.
		**/
		virtual bool GetInputAudioChannelPairsWithPCM (const NTV2Channel inSDIInputChannel, NTV2AudioChannelPairs & outChannelPairs);

		/**
			@brief		For the given SDI input (specified as a channel number), returns the set of audio channel pairs that are currently not PCM-encoded.
			@param[in]	inSDIInputChannel		Specifies the SDI input of interest.
			@param[out]	outChannelPairs		Receives the channel pairs that are not currently PCM-encoded.
			@return		True if successful;  otherwise false.
		**/
		virtual bool GetInputAudioChannelPairsWithoutPCM (const NTV2Channel inSDIInputChannel, NTV2AudioChannelPairs & outChannelPairs);
	#endif	//	defined (HAS_PCM_DETECT_SUPPORT_IN_FIRMWARE)
	///@}

	//
	//	Read/Write Particular Register routines
	//
	AJA_VIRTUAL bool	WriteGlobalControl (ULWord value);
	AJA_VIRTUAL bool	ReadGlobalControl (ULWord *value);

	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool	WriteCh1Control (ULWord value);				///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	ReadCh1Control (ULWord *value);				///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	WriteCh1PCIAccessFrame (ULWord value);		///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	ReadCh1PCIAccessFrame (ULWord *value);		///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	WriteCh1OutputFrame (ULWord value);			///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	ReadCh1OutputFrame (ULWord *value);			///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	WriteCh1InputFrame (ULWord value);			///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	ReadCh1InputFrame (ULWord *value);			///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	WriteCh2Control (ULWord value);				///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	ReadCh2Control (ULWord *value);				///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	WriteCh2PCIAccessFrame (ULWord value);		///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	ReadCh2PCIAccessFrame (ULWord *value);		///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	WriteCh2OutputFrame (ULWord value);			///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	ReadCh2OutputFrame (ULWord *value);			///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	WriteCh2InputFrame (ULWord value);			///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	ReadCh2InputFrame (ULWord *value);			///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	WriteCh3Control (ULWord value);				///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	ReadCh3Control (ULWord *value);				///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	WriteCh3PCIAccessFrame (ULWord value);		///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	ReadCh3PCIAccessFrame (ULWord *value);		///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	WriteCh3OutputFrame (ULWord value);			///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	ReadCh3OutputFrame (ULWord *value);			///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	WriteCh3InputFrame (ULWord value);			///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	ReadCh3InputFrame (ULWord *value);			///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	WriteCh4Control (ULWord value);				///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	ReadCh4Control (ULWord *value);				///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	WriteCh4PCIAccessFrame (ULWord value);		///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	ReadCh4PCIAccessFrame (ULWord *value);		///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	WriteCh4OutputFrame (ULWord value);			///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	ReadCh4OutputFrame (ULWord *value);			///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	WriteCh4InputFrame (ULWord value);			///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	ReadCh4InputFrame (ULWord *value);			///< @deprecated	This function is obsolete.
	#endif	//	!defined (NTV2_DEPRECATE)


	/**
		@name	Programming
	**/
	///@{
	AJA_VIRTUAL bool	ReadFlashProgramControl(ULWord *value);
	AJA_VIRTUAL bool	IsXilinxProgrammed();
	AJA_VIRTUAL bool	ProgramMainFlash(const char *fileName);
	AJA_VIRTUAL bool	EraseFlashBlock(ULWord numSectors, ULWord sectorSize);
	AJA_VIRTUAL bool	CheckFlashErased(ULWord numSectors);
	AJA_VIRTUAL bool	VerifyMainFlash(const char *fileName);
	AJA_VIRTUAL bool	GetProgramStatus(SSC_GET_FIRMWARE_PROGRESS_STRUCT *statusStruct);
	AJA_VIRTUAL bool	WaitForFlashNOTBusy();
	///@}

	//
	//	OEM Mapping to Userspace Functions
	//
	AJA_VIRTUAL bool	GetBaseAddress (NTV2Channel channel, ULWord **pBaseAddress);
	AJA_VIRTUAL bool	GetBaseAddress (ULWord **pBaseAddress);
	AJA_VIRTUAL bool	GetRegisterBaseAddress (ULWord regNumber, ULWord ** pRegAddress);
	AJA_VIRTUAL bool	GetXena2FlashBaseAddress (ULWord ** pXena2FlashAddress);

	//
	//	Read-Only Status Registers
	//
	AJA_VIRTUAL bool	ReadStatusRegister (ULWord *value);
	AJA_VIRTUAL bool	ReadStatus2Register (ULWord *value);
	AJA_VIRTUAL bool	ReadInputStatusRegister (ULWord *value);
	AJA_VIRTUAL bool	ReadInputStatus2Register (ULWord *value);
	AJA_VIRTUAL bool	ReadInput56StatusRegister (ULWord *value);
	AJA_VIRTUAL bool	ReadInput78StatusRegister (ULWord *value);
	AJA_VIRTUAL bool	Read3GInputStatusRegister(ULWord *value);
	AJA_VIRTUAL bool	Read3GInputStatus2Register(ULWord *value);
	AJA_VIRTUAL bool	Read3GInput5678StatusRegister(ULWord *value);

	AJA_VIRTUAL bool	ReadSDIInVPID (const NTV2Channel channel, ULWord & outValueA, ULWord & outValueB);
	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool	ReadSDIInVPID(NTV2Channel channel, ULWord* valueA, ULWord* valueB = NULL);	///< @deprecated	Use ReadSDIInVPID(NTV2Channel,ULWord&,ULWord&) instead
		AJA_VIRTUAL bool	ReadSDI1InVPID(ULWord* valueA, ULWord* valueB = NULL);						///< @deprecated	Use ReadSDIInVPID(NTV2Channel,ULWord&,ULWord&) instead
		AJA_VIRTUAL bool	ReadSDI2InVPID(ULWord* valueA, ULWord* valueB = NULL);						///< @deprecated	Use ReadSDIInVPID(NTV2Channel,ULWord&,ULWord&) instead
		AJA_VIRTUAL bool	ReadSDI3InVPID(ULWord* valueA, ULWord* valueB = NULL);						///< @deprecated	Use ReadSDIInVPID(NTV2Channel,ULWord&,ULWord&) instead
		AJA_VIRTUAL bool	ReadSDI4InVPID(ULWord* valueA, ULWord* valueB = NULL);						///< @deprecated	Use ReadSDIInVPID(NTV2Channel,ULWord&,ULWord&) instead
	#endif	//	!defined (NTV2_DEPRECATE)

	AJA_VIRTUAL bool	SupportsP2PTransfer ();
	AJA_VIRTUAL bool	SupportsP2PTarget ();


	/**
		@name	On-Device LEDs
	**/
	///@{
	/**
		@brief	The four on-board LEDs can be set by writing 0-15
		@param[in]	inValue		Sets the state of the four on-board LEDs using the least significant
								four bits of the given ULWord value.
		@return	True if successful;  otherwise, false.
	**/
	AJA_VIRTUAL bool	SetLEDState (ULWord inValue);

	/**
		@brief	Answers with the current state of the four on-board LEDs.
		@param[out]	outValue	Receives the current state of the four on-board LEDs.
								Only the least significant four bits of the ULWord have any meaning.
		@return	True if successful;  otherwise, false.
	**/
	AJA_VIRTUAL bool			GetLEDState (ULWord & outValue);

	AJA_VIRTUAL inline  bool	GetLEDState (ULWord * pOutValue)											{return pOutValue ? GetLEDState (*pOutValue) : false;}
	///@}


	/**
		@name	RP-188
	**/
	///@{
	/**
		@brief		Sets the current RP188 mode -- NTV2_RP188_INPUT or NTV2_RP188_OUTPUT -- for the given channel.
		@param[in]	inChannel		Specifies the channel of interest.
		@param[in]	inMode			Specifies the new RP-188 mode for the given channel.
									Must be one of NTV2_RP188_INPUT or NTV2_RP188_OUTPUT. All other values are illegal.
		@return		True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	SetRP188Mode			(const NTV2Channel inChannel,	const NTV2_RP188Mode inMode);

	/**
		@brief		Returns the current RP188 mode -- NTV2_RP188_INPUT or NTV2_RP188_OUTPUT -- for the given channel.
		@param[in]	inChannel		Specifies the channel of interest.
		@param[out]	outMode			Receives the RP-188 mode for the given channel.
		@return		True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	GetRP188Mode			(const NTV2Channel inChannel,	NTV2_RP188Mode & outMode);

	/**
		@note		This needs to be documented.
	**/
	AJA_VIRTUAL bool	SetRP188Data			(const NTV2Channel inChannel,	const ULWord frame, const RP188_STRUCT & inRP188Data);

	/**
		@note		This needs to be documented.
	**/
	AJA_VIRTUAL bool	GetRP188Data			(const NTV2Channel inChannel,	const ULWord frame, RP188_STRUCT & outRP188Data);

	/**
		@note		This needs to be documented.
	**/
	AJA_VIRTUAL bool	SetRP188Source			(const NTV2Channel inChannel,	const ULWord value);

	/**
		@brief		Sets the current SDI input source being used to acquire RP188 timecode for the given (output) channel,
					assuming the channel's RP188 bypass mode is enabled (i.e., E-E operation).
		@param[in]	inChannel		Specifies the output channel of interest.
		@param[in]	inSource		Specifies the given output channel's current RP188 SDI input source.
		@note		Devices having more than 4 channels restrict the SDI input choices to banks of four inputs. For example,
					channels NTV2_CHANNEL1 thru NTV2_CHANNEL4 can only select from NTV2_INPUTSOURCE_SDI1 thru NTV2_INPUTSOURCE_SDI4,
					while channels NTV2_CHANNEL5 thru NTV2_CHANNEL8 can only select from NTV2_INPUTSOURCE_SDI5 thru NTV2_INPUTSOURCE_SDI8.
		@return		True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	SetRP188Source			(const NTV2Channel inChannel,	const NTV2InputSource inSource);

	/**
		@brief		Returns the current SDI source being used to acquire RP188 timecode for the given (output) channel,
					assuming the channel's RP188 bypass mode is enabled (i.e., for E-E operation).
		@param[in]	inChannel		Specifies the channel of interest.
		@param[out]	outValue		Receives the given channel's current RP188 SDI input source, which will be a value between 0 and 3.
									For channels NTV2_CHANNEL1 thru NTV2_CHANNEL4, this value will identify SDI inputs 1 thru 4.
									For channels NTV2_CHANNEL5 thru NTV2_CHANNEL8, this value will identify SDI inputs 5 thru 8.
		@return		True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	GetRP188Source			(const NTV2Channel inChannel,	ULWord & outValue);

	/**
		@brief		Returns the current SDI source being used to acquire RP188 timecode for the given (output) channel,
					assuming the channel's RP188 bypass mode is enabled (i.e., E-E operation).
		@param[in]	inChannel		Specifies the output channel of interest.
		@param[out]	outSource		Receives the given channel's current RP188 SDI input source.
		@return		True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	GetRP188Source			(const NTV2Channel inChannel,	NTV2InputSource & outSource);

	/**
		@brief		Answers whether or not the channel's RP-188 bypass mode is in effect.
		@param[in]	inChannel	Specifies the output channel of interest.
		@param[out]	outIsBypassEnabled	Receives true if the output channel's RP188 timecode is currently sourced from the channel's
										RP188 registers (i.e., from calls to SetRP188Data). Receives false if its output timecode is
										currently sourced from the input (specified from a prior call to SetRP188Source).
		@return		True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	IsRP188BypassEnabled	(const NTV2Channel inChannel,	bool & outIsBypassEnabled);

	/**
		@brief		Normally, if the RP188 mode is NTV2_RP188_OUTPUT, an SDI output will embed RP188 timecode as fed into its three
					DBB/Bits0_31/Bits32_63 registers (via calls to SetRP188Data). These registers can be bypassed to grab RP188 from an input, which
					is useful in E-E mode.
		@param[in]	inChannel			Specifies the SDI output channel of interest.
		@param[in]	inBypassDisabled	Specify 'true' to disable bypass mode, which will only embed RP188 as written into the channel's
										RP188 registers (i.e., via calls to SetRP188Data). Specify 'false' to enable bypass mode, which
										will only embed timecode that's been decoded from an SDI input (as specified by a prior call to
										SetRP188Source).
		@return		True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool	DisableRP188Bypass		(const NTV2Channel inChannel,	const bool inBypassDisabled);

		AJA_VIRTUAL inline bool	GetRP188Mode (NTV2Channel inChannel, NTV2_RP188Mode * pOutMode)
									{return pOutMode ? GetRP188Mode (inChannel, *pOutMode) : false;}	///< @deprecated	Use GetRP188Mode(const NTV2Channel, NTV2_RP188Mode &) instead.
		AJA_VIRTUAL inline bool	GetRP188Data (NTV2Channel inChannel, ULWord inFrame, RP188_STRUCT * pOutRP188Data)
									{return pOutRP188Data ? GetRP188Data (inChannel, inFrame, *pOutRP188Data) : false;}		///< @deprecated	Use GetRP188Mode(const NTV2Channel, const ULWord, RP188_STRUCT &) instead.
		AJA_VIRTUAL inline bool	GetRP188Source (NTV2Channel inChannel, ULWord * pOutValue)
									{return pOutValue ? GetRP188Source (inChannel, *pOutValue) : false;}	///< @deprecated	Use GetRP188Mode(const NTV2Channel, ULWord &) instead.
	///@}


	//
	//	RegisterAccess Control
	//
	AJA_VIRTUAL bool	SetRegisterWritemode (NTV2RegisterWriteMode inValue, const NTV2Channel inChannel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool	GetRegisterWritemode (NTV2RegisterWriteMode & outValue, const NTV2Channel inChannel = NTV2_CHANNEL1);
	AJA_VIRTUAL inline bool	GetRegisterWritemode (NTV2RegisterWriteMode * pOutValue, NTV2Channel inChannel = NTV2_CHANNEL1)		{return pOutValue ? GetRegisterWritemode (*pOutValue, inChannel) : false;}


	/**
		@name	Interrupts & Events
	**/
	///@{
	//
	//	Enable Interrupt/Event
	//
	AJA_VIRTUAL bool	EnableInterrupt (const INTERRUPT_ENUMS inEventCode);							//	GENERIC!

	/**
		@brief		Allows the CNTV2Card instance to wait for and respond to vertical blanking interrupts
					originating from the given output channel on the receiver's AJA device.
		@param[in]	channel		Specifies the output channel of interest. Defaults to NTV2_CHANNEL1.
		@return		True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool	EnableOutputInterrupt (const NTV2Channel channel = NTV2_CHANNEL1);

	/**
		@brief		Allows the CNTV2Card instance to wait for and respond to vertical blanking interrupts
					originating from the given input channel on the receiver's AJA device.
		@param[in]	channel		Specifies the input channel of interest. Defaults to NTV2_CHANNEL1.
		@return		True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool	EnableInputInterrupt (const NTV2Channel channel = NTV2_CHANNEL1);


	//
	//	Disable Interrupt/Event
	//
	AJA_VIRTUAL bool	DisableInterrupt (const INTERRUPT_ENUMS inEventCode);						//	GENERIC!

	/**
		@brief		Prevents the CNTV2Card instance from waiting for and responding to vertical blanking
					interrupts originating from the given output channel on the receiver's AJA device.
		@param[in]	channel		Specifies the output channel of interest. Defaults to NTV2_CHANNEL1.
		@return		True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool	DisableOutputInterrupt (const NTV2Channel channel = NTV2_CHANNEL1);

	/**
		@brief		Prevents the CNTV2Card instance from waiting for and responding to vertical blanking
					interrupts originating from the given input channel on the receiver's AJA device.
		@param[in]	channel		Specifies the input channel of interest. Defaults to NTV2_CHANNEL1.
		@return		True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool	DisableInputInterrupt (const NTV2Channel channel = NTV2_CHANNEL1);

	AJA_VIRTUAL bool	GetCurrentInterruptMasks (NTV2InterruptMask & outIntMask1, NTV2Interrupt2Mask & outIntMask2);


	//
	//	Subscribe to events
	//
	/**
		@brief		Registers the CNTV2Card instance to be notified when the given event/interrupt
					is triggered for the AJA device.
		@param[in]	inEventCode		Specifies the INTERRUPT_ENUMS of interest.
		@return		True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool	SubscribeEvent (const INTERRUPT_ENUMS inEventCode);						//	GENERIC!

	/**
		@brief		Registers the CNTV2Card instance to be notified when a vertical blanking interrupt
					is generated for the given AJA device's output channel.
		@param[in]	inChannel	Specifies the output channel of interest.
		@return		True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool	SubscribeOutputVerticalEvent (const NTV2Channel inChannel);


	/**
		@brief		Registers the CNTV2Card instance to be notified when a vertical blanking interrupt
					occurs on the given input channel of the AJA device.
		@param[in]	inChannel	Specifies the input channel of interest. Defaults to NTV2_CHANNEL1.
		@return		True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool	SubscribeInputVerticalEvent (const NTV2Channel inChannel = NTV2_CHANNEL1);


	//
	//	Unsubscribe from events
	//
	/**
		@brief		Unregisters the CNTV2Card instance so that it's no longer notified when the given
					event/interrupt is triggered on the AJA device.
		@param[in]	inEventCode		Specifies the INTERRUPT_ENUMS of interest.
		@return		True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool	UnsubscribeEvent (const INTERRUPT_ENUMS inEventCode);						//	GENERIC!

	/**
		@brief		Unregisters the CNTV2Card instance so that it's no longer notified when a vertical
					blanking interrupt is generated for the given AJA device's output channel.
		@param[in]	inChannel	Specifies the output channel of interest. Defaults to NTV2_CHANNEL1.
		@return		True if successful; otherwise false.
		@details	This function undoes the effect of a prior call to SubscribeOutputVerticalEvent.
	**/
	AJA_VIRTUAL bool	UnsubscribeOutputVerticalEvent (const NTV2Channel inChannel);

	/**
		@brief		Unregisters the CNTV2Card instance so that it's no longer notified when a vertical
					blanking interrupt occurs on the given input channel of the AJA device.
		@param[in]	inChannel	Specifies the input channel of interest. Defaults to NTV2_CHANNEL1.
		@return		True if successful; otherwise false.
		@details	This function undoes the effects of a prior call to SubscribeInputVerticalEvent.
	**/
	AJA_VIRTUAL bool	UnsubscribeInputVerticalEvent (const NTV2Channel inChannel = NTV2_CHANNEL1);


	//
	//	Get interrupt counts
	//
	AJA_VIRTUAL bool	GetOutputVerticalInterruptCount (ULWord & outCount, const NTV2Channel channel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool	GetInputVerticalInterruptCount (ULWord & outCount, const NTV2Channel channel = NTV2_CHANNEL1);


	//
	//	Get event counts
	//
	AJA_VIRTUAL bool	GetInterruptEventCount (const INTERRUPT_ENUMS inEventCode, ULWord & outCount);						//	GENERIC!

	AJA_VIRTUAL bool	GetOutputVerticalEventCount (ULWord & outCount, const NTV2Channel channel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool	GetInputVerticalEventCount (ULWord & outCount, const NTV2Channel channel = NTV2_CHANNEL1);


	//
	//	Set event counts
	//
	AJA_VIRTUAL bool	SetInterruptEventCount (const INTERRUPT_ENUMS inEventCode, const ULWord inCount);						//	GENERIC!

	AJA_VIRTUAL bool	SetOutputVerticalEventCount (const ULWord inCount, const NTV2Channel channel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool	SetInputVerticalEventCount (const ULWord inCount, const NTV2Channel channel = NTV2_CHANNEL1);


	//
	//	Wait for event
	//
	/**
		@brief		Efficiently sleeps the calling thread/process until the next output Vertical Blanking Interrupt
					for the given output channel occurs on the AJA device.
		@param[in]	channel		Specifies the output channel of interest. Defaults to NTV2_CHANNEL1.
		@return		True if successful; otherwise false.
		@note		The device's timing reference source affects how often -- or even if -- the VBI occurs.
		@note		If the wait period exceeds about 50 milliseconds, the function will fail and return false.
	**/
	AJA_VIRTUAL bool	WaitForOutputVerticalInterrupt (const NTV2Channel channel = NTV2_CHANNEL1);

	/**
		@brief		Efficiently sleeps the calling thread/process until the next output Vertical Blanking Interrupt
					for the given field and output channel occurs on the AJA device.
		@param[in]	inFieldID	Specifies the field identifier of interest.
		@param[in]	channel		Specifies the output channel of interest. Defaults to NTV2_CHANNEL1.
		@return		True if successful; otherwise false.
		@note		The device's timing reference source affects how often -- or even if -- the VBI occurs.
		@note		This function assumes an interlaced video format on the given channel on the device.
		@note		If the wait period exceeds about 50 milliseconds, the function will fail and return false.
	**/
	AJA_VIRTUAL bool	WaitForOutputFieldID (const NTV2FieldID inFieldID, const NTV2Channel channel = NTV2_CHANNEL1);

	/**
		@brief		Efficiently sleeps the calling thread/process until the next input Vertical Blanking Interrupt
					for the given input channel occurs on the AJA device.
		@param[in]	channel		Specifies the input channel of interest. Defaults to NTV2_CHANNEL1.
		@return		True if successful; otherwise false.
		@note		The device's timing reference source affects how often -- or even if -- the VBI occurs.
		@note		If the wait period exceeds about 50 milliseconds, the function will fail and return false.
	**/
	AJA_VIRTUAL bool	WaitForInputVerticalInterrupt (const NTV2Channel channel = NTV2_CHANNEL1);

	/**
		@brief		Efficiently sleeps the calling thread/process until the next input Vertical Blanking Interrupt
					for the given field and input channel occurs on the AJA device.
		@param[in]	inFieldID	Specifies the field identifier of interest.
		@param[in]	channel		Specifies the input channel of interest. Defaults to NTV2_CHANNEL1.
		@return		True if successful; otherwise false.
		@note		The device's timing reference source affects how often -- or even if -- the VBI occurs.
		@note		This function assumes an interlaced video format on the given channel on the device. Calling this
					function with a progressive signal will give unpredictable results (although 24psf works).
		@note		If the wait period exceeds about 50 milliseconds, the function will fail and return false.
	**/
	AJA_VIRTUAL bool	WaitForInputFieldID (const NTV2FieldID inFieldID, const NTV2Channel channel = NTV2_CHANNEL1);


	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool	EnableVerticalInterrupt();					///< @deprecated	Use EnableOutputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	EnableOutput2VerticalInterrupt();			///< @deprecated	Use EnableOutputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	EnableOutput3VerticalInterrupt();			///< @deprecated	Use EnableOutputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	EnableOutput4VerticalInterrupt();			///< @deprecated	Use EnableOutputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	EnableOutput5VerticalInterrupt();			///< @deprecated	Use EnableOutputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	EnableOutput6VerticalInterrupt();			///< @deprecated	Use EnableOutputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	EnableOutput7VerticalInterrupt();			///< @deprecated	Use EnableOutputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	EnableOutput8VerticalInterrupt();			///< @deprecated	Use EnableOutputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	EnableInput1Interrupt();					///< @deprecated	Use EnableInputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	EnableInput2Interrupt();					///< @deprecated	Use EnableInputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	EnableInput3Interrupt();					///< @deprecated	Use EnableInputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	EnableInput4Interrupt();					///< @deprecated	Use EnableInputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	EnableInput5Interrupt();					///< @deprecated	Use EnableInputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	EnableInput6Interrupt();					///< @deprecated	Use EnableInputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	EnableInput7Interrupt();					///< @deprecated	Use EnableInputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	EnableInput8Interrupt();					///< @deprecated	Use EnableInputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	EnableAudioInterrupt();						///< @deprecated	Use EnableInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	EnableAudioInWrapInterrupt();				///< @deprecated	Use EnableInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	EnableAudioOutWrapInterrupt();				///< @deprecated	Use EnableInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	EnableUartTxInterrupt();					///< @deprecated	Use EnableInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	EnableUart2TxInterrupt();					///< @deprecated	Use EnableInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	EnableUartRxInterrupt();					///< @deprecated	Use EnableInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	EnableUart2RxInterrupt();					///< @deprecated	Use EnableInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	EnableHDMIHotplugInterrupt();				///< @deprecated	Use EnableInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	EnableAuxVerticalInterrupt();				///< @deprecated	Use EnableInterrupt(INTERRUPT_ENUMS) instead.

		AJA_VIRTUAL bool	DisableVerticalInterrupt();					///< @deprecated	Use DisableOutputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	DisableOutput2VerticalInterrupt();			///< @deprecated	Use DisableOutputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	DisableOutput3VerticalInterrupt();			///< @deprecated	Use DisableOutputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	DisableOutput4VerticalInterrupt();			///< @deprecated	Use DisableOutputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	DisableOutput5VerticalInterrupt();			///< @deprecated	Use DisableOutputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	DisableOutput6VerticalInterrupt();			///< @deprecated	Use DisableOutputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	DisableOutput7VerticalInterrupt();			///< @deprecated	Use DisableOutputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	DisableOutput8VerticalInterrupt();			///< @deprecated	Use DisableOutputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	DisableInput1Interrupt();					///< @deprecated	Use DisableInputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	DisableInput2Interrupt();					///< @deprecated	Use DisableInputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	DisableInput3Interrupt();					///< @deprecated	Use DisableInputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	DisableInput4Interrupt();					///< @deprecated	Use DisableInputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	DisableInput5Interrupt();					///< @deprecated	Use DisableInputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	DisableInput6Interrupt();					///< @deprecated	Use DisableInputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	DisableInput7Interrupt();					///< @deprecated	Use DisableInputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	DisableInput8Interrupt();					///< @deprecated	Use DisableInputInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	DisableAudioInterrupt();					///< @deprecated	Use DisableInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	DisableAudioInWrapInterrupt();				///< @deprecated	Use DisableInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	DisableAudioOutWrapInterrupt();				///< @deprecated	Use DisableInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	DisableUartTxInterrupt();					///< @deprecated	Use DisableInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	DisableUart2TxInterrupt();					///< @deprecated	Use DisableInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	DisableUartRxInterrupt();					///< @deprecated	Use DisableInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	DisableUart2RxInterrupt();					///< @deprecated	Use DisableInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	DisableHDMIHotplugInterrupt();				///< @deprecated	Use DisableInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	DisableAuxVerticalInterrupt();				///< @deprecated	Use DisableInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	GetCurrentInterruptMask (NTV2InterruptMask * outInterruptMask);		///< @deprecated	Use GetCurrentInterruptMasks instead.

		//	Subscribe to events
		AJA_VIRTUAL bool	SubscribeOutputVerticalEvent ();			///< @deprecated	Use SubscribeOutputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	SubscribeOutput2VerticalEvent ();			///< @deprecated	Use SubscribeOutputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	SubscribeOutput3VerticalEvent ();			///< @deprecated	Use SubscribeOutputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	SubscribeOutput4VerticalEvent ();			///< @deprecated	Use SubscribeOutputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	SubscribeOutput5VerticalEvent ();			///< @deprecated	Use SubscribeOutputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	SubscribeOutput6VerticalEvent ();			///< @deprecated	Use SubscribeOutputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	SubscribeOutput7VerticalEvent ();			///< @deprecated	Use SubscribeOutputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	SubscribeOutput8VerticalEvent ();			///< @deprecated	Use SubscribeOutputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	SubscribeInput1VerticalEvent ();			///< @deprecated	Use SubscribeInputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	SubscribeInput2VerticalEvent ();			///< @deprecated	Use SubscribeInputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	SubscribeInput3VerticalEvent ();			///< @deprecated	Use SubscribeInputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	SubscribeInput4VerticalEvent ();			///< @deprecated	Use SubscribeInputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	SubscribeInput5VerticalEvent ();			///< @deprecated	Use SubscribeInputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	SubscribeInput6VerticalEvent ();			///< @deprecated	Use SubscribeInputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	SubscribeInput7VerticalEvent ();			///< @deprecated	Use SubscribeInputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	SubscribeInput8VerticalEvent ();			///< @deprecated	Use SubscribeInputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	SubscribeAudioInterruptEvent ();			///< @deprecated	Use SubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	SubscribeAudioInWrapInterruptEvent ();		///< @deprecated	Use SubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	SubscribeAudioOutWrapInterruptEvent ();		///< @deprecated	Use SubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	SubscribeUartTxInterruptEvent ();			///< @deprecated	Use SubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	SubscribeUartRxInterruptEvent ();			///< @deprecated	Use SubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	SubscribeUart2TxInterruptEvent ();			///< @deprecated	Use SubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	SubscribeUart2RxInterruptEvent ();			///< @deprecated	Use SubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	SubscribeHDMIHotplugInterruptEvent ();		///< @deprecated	Use SubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	SubscribeAuxVerticalInterruptEvent ();		///< @deprecated	Use SubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	SubscribeDMA1InterruptEvent ();				///< @deprecated	Use SubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	SubscribeDMA2InterruptEvent ();				///< @deprecated	Use SubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	SubscribeDMA3InterruptEvent ();				///< @deprecated	Use SubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	SubscribeDMA4InterruptEvent ();				///< @deprecated	Use SubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	SubscribeChangeEvent();	// subscribe to get notified upon any Register changes

		//	Unsubscribe from events
		AJA_VIRTUAL bool	UnsubscribeOutputVerticalEvent ();			///< @deprecated	Use UnsubscribeOutputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	UnsubscribeOutput2VerticalEvent ();			///< @deprecated	Use UnsubscribeOutputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	UnsubscribeOutput3VerticalEvent ();			///< @deprecated	Use UnsubscribeOutputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	UnsubscribeOutput4VerticalEvent ();			///< @deprecated	Use UnsubscribeOutputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	UnsubscribeOutput5VerticalEvent ();			///< @deprecated	Use UnsubscribeOutputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	UnsubscribeOutput6VerticalEvent ();			///< @deprecated	Use UnsubscribeOutputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	UnsubscribeOutput7VerticalEvent ();			///< @deprecated	Use UnsubscribeOutputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	UnsubscribeOutput8VerticalEvent ();			///< @deprecated	Use UnsubscribeOutputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	UnsubscribeInput1VerticalEvent ();			///< @deprecated	Use UnsubscribeInputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	UnsubscribeInput2VerticalEvent ();			///< @deprecated	Use UnsubscribeInputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	UnsubscribeInput3VerticalEvent ();			///< @deprecated	Use UnsubscribeInputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	UnsubscribeInput4VerticalEvent ();			///< @deprecated	Use UnsubscribeInputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	UnsubscribeInput5VerticalEvent ();			///< @deprecated	Use UnsubscribeInputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	UnsubscribeInput6VerticalEvent ();			///< @deprecated	Use UnsubscribeInputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	UnsubscribeInput7VerticalEvent ();			///< @deprecated	Use UnsubscribeInputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	UnsubscribeInput8VerticalEvent ();			///< @deprecated	Use UnsubscribeInputVerticalEvent(NTV2Channel) instead.
		AJA_VIRTUAL bool	UnsubscribeAudioInterruptEvent ();			///< @deprecated	Use UnsubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	UnsubscribeAudioInWrapInterruptEvent ();	///< @deprecated	Use UnsubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	UnsubscribeAudioOutWrapInterruptEvent ();	///< @deprecated	Use UnsubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	UnsubscribeUartTxInterruptEvent ();			///< @deprecated	Use UnsubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	UnsubscribeUartRxInterruptEvent ();			///< @deprecated	Use UnsubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	UnsubscribeUart2TxInterruptEvent ();		///< @deprecated	Use UnsubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	UnsubscribeUart2RxInterruptEvent ();		///< @deprecated	Use UnsubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	UnsubscribeHDMIHotplugInterruptEvent ();	///< @deprecated	Use UnsubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	UnsubscribeAuxVerticalInterruptEvent ();	///< @deprecated	Use UnsubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	UnsubscribeDMA1InterruptEvent ();			///< @deprecated	Use UnsubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	UnsubscribeDMA2InterruptEvent ();			///< @deprecated	Use UnsubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	UnsubscribeDMA3InterruptEvent ();			///< @deprecated	Use UnsubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	UnsubscribeDMA4InterruptEvent ();			///< @deprecated	Use UnsubscribeEvent(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	UnsubscribeChangeEvent ();

		//	Get interrupt counts
		AJA_VIRTUAL bool	GetOutputVerticalInterruptCount (ULWord *pCount);		///< @deprecated	Use GetOutputVerticalInterruptCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetOutput2VerticalInterruptCount (ULWord *pCount);		///< @deprecated	Use GetOutputVerticalInterruptCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetOutput3VerticalInterruptCount (ULWord *pCount);		///< @deprecated	Use GetOutputVerticalInterruptCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetOutput4VerticalInterruptCount (ULWord *pCount);		///< @deprecated	Use GetOutputVerticalInterruptCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetOutput5VerticalInterruptCount (ULWord *pCount);		///< @deprecated	Use GetOutputVerticalInterruptCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetOutput6VerticalInterruptCount (ULWord *pCount);		///< @deprecated	Use GetOutputVerticalInterruptCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetOutput7VerticalInterruptCount (ULWord *pCount);		///< @deprecated	Use GetOutputVerticalInterruptCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetOutput8VerticalInterruptCount (ULWord *pCount);		///< @deprecated	Use GetOutputVerticalInterruptCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetInput1VerticalInterruptCount (ULWord *pCount);		///< @deprecated	Use GetInputVerticalInterruptCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetInput2VerticalInterruptCount (ULWord *pCount);		///< @deprecated	Use GetInputVerticalInterruptCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetInput3VerticalInterruptCount (ULWord *pCount);		///< @deprecated	Use GetInputVerticalInterruptCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetInput4VerticalInterruptCount (ULWord *pCount);		///< @deprecated	Use GetInputVerticalInterruptCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetInput5VerticalInterruptCount (ULWord *pCount);		///< @deprecated	Use GetInputVerticalInterruptCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetInput6VerticalInterruptCount (ULWord *pCount);		///< @deprecated	Use GetInputVerticalInterruptCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetInput7VerticalInterruptCount (ULWord *pCount);		///< @deprecated	Use GetInputVerticalInterruptCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetInput8VerticalInterruptCount (ULWord *pCount);		///< @deprecated	Use GetInputVerticalInterruptCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetAudioInterruptCount (ULWord *pCount);				///< @deprecated	Use GetInterruptEventCount(INTERRUPT_ENUMS,ULWord&) instead.
		AJA_VIRTUAL bool	GetAudioInWrapInterruptCount (ULWord *pCount);			///< @deprecated	Use GetInterruptEventCount(INTERRUPT_ENUMS,ULWord&) instead.
		AJA_VIRTUAL bool	GetAudioOutWrapInterruptCount (ULWord *pCount);			///< @deprecated	Use GetInterruptEventCount(INTERRUPT_ENUMS,ULWord&) instead.
		AJA_VIRTUAL bool	GetAuxVerticalInterruptCount (ULWord *pCount);			///< @deprecated	Use GetInterruptEventCount(INTERRUPT_ENUMS,ULWord&) instead.

		//	Get event counts
		AJA_VIRTUAL bool	GetOutputVerticalEventCount (ULWord *pCount);			///< @deprecated	Use GetOutputVerticalEventCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetOutput2VerticalEventCount (ULWord *pCount);			///< @deprecated	Use GetOutputVerticalEventCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetOutput3VerticalEventCount (ULWord *pCount);			///< @deprecated	Use GetOutputVerticalEventCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetOutput4VerticalEventCount (ULWord *pCount);			///< @deprecated	Use GetOutputVerticalEventCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetOutput5VerticalEventCount (ULWord *pCount);			///< @deprecated	Use GetOutputVerticalEventCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetOutput6VerticalEventCount (ULWord *pCount);			///< @deprecated	Use GetOutputVerticalEventCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetOutput7VerticalEventCount (ULWord *pCount);			///< @deprecated	Use GetOutputVerticalEventCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetOutput8VerticalEventCount (ULWord *pCount);			///< @deprecated	Use GetOutputVerticalEventCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetInput1VerticalEventCount (ULWord *pCount);			///< @deprecated	Use GetInputVerticalEventCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetInput2VerticalEventCount (ULWord *pCount);			///< @deprecated	Use GetInputVerticalEventCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetInput3VerticalEventCount (ULWord *pCount);			///< @deprecated	Use GetInputVerticalEventCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetInput4VerticalEventCount (ULWord *pCount);			///< @deprecated	Use GetInputVerticalEventCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetInput5VerticalEventCount (ULWord *pCount);			///< @deprecated	Use GetInputVerticalEventCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetInput6VerticalEventCount (ULWord *pCount);			///< @deprecated	Use GetInputVerticalEventCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetInput7VerticalEventCount (ULWord *pCount);			///< @deprecated	Use GetInputVerticalEventCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetInput8VerticalEventCount (ULWord *pCount);			///< @deprecated	Use GetInputVerticalEventCount(ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetAudioInterruptEventCount (ULWord *pCount);			///< @deprecated	Use GetInterruptEventCount(INTERRUPT_ENUMS,ULWord&) instead.
		AJA_VIRTUAL bool	GetAudioInWrapInterruptEventCount (ULWord *pCount);		///< @deprecated	Use GetInterruptEventCount(INTERRUPT_ENUMS,ULWord&) instead.
		AJA_VIRTUAL bool	GetAudioOutWrapInterruptEventCount (ULWord *pCount);	///< @deprecated	Use GetInterruptEventCount(INTERRUPT_ENUMS,ULWord&) instead.
		AJA_VIRTUAL bool	GetAuxVerticalEventCount (ULWord *pCount);				///< @deprecated	Use GetInterruptEventCount(INTERRUPT_ENUMS,ULWord&) instead.

		//	Set event counts
		AJA_VIRTUAL bool	SetOutput2VerticalEventCount (ULWord Count);			///< @deprecated	Use SetOutputVerticalEventCount(ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetOutput3VerticalEventCount (ULWord Count);			///< @deprecated	Use SetOutputVerticalEventCount(ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetOutput4VerticalEventCount (ULWord Count);			///< @deprecated	Use SetOutputVerticalEventCount(ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetOutput5VerticalEventCount (ULWord Count);			///< @deprecated	Use SetOutputVerticalEventCount(ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetOutput6VerticalEventCount (ULWord Count);			///< @deprecated	Use SetOutputVerticalEventCount(ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetOutput7VerticalEventCount (ULWord Count);			///< @deprecated	Use SetOutputVerticalEventCount(ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetOutput8VerticalEventCount (ULWord Count);			///< @deprecated	Use SetOutputVerticalEventCount(ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetInput1VerticalEventCount (ULWord Count);				///< @deprecated	Use SetInputVerticalEventCount(ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetInput2VerticalEventCount (ULWord Count);				///< @deprecated	Use SetInputVerticalEventCount(ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetInput3VerticalEventCount (ULWord Count);				///< @deprecated	Use SetInputVerticalEventCount(ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetInput4VerticalEventCount (ULWord Count);				///< @deprecated	Use SetInputVerticalEventCount(ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetInput5VerticalEventCount (ULWord Count);				///< @deprecated	Use SetInputVerticalEventCount(ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetInput6VerticalEventCount (ULWord Count);				///< @deprecated	Use SetInputVerticalEventCount(ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetInput7VerticalEventCount (ULWord Count);				///< @deprecated	Use SetInputVerticalEventCount(ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetInput8VerticalEventCount (ULWord Count);				///< @deprecated	Use SetInputVerticalEventCount(ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetAudioInterruptEventCount (ULWord Count);				///< @deprecated	Use SetInterruptEventCount(INTERRUPT_ENUMS,ULWord) instead.
		AJA_VIRTUAL bool	SetAudioInWrapInterruptEventCount (ULWord Count);		///< @deprecated	Use SetInterruptEventCount(INTERRUPT_ENUMS,ULWord) instead.
		AJA_VIRTUAL bool	SetAudioOutWrapInterruptEventCount (ULWord Count);		///< @deprecated	Use SetInterruptEventCount(INTERRUPT_ENUMS,ULWord) instead.
		AJA_VIRTUAL bool	SetAuxVerticalEventCount (ULWord Count);				///< @deprecated	Use SetInterruptEventCount(INTERRUPT_ENUMS,ULWord) instead.

		//	Wait for event
		AJA_VIRTUAL bool	WaitForVerticalInterrupt();								///< @deprecated	Use WaitForOutputVerticalInterrupt or WaitForInputVerticalInterrupt, as appropriate, instead.
		AJA_VIRTUAL bool	WaitForOutput1VerticalInterrupt();						///< @deprecated	Use WaitForOutputVerticalInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForOutput2VerticalInterrupt();						///< @deprecated	Use WaitForOutputVerticalInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForOutput3VerticalInterrupt();						///< @deprecated	Use WaitForOutputVerticalInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForOutput4VerticalInterrupt();						///< @deprecated	Use WaitForOutputVerticalInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForOutput5VerticalInterrupt();						///< @deprecated	Use WaitForOutputVerticalInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForOutput6VerticalInterrupt();						///< @deprecated	Use WaitForOutputVerticalInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForOutput7VerticalInterrupt();						///< @deprecated	Use WaitForOutputVerticalInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForOutput8VerticalInterrupt();						///< @deprecated	Use WaitForOutputVerticalInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForFieldID (NTV2FieldID fieldID);					///< @deprecated	Use WaitForOutputFieldID(NTV2FieldID,NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForOutput1FieldID (NTV2FieldID fieldID);			///< @deprecated	Use WaitForOutputFieldID(NTV2FieldID,NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForOutput2FieldID (NTV2FieldID fieldID);			///< @deprecated	Use WaitForOutputFieldID(NTV2FieldID,NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForOutput3FieldID (NTV2FieldID fieldID);			///< @deprecated	Use WaitForOutputFieldID(NTV2FieldID,NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForOutput4FieldID (NTV2FieldID fieldID);			///< @deprecated	Use WaitForOutputFieldID(NTV2FieldID,NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForOutput5FieldID (NTV2FieldID fieldID);			///< @deprecated	Use WaitForOutputFieldID(NTV2FieldID,NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForOutput6FieldID (NTV2FieldID fieldID);			///< @deprecated	Use WaitForOutputFieldID(NTV2FieldID,NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForOutput7FieldID (NTV2FieldID fieldID);			///< @deprecated	Use WaitForOutputFieldID(NTV2FieldID,NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForOutput8FieldID (NTV2FieldID fieldID);			///< @deprecated	Use WaitForOutputFieldID(NTV2FieldID,NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForInput1FieldID (NTV2FieldID fieldID);				///< @deprecated	Use WaitForInputFieldID(NTV2FieldID,NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForInput2FieldID (NTV2FieldID fieldID);				///< @deprecated	Use WaitForInputFieldID(NTV2FieldID,NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForInput3FieldID (NTV2FieldID fieldID);				///< @deprecated	Use WaitForInputFieldID(NTV2FieldID,NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForInput4FieldID (NTV2FieldID fieldID);				///< @deprecated	Use WaitForInputFieldID(NTV2FieldID,NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForInput5FieldID (NTV2FieldID fieldID);				///< @deprecated	Use WaitForInputFieldID(NTV2FieldID,NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForInput6FieldID (NTV2FieldID fieldID);				///< @deprecated	Use WaitForInputFieldID(NTV2FieldID,NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForInput7FieldID (NTV2FieldID fieldID);				///< @deprecated	Use WaitForInputFieldID(NTV2FieldID,NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForInput8FieldID (NTV2FieldID fieldID);				///< @deprecated	Use WaitForInputFieldID(NTV2FieldID,NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForInput1Vertical();								///< @deprecated	Use WaitForInputVerticalInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForInput2Vertical();								///< @deprecated	Use WaitForInputVerticalInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForInput3Vertical();								///< @deprecated	Use WaitForInputVerticalInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForInput4Vertical();								///< @deprecated	Use WaitForInputVerticalInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForInput5Vertical();								///< @deprecated	Use WaitForInputVerticalInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForInput6Vertical();								///< @deprecated	Use WaitForInputVerticalInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForInput7Vertical();								///< @deprecated	Use WaitForInputVerticalInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForInput8Vertical();								///< @deprecated	Use WaitForInputVerticalInterrupt(NTV2Channel) instead.
		AJA_VIRTUAL bool	WaitForAudioInterrupt();								///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForAudioInWrapInterrupt();							///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForAudioOutWrapInterrupt();							///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForUartTxInterruptEvent(ULWord timeoutMS=15);		///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForUart2TxInterruptEvent(ULWord timeoutMS=15);		///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForUartRxInterruptEvent(ULWord timeoutMS=15);		///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForUart2RxInterruptEvent(ULWord timeoutMS=15);		///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForHDMIHotplugInterruptEvent(ULWord timeoutMS=15);	///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForAuxVerticalInterrupt();							///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForDMA1Interrupt();									///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForDMA2Interrupt();									///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForDMA3Interrupt();									///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForDMA4Interrupt();									///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForPushButtonChangeInterrupt(ULWord timeoutMS=200);	///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	WaitForLowPowerInterrupt(ULWord timeoutMS=1000);		///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForDisplayFIFOInterrupt(ULWord timeoutMS=1000);		///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForSATAChangeInterrupt(ULWord timeoutMS=200);		///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForTemp1HighInterrupt(ULWord timeoutMS=1000);		///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForTemp2HighInterrupt(ULWord timeoutMS=1000);		///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForPowerButtonChangeInterrupt(ULWord timeoutMS=1000);	///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
		AJA_VIRTUAL bool	WaitForChangeEvent();									///< @deprecated	Use WaitForInterrupt(INTERRUPT_ENUMS) instead.
	#endif	//	!defined (NTV2_DEPRECATE)
	///@}


	//
	// Color Correction Functions (KHD only )
	//
	AJA_VIRTUAL bool	SetColorCorrectionMode(NTV2Channel channel, NTV2ColorCorrectionMode mode);
	AJA_VIRTUAL bool	GetColorCorrectionMode(NTV2Channel channel, NTV2ColorCorrectionMode *mode);
	AJA_VIRTUAL bool	SetColorCorrectionOutputBank (NTV2Channel channel, ULWord bank);
	AJA_VIRTUAL bool	GetColorCorrectionOutputBank (NTV2Channel channel, ULWord *bank);
	AJA_VIRTUAL bool	SetColorCorrectionHostAccessBank (NTV2ColorCorrectionHostAccessBank value);
	AJA_VIRTUAL bool	GetColorCorrectionHostAccessBank (NTV2ColorCorrectionHostAccessBank *value, NTV2Channel channel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool	SetColorCorrectionSaturation (NTV2Channel channel, ULWord value);
	AJA_VIRTUAL bool	GetColorCorrectionSaturation (NTV2Channel channel, ULWord *value);

	AJA_VIRTUAL bool	SetDitherFor8BitInputs (NTV2Channel channel, ULWord dither);
	AJA_VIRTUAL bool	GetDitherFor8BitInputs (NTV2Channel channel, ULWord* dither);

	AJA_VIRTUAL bool	SetForce64(ULWord force64);
	AJA_VIRTUAL bool	GetForce64(ULWord* force64);
	AJA_VIRTUAL bool	Get64BitAutodetect(ULWord* autodetect64);
	AJA_VIRTUAL bool	GetFirmwareRev(ULWord* firmwareRev);


	/**
		@name	AutoCirculate
	**/
	///@{
	#if !defined (NTV2_DEPRECATE)
	/**
		@deprecated	This function is obsolete.
	**/
	AJA_VIRTUAL bool	InitAutoCirculate (NTV2Crosspoint inChannelSpec,
											LWord inStartFrameNumber,
											LWord inEndFrameNumber,
											bool bWithAudio				= false,
											bool bWithRP188				= false,
											bool bFbfChange				= false,
											bool bFboChange				= false,
											bool bWithColorCorrection	= false,
											bool bWithVidProc			= false,
											bool bWithCustomAncData		= false,
											bool bWithLTC				= false,
											bool bUseAudioSystem2		= false);
	#endif	//	!defined (NTV2_DEPRECATE)

	AJA_VIRTUAL bool	InitAutoCirculate (NTV2Crosspoint	inChannelSpec,
											LWord			inStartFrameNumber,
											LWord			inEndFrameNumber,
											LWord			inNumChannels,
											NTV2AudioSystem	inAudioSystem,
											bool			bWithAudio				= false,
											bool			bWithRP188				= false,
											bool			bFbfChange				= false,
											bool			bFboChange				= false,
											bool			bWithColorCorrection	= false,
											bool			bWithVidProc			= false,
											bool			bWithCustomAncData		= false,
											bool			bWithLTC				= false);	///< @deprecated	Use AutoCirculateInitForInput or AutoCirculateInitForOutput instead.

	AJA_VIRTUAL bool  StartAutoCirculate (const NTV2Crosspoint inChannelSpec, const ULWord64 inStartTime = 0);				///< @deprecated	Use AutoCirculateStart instead.

	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool  StartAutoCirculateAtTime (NTV2Crosspoint channelSpec, ULWord64 startTime);						///< @deprecated	Use AutoCirculateStart instead.
	#endif	//	!NTV2_DEPRECATE

	AJA_VIRTUAL bool	StopAutoCirculate (NTV2Crosspoint channelSpec);														///< @deprecated	Use AutoCirculateStop instead.

	AJA_VIRTUAL bool	AbortAutoCirculate (NTV2Crosspoint channelSpec);													///< @deprecated	Use AbortAutoCirculate instead.

	AJA_VIRTUAL bool	PauseAutoCirculate (NTV2Crosspoint channelSpec, bool bPlay);										///< @deprecated	Use AutoCirculatePause or AutoCirculateResume instead.

	AJA_VIRTUAL bool	GetAutoCirculate (NTV2Crosspoint channelSpec, AUTOCIRCULATE_STATUS_STRUCT* autoCirculateStatus);	///< @deprecated	Use AutoCirculateGetStatus instead.

	AJA_VIRTUAL bool	GetFrameStamp (NTV2Crosspoint channelSpec, ULWord frameNum, FRAME_STAMP_STRUCT* pFrameStamp);		///< @deprecated	Use AutoCirculateGetFrame instead.

	AJA_VIRTUAL bool	GetFrameStampEx2 (NTV2Crosspoint channelSpec, ULWord frameNum,
											FRAME_STAMP_STRUCT* pFrameStamp,
											PAUTOCIRCULATE_TASK_STRUCT pTaskStruct = NULL);									///< @deprecated	Use AutoCirculateGetFrame instead.

	AJA_VIRTUAL bool	FlushAutoCirculate (NTV2Crosspoint channelSpec);													///< @deprecated	Use FlushAutoCirculate instead.

	AJA_VIRTUAL bool	SetActiveFrameAutoCirculate (NTV2Crosspoint channelSpec, ULWord lActiveFrame);						///< @deprecated	Use SetActiveFrameAutoCirculate instead.

	AJA_VIRTUAL bool	PrerollAutoCirculate (NTV2Crosspoint channelSpec, ULWord lPrerollFrames);							///< @deprecated	Use AutoCirculatePreRoll instead.


	AJA_VIRTUAL bool	TransferWithAutoCirculate (PAUTOCIRCULATE_TRANSFER_STRUCT pTransferStruct,
 												 PAUTOCIRCULATE_TRANSFER_STATUS_STRUCT pTransferStatusStruct);				///< @deprecated	Use AutoCirculateTransfer instead.

	AJA_VIRTUAL bool	TransferWithAutoCirculateEx(PAUTOCIRCULATE_TRANSFER_STRUCT pTransferStruct,
													PAUTOCIRCULATE_TRANSFER_STATUS_STRUCT pTransferStatusStruct,
													NTV2RoutingTable* pXena2RoutingTable = NULL);							///< @deprecated	Use AutoCirculateTransfer instead.
	AJA_VIRTUAL bool	TransferWithAutoCirculateEx2(PAUTOCIRCULATE_TRANSFER_STRUCT pTransferStruct,
													PAUTOCIRCULATE_TRANSFER_STATUS_STRUCT pTransferStatusStruct,
													NTV2RoutingTable* pXena2RoutingTable = NULL,
													PAUTOCIRCULATE_TASK_STRUCT pTaskStruct = NULL);							///< @deprecated	Use AutoCirculateTransfer instead.
	AJA_VIRTUAL bool	SetAutoCirculateCaptureTask(NTV2Crosspoint channelSpec, PAUTOCIRCULATE_TASK_STRUCT pTaskStruct);	///< @deprecated	Use AutoCirculateTransfer instead.


	/**
		@brief		Prepares for subsequent AutoCirculate capture/ingest operations by reserving and dedicating a contiguous block
					of frame buffers on the AJA device for exclusive use. It also specifies other optional behaviors.
					Upon successful return, the driver's AutoCirculate status for the given channelSpec will be NTV2_AUTOCIRCULATE_INIT.

		@return		True if successful; otherwise false.

		@param[in]		inChannel				Specifies the NTV2Channel to use. Some devices will not have all of the possible input
												channels. Use the NTV2DeviceGetNumFrameStores function to discover how many frame stores
												are on the device.

		@param[in]		inFrameCount			Specifies the number of contiguous device frame buffers to be continuously cycled through.
												Must exceed zero. Defaults to 10.

		@param[in]		inAudioSystem			Specifies the audio system to use, if any. Defaults to NTV2_AUDIOSYSTEM_INVALID (no audio).

		@param[in]		inOptionFlags			A bit mask that specifies additional AutoCirculate options (e.g., AUTOCIRCULATE_WITH_RP188,
												AUTOCIRCULATE_WITH_LTC, AUTOCIRCULATE_WITH_ANC, etc.). Defaults to zero (no options).

		@param[in]		inNumChannels			Optionally specifies the number of channels to operate on when StartAutoCirculate or
												StopAutoCirculate are called. Defaults to 1.

		@details	If this function returns true, the driver will have reserved a contiguous set of device frame buffers, and placed the
					specified channel into the "initialized" state (NTV2_AUTOCIRCULATE_INIT). The channel will then be ready for a subsequent
					call to StartAutoCirculate or TransferWithAutoCirculate.
					InitAutoCirculate's behavior depends on the device's "every frame task mode" (NTV2EveryFrameTaskMode). This mode can
					be discovered by calling GetEveryFrameServices, and can be changed by calling SetEveryFrameServices.
					If the device's task mode is set to "OEM Tasks" (NTV2_OEM_TASKS), the driver will perform most of the device setup,
					including setting the input format or output standard, enabling the frame store, setting the frame store's mode, etc.
					The driver will not, however, perform routing changes. All widget routing must be completed prior to calling InitAutoCirculate.
					If the device's task mode is set to "Disable Tasks" (NTV2_DISABLE_TASKS), on some platforms, the driver will not
					perform any device setup. In this case, all aspects of the device -- the frame store mode, output or input standards, etc. --
					must be configured properly before calling InitAutoCirculate.
					If the device's task mode is set to "Standard Tasks" (NTV2_STANDARD_TASKS), and the retail mode service (or "daemon"
					on Macintosh or Linux) is running on the host, the device configuration will be dictated by the device's current
					"AJA ControlPanel" settings. In this case, the Control Panel settings should agree with what InitAutoCirculate is
					being asked to do. For example, setting the device output to "Test Pattern" in the Control Panel, then calling
					InitAutoCirculate with NTV2CROSSPOINT_INPUT1 is contradictory, because AutoCirculate is being asked asking to capture
					from a device that's configured to playout a test pattern.
	**/

	AJA_VIRTUAL bool	AutoCirculateInitForInput (	const NTV2Channel		inChannel,
													const UByte				inFrameCount		= 10,
													const NTV2AudioSystem	inAudioSystem		= NTV2_AUDIOSYSTEM_INVALID,
													const ULWord			inOptionFlags		= 0,
													const UByte				inNumChannels		= 1);

	/**
		@brief		Prepares for subsequent AutoCirculate playout operations by reserving and dedicating a contiguous block
					of frame buffers on the AJA device for exclusive use. It also specifies other optional behaviors.
					Upon successful return, the driver's AutoCirculate status for the given channelSpec will be NTV2_AUTOCIRCULATE_INIT.

		@return		True if successful; otherwise false.

		@param[in]		inChannel				Specifies the NTV2Channel to use. Some devices will not have all of the possible input
												channels. Use the NTV2DeviceGetNumFrameStores function to discover how many frame stores
												are on the device.

		@param[in]		inFrameCount			Specifies the number of contiguous device frame buffers to be continuously cycled through.
												Must exceed zero. Defaults to 10.

		@param[in]		inAudioSystem			Specifies the audio system to use, if any. Defaults to NTV2_AUDIOSYSTEM_INVALID (no audio).

		@param[in]		inOptionFlags			A bit mask that specifies additional AutoCirculate options (e.g., AUTOCIRCULATE_WITH_RP188,
												AUTOCIRCULATE_WITH_LTC, AUTOCIRCULATE_WITH_ANC, etc.). Defaults to zero (no options).

		@param[in]		inNumChannels			Optionally specifies the number of channels to operate on when StartAutoCirculate or
												StopAutoCirculate are called. Defaults to 1.

		@details	If this function returns true, the driver will have reserved a contiguous set of device frame buffers, and placed the
					specified channel into the "initialized" state (NTV2_AUTOCIRCULATE_INIT). The channel will then be ready for a subsequent
					call to StartAutoCirculate or TransferWithAutoCirculate.
					InitAutoCirculate's behavior depends on the device's "every frame task mode" (NTV2EveryFrameTaskMode). This mode can
					be discovered by calling GetEveryFrameServices, and can be changed by calling SetEveryFrameServices.
					If the device's task mode is set to "OEM Tasks" (NTV2_OEM_TASKS), the driver will perform most of the device setup,
					including setting the input format or output standard, enabling the frame store, setting the frame store's mode, etc.
					The driver will not, however, perform routing changes. All widget routing must be completed prior to calling InitAutoCirculate.
					If the device's task mode is set to "Disable Tasks" (NTV2_DISABLE_TASKS), on some platforms, the driver will not
					perform any device setup. In this case, all aspects of the device -- the frame store mode, output or input standards, etc. --
					must be configured properly before calling InitAutoCirculate.
					If the device's task mode is set to "Standard Tasks" (NTV2_STANDARD_TASKS), and the retail mode service (or "daemon"
					on Macintosh or Linux) is running on the host, the device configuration will be dictated by the device's current
					"AJA ControlPanel" settings. In this case, the Control Panel settings should agree with what InitAutoCirculate is
					being asked to do. For example, setting the device output to "Test Pattern" in the Control Panel, then calling
					InitAutoCirculate with NTV2CROSSPOINT_INPUT1 is contradictory, because AutoCirculate is being asked asking to capture
					from a device that's configured to playout a test pattern.
	**/

	AJA_VIRTUAL bool	AutoCirculateInitForOutput (const NTV2Channel		inChannel,
													const UByte				inFrameCount		= 10,
													const NTV2AudioSystem	inAudioSystem		= NTV2_AUDIOSYSTEM_INVALID,
													const ULWord			inOptionFlags		= 0,
													const UByte				inNumChannels		= 1);

	/**
		@brief		Starts AutoCirculating the specified channel that was previously initialized by AutoCirculateInitForInput or
					AutoCirculateInitForOutput.

		@return		True if successful; otherwise false.

		@param[in]		inChannel		Specifies the NTV2Channel to use. Some devices will not have all of the possible input
										channels. Use the NTV2DeviceGetNumFrameStores function to discover how many frame stores
										are on the device.

		@param[in]		inStartTime		Optionally specifies a future start time as an unsigned 64-bit "tick count" value that
										is host-OS-dependent. If set to zero, the default, AutoCirculate will switch to the
										"running" state at the next VBI received by the given channel. If non-zero, AutoCirculate
										will remain in the "starting" state until the system tick clock exceeds this value, at
										which point it will switch to the "running" state. This value is denominated in the same
										time units as the finest-grained time counter available on the host's operating system.

		@details	This function sets the state of the channel in the driver from "initializing" to "starting", then at the next
					VBI, the driver's interrupt service routine (ISR) will check the OS tick clock, and if it exceeds the given
					start time value, it will proceed to start AutoCirculate -- otherwise it will remain in the "starting" phase,
					and recheck the clock at the next VBI. The driver will start tracking which memory frames are available and
					which are empty, and will change the channel's status to "running".
					When capturing, the next frame (to be recorded) is determined, and the current last input audio sample is
					written into the next frame's FRAME_STAMP's acAudioInStartAddress field. Finally, the channel's active frame
					is set to the next frame number.
					During playout, the next frame (to go out the jack) is determined, and the current last output audio sample
					is written into the next frame's FRAME_STAMP's acAudioOutStartAddress field. Finally, the channel's active
					frame is set to the next frame number. Henceforth, the driver will AutoCirculate frames at every VBI on a
					per-channel basis.

		@note		This method will fail if the specified channel's AutoCirculate state is not "initializing".

		@note		Calling AutoCirculateStart while in the "paused" state will not un-pause AutoCirculate, but instead will
					restart it.
	**/
	AJA_VIRTUAL bool	AutoCirculateStart (const NTV2Channel inChannel, const ULWord64 inStartTime = 0);

	/**
		@brief		Stops AutoCirculate for the given channel, and releases the on-device frame buffers that were allocated to it.

		@return		True if successful; otherwise false.

		@param[in]		inChannel		Specifies the NTV2Channel to use. Some devices will not have all of the possible input
										channels. Use the NTV2DeviceGetNumFrameStores function to discover how many frame stores
										are on the device.

		@param[in]		inAbort			Specifies if AutoCirculate is to be immediately stopped, not gracefully.
										Defaults to false (graceful stop).

		@details	If asked to stop gracefully (the default), the channel's AutoCirculate state is set to "stopping", and at the
					next VBI, AutoCirculate is explicitly stopped, after which the channel's AutoCirculate state is set to "disabled".
					Once this method has been called, the channel cannot be used until it gets reinitialized by a subsequent call
					to AutoCirculateInitForInput or AutoCirculateInitForOutput.
					When called with inAbort set to 'true', audio capture or playback is immediately stopped (if a valid audio system
					was specified at initialization time), and the AutoCirculate channel status is changed to "disabled".
	**/
	AJA_VIRTUAL bool	AutoCirculateStop (const NTV2Channel inChannel, const bool inAbort = false);

	/**
		@brief		Pauses AutoCirculate for the given channel. Once paused, AutoCirculate can be resumed later by calling AutoCirculateResume,
					picking up at the next frame without any loss of audio synchronization.

		@return		True if successful; otherwise false.

		@param[in]		inChannel		Specifies the NTV2Channel to use. Some devices will not have all of the possible input
										channels. Use the NTV2DeviceGetNumFrameStores function to discover how many frame stores
										are on the device.

		@details	When pausing, if the channel is in the "running" state, it will be set to "paused", and at the next VBI, the driver
					will explicitly stop audio circulating.
	**/
	AJA_VIRTUAL bool	AutoCirculatePause (const NTV2Channel inChannel);

	/**
		@brief		Resumes AutoCirculate for the given channel, picking up at the next frame without loss of audio synchronization.

		@return		True if successful; otherwise false.

		@param[in]		inChannel		Specifies the NTV2Channel to use. Some devices will not have all of the possible input
										channels. Use the NTV2DeviceGetNumFrameStores function to discover how many frame stores
										are on the device.

		@details	When resuming, if the channel is in the "paused" state, it will be changed to "running", and at the next VBI, the
					driver will restart audio AutoCirculate.
	**/
	AJA_VIRTUAL bool	AutoCirculateResume (const NTV2Channel inChannel);

	/**
		@brief		Flushes AutoCirculate for the given channel.

		@return		True if successful; otherwise false.

		@param[in]		inChannel		Specifies the NTV2Channel to use. Some devices will not have all of the possible input
										channels. Use the NTV2DeviceGetNumFrameStores function to discover how many frame stores
										are on the device.

		@details	On capture, flushes all recorded frames that haven't yet been transferred to the host.
					On playout, all queued frames that have already been transferred to the device (that haven't yet played)
					are discarded.
					In either mode, this function has no effect on the Active Frame (the frame currently being captured
					or played by the device hardware at the moment the function was called).
					The NTV2AutoCirculateState ("running", etc.) for the given channel will remain unchanged.
	**/
	AJA_VIRTUAL bool	AutoCirculateFlush (const NTV2Channel inChannel);

	/**
		@brief		Tells AutoCirculate how many frames to skip before playout starts for the given channel.

		@return		True if successful; otherwise false.

		@param[in]		inChannel		Specifies the NTV2Channel to use. Some devices will not have all of the possible input
										channels. Use the NTV2DeviceGetNumFrameStores function to discover how many frame stores
										are on the device.

		@param[in]		inPreRollFrames	Specifies the number of frames to skip (ignore) before starting AutoCirculate.

		@details	Normally used for playout, this method instructs the driver to mark the given number of frames as valid.
					It's useful only in the rare case when, after AutoCirculateInitForOutput was called, several frames have
					already been transferred to the device (perhaps using DmaWrite), and calling AutoCirculateStart will
					ignore those pre-rolled frames without an intervening AutoCirculateTransfer call.

		@note		This method does nothing if the channel's state is not currently "starting", "running" or "paused",
					or if the channel was initialized by AutoCirculateInitForInput.
	**/
	AJA_VIRTUAL bool	AutoCirculatePreRoll (const NTV2Channel inChannel, const ULWord inPreRollFrames);

	/**
		@brief		Returns the current AutoCirculate status for the given channel.

		@return		True if successful; otherwise false.

		@param[in]		inChannel		Specifies the NTV2Channel to use. Some devices will not have all of the possible input
										channels. Use the NTV2DeviceGetNumFrameStores function to discover how many frame stores
										are on the device.

		@param[out]		outStatus		Receives the AUTOCIRCULATE_STATUS information for the channel.

		@details	Clients can use the AUTOCIRCULATE_STATUS information to determine if there are sufficient readable frames
					in the driver to safely support a DMA transfer to host memory (for capture);  or to determine if any frames
					have been dropped.
	**/
	AJA_VIRTUAL bool	AutoCirculateGetStatus (const NTV2Channel inChannel, AUTOCIRCULATE_STATUS & outStatus);


	/**
		@brief		Returns precise timing information for the given frame and channel that's currently AutoCirculating.

		@return		True if successful; otherwise false.

		@param[in]		inChannel				Specifies the NTV2Channel to use. Some devices will not have all of the possible input
												channels. Use the NTV2DeviceGetNumFrameStores function to discover how many frame stores
												are on the device.

		@param[in]		inFrameNumber			Specifies the zero-based frame number of interest. This value must be no less than acStartFrame
												and no more than acEndFrame for the given channel. For capture/ingest, it should be "behind
												the record head". For playout, it should be "behind the play head."

		@param[out]		outFrameInfo			Receives the FRAME_STAMP information for the given frame number and channel.

		@details		When the given channel is AutoCirculating, the driver will continuously fill in a FRAME_STAMP record for the frame
						it's currently working on, which is intended to give enough information to determine if frames have been dropped
						either on input or output. Moreover, it allows for synchronization of audio and video by time-stamping the audio
						input address at the start and end of a video frame.
	**/
	AJA_VIRTUAL bool	AutoCirculateGetFrameStamp (const NTV2Channel inChannel, const ULWord inFrameNumber, FRAME_STAMP & outFrameInfo);

	/**
		@brief		Immediately changes the Active Frame for the given channel.

		@return		True if successful; otherwise false.

		@param[in]		inChannel			Specifies the NTV2Channel to use. Some devices will not have all of the possible input
											channels. Use the NTV2DeviceGetNumFrameStores function to discover how many frame stores
											are on the device.

		@param[in]		inNewActiveFrame	Specifies the zero-based frame number to use. This value must be no less than acStartFrame
											and no more than acEndFrame for the given channel (see AUTOCIRCULATE_STATUS).

		@details	This method, assuming it succeeds, changes the Active Frame for the given channel.
					The device driver accomplishes this by changing the Active Frame register (input or output) for the given channel.

		@note		When one of these registers change on the device, it won't take effect until the next VBI, which ensures, for
					example, that an outgoing frame won't suddenly change mid-frame.

		@note		This method does nothing if the channel's AutoCirculate state is not currently "starting", "running" or "paused".
	**/
	AJA_VIRTUAL bool	AutoCirculateSetActiveFrame (const NTV2Channel inChannel, const ULWord inNewActiveFrame);

	/**
		@brief		Transfers all or part of a frame as specified in the given AUTOCIRCULATE_TRANSFER object to/from the host.

		@param[in]		inChannel				Specifies the NTV2Channel to use. Some devices will not have all of the possible input
												channels. Use the NTV2DeviceGetNumFrameStores function to discover how many frame stores
												are on the device.

		@param[in]		inOutTransferInfo		Specifies the AUTOCIRCULATE_TRANSFER information to use, which specifies the transfer
												details.

		@details	It is recommended that this method be called from inside a loop in a separate execution thread, with a way to gracefully
					exit the loop. Once outside of the loop, AutoCirculateStop can then be called. It is the application's responsibility
					to provide valid video, audio and ancillary data pointers (and byte counts) to the transfer object via its SetBuffers
					function. See the documentation for the AUTOCIRCULATE_TRANSFER class for details.

		@note		Do not call this method using a channel that was not previously initialized with a call to AutoCirculateInitForInput
					or AutoCirculateInitForOutput. The channel's AutoCirculate state must not be "disabled".

		@note		The calling thread will block until the transfer completes (or fails).
	**/
	AJA_VIRTUAL bool	AutoCirculateTransfer (const NTV2Channel inChannel, AUTOCIRCULATE_TRANSFER & inOutTransferInfo);

	/**
		@brief		Returns the device frame buffer numbers of the first unallocated contiguous band of frame buffers having the given
					size that are available for use with AutoCirculate.

		@param[in]		inFrameCount			Specifies the desired number of contiguous device frame buffers. Must exceed zero.

		@param[out]		outStartFrameNumber		Receives the starting device frame buffer number.

		@param[out]		outEndFrameNumber		Receives the ending device frame buffer number.

		@return		True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool	FindUnallocatedFrames (const UByte inFrameCount, LWord & outStartFrameNumber, LWord & outEndFrameNumber);

	/**
		@brief			Reads the given set of registers.
		@param[in]		inRegisters				Specifies the set of registers to be read.
		@param[out]		outValues				Receives the resulting register/value map. Any registers in the "inRegisters" set that don't
												appear in this map were not able to be read successfully.
		@return			True if all requested registers were successfully read; otherwise false.
		@note			This operation is not guaranteed to be performed atomically. A VBI may occur while the requested registers are being read.
	**/
	AJA_VIRTUAL bool	ReadRegisters (const NTV2RegNumSet & inRegisters,  NTV2RegisterValueMap & outValues);

	/**
		@brief			Writes the given sequence of NTV2RegInfo's.
		@param[in]		inRegWrites		Specifies the sequence of NTV2RegInfo's to be written.
		@return			True if all registers were written successfully; otherwise false.
		@note			This operation is not guaranteed to be performed atomically. A VBI may occur while the requested registers are being written.
	**/
	AJA_VIRTUAL bool	WriteRegisters (const NTV2RegisterWrites & inRegWrites);
	///@}

	AJA_VIRTUAL bool	SetFrameBufferSize(NTV2Channel inChannel, NTV2Framesize inValue);
	AJA_VIRTUAL bool	GetFrameBufferSize (NTV2Channel inChannel, NTV2Framesize & outValue);
	AJA_VIRTUAL bool	GetFrameBufferSize (NTV2Channel inChannel, NTV2Framesize * pOutValue)		{return pOutValue ? GetFrameBufferSize (inChannel, *pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.
	using CNTV2DriverInterface::GetFrameBufferSize;		//	Keep CNTV2DriverInterface::GetFrameBufferSize visible after being shadowed by CNTV2Card::GetFrameBufferSize

	/**
		@brief		Disables the given frame store.
		@param[in]	inChannel	Specifies the frame store, as identified by an NTV2Channel value.
		@return		True if successful;  otherwise false.
		@note		It is not an error to disable a frame store that is already disabled.
	**/
	AJA_VIRTUAL bool	DisableChannel (const NTV2Channel inChannel);

	/**
		@brief		Enables the given frame store.
		@param[in]	inChannel	Specifies the frame store, as identified by an NTV2Channel value.
		@return		True if successful;  otherwise false.
		@note		It is not an error to enable a frame store that is already enabled.
	**/
	AJA_VIRTUAL bool	EnableChannel (const NTV2Channel inChannel);

	/**
		@brief		Answers whether or not the given frame store is enabled.
		@param[in]	inChannel	Specifies the frame store, as identified by an NTV2Channel value.
		@param[in]	outEnabled	Specifies a boolean variable that is to receive the value "true" if
								the frame store is enabled, or "false" if the frame store is disabled.
		@return		True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	IsChannelEnabled (const NTV2Channel inChannel, bool & outEnabled);

	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool	SetChannel2Disable (bool value);								///< @deprecated	Use EnableChannel or DisableChannel instead.
		AJA_VIRTUAL bool	GetChannel2Disable (bool* value);								///< @deprecated	Use IsChannelEnabled instead.
		AJA_VIRTUAL bool	SetChannel3Disable (bool value);								///< @deprecated	Use EnableChannel or DisableChannel instead.
		AJA_VIRTUAL bool	GetChannel3Disable (bool* value);								///< @deprecated	Use IsChannelEnabled instead.
		AJA_VIRTUAL bool	SetChannel4Disable (bool value);								///< @deprecated	Use EnableChannel or DisableChannel instead.
		AJA_VIRTUAL bool	GetChannel4Disable (bool* value);								///< @deprecated	Use IsChannelEnabled instead.
	#endif	//	!defined (NTV2_DEPRECATE)

	AJA_VIRTUAL bool	SetVideoDACMode (NTV2VideoDACMode inValue);
	AJA_VIRTUAL bool	GetVideoDACMode (NTV2VideoDACMode & outValue);
	AJA_VIRTUAL bool	GetVideoDACMode (NTV2VideoDACMode * pOutValue)		{return pOutValue ? GetVideoDACMode (*pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	/**
		@name	Timing/Offset Control
	**/
	///@{
	AJA_VIRTUAL bool	GetNominalMinMaxHV (int* nominalH, int* minH, int* maxH, int* nominalV, int* minV, int* maxV);
	AJA_VIRTUAL bool	SetVideoHOffset (int hOffset);
	AJA_VIRTUAL bool	GetVideoHOffset (int* hOffset);
	AJA_VIRTUAL bool	SetVideoVOffset (int vOffset);
	AJA_VIRTUAL bool	GetVideoVOffset (int* vOffset);
	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool	SetVideoFinePhase (int fOffset);		///< @deprecated	This function is obsolete.
		AJA_VIRTUAL bool	GetVideoFinePhase (int* fOffset);		///< @deprecated	This function is obsolete.
	#endif	//	!defined (NTV2_DEPRECATE)

	AJA_VIRTUAL bool	SetAnalogOutHTiming (ULWord inValue);
	AJA_VIRTUAL bool	GetAnalogOutHTiming (ULWord & outValue);
	AJA_VIRTUAL bool	GetAnalogOutHTiming (ULWord * pOutValue)			{return pOutValue ? GetAnalogOutHTiming (*pOutValue) : false;}	///< @deprecated	Use the alternate function that has the non-constant reference output parameter instead.

	AJA_VIRTUAL bool	WriteOutputTimingControl (ULWord inValue, NTV2Channel inChannel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool	ReadOutputTimingControl (ULWord * pOutValue, NTV2Channel inChannel = NTV2_CHANNEL1);

	AJA_VIRTUAL bool	SetSDI1OutHTiming (ULWord value);
	AJA_VIRTUAL bool	GetSDI1OutHTiming(ULWord* value);
	AJA_VIRTUAL bool	SetSDI2OutHTiming (ULWord value);
	AJA_VIRTUAL bool	GetSDI2OutHTiming(ULWord* value);
	///@}

	AJA_VIRTUAL bool	SetSDIOutputStandard (const NTV2Channel inChannel, const NTV2Standard inValue);
	AJA_VIRTUAL bool	GetSDIOutputStandard (const NTV2Channel inChannel, NTV2Standard & outValue);
	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool	SetSDIOutStandard (const NTV2Standard value, const NTV2Channel channel = NTV2_CHANNEL1);	///< @deprecated	Use SetSDIOutputStandard instead.
		AJA_VIRTUAL bool	GetSDIOutStandard (NTV2Standard & outStandard, const NTV2Channel channel = NTV2_CHANNEL1);	///< @deprecated	Use GetSDIOutputStandard instead.
		AJA_VIRTUAL bool	GetSDIOutStandard (NTV2Standard* value, NTV2Channel channel);								///< @deprecated	Use GetSDIOutputStandard instead.
		AJA_VIRTUAL bool	SetK2SDI1OutStandard (NTV2Standard value);		///< @deprecated	Use SetSDIOutputStandard(NTV2Channel,NTV2Standard) instead.
		AJA_VIRTUAL bool	GetK2SDI1OutStandard (NTV2Standard* value);		///< @deprecated	Use GetSDIOutputStandard(NTV2Channel,NTV2Standard&) instead.
		AJA_VIRTUAL bool	SetK2SDI2OutStandard (NTV2Standard value);		///< @deprecated	Use SetSDIOutputStandard(NTV2Channel,NTV2Standard) instead.
		AJA_VIRTUAL bool	GetK2SDI2OutStandard (NTV2Standard* value);		///< @deprecated	Use GetSDIOutputStandard(NTV2Channel,NTV2Standard&) instead.
		AJA_VIRTUAL bool	SetK2SDI3OutStandard (NTV2Standard value);		///< @deprecated	Use SetSDIOutputStandard(NTV2Channel,NTV2Standard) instead.
		AJA_VIRTUAL bool	GetK2SDI3OutStandard (NTV2Standard* value);		///< @deprecated	Use GetSDIOutputStandard(NTV2Channel,NTV2Standard&) instead.
		AJA_VIRTUAL bool	SetK2SDI4OutStandard (NTV2Standard value);		///< @deprecated	Use SetSDIOutputStandard(NTV2Channel,NTV2Standard) instead.
		AJA_VIRTUAL bool	GetK2SDI4OutStandard (NTV2Standard* value);		///< @deprecated	Use GetSDIOutputStandard(NTV2Channel,NTV2Standard&) instead.
		AJA_VIRTUAL bool	SetK2SDI5OutStandard (NTV2Standard value);		///< @deprecated	Use SetSDIOutputStandard(NTV2Channel,NTV2Standard) instead.
		AJA_VIRTUAL bool	GetK2SDI5OutStandard (NTV2Standard* value);		///< @deprecated	Use GetSDIOutputStandard(NTV2Channel,NTV2Standard&) instead.
		AJA_VIRTUAL bool	SetK2SDI6OutStandard (NTV2Standard value);		///< @deprecated	Use SetSDIOutputStandard(NTV2Channel,NTV2Standard) instead.
		AJA_VIRTUAL bool	GetK2SDI6OutStandard (NTV2Standard* value);		///< @deprecated	Use GetSDIOutputStandard(NTV2Channel,NTV2Standard&) instead.
		AJA_VIRTUAL bool	SetK2SDI7OutStandard (NTV2Standard value);		///< @deprecated	Use SetSDIOutputStandard(NTV2Channel,NTV2Standard) instead.
		AJA_VIRTUAL bool	GetK2SDI7OutStandard (NTV2Standard* value);		///< @deprecated	Use GetSDIOutputStandard(NTV2Channel,NTV2Standard&) instead.
		AJA_VIRTUAL bool	SetK2SDI8OutStandard (NTV2Standard value);		///< @deprecated	Use SetSDIOutputStandard(NTV2Channel,NTV2Standard) instead.
		AJA_VIRTUAL bool	GetK2SDI8OutStandard (NTV2Standard* value);		///< @deprecated	Use GetSDIOutputStandard(NTV2Channel,NTV2Standard&) instead.
	#endif	//	!NTV2_DEPRECATE

	AJA_VIRTUAL bool	SetSDIOutVPID (const ULWord inValueA, const ULWord inValueB, const NTV2Channel channel = NTV2_CHANNEL1);
	AJA_VIRTUAL bool	GetSDIOutVPID (ULWord & outValueA, ULWord & outValueB, const NTV2Channel channel = NTV2_CHANNEL1);
	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool	SetK2SDI1OutVPID(ULWord valueA, ULWord valueB = 0);			///< @deprecated	Use SetSDIOutVPID(ULWord,ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetK2SDI1OutVPID(ULWord* valueA, ULWord* valueB = NULL);	///< @deprecated	Use GetSDIOutVPID(ULWord&,ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetK2SDI2OutVPID(ULWord valueA, ULWord valueB = 0);			///< @deprecated	Use SetSDIOutVPID(ULWord,ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetK2SDI2OutVPID(ULWord* valueA, ULWord* valueB = NULL);	///< @deprecated	Use GetSDIOutVPID(ULWord&,ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetK2SDI3OutVPID(ULWord valueA, ULWord valueB = 0);			///< @deprecated	Use SetSDIOutVPID(ULWord,ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetK2SDI3OutVPID(ULWord* valueA, ULWord* valueB = NULL);	///< @deprecated	Use GetSDIOutVPID(ULWord&,ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetK2SDI4OutVPID(ULWord valueA, ULWord valueB = 0);			///< @deprecated	Use SetSDIOutVPID(ULWord,ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetK2SDI4OutVPID(ULWord* valueA, ULWord* valueB = NULL);	///< @deprecated	Use GetSDIOutVPID(ULWord&,ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetK2SDI5OutVPID(ULWord valueA, ULWord valueB = 0);			///< @deprecated	Use SetSDIOutVPID(ULWord,ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetK2SDI5OutVPID(ULWord* valueA, ULWord* valueB = NULL);	///< @deprecated	Use GetSDIOutVPID(ULWord&,ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetK2SDI6OutVPID(ULWord valueA, ULWord valueB = 0);			///< @deprecated	Use SetSDIOutVPID(ULWord,ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetK2SDI6OutVPID(ULWord* valueA, ULWord* valueB = NULL);	///< @deprecated	Use GetSDIOutVPID(ULWord&,ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetK2SDI7OutVPID(ULWord valueA, ULWord valueB = 0);			///< @deprecated	Use SetSDIOutVPID(ULWord,ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetK2SDI7OutVPID(ULWord* valueA, ULWord* valueB = NULL);	///< @deprecated	Use GetSDIOutVPID(ULWord&,ULWord&,NTV2Channel) instead.
		AJA_VIRTUAL bool	SetK2SDI8OutVPID(ULWord valueA, ULWord valueB = 0);			///< @deprecated	Use SetSDIOutVPID(ULWord,ULWord,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetK2SDI8OutVPID(ULWord* valueA, ULWord* valueB = NULL);	///< @deprecated	Use GetSDIOutVPID(ULWord&,ULWord&,NTV2Channel) instead.
	#endif	//	!defined (NTV2_DEPRECATE)

	AJA_VIRTUAL bool	SetUpConvertMode (NTV2UpConvertMode value);
	AJA_VIRTUAL bool	GetUpConvertMode(NTV2UpConvertMode* value);
	AJA_VIRTUAL bool	SetConverterOutStandard (NTV2Standard value);
	AJA_VIRTUAL bool	GetConverterOutStandard(NTV2Standard* value);
	AJA_VIRTUAL bool	SetConverterOutRate (NTV2FrameRate value);
	AJA_VIRTUAL bool	GetConverterOutRate(NTV2FrameRate* value);
	AJA_VIRTUAL bool	SetConverterInStandard (NTV2Standard value);
	AJA_VIRTUAL bool	GetConverterInStandard(NTV2Standard* value);
	AJA_VIRTUAL bool	SetConverterInRate (NTV2FrameRate value);
	AJA_VIRTUAL bool	GetConverterInRate(NTV2FrameRate* value);
	AJA_VIRTUAL bool	SetConverterPulldown(ULWord value);
	AJA_VIRTUAL bool	GetConverterPulldown(ULWord* value);
	AJA_VIRTUAL bool	SetUCPassLine21 (ULWord value);
	AJA_VIRTUAL bool	GetUCPassLine21(ULWord* value);
	AJA_VIRTUAL bool	SetUCAutoLine21 (ULWord value);
	AJA_VIRTUAL bool	GetUCAutoLine21(ULWord* value);

	AJA_VIRTUAL bool	SetDownConvertMode (NTV2DownConvertMode value);
	AJA_VIRTUAL bool	GetDownConvertMode(NTV2DownConvertMode* value);
	AJA_VIRTUAL bool	SetIsoConvertMode (NTV2IsoConvertMode value);
	AJA_VIRTUAL bool	GetIsoConvertMode(NTV2IsoConvertMode* value);
	AJA_VIRTUAL bool	SetEnableConverter(bool value);
	AJA_VIRTUAL bool	GetEnableConverter(bool* value);
	AJA_VIRTUAL bool	SetDeinterlaceMode(ULWord value);
	AJA_VIRTUAL bool	GetDeinterlaceMode(ULWord* value);

	AJA_VIRTUAL bool	SetSecondConverterOutStandard (NTV2Standard value);
	AJA_VIRTUAL bool	GetSecondConverterOutStandard(NTV2Standard* value);
	AJA_VIRTUAL bool	SetSecondConverterInStandard (NTV2Standard value);
	AJA_VIRTUAL bool	GetSecondConverterInStandard(NTV2Standard* value);
	AJA_VIRTUAL bool	SetSecondDownConvertMode (NTV2DownConvertMode value);
	AJA_VIRTUAL bool	GetSecondDownConvertMode(NTV2DownConvertMode* value);
	AJA_VIRTUAL bool	SetSecondIsoConvertMode (NTV2IsoConvertMode value);
	AJA_VIRTUAL bool	GetSecondIsoConvertMode(NTV2IsoConvertMode* value);
	AJA_VIRTUAL bool	SetSecondConverterPulldown (ULWord value);
	AJA_VIRTUAL bool	GetSecondConverterPulldown(ULWord* value);

	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool	SetK2FrameSyncControlFrameDelay (NTV2FrameSyncSelect select, ULWord value);						///< @deprecated	This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetK2FrameSyncControlFrameDelay (NTV2FrameSyncSelect select, ULWord *value);					///< @deprecated	This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetK2FrameSyncControlStandard (NTV2FrameSyncSelect select, NTV2Standard value);					///< @deprecated	This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetK2FrameSyncControlStandard (NTV2FrameSyncSelect select, NTV2Standard *value);				///< @deprecated	This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetK2FrameSyncControlGeometry (NTV2FrameSyncSelect select, NTV2FrameGeometry value);			///< @deprecated	This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetK2FrameSyncControlGeometry (NTV2FrameSyncSelect select, NTV2FrameGeometry *value);			///< @deprecated	This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetK2FrameSyncControlFrameFormat (NTV2FrameSyncSelect select, NTV2FrameBufferFormat value);		///< @deprecated	This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetK2FrameSyncControlFrameFormat (NTV2FrameSyncSelect select, NTV2FrameBufferFormat *value);	///< @deprecated	This SDK no longer supports the FS1.
	#endif	//	!defined (NTV2_DEPRECATE)

#if !defined (FORCE_USE_SIGNAL_ROUTER)
	/**
		@brief	Backtraces the current signal routing from the given output channel to determine the video format being used,
				then sets the output standard based on that format.
		@note	This functionality is now performed automatically by the driver when AutoCirculate is initialized.
		@note	This function will be deprecated in a future SDK.
	**/
	AJA_VIRTUAL bool	SetVideoOutputStandard (const NTV2Channel inChannel);	///< @deprecated	This function is obsolete.
#endif	//	!defined (FORCE_USE_SIGNAL_ROUTER)

	/**
		@brief		Selects the color space converter operation method.
		@param[in]	inCSCMethod		Specifies the method by which the color space converter will transform its input into its output.
		@param[in]	inChannel		Specifies the CSC of interest.
		@return		True if the call was successful; otherwise false. 
		@note		When selecting NTV2_CSC_Method_Enhanced_4K as the method, the channel must be NTV2_CHANNEL1 or NTV2_CHANNEL5.
					This will group four CSCs together to process the 4K image. To leave 4K, take CSC 1 (or CSC 5) out of 4K mode. 
	**/
	AJA_VIRTUAL bool					SetColorSpaceMethod (const NTV2ColorSpaceMethod inCSCMethod, const NTV2Channel inChannel);

	/**
		@brief		Returns the color space converter operation method.
		@param[in]	inChannel		Specifies the CSC of interest.
		@return		An enum value indicating the operationg mode of the color space converter. 
	**/
	AJA_VIRTUAL NTV2ColorSpaceMethod	GetColorSpaceMethod (const NTV2Channel inChannel);

	AJA_VIRTUAL bool	SetColorSpaceMatrixSelect (NTV2ColorSpaceMatrixType  type, NTV2Channel channel= NTV2_CHANNEL1);
	AJA_VIRTUAL bool	GetColorSpaceMatrixSelect (NTV2ColorSpaceMatrixType* type, NTV2Channel channel= NTV2_CHANNEL1);

	AJA_VIRTUAL bool	GenerateGammaTable(NTV2LutType lutType, int bank, double *table);
	AJA_VIRTUAL bool	DownloadLUTToHW (double *table, NTV2Channel channel, int bank);
	AJA_VIRTUAL bool	SetLUTEnable(bool enable, NTV2Channel channel);
	AJA_VIRTUAL void	LoadLUTTable(double *table);
	AJA_VIRTUAL bool	SetLUTV2HostAccessBank (NTV2ColorCorrectionHostAccessBank value);
	AJA_VIRTUAL bool	GetLUTV2HostAccessBank (NTV2ColorCorrectionHostAccessBank *value, NTV2Channel channel);
	AJA_VIRTUAL bool	SetLUTV2OutputBank (NTV2Channel channel, ULWord bank);
	AJA_VIRTUAL bool	GetLUTV2OutputBank (NTV2Channel channel, ULWord *bank);

	AJA_VIRTUAL bool	SetColorSpaceRGBBlackRange(NTV2RGBBlackRange rgbBlackRange,NTV2Channel channel= NTV2_CHANNEL1);
	AJA_VIRTUAL bool	GetColorSpaceRGBBlackRange(NTV2RGBBlackRange* rgbBlackRange,NTV2Channel channel= NTV2_CHANNEL1);

	AJA_VIRTUAL bool	SetColorSpaceUseCustomCoefficient(ULWord useCustomCoefficient, NTV2Channel channel= NTV2_CHANNEL1);
	AJA_VIRTUAL bool	GetColorSpaceUseCustomCoefficient(ULWord* useCustomCoefficient, NTV2Channel channel= NTV2_CHANNEL1);

	AJA_VIRTUAL bool	SetColorSpaceMakeAlphaFromKey(ULWord makeAlphaFromKey, NTV2Channel channel= NTV2_CHANNEL1);
	AJA_VIRTUAL bool	GetColorSpaceMakeAlphaFromKey(ULWord* makeAlphaFromKey, NTV2Channel channel= NTV2_CHANNEL1);

	AJA_VIRTUAL bool	GetColorSpaceVideoKeySyncFail(bool* videoKeySyncFail, NTV2Channel channel= NTV2_CHANNEL1);

	AJA_VIRTUAL bool	SetColorSpaceCustomCoefficients(ColorSpaceConverterCustomCoefficients useCustomCoefficient, NTV2Channel channel= NTV2_CHANNEL1);
	AJA_VIRTUAL bool	GetColorSpaceCustomCoefficients(ColorSpaceConverterCustomCoefficients* useCustomCoefficient, NTV2Channel channel= NTV2_CHANNEL1);

	AJA_VIRTUAL bool	SetColorSpaceCustomCoefficients12Bit(ColorSpaceConverterCustomCoefficients useCustomCoefficient, NTV2Channel channel= NTV2_CHANNEL1);
	AJA_VIRTUAL bool	GetColorSpaceCustomCoefficients12Bit(ColorSpaceConverterCustomCoefficients* useCustomCoefficient, NTV2Channel channel= NTV2_CHANNEL1);

	AJA_VIRTUAL bool	SetConversionMode(NTV2ConversionMode inConversionMode);
	AJA_VIRTUAL bool	GetConversionMode(NTV2ConversionMode * pOutConversionMode)			{return pOutConversionMode ? GetConversionMode (*pOutConversionMode) : false;}
	AJA_VIRTUAL bool	GetConversionMode(NTV2ConversionMode & outConversionMode);

	AJA_VIRTUAL bool	SetSecondaryVideoFormat(NTV2VideoFormat inFormat);
	AJA_VIRTUAL bool	GetSecondaryVideoFormat(NTV2VideoFormat * pOutFormat)				{return pOutFormat ? GetSecondaryVideoFormat (*pOutFormat) : false;}
	AJA_VIRTUAL bool	GetSecondaryVideoFormat(NTV2VideoFormat & outFormat);

	AJA_VIRTUAL bool	SetInputVideoSelect (NTV2InputVideoSelect inInputSelect);
	AJA_VIRTUAL bool	GetInputVideoSelect(NTV2InputVideoSelect * pOutInputSelect)			{return pOutInputSelect ? GetInputVideoSelect (*pOutInputSelect) : false;}
	AJA_VIRTUAL bool	GetInputVideoSelect(NTV2InputVideoSelect & outInputSelect);

	AJA_VIRTUAL bool		SetLUTControlSelect(NTV2LUTControlSelect inLUTSelect);
	AJA_VIRTUAL inline bool	GetLUTControlSelect(NTV2LUTControlSelect * pOutLUTSelect)		{return pOutLUTSelect ? GetLUTControlSelect (*pOutLUTSelect) : false;}
	AJA_VIRTUAL bool		GetLUTControlSelect(NTV2LUTControlSelect & outLUTSelect);

	//	--------------------------------------------
	//	GetNTV2VideoFormat functions
	//		@deprecated		These static functions don't work correctly, and will be deprecated.
	//		For a given frame rate, geometry and transport, there may be 2 (or more!) possible matching video formats.
	//		The improved GetNTV2VideoFormat function may return a new CNTV2SDIVideoInfo object that can be interrogated about many things.
	//		@note			This function originated in CNTV2Status.
	static NTV2VideoFormat		GetNTV2VideoFormat (NTV2FrameRate frameRate, UByte inputGeometry, bool progressiveTransport, bool isThreeG, bool progressivePicture=false);
	static NTV2VideoFormat		GetNTV2VideoFormat (NTV2FrameRate frameRate, NTV2Standard standard, bool isThreeG, UByte inputGeometry=0, bool progressivePicture=false);
	static NTV2VideoFormat		GetNTV2VideoFormat (NTV2FrameRate frameRate, NTV2V2Standard standard, bool isThreeG, UByte inputGeometry=0, bool progressivePicture=false);
	//	--------------------------------------------

	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL NTV2VideoFormat GetNTV2VideoFormat(UByte status, UByte frameRateHiBit);											///< @deprecated	Does not support progressivePicture, 3G, 2K, etc.
		AJA_VIRTUAL NTV2VideoFormat GetNTV2VideoFormat(NTV2FrameRate frameRate, NTV2Standard standard);								///< @deprecated	Does not support progressivePicture, 3G, 2K, etc.
		AJA_VIRTUAL NTV2VideoFormat GetNTV2VideoFormat(NTV2FrameRate frameRate, UByte inputGeometry, bool progressiveTransport);	///< @deprecated	Does not support progressivePicture, 3G, etc.
	#else	//	else defined (NTV2_DEPRECATE)
protected:
	#endif	//	else defined (NTV2_DEPRECATE)
	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL NTV2VideoFormat GetInput1VideoFormat (bool progressivePicture = false);		///< @deprecated	Use GetInputVideoFormat or GetSDIInputVideoFormat instead.
		AJA_VIRTUAL NTV2VideoFormat GetInput2VideoFormat (bool progressivePicture = false);		///< @deprecated	Use GetInputVideoFormat or GetSDIInputVideoFormat instead.
		AJA_VIRTUAL NTV2VideoFormat GetInput3VideoFormat (bool progressivePicture = false);		///< @deprecated	Use GetInputVideoFormat or GetSDIInputVideoFormat instead.
		AJA_VIRTUAL NTV2VideoFormat GetInput4VideoFormat (bool progressivePicture = false);		///< @deprecated	Use GetInputVideoFormat or GetSDIInputVideoFormat instead.
		AJA_VIRTUAL NTV2VideoFormat GetInput5VideoFormat (bool progressivePicture = false);		///< @deprecated	Use GetInputVideoFormat or GetSDIInputVideoFormat instead.
		AJA_VIRTUAL NTV2VideoFormat GetInput6VideoFormat (bool progressivePicture = false);		///< @deprecated	Use GetInputVideoFormat or GetSDIInputVideoFormat instead.
		AJA_VIRTUAL NTV2VideoFormat GetInput7VideoFormat (bool progressivePicture = false);		///< @deprecated	Use GetInputVideoFormat or GetSDIInputVideoFormat instead.
		AJA_VIRTUAL NTV2VideoFormat GetInput8VideoFormat (bool progressivePicture = false);		///< @deprecated	Use GetInputVideoFormat or GetSDIInputVideoFormat instead.
	#endif

public:
	/**
		@brief		Returns the video format of the signal that is present on the given input source.
		@param[in]	inVideoSource		Specifies the video input source.
		@param[in]	inIsProgressive		Optionally specifies if the video format is expected to be progressive or not.
		@return		A valid NTV2VideoFormat if successful; otherwise returns NTV2_FORMAT_UNKNOWN.
		@details	This function allows client applications to determine the kind of video signal, if any, is being presented
					to a given input source of the device. Because the hardware has no way of knowing if the incoming signal
					is progressive or interlaced (e.g., 525/29.97fps progressive versus 525/59.94fps interlaced),
					the function assumes interlaced, but the caller can override the function's "interlace" assumption.
	**/
	AJA_VIRTUAL NTV2VideoFormat GetInputVideoFormat (const NTV2InputSource inVideoSource = NTV2_INPUTSOURCE_SDI1, const bool inIsProgressive = false);

	/**
		@brief		Returns the video format of the signal that is present on the given SDI input source.
		@param[in]	inChannel			Specifies the input channel of interest.
		@param[in]	inIsProgressive		Optionally specifies if the video format is expected to be progressive or not.
		@return		A valid NTV2VideoFormat if successful; otherwise returns NTV2_FORMAT_UNKNOWN.
		@details	This function allows client applications to determine the kind of video signal, if any, is being presented
					to a given input source of the device. Because the hardware has no way of knowing if the incoming signal
					is progressive or interlaced (e.g., 525/29.97fps progressive versus 525/59.94fps interlaced),
					the function assumes interlaced, but the caller can override the function's "interlace" assumption.
	**/
	AJA_VIRTUAL NTV2VideoFormat GetSDIInputVideoFormat (NTV2Channel inChannel, bool inIsProgressive = false);

	/**
		@brief		Returns the video format of the signal that is present on the device's HDMI input.
		@return		A valid NTV2VideoFormat if successful; otherwise returns NTV2_FORMAT_UNKNOWN.
	**/
	AJA_VIRTUAL NTV2VideoFormat GetHDMIInputVideoFormat (void);

	/**
		@brief		Returns the video format of the signal that is present on the device's analog video input.
		@return		A valid NTV2VideoFormat if successful; otherwise returns NTV2_FORMAT_UNKNOWN.
	**/
	AJA_VIRTUAL NTV2VideoFormat GetAnalogInputVideoFormat (void);

	/**
		@brief		Returns the video format of the signal that is present on the device's composite video input.
		@return		A valid NTV2VideoFormat if successful; otherwise returns NTV2_FORMAT_UNKNOWN.
	**/
	AJA_VIRTUAL NTV2VideoFormat GetAnalogCompositeInputVideoFormat (void);

	/**
		@brief		Returns the video format of the signal that is present on the device's reference input.
		@return		A valid NTV2VideoFormat if successful; otherwise returns NTV2_FORMAT_UNKNOWN.
	**/
	AJA_VIRTUAL NTV2VideoFormat GetReferenceVideoFormat (void);

	AJA_VIRTUAL bool	GetSDIInput3GPresent (bool & outValue, const NTV2Channel channel);
	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL NTV2VideoFormat GetInputVideoFormat (int inputNum, bool progressivePicture = false);	///< @deprecated		Use GetInputVideoFormat(NTV2InputSource...) instead.
		AJA_VIRTUAL bool	GetSDIInput3GPresent (bool* value, NTV2Channel channel);		///< @deprecated		Use GetSDIInput3GPresent(bool&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetSDI1Input3GPresent (bool* value);							///< @deprecated		Use GetSDIInput3GPresent(bool&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetSDI2Input3GPresent (bool* value);							///< @deprecated		Use GetSDIInput3GPresent(bool&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetSDI3Input3GPresent (bool* value);							///< @deprecated		Use GetSDIInput3GPresent(bool&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetSDI4Input3GPresent (bool* value);							///< @deprecated		Use GetSDIInput3GPresent(bool&,NTV2Channel) instead.
	#endif	//	!NTV2_DEPRECATE

	AJA_VIRTUAL bool	GetSDIInput3GbPresent (bool & outValue, const NTV2Channel channel);
	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool	GetSDIInput3GbPresent (bool* value, NTV2Channel channel);		///< @deprecated		Use GetSDIInput3GbPresent(bool&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetSDI1Input3GbPresent (bool* value);							///< @deprecated		Use GetSDIInput3GbPresent(bool&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetSDI2Input3GbPresent (bool* value);							///< @deprecated		Use GetSDIInput3GbPresent(bool&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetSDI3Input3GbPresent (bool* value);							///< @deprecated		Use GetSDIInput3GbPresent(bool&,NTV2Channel) instead.
		AJA_VIRTUAL bool	GetSDI4Input3GbPresent (bool* value);							///< @deprecated		Use GetSDIInput3GbPresent(bool&,NTV2Channel) instead.

		// Kona/Xena LS specific
		AJA_VIRTUAL bool	SetLSVideoADCMode(NTV2LSVideoADCMode value);					///< @deprecated		The Kona/Xena LS is obsolete and unsupported.
		AJA_VIRTUAL bool	GetLSVideoADCMode(NTV2LSVideoADCMode* value);					///< @deprecated		The Kona/Xena LS is obsolete and unsupported.

		AJA_VIRTUAL bool	SetKLSInputSelect(NTV2InputSource value);						///< @deprecated		The Kona/Xena LS is obsolete and unsupported.
		AJA_VIRTUAL bool	GetKLSInputSelect(NTV2InputSource* value);						///< @deprecated		The Kona/Xena LS is obsolete and unsupported.

		// Kona/Xena LH specific
		// Used to pick downconverter on inputs(sd bitfile only)
		AJA_VIRTUAL bool	SetLHDownconvertInput(bool value);								///< @deprecated		The Kona/Xena LH is obsolete and unsupported.
		AJA_VIRTUAL bool	GetLHDownconvertInput(bool* value);								///< @deprecated		The Kona/Xena LH is obsolete and unsupported.

		// Used to pick downconverter on outputs(hd bitfile only)
		AJA_VIRTUAL bool	SetLHSDIOutput1Select(NTV2LHOutputSelect value);				///< @deprecated		The Kona/Xena LH is obsolete and unsupported.
		AJA_VIRTUAL bool	GetLHSDIOutput1Select(NTV2LHOutputSelect* value);				///< @deprecated		The Kona/Xena LH is obsolete and unsupported.
		AJA_VIRTUAL bool	SetLHSDIOutput2Select(NTV2LHOutputSelect value);				///< @deprecated		The Kona/Xena LH is obsolete and unsupported.
		AJA_VIRTUAL bool	GetLHSDIOutput2Select(NTV2LHOutputSelect* value);				///< @deprecated		The Kona/Xena LH is obsolete and unsupported.
		AJA_VIRTUAL bool	SetLHAnalogOutputSelect(NTV2LHOutputSelect value);				///< @deprecated		The Kona/Xena LH is obsolete and unsupported.
		AJA_VIRTUAL bool	GetLHAnalogOutputSelect(NTV2LHOutputSelect* value);				///< @deprecated		The Kona/Xena LH is obsolete and unsupported.
	#endif	//	!NTV2_DEPRECATE

#if !defined (FORCE_USE_SIGNAL_ROUTER)
	/**
		@name	Signal Routing
	**/
	///@{
	AJA_VIRTUAL bool	SetXptCompressionModInputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptCompressionModInputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptConversionModInputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptConversionModInputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptColorSpaceConverterInputSelect (NTV2OutputCrosspointID value);	///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptColorSpaceConverterInputSelect(NTV2OutputCrosspointID* value);	///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptCSC1VidInputSelect(NTV2OutputCrosspointID value);					///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptCSC1VidInputSelect(NTV2OutputCrosspointID* value);				///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptLUTInputSelect (NTV2OutputCrosspointID value);					///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptLUTInputSelect(NTV2OutputCrosspointID* value);					///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptDuallinkOutInputSelect (NTV2OutputCrosspointID value);		///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptDuallinkOutInputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptFrameSync2InputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptFrameSync2InputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptFrameSync1InputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptFrameSync1InputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptFrameBuffer1InputSelect (NTV2OutputCrosspointID value);		///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptFrameBuffer1InputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptCSC1KeyInputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptCSC1KeyInputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptSDIOut2InputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptSDIOut2InputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptSDIOut1InputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptSDIOut1InputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptAnalogOutInputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptAnalogOutInputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptMixer1BGKeyInputSelect(NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptMixer1BGKeyInputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptMixer1BGVidInputSelect(NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptMixer1BGVidInputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptMixer1FGKeyInputSelect(NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptMixer1FGKeyInputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptMixer1FGVidInputSelect(NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptMixer1FGVidInputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptCSC2KeyInputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptCSC2KeyInputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptCSC2VidInputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptCSC2VidInputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptLUT2InputSelect (NTV2OutputCrosspointID value);				///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptLUT2InputSelect(NTV2OutputCrosspointID* value);				///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptFrameBuffer2InputSelect (NTV2OutputCrosspointID value);		///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptFrameBuffer2InputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptWaterMarkerInputSelect (NTV2OutputCrosspointID value);		///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptWaterMarkerInputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptIICTInputSelect (NTV2OutputCrosspointID value);				///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptIICTInputSelect(NTV2OutputCrosspointID* value);				///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptHDMIOutInputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptHDMIOutInputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptSecondConverterInputSelect (NTV2OutputCrosspointID value);	///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptSecondConverterInputSelect(NTV2OutputCrosspointID* value);	///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptWaterMarker2InputSelect (NTV2OutputCrosspointID value);		///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptWaterMarker2InputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptIICT2InputSelect (NTV2OutputCrosspointID value);				///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptIICT2InputSelect(NTV2OutputCrosspointID* value);				///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptDuallinkOut2InputSelect (NTV2OutputCrosspointID value);		///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptDuallinkOut2InputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptSDIOut3InputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptSDIOut3InputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptSDIOut4InputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptSDIOut4InputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptSDIOut5InputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptSDIOut5InputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptMixer2BGKeyInputSelect(NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptMixer2BGKeyInputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptMixer2BGVidInputSelect(NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptMixer2BGVidInputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptMixer2FGKeyInputSelect(NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptMixer2FGKeyInputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptMixer2FGVidInputSelect(NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptMixer2FGVidInputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptSDIOut1DS2InputSelect(NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptSDIOut1DS2InputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptSDIOut2DS2InputSelect(NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptSDIOut2DS2InputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptDualLinkIn1Select(NTV2OutputCrosspointID value);				///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptDualLinkIn1Select(NTV2OutputCrosspointID* value);				///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptDualLinkIn1DSSelect(NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptDualLinkIn1DSSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptDualLinkIn2Select(NTV2OutputCrosspointID value);				///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptDualLinkIn2Select(NTV2OutputCrosspointID* value);				///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptDualLinkIn2DSSelect(NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptDualLinkIn2DSSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptLUT3InputSelect(NTV2OutputCrosspointID value);				///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptLUT3InputSelect(NTV2OutputCrosspointID* value);				///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptLUT4InputSelect (NTV2OutputCrosspointID value);				///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptLUT4InputSelect(NTV2OutputCrosspointID* value);				///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptLUT5InputSelect (NTV2OutputCrosspointID value);				///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptLUT5InputSelect(NTV2OutputCrosspointID* value);				///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptFrameBuffer3InputSelect(NTV2OutputCrosspointID value);		///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptFrameBuffer3InputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptFrameBuffer4InputSelect(NTV2OutputCrosspointID value);		///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptFrameBuffer4InputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptSDIOut3DS2InputSelect(NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptSDIOut3DS2InputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptSDIOut4DS2InputSelect(NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptSDIOut4DS2InputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptSDIOut5DS2InputSelect(NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptSDIOut5DS2InputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptDualLinkIn3Select (NTV2OutputCrosspointID value);				///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptDualLinkIn3Select(NTV2OutputCrosspointID* value);				///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptDualLinkIn3DSSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptDualLinkIn3DSSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptDualLinkIn4Select (NTV2OutputCrosspointID value);				///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptDualLinkIn4Select(NTV2OutputCrosspointID* value);				///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptDualLinkIn4DSSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptDualLinkIn4DSSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptDuallinkOut3InputSelect (NTV2OutputCrosspointID value);		///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptDuallinkOut3InputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptDuallinkOut4InputSelect (NTV2OutputCrosspointID value);		///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptDuallinkOut4InputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptDuallinkOut5InputSelect (NTV2OutputCrosspointID value);		///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptDuallinkOut5InputSelect(NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptCSC3VidInputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptCSC3VidInputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptCSC3KeyInputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptCSC3KeyInputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptCSC4VidInputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptCSC4VidInputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptCSC4KeyInputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptCSC4KeyInputSelect(NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptCSC5VidInputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptCSC5VidInputSelect (NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptCSC5KeyInputSelect (NTV2OutputCrosspointID value);			///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptCSC5KeyInputSelect (NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXpt4KDCQ1InputSelect (NTV2OutputCrosspointID value);				///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXpt4KDCQ1InputSelect (NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXpt4KDCQ2InputSelect (NTV2OutputCrosspointID value);				///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXpt4KDCQ2InputSelect (NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXpt4KDCQ3InputSelect (NTV2OutputCrosspointID value);				///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXpt4KDCQ3InputSelect (NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXpt4KDCQ4InputSelect (NTV2OutputCrosspointID value);				///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXpt4KDCQ4InputSelect (NTV2OutputCrosspointID* value);			///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.

	AJA_VIRTUAL bool	SetXptHDMIOutV2Q1InputSelect (NTV2OutputCrosspointID value);		///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptHDMIOutV2Q1InputSelect (NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptHDMIOutV2Q2InputSelect (NTV2OutputCrosspointID value);		///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptHDMIOutV2Q2InputSelect (NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptHDMIOutV2Q3InputSelect (NTV2OutputCrosspointID value);		///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptHDMIOutV2Q3InputSelect (NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
	AJA_VIRTUAL bool	SetXptHDMIOutV2Q4InputSelect (NTV2OutputCrosspointID value);		///< @deprecated	Use the Connect or Disconnect function instead.
	AJA_VIRTUAL bool	GetXptHDMIOutV2Q4InputSelect (NTV2OutputCrosspointID* value);		///< @deprecated	Use the GetConnectedOutput, IsConnected or IsConnectedTo functions instead.
#endif	//	!defined (FORCE_USE_SIGNAL_ROUTER)

	/**
		@brief		Answers with the current NTV2OutputCrosspointID for the given NTV2InputCrosspointID.
		@param[in]	inInputXpt		Specifies the input (signal sink) of interest.
		@param[out]	outOutputXpt	Receives the output (signal source) the given input is connected to (if connected),
									or NTV2_XptBlack if not connected.
		@return		True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	GetConnectedOutput (const NTV2InputCrosspointID inInputXpt, NTV2OutputCrosspointID & outOutputXpt);

	/**
		@brief		Connects the given widget signal input (sink) to the given widget signal output (source).
		@param[in]	inInputXpt		Specifies the input (signal sink) to be connected to the given output.
		@param[in]	inOutputXpt		Specifies the output (signal source) to be connected to the given input.
									Specifying NTV2_XptBlack effects a disconnect.
		@return		True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	Connect (const NTV2InputCrosspointID inInputXpt, const NTV2OutputCrosspointID inOutputXpt);

	/**
		@brief		Disconnects the given widget signal input (sink) from whatever output (source) it may be connected.
		@param[in]	inInputXpt		Specifies the input (signal sink) to be disconnected.
		@return		True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	Disconnect (const NTV2InputCrosspointID inInputXpt);

	/**
		@brief		Answers whether or not the given widget signal input (sink) is connected to another output (source).
		@param[in]	inInputXpt		Specifies the input (signal sink) of interest.
		@param[out]	outIsConnected	Receives true if the input is connected to any other output (other than NTV2_XptBlack).
		@return		True if successful;  otherwise false.
		@note		If the input is connected to NTV2_XptBlack, "outIsConnected" will be "false".
	**/
	AJA_VIRTUAL bool	IsConnected (const NTV2InputCrosspointID inInputXpt, bool & outIsConnected);

	/**
		@brief		Answers whether or not the given widget signal input (sink) is connected to another output (source).
		@param[in]	inInputXpt		Specifies the input (signal sink) of interest.
		@param[in]	inOutputXpt		Specifies the output (signal source) of interest. It's okay to specify NTV2_XptBlack.
		@param[out]	outIsConnected	Receives true if the input is connected to the specified output.
		@return		True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	IsConnectedTo (const NTV2InputCrosspointID inInputXpt, const NTV2OutputCrosspointID inOutputXpt, bool & outIsConnected);


	/**
		@brief		Answers whether or not the given widget signal input (sink) can legally be connected to the given signal output (source).
		@param[in]	inInputXpt		Specifies the input (signal sink) of interest.
		@param[in]	inOutputXpt		Specifies the output (signal source) of interest.
		@param[out]	outCanConnect	Receives true if the input can be connected to the specified output;  otherwise false.
		@return		True if successful;  otherwise false.
		@bug		This function is not currently implemented.
		@todo		This needs to be implemented.
	**/
	AJA_VIRTUAL bool	CanConnect (const NTV2InputCrosspointID inInputXpt, const NTV2OutputCrosspointID inOutputXpt, bool & outCanConnect);


	/**
		@brief		Applies the given routing table to the AJA device.
		@return		True if successful; otherwise false.
		@param[in]	inRouter		Specifies the CNTV2SignalRouter that contains the routing to be applied to the device.
		@param[in]	inReplace		If true, replaces the device's existing widget routing with the given one.
									If false, augments the device's existing widget routing.
									Defaults to false.
		@details	Most modern AJA devices do not have fixed interconnections between inputs, outputs, frame stores
					and the various video processing widgets (e.g., colorspace converters, mixer/keyers, etc.).
					Instead, these routing configurations are designated by a set of registers, one for each input
					of each widget. The register's value determines which widget output node (crosspoint) the input
					is connected to. A zero value in the register means that the input is not connected to anything.
					To simplify this process of routing widgets on the device, a set of signal paths (i.e., interconnects)
					are built and then applied to the device in this function call.
					This function iterates over each connection that's specified in the given routing table and updates
					the appropriate register in the device.
	**/
	AJA_VIRTUAL bool	ApplySignalRoute (const CNTV2SignalRouter & inRouter, const bool inReplace = false);

	/**
		@brief		Removes all existing signal path connections between any and all widgets on the AJA device.
		@return		True if successful; otherwise false.
		@details	This function writes zeroes into all crosspoint selection registers, effectively
					clearing any existing routing configuration on the device.
	**/
	AJA_VIRTUAL bool	ClearRouting (void);

	/**
		@brief		Answers with the current signal routing between any and all widgets on the AJA device.
		@param[out]	outRouting	Receives the current signal routing.
		@return		True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool	GetRouting (CNTV2SignalRouter & outRouting);

	/**
		@brief		Answers with the current signal routing for the given channel.
		@param[in]	inChannel	Specifies the NTV2Channel of interest.
		@param[out]	outRouting	Receives the current signal routing for the given channel.
		@return		True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool	GetRoutingForChannel (const NTV2Channel inChannel, CNTV2SignalRouter & outRouting);

	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool	OutputRoutingTable (const NTV2RoutingTable * pInRoutingTable);	///< @deprecated		Use the ApplySignalRoute call instead.
	#endif	//	!NTV2_DEPRECATE
	///@}


	/**
		@name	Analog
		@brief	These functions only work on devices with analog inputs.
	**/
	///@{
	AJA_VIRTUAL bool	WriteSDProcAmpControlsInitialized(ULWord value=1);
	AJA_VIRTUAL bool	WriteSDBrightnessAdjustment(ULWord value);
	AJA_VIRTUAL bool	WriteSDContrastAdjustment(ULWord value);
	AJA_VIRTUAL bool	WriteSDSaturationAdjustment(ULWord value);
	AJA_VIRTUAL bool	WriteSDHueAdjustment(ULWord value);
	AJA_VIRTUAL bool	WriteSDCbOffsetAdjustment(ULWord value);
	AJA_VIRTUAL bool	WriteSDCrOffsetAdjustment(ULWord value);
	AJA_VIRTUAL bool	WriteHDProcAmpControlsInitialized(ULWord value=1);
	AJA_VIRTUAL bool	WriteHDBrightnessAdjustment(ULWord value);
	AJA_VIRTUAL bool	WriteHDContrastAdjustment(ULWord value);
	AJA_VIRTUAL bool	WriteHDSaturationAdjustmentCb(ULWord value);
	AJA_VIRTUAL bool	WriteHDSaturationAdjustmentCr(ULWord value);
	AJA_VIRTUAL bool	WriteHDCbOffsetAdjustment(ULWord value);
	AJA_VIRTUAL bool	WriteHDCrOffsetAdjustment(ULWord value);

	AJA_VIRTUAL bool	ReadSDProcAmpControlsInitialized(ULWord *value);
	AJA_VIRTUAL bool	ReadSDBrightnessAdjustment(ULWord *value);
	AJA_VIRTUAL bool	ReadSDContrastAdjustment(ULWord *value);
	AJA_VIRTUAL bool	ReadSDSaturationAdjustment(ULWord *value);
	AJA_VIRTUAL bool	ReadSDHueAdjustment(ULWord *value);
	AJA_VIRTUAL bool	ReadSDCbOffsetAdjustment(ULWord *value);
	AJA_VIRTUAL bool	ReadSDCrOffsetAdjustment(ULWord *value);
	AJA_VIRTUAL bool	ReadHDProcAmpControlsInitialized(ULWord *value);
	AJA_VIRTUAL bool	ReadHDBrightnessAdjustment(ULWord *value);
	AJA_VIRTUAL bool	ReadHDContrastAdjustment(ULWord *value);
	AJA_VIRTUAL bool	ReadHDSaturationAdjustmentCb(ULWord *value);
	AJA_VIRTUAL bool	ReadHDSaturationAdjustmentCr(ULWord *value);
	AJA_VIRTUAL bool	ReadHDCbOffsetAdjustment(ULWord *value);
	AJA_VIRTUAL bool	ReadHDCrOffsetAdjustment(ULWord *value);

	// FS1 (and other?) ProcAmp methods
	AJA_VIRTUAL bool	WriteProcAmpC1YAdjustment(ULWord value);
	AJA_VIRTUAL bool	WriteProcAmpC1CBAdjustment(ULWord value);
	AJA_VIRTUAL bool	WriteProcAmpC1CRAdjustment(ULWord value);
	AJA_VIRTUAL bool	WriteProcAmpC2CBAdjustment(ULWord value);
	AJA_VIRTUAL bool	WriteProcAmpC2CRAdjustment(ULWord value);
	AJA_VIRTUAL bool	WriteProcAmpOffsetYAdjustment(ULWord value);
	AJA_VIRTUAL bool	ReadProcAmpC1YAdjustment(ULWord* value);
	AJA_VIRTUAL bool	ReadProcAmpC1CBAdjustment(ULWord* value);
	AJA_VIRTUAL bool	ReadProcAmpC1CRAdjustment(ULWord* value);
	AJA_VIRTUAL bool	ReadProcAmpC2CBAdjustment(ULWord* value);
	AJA_VIRTUAL bool	ReadProcAmpC2CRAdjustment(ULWord* value);
	AJA_VIRTUAL bool	ReadProcAmpOffsetYAdjustment(ULWord* value);

	AJA_VIRTUAL bool		SetAnalogInputADCMode (NTV2LSVideoADCMode inValue);
	AJA_VIRTUAL inline bool	GetAnalogInputADCMode (NTV2LSVideoADCMode * pOutValue)						{return pOutValue ? GetAnalogInputADCMode (*pOutValue) : false;}
	AJA_VIRTUAL bool		GetAnalogInputADCMode (NTV2LSVideoADCMode & outValue);
	///@}

	/**
		@name	HDMI
	**/
	///@{
	AJA_VIRTUAL bool		SetHDMIOut3DPresent (bool value);
	AJA_VIRTUAL bool		GetHDMIOut3DPresent (bool & out3DPresent);
	AJA_VIRTUAL inline bool	GetHDMIOut3DPresent (bool * pOut3DPresent)									{return pOut3DPresent ? GetHDMIOut3DPresent (*pOut3DPresent) : false;}

	AJA_VIRTUAL bool		SetHDMIOut3DMode (NTV2HDMIOut3DMode value);
	AJA_VIRTUAL bool		GetHDMIOut3DMode (NTV2HDMIOut3DMode & outValue);
	AJA_VIRTUAL inline bool	GetHDMIOut3DMode (NTV2HDMIOut3DMode * pOutValue)							{return pOutValue ? GetHDMIOut3DMode (*pOutValue) : false;}

	AJA_VIRTUAL bool		GetHDMIInputStatusRegister (ULWord & outValue);
	AJA_VIRTUAL inline bool	GetHDMIInputStatusRegister (ULWord * pOutValue)								{return pOutValue ? GetHDMIInputStatusRegister (*pOutValue) : false;}

	AJA_VIRTUAL bool		GetHDMIInputColor (NTV2LHIHDMIColorSpace & outValue);
	AJA_VIRTUAL inline bool	GetHDMIInputColor (NTV2LHIHDMIColorSpace * pOutValue)						{return pOutValue ? GetHDMIInputColor (*pOutValue) : false;}

	AJA_VIRTUAL bool	SetHDMIV2TxBypass (bool inBypass);

	AJA_VIRTUAL bool		SetHDMIInputRange (NTV2HDMIRange inNewValue);
	AJA_VIRTUAL bool		GetHDMIInputRange (NTV2HDMIRange & outValue);
	AJA_VIRTUAL inline bool	GetHDMIInputRange (NTV2HDMIRange * pOutValue)								{return pOutValue ? GetHDMIInputRange (*pOutValue) : false;}

	AJA_VIRTUAL bool		SetHDMIOutVideoStandard (NTV2Standard inNewValue);
	AJA_VIRTUAL bool		GetHDMIOutVideoStandard (NTV2Standard & outValue);
	AJA_VIRTUAL inline bool	GetHDMIOutVideoStandard (NTV2Standard * pOutValue)							{return pOutValue ? GetHDMIOutVideoStandard (*pOutValue) : false;}

	AJA_VIRTUAL bool		SetHDMIV2OutVideoStandard (NTV2V2Standard inNewValue);
	AJA_VIRTUAL bool		GetHDMIOutVideoStandard (NTV2V2Standard & outValue);
	AJA_VIRTUAL inline bool	GetHDMIV2OutVideoStandard (NTV2V2Standard * pOutValue)						{return pOutValue ? GetHDMIOutVideoStandard (*pOutValue) : false;}

	AJA_VIRTUAL bool		SetHDMISampleStructure (NTV2HDMISampleStructure inNewValue);
	AJA_VIRTUAL bool		GetHDMISampleStructure (NTV2HDMISampleStructure & outValue);
	AJA_VIRTUAL inline bool	GetHDMISampleStructure (NTV2HDMISampleStructure * pOutValue)				{return pOutValue ? GetHDMISampleStructure (*pOutValue) : false;}

	AJA_VIRTUAL bool		SetHDMIOutVideoFPS (NTV2FrameRate inNewValue);
	AJA_VIRTUAL bool		GetHDMIOutVideoFPS (NTV2FrameRate & outValue);
	AJA_VIRTUAL inline bool	GetHDMIOutVideoFPS (NTV2FrameRate * pOutValue)								{return pOutValue ? GetHDMIOutVideoFPS (*pOutValue) : false;}

	AJA_VIRTUAL bool		SetHDMIOutRange (NTV2HDMIRange inNewValue);
	AJA_VIRTUAL bool		GetHDMIOutRange (NTV2HDMIRange & outValue);
	AJA_VIRTUAL inline bool	GetHDMIOutRange (NTV2HDMIRange * pOutValue)									{return pOutValue ? GetHDMIOutRange (*pOutValue) : false;}

	AJA_VIRTUAL bool		SetHDMIOutAudioChannels (NTV2HDMIAudioChannels inNewValue);
	AJA_VIRTUAL bool		GetHDMIOutAudioChannels (NTV2HDMIAudioChannels & outValue);
	AJA_VIRTUAL inline bool	GetHDMIOutAudioChannels (NTV2HDMIAudioChannels * pOutValue)					{return pOutValue ? GetHDMIOutAudioChannels (*pOutValue) : false;}

	AJA_VIRTUAL bool		SetHDMIInColorSpace (NTV2HDMIColorSpace inNewValue);
	AJA_VIRTUAL bool		GetHDMIInColorSpace (NTV2HDMIColorSpace & outValue);
	AJA_VIRTUAL inline bool	GetHDMIInColorSpace (NTV2HDMIColorSpace * pOutValue)						{return pOutValue ? GetHDMIInColorSpace (*pOutValue) : false;}
	AJA_VIRTUAL bool		SetHDMIOutColorSpace (NTV2HDMIColorSpace inNewValue);
	AJA_VIRTUAL bool		GetHDMIOutColorSpace (NTV2HDMIColorSpace & outValue);
	AJA_VIRTUAL inline bool	GetHDMIOutColorSpace (NTV2HDMIColorSpace * pOutValue)						{return pOutValue ? GetHDMIOutColorSpace (*pOutValue) : false;}
	AJA_VIRTUAL bool	SetLHIHDMIOutColorSpace (NTV2LHIHDMIColorSpace inNewValue);
	AJA_VIRTUAL bool	GetLHIHDMIOutColorSpace (NTV2LHIHDMIColorSpace* value);

	AJA_VIRTUAL bool		SetHDMIOutBitDepth (NTV2HDMIBitDepth inNewValue);
	AJA_VIRTUAL bool		GetHDMIOutBitDepth (NTV2HDMIBitDepth & outValue);
	AJA_VIRTUAL inline bool	GetHDMIOutBitDepth (NTV2HDMIBitDepth * pOutValue)							{return pOutValue ? GetHDMIOutBitDepth (*pOutValue) : false;}

	AJA_VIRTUAL bool		SetHDMIOutProtocol (NTV2HDMIProtocol inNewValue);
	AJA_VIRTUAL bool		GetHDMIOutProtocol (NTV2HDMIProtocol & outValue);
	AJA_VIRTUAL inline bool	GetHDMIOutProtocol (NTV2HDMIProtocol * pOutValue)							{return pOutValue ? GetHDMIOutProtocol (*pOutValue) : false;}

	AJA_VIRTUAL bool		GetHDMIOutDownstreamBitDepth (NTV2HDMIBitDepth & outValue);
	AJA_VIRTUAL inline bool	GetHDMIOutDownstreamBitDepth (NTV2HDMIBitDepth * pOutValue)					{return pOutValue ? GetHDMIOutDownstreamBitDepth (*pOutValue) : false;}

	AJA_VIRTUAL bool		GetHDMIOutDownstreamColorSpace (NTV2LHIHDMIColorSpace & outValue);
	AJA_VIRTUAL inline bool	GetHDMIOutDownstreamColorSpace (NTV2LHIHDMIColorSpace * pOutValue)			{return pOutValue ? GetHDMIOutDownstreamColorSpace (*pOutValue) : false;}

	AJA_VIRTUAL bool		SetHDMIAudioSampleRateConverterEnable (bool inNewValue);
	AJA_VIRTUAL bool		GetHDMIAudioSampleRateConverterEnable (bool & outIsEnabled);
	AJA_VIRTUAL inline bool	GetHDMIAudioSampleRateConverterEnable (bool* pOutIsEnabled)					{return pOutIsEnabled ? GetHDMIAudioSampleRateConverterEnable (*pOutIsEnabled) : false;}

	/**
		@brief						Sets the HDMI output's 2-channel audio source.
		@param[in]	inNewValue		Specifies the audio channels from the given audio system to be used.
		@param[in]	inAudioSystem	Specifies the audio system that will supply audio samples to the HDMI output. Defaults to NTV2_AUDIOSYSTEM_1.
		@return						True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	SetHDMIOutAudioSource2Channel (const NTV2AudioChannelPair inNewValue, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);

	/**
		@brief						Answers with the HDMI output's current 2-channel audio source.
		@param[out]	outValue		Receives the audio channels that are currently being used.
		@param[out]	outAudioSystem	Receives the audio system that is currently supplying audio samples to the HDMI output.
		@return						True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	GetHDMIOutAudioSource2Channel (NTV2AudioChannelPair & outValue, NTV2AudioSystem & outAudioSystem);

	/**
		@brief						Changes the HDMI output's 8-channel audio source.
		@param[in]	inNewValue		Specifies the audio channels from the given audio system to be used.
		@param[in]	inAudioSystem	Specifies the audio system that will supply audio samples to the HDMI output. Defaults to NTV2_AUDIOSYSTEM_1.
		@return						True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	SetHDMIOutAudioSource8Channel (const NTV2Audio8ChannelSelect inNewValue, const NTV2AudioSystem inAudioSystem = NTV2_AUDIOSYSTEM_1);

	/**
		@brief						Answers with the HDMI output's current 8-channel audio source.
		@param[out]	outValue		Receives the audio channels that are currently being used.
		@param[out]	outAudioSystem	Receives the audio system that is currently supplying audio samples to the HDMI output.
		@return						True if successful;  otherwise false.
	**/
	AJA_VIRTUAL bool	GetHDMIOutAudioSource8Channel (NTV2Audio8ChannelSelect & outValue, NTV2AudioSystem & outAudioSystem);

	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool	GetHDMIOutAudioSource2Channel (NTV2AudioChannelPair * pOutValue, NTV2Channel * pOutChannel = NULL);		///< @deprecated	Use the GetHDMIOutAudioSource8Channel function that has an NTV2AudioSystem reference.
		AJA_VIRTUAL bool	GetHDMIOutAudioSource2Channel (NTV2AudioChannelPair & outValue, NTV2Channel & outChannel);				///< @deprecated	Use the GetHDMIOutAudioSource8Channel function that has an NTV2AudioSystem reference.
		AJA_VIRTUAL bool	GetHDMIOutAudioSource8Channel (NTV2Audio8ChannelSelect * pOutValue, NTV2Channel * pOutChannel = NULL);	///< @deprecated	Use the GetHDMIOutAudioSource8Channel function that has an NTV2AudioSystem reference.
		AJA_VIRTUAL bool	GetHDMIOutAudioSource8Channel (NTV2Audio8ChannelSelect & outValue, NTV2Channel & outChannel);			///< @deprecated	Use the GetHDMIOutAudioSource8Channel function that has an NTV2AudioSystem reference.
	#endif	//	!defined (NTV2_DEPRECATE)

	/**
		@brief		Enables or disables decimate mode on the device's HDMI rasterizer, which halves the
					output frame rate when enabled. This allows a 60 Hz video stream to be displayed on
					a 30 Hz HDMI montitor.
		@return		True if successful; otherwise false.
		@param[in]	inEnable		If true, enables decimation mode; otherwise disables decimation mode.
	**/
	AJA_VIRTUAL bool		SetHDMIV2DecimateMode(bool inEnable);

	AJA_VIRTUAL bool		GetHDMIV2DecimateMode(bool & outIsEnabled);
	AJA_VIRTUAL inline bool	GetHDMIV2DecimateMode(bool* pOutIsEnabled)					{return pOutIsEnabled ? GetHDMIV2DecimateMode (*pOutIsEnabled) : false;}

	/**
	@brief		Enables or disables two sample interleave I/O mode on the device's HDMI rasterizere
	@return		True if successful; otherwise false.
	@param[in]	tsiEnable		If true, enables two sample interleave I/O; otherwise disables two sample interleave I/O.
	**/
	AJA_VIRTUAL bool		SetHDMIV2TsiIO(bool tsiEnable);

	AJA_VIRTUAL bool		GetHDMIV2TsiIO(bool & tsiEnabled);
	AJA_VIRTUAL inline bool GetHDMIV2TsiIO(bool * pTsiEnabled)								{ return pTsiEnabled ? GetHDMIV2TsiIO (*pTsiEnabled) : false; }



	/**
		@brief		Enables or disables level-B mode on the device's HDMI rasterizer.
		@return		True if successful; otherwise false.
		@param[in]	inEnable		If true, enables level-B mode; otherwise disables level-B mode.
	**/
	AJA_VIRTUAL bool		SetHDMIV2LevelBMode(bool inEnable);

	AJA_VIRTUAL bool		GetHDMIV2LevelBMode(bool & outIsEnabled);
	AJA_VIRTUAL inline bool	GetHDMIV2LevelBMode(bool* pOutIsEnabled)					{return pOutIsEnabled ? GetHDMIV2LevelBMode (*pOutIsEnabled) : false;}

	/**
		@brief		Sets HDMI V2 mode for the device.
		@return		True if successful; otherwise false.
		@param[in]	inMode	Specifies the HDMI V2 operation mode for the device.
							Use NTV2_HDMI_V2_HDSD_BIDIRECTIONAL for HD or SD capture and playback.
							Use NTV2_HDMI_V2_4K_CAPTURE for 4K capture.
							Use NTV2_HDMI_V2_4K_PLAYBACK for 4K playback.
							Note that 4K modes are exclusive.
	**/
	AJA_VIRTUAL bool		SetHDMIV2Mode (NTV2HDMIV2Mode inMode);

	/**
		@brief		Answers with the current HDMI V2 mode of the device.
		@return		True if successful; otherwise false.
		@param[out]	outMode	Receives the current HDMI V2 operation mode for the device.
	**/
	AJA_VIRTUAL bool		GetHDMIV2Mode (NTV2HDMIV2Mode & outMode);
	AJA_VIRTUAL inline bool	GetHDMIV2Mode (NTV2HDMIV2Mode* pOutMode)					{return pOutMode ? GetHDMIV2Mode (*pOutMode) : false;}
	///@}

	#if !defined (NTV2_DEPRECATE)		////	FS1		////////////////////////////////////////
		// Analog (FS1 / MOAB)
		AJA_VIRTUAL NTV2VideoFormat GetFS1AnalogCompositeInputVideoFormat();						///< @deprecated		This SDK no longer supports the FS1.

		// Reg 95 stuff
		AJA_VIRTUAL bool	SetFS1ReferenceSelect(NTV2FS1ReferenceSelect value);					///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1ReferenceSelect(NTV2FS1ReferenceSelect *value);					///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1ColorFIDSubcarrierReset(bool value);								///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1ColorFIDSubcarrierReset(bool *value);								///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1FreezeOutput(NTV2FS1FreezeOutput value);							///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1FreezeOutput(NTV2FS1FreezeOutput *value);							///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1XptProcAmpInputSelect(NTV2OutputCrosspointID value);				///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1XptProcAmpInputSelect(NTV2OutputCrosspointID *value);				///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1XptSecondAnalogOutInputSelect(NTV2OutputCrosspointID value);		///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1XptSecondAnalogOutInputSelect(NTV2OutputCrosspointID *value);		///< @deprecated		This SDK no longer supports the FS1.


		AJA_VIRTUAL bool	SetFS1AudioDelay(int value);											///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1AudioDelay(int *value);											///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetLossOfInput(ULWord value);											///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioAnalogLevel(NTV2FS1AudioLevel value);						///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1AudioAnalogLevel(NTV2FS1AudioLevel *value);						///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioTone(NTV2FS1AudioTone value);								///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1AudioTone(NTV2FS1AudioTone *value);								///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1OutputTone(NTV2FS1OutputTone value);								///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1OutputTone(NTV2FS1OutputTone *value);								///< @deprecated		This SDK no longer supports the FS1.

		// Audio Channel Mapping registers
		AJA_VIRTUAL bool	SetFS1AudioGain_Ch1(int value);											///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioGain_Ch2(int value);											///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioGain_Ch3(int value);											///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioGain_Ch4(int value);											///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioGain_Ch5(int value);											///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioGain_Ch6(int value);											///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioGain_Ch7(int value);											///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioGain_Ch8(int value);											///< @deprecated		This SDK no longer supports the FS1.

		AJA_VIRTUAL bool	SetFS1AudioPhase_Ch1(bool value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioPhase_Ch2(bool value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioPhase_Ch3(bool value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioPhase_Ch4(bool value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioPhase_Ch5(bool value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioPhase_Ch6(bool value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioPhase_Ch7(bool value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioPhase_Ch8(bool value);										///< @deprecated		This SDK no longer supports the FS1.

		AJA_VIRTUAL bool	SetFS1AudioSource_Ch1(NTV2AudioChannelMapping value);					///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioSource_Ch2(NTV2AudioChannelMapping value);					///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioSource_Ch3(NTV2AudioChannelMapping value);					///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioSource_Ch4(NTV2AudioChannelMapping value);					///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioSource_Ch5(NTV2AudioChannelMapping value);					///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioSource_Ch6(NTV2AudioChannelMapping value);					///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioSource_Ch7(NTV2AudioChannelMapping value);					///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioSource_Ch8(NTV2AudioChannelMapping value);					///< @deprecated		This SDK no longer supports the FS1.

		AJA_VIRTUAL bool	SetFS1AudioMute_Ch1(bool value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioMute_Ch2(bool value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioMute_Ch3(bool value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioMute_Ch4(bool value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioMute_Ch5(bool value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioMute_Ch6(bool value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioMute_Ch7(bool value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1AudioMute_Ch8(bool value);										///< @deprecated		This SDK no longer supports the FS1.

		AJA_VIRTUAL bool	SetFS1VideoDAC2Mode (NTV2K2VideoDACMode value);							///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1VideoDAC2Mode (NTV2K2VideoDACMode* value);						///< @deprecated		This SDK no longer supports the FS1.

		AJA_VIRTUAL bool	GetFS1I2C1ControlWrite(ULWord *value);									///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1I2C1ControlWrite(ULWord value);									///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1I2C1ControlRead(ULWord *value);									///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1I2C1ControlRead(ULWord value);									///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1I2C1ControlBusy(ULWord *value);									///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1I2C1ControlError(ULWord *value);									///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1I2C1Address(ULWord *value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1I2C1Address(ULWord value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1I2C1SubAddress(ULWord *value);									///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1I2C1SubAddress(ULWord value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1I2C1Data(ULWord *value);											///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1I2C1Data(ULWord value);											///< @deprecated		This SDK no longer supports the FS1.

		AJA_VIRTUAL bool	GetFS1I2C2ControlWrite(ULWord *value);									///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1I2C2ControlWrite(ULWord value);									///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1I2C2ControlRead(ULWord *value);									///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1I2C2ControlRead(ULWord value);									///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1I2C2ControlBusy(ULWord *value);									///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1I2C2ControlError(ULWord *value);									///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1I2C2Address(ULWord *value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1I2C2Address(ULWord value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1I2C2SubAddress(ULWord *value);									///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1I2C2SubAddress(ULWord value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1I2C2Data(ULWord *value);											///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1I2C2Data(ULWord value);											///< @deprecated		This SDK no longer supports the FS1.

		AJA_VIRTUAL bool	SetFS1DownConvertAFDAutoEnable(bool value);								///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1DownConvertAFDAutoEnable(bool* value);							///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1SecondDownConvertAFDAutoEnable(bool value);						///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1SecondDownConvertAFDAutoEnable(bool* value);						///< @deprecated		This SDK no longer supports the FS1.

		AJA_VIRTUAL bool	SetFS1DownConvertAFDDefaultHoldLast(bool value);						///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1DownConvertAFDDefaultHoldLast(bool* value);						///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetFS1SecondDownConvertAFDDefaultHoldLast(bool value);					///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetFS1SecondDownConvertAFDDefaultHoldLast(bool* value);					///< @deprecated		This SDK no longer supports the FS1.

		// Received AFD (Read-only)
		AJA_VIRTUAL bool	GetAFDReceivedCode(ULWord* value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetAFDReceivedAR(ULWord* value);										///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetAFDReceivedVANCPresent(bool* value);									///< @deprecated		This SDK no longer supports the FS1.

		AJA_VIRTUAL bool	SetAFDInsertMode_SDI1(NTV2AFDInsertMode value);							///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetAFDInsertMode_SDI1(NTV2AFDInsertMode* value);						///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetAFDInsertMode_SDI2(NTV2AFDInsertMode value);							///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetAFDInsertMode_SDI2(NTV2AFDInsertMode* value);						///< @deprecated		This SDK no longer supports the FS1.

		AJA_VIRTUAL bool	SetAFDInsertAR_SDI1(NTV2AFDInsertAspectRatio value);					///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetAFDInsertAR_SDI1(NTV2AFDInsertAspectRatio* value);					///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetAFDInsertAR_SDI2(NTV2AFDInsertAspectRatio value);					///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetAFDInsertAR_SDI2(NTV2AFDInsertAspectRatio* value);					///< @deprecated		This SDK no longer supports the FS1.

		AJA_VIRTUAL bool	SetAFDInsert_SDI1(NTV2AFDInsertCode value);								///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetAFDInsert_SDI1(NTV2AFDInsertCode* value);							///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetAFDInsert_SDI2(NTV2AFDInsertCode value);								///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetAFDInsert_SDI2(NTV2AFDInsertCode* value);							///< @deprecated		This SDK no longer supports the FS1.

		AJA_VIRTUAL bool	SetAFDInsertLineNumber_SDI1(ULWord value);								///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetAFDInsertLineNumber_SDI1(ULWord* value);								///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	SetAFDInsertLineNumber_SDI2(ULWord value);								///< @deprecated		This SDK no longer supports the FS1.
		AJA_VIRTUAL bool	GetAFDInsertLineNumber_SDI2(ULWord* value);								///< @deprecated		This SDK no longer supports the FS1.
	#endif	//	!NTV2_DEPRECATE

	AJA_VIRTUAL bool	SetLHIVideoDACStandard(NTV2Standard value);
	AJA_VIRTUAL bool	GetLHIVideoDACStandard(NTV2Standard *value);
	AJA_VIRTUAL bool	SetLHIVideoDACMode(NTV2LHIVideoDACMode value);
	AJA_VIRTUAL bool	GetLHIVideoDACMode(NTV2LHIVideoDACMode* value);
	AJA_VIRTUAL bool	SetLHIVideoDACMode(NTV2VideoDACMode value);	// overloaded
	AJA_VIRTUAL bool	GetLHIVideoDACMode(NTV2VideoDACMode* value);	// overloaded

	/**
		@name	Analog LTC
	**/
	///@{
	AJA_VIRTUAL bool		SetLTCInputEnable (bool inEnable);
	AJA_VIRTUAL bool		GetLTCInputEnable (bool & outIsEnabled);
	AJA_VIRTUAL inline bool	GetLTCInputEnable (bool * pOutValue)										{return pOutValue ? GetLTCInputEnable (*pOutValue) : false;}
	AJA_VIRTUAL bool		GetLTCInputPresent (bool & outIsPresent);
	AJA_VIRTUAL inline bool	GetLTCInputPresent (bool * pOutValue)										{return pOutValue ? GetLTCInputPresent (*pOutValue) : false;}
	AJA_VIRTUAL bool		SetLTCOnReference (bool inNewValue);
	AJA_VIRTUAL bool		GetLTCOnReference (bool & outLTCIsOnReference);
	AJA_VIRTUAL inline bool	GetLTCOnReference (bool * pOutValue)										{return pOutValue ? GetLTCOnReference (*pOutValue) : false;}

	AJA_VIRTUAL bool		SetLTCEmbeddedOutEnable (bool inNewValue);
	AJA_VIRTUAL bool		GetLTCEmbeddedOutEnable (bool & outValue);
	AJA_VIRTUAL inline bool	GetLTCEmbeddedOutEnable (bool * pOutValue)									{return pOutValue ? GetLTCEmbeddedOutEnable (*pOutValue) : false;}

	/**
		@brief	Reads the current contents of the device's analog LTC input registers.
		@param[in]	inLTCInput		Specifies the device's analog LTC input to use. Use 0 for LTC In 1, or 1 for LTC In 2.
									(Call ::NTV2DeviceCanDoLTCInN to determine if the device has 1, 2 or more analog LTC inputs.)
		@param[out]	outRP188Data	Receives the timecode read from the device registers. Only the "Low" and "High" fields are set --
									the "DBB" field is set to zero.
		@return		True if successful; otherwise false.
		@note		The registers are read immediately, and should contain stable data if called soon after the VBI.
	**/
	AJA_VIRTUAL bool	ReadAnalogLTCInput (const UWord inLTCInput, RP188_STRUCT & outRP188Data);

	/**
		@brief	Answers with the (SDI) input channel that's providing the clock reference being used by the given device's analog LTC input.
		@param[in]	inLTCInput		Specifies the device's analog LTC input. Use 0 for LTC In 1, or 1 for LTC In 2.
									(Call ::NTV2DeviceCanDoLTCInN to determine if the device has 1, 2 or more analog LTC inputs.)
		@param[out]	outChannel		Receives the NTV2Channel that is currently providing the clock reference for reading the given analog LTC input.
		@return		True if successful; otherwise false.
		@note		This function is provided for devices that are capable of handling multiple, disparate video formats (see ::NTV2DeviceCanDoMultiFormat
					and GetMultiFormatMode functions). It doesn't make sense to call this function on a device that is running in "UniFormat" mode.
	**/
	AJA_VIRTUAL bool	GetAnalogLTCInClockChannel (const UWord inLTCInput, NTV2Channel & outChannel);

	/**
		@brief	Sets the (SDI) input channel that is to provide the clock reference to be used by the given analog LTC input.
		@param[in]	inLTCInput		Specifies the device's analog LTC input. Use 0 for LTC In 1, or 1 for LTC In 2.
									(Use ::NTV2DeviceCanDoLTCInN to determine if the device has 1, 2 or more analog LTC inputs.)
		@param[in]	inChannel		Specifies the NTV2Channel that should provide the clock reference for reading the given analog LTC input.
		@return		True if successful; otherwise false.
		@note		This function is provided for devices that are capable of handling multiple, disparate video formats (see ::NTV2DeviceCanDoMultiFormat
					and GetMultiFormatMode functions). It doesn't make sense to call this function on a device that is running in "UniFormat" mode.
	**/
	AJA_VIRTUAL bool	SetAnalogLTCInClockChannel (const UWord inLTCInput, const NTV2Channel inChannel);

	/**
		@brief	Answers with the (SDI) output channel that's providing the clock reference being used by the given device's analog LTC output.
		@param[in]	inLTCOutput		Specifies the device's analog LTC output. Use 0 for LTC Out 1, or 1 for LTC Out 2.
									(Call ::NTV2DeviceCanDoLTCOutN to determine if the device has 1, 2 or more analog LTC outputs.)
		@param[out]	outChannel		Receives the NTV2Channel that is currently providing the clock reference for writing the given analog LTC output.
		@return		True if successful; otherwise false.
		@note		This function is provided for devices that are capable of handling multiple, disparate video formats (see ::NTV2DeviceCanDoMultiFormat
					and GetMultiFormatMode functions). It doesn't make sense to call this function on a device that is running in "UniFormat" mode.
	**/
	AJA_VIRTUAL bool	GetAnalogLTCOutClockChannel (const UWord inLTCOutput, NTV2Channel & outChannel);

	/**
		@brief	Sets the (SDI) output channel that is to provide the clock reference to be used by the given analog LTC output.
		@param[in]	inLTCInput		Specifies the device's analog LTC output. Use 0 for LTC Out 1, or 1 for LTC Out 2.
									(Use ::NTV2DeviceCanDoLTCOutN to determine if the device has 1, 2 or more analog LTC outputs.)
		@param[in]	inChannel		Specifies the NTV2Channel that should provide the clock reference for writing the given analog LTC output.
		@return		True if successful; otherwise false.
		@note		This function is provided for devices that are capable of handling multiple, disparate video formats (see ::NTV2DeviceCanDoMultiFormat
					and GetMultiFormatMode functions). It doesn't make sense to call this function on a device that is running in "UniFormat" mode.
	**/
	AJA_VIRTUAL bool	SetAnalogLTCOutClockChannel (const UWord inLTCInput, const NTV2Channel inChannel);
	///@}

	/**
		@name	Stereo Compression
	**/
	///@{
	AJA_VIRTUAL bool		SetStereoCompressorOutputMode (NTV2StereoCompressorOutputMode inNewValue);
	AJA_VIRTUAL bool		GetStereoCompressorOutputMode (NTV2StereoCompressorOutputMode & outValue);
	AJA_VIRTUAL inline bool	GetStereoCompressorOutputMode (NTV2StereoCompressorOutputMode * pOutValue)	{return pOutValue ? GetStereoCompressorOutputMode (*pOutValue) : false;}
	AJA_VIRTUAL bool		SetStereoCompressorFlipMode (ULWord inNewValue);
	AJA_VIRTUAL bool		GetStereoCompressorFlipMode (ULWord & outValue);
	AJA_VIRTUAL inline bool	GetStereoCompressorFlipMode (ULWord * pOutValue)							{return pOutValue ? GetStereoCompressorFlipMode (*pOutValue) : false;}
	AJA_VIRTUAL bool		SetStereoCompressorFlipLeftHorz (ULWord inNewValue);
	AJA_VIRTUAL bool		GetStereoCompressorFlipLeftHorz (ULWord & outValue);
	AJA_VIRTUAL inline bool	GetStereoCompressorFlipLeftHorz (ULWord * pOutValue)						{return pOutValue ? GetStereoCompressorFlipLeftHorz (*pOutValue) : false;}
	AJA_VIRTUAL bool		SetStereoCompressorFlipLeftVert (ULWord inNewValue);
	AJA_VIRTUAL bool		GetStereoCompressorFlipLeftVert (ULWord & outValue);
	AJA_VIRTUAL inline bool	GetStereoCompressorFlipLeftVert (ULWord * pOutValue)						{return pOutValue ? GetStereoCompressorFlipLeftVert (*pOutValue) : false;}
	AJA_VIRTUAL bool		SetStereoCompressorFlipRightHorz (ULWord inNewValue);
	AJA_VIRTUAL bool		GetStereoCompressorFlipRightHorz (ULWord & outValue);
	AJA_VIRTUAL inline bool	GetStereoCompressorFlipRightHorz (ULWord * pOutValue)						{return pOutValue ? GetStereoCompressorFlipRightHorz (*pOutValue) : false;}
	AJA_VIRTUAL bool		SetStereoCompressorFlipRightVert (ULWord inNewValue);
	AJA_VIRTUAL bool		GetStereoCompressorFlipRightVert (ULWord & outValue);
	AJA_VIRTUAL inline bool	GetStereoCompressorFlipRightVert (ULWord * pOutValue)						{return pOutValue ? GetStereoCompressorFlipRightVert (*pOutValue) : false;}
	AJA_VIRTUAL bool		SetStereoCompressorStandard (NTV2Standard inNewValue);
	AJA_VIRTUAL bool		GetStereoCompressorStandard (NTV2Standard & outValue);
	AJA_VIRTUAL inline bool	GetStereoCompressorStandard (NTV2Standard * pOutValue)						{return pOutValue ? GetStereoCompressorStandard (*pOutValue) : false;}
	AJA_VIRTUAL bool		SetStereoCompressorLeftSource (NTV2OutputCrosspointID inNewValue);
	AJA_VIRTUAL bool		GetStereoCompressorLeftSource (NTV2OutputCrosspointID & outValue);
	AJA_VIRTUAL inline bool	GetStereoCompressorLeftSource (NTV2OutputCrosspointID * pOutValue)			{return pOutValue ? GetStereoCompressorLeftSource (*pOutValue) : false;}
	AJA_VIRTUAL bool		SetStereoCompressorRightSource (NTV2OutputCrosspointID inNewValue);
	AJA_VIRTUAL bool		GetStereoCompressorRightSource (NTV2OutputCrosspointID & outValue);
	AJA_VIRTUAL inline bool	GetStereoCompressorRightSource (NTV2OutputCrosspointID * pOutValue)			{return pOutValue ? GetStereoCompressorRightSource (*pOutValue) : false;}
	///@}

	/**
		@name	Bi-directional SDI
	**/
	///@{
	/**
		@brief		Assuming the device has bi-directional SDI connectors, this function determines whether
					a given SDI channel will behave as an input or an output.
		@return		True if successful; otherwise false.
		@param[in]	inChannel		Specifies the channel to be affected, which must be one of NTV2_CHANNEL1,
									NTV2_CHANNEL2, NTV2_CHANNEL3, or NTV2_CHANNEL4.
		@param[in]	inEnable		If true, specifies that the channel connector is to be used as an output.
									If false, specifies it's to be used as an input.
	**/
	AJA_VIRTUAL bool		SetSDITransmitEnable (NTV2Channel inChannel, bool inEnable);

	/**
		@brief		Assuming the device has bi-directional SDI connectors, this function answers if a given SDI
					channel is currently acting as an input or an output.
		@return		True if successful; otherwise false.
		@param[in]	inChannel		Specifies the channel to be affected, which must be one of NTV2_CHANNEL1,
									NTV2_CHANNEL2, NTV2_CHANNEL3, or NTV2_CHANNEL4.
		@param[in]	outEnabled		Receives true if the SDI channel connector is transmitting, or false if it's acting as an input.
	**/
	AJA_VIRTUAL bool		GetSDITransmitEnable (NTV2Channel inChannel, bool & outEnabled);
	AJA_VIRTUAL inline bool	GetSDITransmitEnable (NTV2Channel inChannel, bool* pOutEnabled)			{return pOutEnabled ? GetSDITransmitEnable (inChannel, *pOutEnabled) : false;}
	///@}

	AJA_VIRTUAL bool		SetSDIOut2Kx1080Enable (NTV2Channel inChannel, const bool inIsEnabled);
	AJA_VIRTUAL bool		GetSDIOut2Kx1080Enable (NTV2Channel inChannel, bool & outIsEnabled);
	AJA_VIRTUAL inline bool	GetSDIOut2Kx1080Enable (NTV2Channel inChannel, bool* pOutIsEnabled)		{return pOutIsEnabled ? GetSDIOut2Kx1080Enable (inChannel, *pOutIsEnabled) : false;}

	AJA_VIRTUAL bool		SetSDIOut3GEnable (NTV2Channel inChannel, bool enable);
	AJA_VIRTUAL bool		GetSDIOut3GEnable (NTV2Channel inChannel, bool & outIsEnabled);
	AJA_VIRTUAL inline bool	GetSDIOut3GEnable (NTV2Channel inChannel, bool* pOutIsEnabled)			{return pOutIsEnabled ? GetSDIOut3GEnable (inChannel, *pOutIsEnabled) : false;}

	AJA_VIRTUAL bool		SetSDIOut3GbEnable (NTV2Channel inChannel, bool enable);
	AJA_VIRTUAL bool		GetSDIOut3GbEnable (NTV2Channel inChannel, bool & outIsEnabled);
	AJA_VIRTUAL inline bool	GetSDIOut3GbEnable (NTV2Channel inChannel, bool* pOutIsEnabled)			{return pOutIsEnabled ? GetSDIOut3GbEnable (inChannel, *pOutIsEnabled) : false;}


	/**
		@name	SDI Bypass Relays
	**/
	///@{
	/**
		@brief	Passes back an enum specifying if the watchdog timer would put
				the SDI relays into bypass or send the signals through the device.
		@return	True if successful; otherwise false.
		@param[out]		outValue	Receives the current state of the watchdog
									timer, either NTV2_BYPASS or NTV2_NORMAL.
		@note	The watchdog timer will not change the state of the relays
				if they are under manual control.
	**/
	AJA_VIRTUAL bool	GetSDIWatchdogStatus (NTV2RelayState & outValue);

	/**
		@brief	Answers if the bypass relays between connectors 1 and 2 are currently
				in bypass or routing the signals through the device.
		@return	True if successful; otherwise false.
		@param[out]		outValue	Receives the current state of the relays (NTV2_BYPASS or NTV2_NORMAL).
	**/
	AJA_VIRTUAL bool	GetSDIRelayPosition12 (NTV2RelayState & outValue);

	/**
		@brief	Answers if the bypass relays between connectors 3 and 4 are currently
				in bypass or routing the signals through the device.
		@return	True if successful; otherwise false.
		@param[out]		outValue	Receives the current state of the relays (NTV2_BYPASS or NTV2_NORMAL).
	**/
	AJA_VIRTUAL bool	GetSDIRelayPosition34 (NTV2RelayState & outValue);

	/**
		@brief	Answers if the bypass relays between connectors 1 and 2 would be in
				bypass or would route signals through the device, if under manual control.
		@return	True if successful; otherwise false.
		@param[out]		outValue	Receives the relay state (NTV2_BYPASS or NTV2_NORMAL).
		@note	Manual control will not change the state of the relays if
				the watchdog timer for the relays is enabled.
	**/
	AJA_VIRTUAL bool	GetSDIRelayManualControl12 (NTV2RelayState & outValue);

	/**
		@brief	Sets the state of the relays between connectors 1 and 2 to
				bypass or through the device, if under manual control.
		@return	True if successful; otherwise false.
		@param[in]	inValue		Specifies the desired relay state (NTV2_BYPASS or NTV2_NORMAL).
		@note	Manual control will not change the state of the relays if
				the watchdog timer for the relays is enabled. Because this
				call modifies the control register, it sends a kick
				sequence, which has the side effect of restarting the
				timeout counter.
	**/
	AJA_VIRTUAL bool	SetSDIRelayManualControl12 (NTV2RelayState inValue);

	/**
		@brief	Answers if the bypass relays between connectors 3 and 4 would be
				in bypass or would route through the device, if under manual control.
		@return	True if successful; otherwise false.
		@param[out]		outValue	Receives the relay state (NTV2_BYPASS or NTV2_NORMAL).
		@note	Manual control will not change the state of the relays if
				the watchdog timer for the relays is enabled.
	**/
	AJA_VIRTUAL bool	GetSDIRelayManualControl34 (NTV2RelayState & outValue);

	/**
		@brief	Sets the state of the relays between connectors 3 and 4 to
				bypass or through the device, if under manual control.
		@return	True if successful; otherwise false.
		@param[in]	inValue		Specifies the relay state (NTV2_BYPASS or NTV2_NORMAL).
		@note	Manual control will not change the state of the relays if
				the watchdog timer for the relays is enabled. Because this
				call modifies the control register, it sends a kick
				sequence, which has the side effect of restarting the
				timeout counter.
	**/
	AJA_VIRTUAL bool	SetSDIRelayManualControl34 (NTV2RelayState inValue);

	/**
		@brief	Answers true if the relays between connectors 1 and 2 are under
				watchdog timer control, or false if they are under manual control.
		@return	True if successful; otherwise false.
		@param[out]		outValue	Receives 'true' if the watchdog timer is in control
				of the relays; otherwise false if the relays are under manual control.
	**/
	AJA_VIRTUAL bool	GetSDIWatchdogEnable12 (bool & outValue);

	/**
		@brief	Specifies if the relays between connectors 1 and 2 should be under
				watchdog timer control or manual control.
		@return	True if successful; otherwise false.
		@param[in] 	inValue	Specify true if if the watchdog timer is to be in control
							of the relays, or false if the relays are to be under
							manual control.
		@note	Because this call modifies the control register, it sends
				a kick sequence, which has the side effect of restarting
				the timeout counter.
	**/
	AJA_VIRTUAL bool	SetSDIWatchdogEnable12 (bool inValue);

	/**
		@brief	Answers true if the relays between connectors 3 and 4 are under
				watchdog timer control, or false if they are under manual control.
		@return	True if successful; otherwise false.
		@param[out]		outValue	Receives 'true' if the watchdog timer is in control
									of the relays; otherwise 'false' if the relays are under
									manual control.
	**/
	AJA_VIRTUAL bool	GetSDIWatchdogEnable34 (bool & outValue);

	/**
		@brief	Specifies if the relays between connectors 3 and 4 should be under
				watchdog timer control or manual control.
		@return	True if successful; otherwise false.
		@param[in]	inValue		Specify true if if the watchdog timer is to be in control
								of the relays, or false if the relays are to be under
								manual control.
		@note	Because this call modifies the control register, it sends
				a kick sequence, which has the side effect of restarting
				the timeout counter.
	**/
	AJA_VIRTUAL bool	SetSDIWatchdogEnable34 (bool inValue);

	/**
		@brief	Answers with the amount of time that must elapse before the watchdog
				timer times out.
		@return	True if successful; otherwise false.
	 	@param[out]		outValue	Receives the time value in units of 8 nanoseconds.
		@note	The timeout interval begins or is reset when a kick
				sequence is received.
	**/
	AJA_VIRTUAL bool	GetSDIWatchdogTimeout (ULWord & outValue);

	/**
		@brief	Specifies the amount of time that must elapse before the watchdog
				timer times out.
		@return	True if successful; otherwise false.
		@param[in]	inValue		Specifies the timeout interval in units of 8 nanoseconds.
		@note	The timeout interval begins or is reset when a kick
				sequence is received. This call resets the timeout counter
				to zero, which will then start counting up until this value
				is reached, triggering the watchdog timer if it's enabled.
	**/
	AJA_VIRTUAL bool	SetSDIWatchdogTimeout (ULWord inValue);

	/**
		@brief	Restarts the countdown timer to prevent the watchdog timer from
				timing out.
		@return	True if successful; otherwise false.
	**/
	AJA_VIRTUAL bool	KickSDIWatchdog (void);

	/**
		@brief	Answers with the current state of all the control registers.
		@return	True if successful; otherwise false.
		@param[out]		outState	Receives the state of the control registers.
	**/
	AJA_VIRTUAL bool	GetSDIWatchdogState(NTV2SDIWatchdogState & outState);

	/**
		@brief	Sets all of the control registers to a given state.
		@return	True if successful; otherwise false.
		@param[in]	inState		Specifies the new control register state.
	**/
	AJA_VIRTUAL bool	SetSDIWatchdogState(const NTV2SDIWatchdogState & inState);
	///@}

	/**
		@name	4K Conversion
	**/
	///@{
	/**
		@brief		Sets 4K Down Convert RGB mode
		@return		True if successful; otherwise false.
		@param[in]	inEnable		If true, specifies RGB mode
									If false, specifies YCC mode
	**/
	AJA_VIRTUAL bool		Enable4KDCRGBMode(bool inEnable);

	AJA_VIRTUAL bool		GetEnable4KDCRGBMode(bool & outIsEnabled);
	AJA_VIRTUAL inline bool	GetEnable4KDCRGBMode(bool* pOutIsEnabled)					{return pOutIsEnabled ? GetEnable4KDCRGBMode (*pOutIsEnabled) : false;}

	/**
		@brief		Sets 4K Down Convert YCC 444 mode
		@return		True if successful; otherwise false.
		@param[in]	inEnable		If true, specifies YCC 444
									If false, specifies YCC 422
	**/
	AJA_VIRTUAL bool		Enable4KDCYCC444Mode(bool inEnable);

	AJA_VIRTUAL bool		GetEnable4KDCYCC444Mode(bool & outIsEnabled);
	AJA_VIRTUAL inline bool	GetEnable4KDCYCC444Mode(bool* pOutIsEnabled)				{return pOutIsEnabled ? GetEnable4KDCYCC444Mode (*pOutIsEnabled) : false;}

	/**
		@brief		Sets 4K Down Convert PSF in mode
		@return		True if successful; otherwise false.
		@param[in]	inEnable		If true, specifies PSF in
									If false, specifies P in
	**/
	AJA_VIRTUAL bool		Enable4KDCPSFInMode(bool inEnable);

	AJA_VIRTUAL bool		GetEnable4KDCPSFInMode(bool & outIsEnabled);
	AJA_VIRTUAL inline bool	GetEnable4KDCPSFInMode(bool* pOutIsEnabled)					{return pOutIsEnabled ? GetEnable4KDCPSFInMode (*pOutIsEnabled) : false;}

	/**
		@brief		Sets 4K Down Convert PSF out Mode
		@return		True if successful; otherwise false.
		@param[in]	inEnable		If true, specifies PSF out
									If false, specifies P out
	**/
	AJA_VIRTUAL bool		Enable4KPSFOutMode(bool inEnable);

	AJA_VIRTUAL bool		GetEnable4KPSFOutMode(bool & outIsEnabled);
	AJA_VIRTUAL inline bool	GetEnable4KPSFOutMode(bool* pOutIsEnabled)					{return pOutIsEnabled ? GetEnable4KPSFOutMode (*pOutIsEnabled) : false;}
	///@}


	/**
		@brief		Enables or disables 3G level b to 3G level a conversion at the SDI output widget (assuming the AJA device can do so).
		@return		True if successful; otherwise false.
		@param[in]	inChannel		Specifies the channel to be affected, which must be NTV2_CHANNEL1 ... NTV2_CHANNEL8.
		@param[in]	inEnable		If true, incomming 3g level b signal converted to 3g level a signal at SDI output widget.
									If false, specifies normal operation.
	**/
	AJA_VIRTUAL bool		SetSDIInLevelBtoLevelAConversion (const NTV2Channel inChannel, const bool inEnable);

	AJA_VIRTUAL bool		GetSDIInLevelBtoLevelAConversion (const NTV2Channel inChannel, bool & outEnable);
	AJA_VIRTUAL inline bool	GetSDIInLevelBtoLevelAConversion (const NTV2Channel inChannel, bool* pOutEnable)	{return pOutEnable ? GetSDIInLevelBtoLevelAConversion (inChannel, *pOutEnable) : false;}

	/**
		@brief		Enables or disables 3G level a to 3G level b conversion at the SDI output widget (assuming the AJA device can do so).
		@return		True if successful; otherwise false.
		@param[in]	inChannel		Specifies the channel to be affected, which must be NTV2_CHANNEL1 ... NTV2_CHANNEL8.
		@param[in]	inEnable		If true, outgoing 3g level a signal converted to 3g level b signal at SDI output widget.
									If false, specifies normal operation.
	**/
	AJA_VIRTUAL bool		SetSDIOutLevelAtoLevelBConversion (const NTV2Channel inChannel, const bool inEnable);

	AJA_VIRTUAL bool		GetSDIOutLevelAtoLevelBConversion (const NTV2Channel inChannel, bool & outEnable);
	AJA_VIRTUAL inline bool	GetSDIOutLevelAtoLevelBConversion (const NTV2Channel inChannel, bool* pOutEnable)	{return pOutEnable ? GetSDIOutLevelAtoLevelBConversion (inChannel, *pOutEnable) : false;}

	/**
		@brief		Enables or disables an RGB-over-3G-level-A conversion at the SDI output widget (assuming the AJA device can do so).
		@return		True if successful; otherwise false.
		@param[in]	inChannel		Specifies the channel to be affected.
		@param[in]	inEnable		If true, perform the conversion at the output SDI spigot;  otherwise have the SDI output spigot operate normally (no conversion).
	**/
	AJA_VIRTUAL bool		SetSDIOutRGBLevelAConversion (const NTV2Channel inChannel, const bool inEnable);

	/**
		@brief		Answers with the device's current RGB-over-3G-level-A conversion at the given SDI output spigot (assuming the device can do such a conversion).
		@return		True if successful; otherwise false.
		@param[in]	inChannel		Specifies the channel (i.e., SDI output spigot) of interest.
		@param[out]	outIsEnabled	Receives true if the device is currently performing the conversion;  otherwise false (not converting).
	**/
	AJA_VIRTUAL bool		GetSDIOutRGBLevelAConversion (const NTV2Channel inChannel, bool & outIsEnabled);


	/**
		@brief		Answers with the TRS error status from a given input channel.
		@return		True if the input channel is currently reporting TRS errors, otherwise false.
		@param[in]	inChannel		Specifies the channel of interest.
	**/
	AJA_VIRTUAL bool		GetSDITRSError (const NTV2Channel inChannel);

	/**
		@brief		Returns SDI Lock Status from inputs.
		@return		True if locked, false if not
		@param[in]	inChannel		Specifies the channel of interest.
	**/
	AJA_VIRTUAL bool		GetSDILock (const NTV2Channel inChannel);

	/**
		@brief		Enables or disables multi-format (per channel) device operation.
					If enabled, each device channel can handle a different video format (provided it's in the same clock family).
					If disabled, all device channels have the same video format.
		@return		True if successful; otherwise false.
		@param[in]	inEnable	If true, sets the device in multi-format mode.
								If false, sets the device in uni-format mode.
	**/
	AJA_VIRTUAL bool	   SetMultiFormatMode (const bool inEnable);

	/**
		@brief		Answers if the device is operating in multi-format (per channel) mode or not.
					If enabled, each device channel can handle a different video format (provided it's in the same clock family).
					If disabled, all device channels have the same video format.
		@return		True if successful; otherwise false.
		@param[in]	outIsEnabled	Receives true if the device is currently in multi-format mode,
									or false if it's in uni-format mode.
	**/
	AJA_VIRTUAL bool		GetMultiFormatMode (bool & outIsEnabled);
	AJA_VIRTUAL inline bool	GetMultiFormatMode (bool* pOutEnabled)						{return pOutEnabled ? GetMultiFormatMode (*pOutEnabled) : false;}


public:
	// Functions for cards that support more than one bitfile
	#if !defined (NTV2_DEPRECATE)
		virtual bool			CheckBitfile(NTV2VideoFormat newValue = NTV2_FORMAT_UNKNOWN);	///< @deprecated	This function is obsolete.
		static int				FormatCompare (NTV2VideoFormat fmt1, NTV2VideoFormat fmt2);		///< @deprecated	This function is obsolete.
	#endif	//	!defined (NTV2_DEPRECATE)

	/**
		@name	RS-422
	**/
	///@{
	/**
		@brief		Sets the parity control on the RS422 port specified by inChannel.
		@return		True if successful; otherwise false.
		@param[in]	inChannel		Specifies the channel to be affected, which must be NTV2_CHANNEL1 or NTV2_CHANNEL2.
		@param[in]	inRS422Parity	Specifies if parity should be used, and if so, whether it should be odd or even.
	**/
	AJA_VIRTUAL bool		SetRS422Parity  (const NTV2Channel inChannel, const NTV2_RS422_PARITY inRS422Parity);

	AJA_VIRTUAL bool		GetRS422Parity (NTV2Channel inChannel, NTV2_RS422_PARITY & outRS422Parity);
	AJA_VIRTUAL inline bool	GetRS422Parity (NTV2Channel inChannel, NTV2_RS422_PARITY * pOutRS422Parity)	{return pOutRS422Parity ? GetRS422Parity (inChannel, *pOutRS422Parity) : false;}

	/**
		@brief		Sets the baud rate the RS422 port specified by inChannel.
		@return		True if successful; otherwise false.
		@param[in]	inChannel		Specifies the channel to be affected, which must be NTV2_CHANNEL1 or NTV2_CHANNEL2.
		@param[in]	inRS422BaudRate	Specifies the baud rate to be used for RS422 communications.
	**/
	AJA_VIRTUAL bool		SetRS422BaudRate  (const NTV2Channel inChannel, const NTV2_RS422_BAUD_RATE inRS422BaudRate);

	AJA_VIRTUAL bool		GetRS422BaudRate (NTV2Channel inChannel, NTV2_RS422_BAUD_RATE & outRS422BaudRate);
	AJA_VIRTUAL inline bool	GetRS422BaudRate (NTV2Channel inChannel, NTV2_RS422_BAUD_RATE * pOutRS422BaudRate)	{return pOutRS422BaudRate ? GetRS422BaudRate (inChannel, *pOutRS422BaudRate) : false;}

	#if !defined (NTV2_DEPRECATE)
		AJA_VIRTUAL bool	ReadUartRxFifoSize (ULWord * pOutSizeInBytes);
	#endif	//	!defined (NTV2_DEPRECATE)
	///@}

public:
	#if !defined (NTV2_DEPRECATE)
		//	These functions all came from the CNTV2Status module...
		AJA_VIRTUAL NTV2ButtonState GetButtonState (int buttonBit);														///< @deprecated	This function is obsolete.
		AJA_VIRTUAL inline Word		GetBoardVersion (void)								{return GetDeviceVersion ();}	///< @deprecated	Use GetDeviceVersion instead.
		AJA_VIRTUAL inline void		GetBoardVersionString (std::string & outString)		{outString = GetDeviceVersionString ();}	///< @deprecated	Use GetDeviceVersionString instead.
		AJA_VIRTUAL inline void		GetFPGAVersionString (std::string & outString, const NTV2XilinxFPGA inFPGA = eFPGAVideoProc)	{outString = GetFPGAVersionString (inFPGA);}	///< @deprecated	Use the GetFPGAVersionString function that returns a std::string instead.
		AJA_VIRTUAL inline void		GetPCIFPGAVersionString (std::string & outString)	{outString = GetPCIFPGAVersionString ();}	///< @deprecated	Use the GetPCIFPGAVersionString function that returns a std::string instead.
		AJA_VIRTUAL inline void		GetBootFPGAVersionString (std::string & outString)	{outString.clear ();}		///< @deprecated	This function is obsolete.
		AJA_VIRTUAL inline void		GetDriverVersionString (std::string & outString)	{outString = GetDriverVersionString ();}	///< @deprecated	Use the GetDriverVersionString function that returns a std::string instead.
		AJA_VIRTUAL inline void		GetBoardIDString (std::string & outString)			{outString = GetBoardIDString ();}	///< @deprecated	Obsolete. Convert the result of GetDeviceID() into a hexa string instead.
		AJA_VIRTUAL std::string		GetBoardIDString (void);		///< @deprecated	Obsolete. Convert the result of GetDeviceID() into a hex string instead.
		AJA_VIRTUAL bool			GetBitFileInformation (ULWord & outNumBytes, std::string & outDateStr, std::string & outTimeStr, NTV2XilinxFPGA inFPGA = eFPGAVideoProc);	///< @deprecated	This function is obsolete (from CNTV2Status).
		AJA_VIRTUAL Word			GetFPGAVersion (const NTV2XilinxFPGA inFPGA = eFPGAVideoProc);			///< @deprecated	This function is obsolete (from CNTV2Status).
	#endif	//	!defined (NTV2_DEPRECATE)

	AJA_VIRTUAL std::string		GetFPGAVersionString (const NTV2XilinxFPGA inFPGA = eFPGAVideoProc);

	AJA_VIRTUAL Word			GetPCIFPGAVersion (void);		//	From CNTV2Status
	AJA_VIRTUAL std::string		GetPCIFPGAVersionString (void);

	/**
		@brief		Returns the size and time/date stamp of the device's currently-installed firmware.
		@param[out]	outNumBytes		Receives the size of the installed firmware image, in bytes.
		@param[out]	outDateStr		Receives a human-readable string containing the date the currently-installed firmware was built.
									The string has the format "YYYY/MM/DD", where "YYYY" is the year, "MM" is the month ("00" thru "12"),
									and "DD" is the day of the month ("00" thru "31").
		@param[out]	outTimeStr		Receives a human-readable string containing the time the currently-installed firmware was built
									(in local Pacific time). The string has the format "HH:MM:SS", where HH is "00" thru "23",
									and both MM and SS are "00" thru "59".
		@return		True if successful;  otherwise false.
		@note		This function has nothing to do with the firmware bitfiles that are currently installed on the local host's file system.
	**/
	AJA_VIRTUAL bool			GetInstalledBitfileInfo (ULWord & outNumBytes, std::string & outDateStr, std::string & outTimeStr);

	AJA_VIRTUAL bool			GetInput1Autotimed (void);			//	From CNTV2Status
	AJA_VIRTUAL bool			GetInput2Autotimed (void);			//	From CNTV2Status
	AJA_VIRTUAL bool			GetAnalogInputAutotimed (void);		//	From CNTV2Status
	AJA_VIRTUAL bool			GetHDMIInputAutotimed (void);		//	From CNTV2Status
	AJA_VIRTUAL bool			GetInputAutotimed (int inInputNum);	//	From CNTV2Status

	/**
		@brief		Returns a string containing the decoded, human-readable device serial number.
		@param[in]	inSerialNumber		Specifies the 64-bit device serial number.
		@return		A string containing the decoded, human-readable device serial number. If invalid, returns the string "INVALID?".
	**/
	static std::string			SerialNum64ToString (const uint64_t inSerialNumber);


protected:
	AJA_VIRTUAL ULWord			GetSerialNumberLow (void);			//	From CNTV2Status
	AJA_VIRTUAL ULWord			GetSerialNumberHigh (void);			//	From CNTV2Status

private:

	// frame buffer sizing helpers
	AJA_VIRTUAL bool	GetLargestFrameBufferFormatInUse(NTV2FrameBufferFormat* format);
	AJA_VIRTUAL bool	GetFrameInfo(NTV2Channel channel, NTV2FrameGeometry* geometry, NTV2FrameBufferFormat* format );
	AJA_VIRTUAL bool	IsBufferSizeChangeRequired(NTV2Channel channel, NTV2FrameGeometry currentGeometry, NTV2FrameGeometry newGeometry,
													NTV2FrameBufferFormat format);
	AJA_VIRTUAL bool	IsBufferSizeChangeRequired(NTV2Channel channel, NTV2FrameGeometry geometry,
									NTV2FrameBufferFormat currentFormat, NTV2FrameBufferFormat newFormat);
	AJA_VIRTUAL bool	GetFBSizeAndCountFromHW(ULWord* size, ULWord* count);

	/**
		@brief		Returns true if the device supports the multi format feature and it's enabled, else false.
	**/
	AJA_VIRTUAL bool	IsMultiFormatActive (void);
};	//	CNTV2Card


/**
	@brief	Instances of this class are able to interrogate and control an NTV2 AJA video/audio capture/playout device.
**/
typedef CNTV2Card	CNTV2Device;


/////////////////////////////////////////////////////////////////////////////


AJAExport std::string NTV2AutoCirculateStateToString (const NTV2AutoCirculateState inState);


#if !defined (NTV2_DEPRECATE)
	#define	GetK2AnalogOutHTiming					GetAnalogOutHTiming						///< @deprecated		Use GetAnalogOutHTiming instead.
	#define	GetK2ColorSpaceCustomCoefficients		GetColorSpaceCustomCoefficients			///< @deprecated		Use GetColorSpaceCustomCoefficients instead.
	#define	GetK2ColorSpaceCustomCoefficients12Bit	GetColorSpaceCustomCoefficients12Bit	///< @deprecated		Use GetColorSpaceCustomCoefficients12Bit instead.
	#define	GetK2ColorSpaceMakeAlphaFromKey			GetColorSpaceMakeAlphaFromKey			///< @deprecated		Use GetColorSpaceMakeAlphaFromKey instead.
	#define	GetK2ColorSpaceMatrixSelect				GetColorSpaceMatrixSelect				///< @deprecated		Use GetColorSpaceMatrixSelect instead.
	#define	GetK2ColorSpaceRGBBlackRange			GetColorSpaceRGBBlackRange				///< @deprecated		Use GetColorSpaceRGBBlackRange instead.
	#define	GetK2ColorSpaceUseCustomCoefficient		GetColorSpaceUseCustomCoefficient		///< @deprecated		Use GetColorSpaceUseCustomCoefficient instead.
	#define	GetK2ColorSpaceVideoKeySyncFail			GetColorSpaceVideoKeySyncFail			///< @deprecated		Use GetColorSpaceVideoKeySyncFail instead.
	#define	GetK2ConversionMode						GetConversionMode						///< @deprecated		Use GetConversionMode instead.
	#define	GetK2ConverterInRate					GetConverterInRate						///< @deprecated		Use GetConverterInRate instead.
	#define	GetK2ConverterInStandard				GetConverterInStandard					///< @deprecated		Use GetConverterInStandard instead.
	#define	GetK2ConverterOutRate					GetConverterOutRate						///< @deprecated		Use GetConverterOutRate instead.
	#define	GetK2ConverterOutStandard				GetConverterOutStandard					///< @deprecated		Use GetConverterOutStandard instead.
	#define	GetK2ConverterPulldown					GetConverterPulldown					///< @deprecated		Use GetConverterPulldown instead.
	#define	GetK2DeinterlaceMode					GetDeinterlaceMode						///< @deprecated		Use GetDeinterlaceMode instead.
	#define	GetK2DownConvertMode					GetDownConvertMode						///< @deprecated		Use GetDownConvertMode instead.
	#define	GetK2EnableConverter					GetEnableConverter						///< @deprecated		Use GetEnableConverter instead.
	#define	GetK2FrameBufferSize					GetFrameBufferSize						///< @deprecated		Use GetFrameBufferSize instead.
	#define	GetK2InputVideoSelect					GetInputVideoSelect						///< @deprecated		Use GetInputVideoSelect instead.
	#define	GetK2IsoConvertMode						GetIsoConvertMode						///< @deprecated		Use GetIsoConvertMode instead.
	#define	GetK2PulldownMode						GetPulldownMode							///< @deprecated		Use GetPulldownMode instead.
	#define	GetK2SDI1OutHTiming						GetSDI1OutHTiming						///< @deprecated		Use GetSDI1OutHTiming instead.
	#define	GetK2SDI2OutHTiming						GetSDI2OutHTiming						///< @deprecated		Use GetSDI2OutHTiming instead.
	#define	GetK2SecondaryVideoFormat				GetSecondaryVideoFormat					///< @deprecated		Use GetSecondaryVideoFormat instead.
	#define	GetK2SecondConverterInStandard			GetSecondConverterInStandard			///< @deprecated		Use GetSecondConverterInStandard instead.
	#define	GetK2SecondConverterOutStandard			GetSecondConverterOutStandard			///< @deprecated		Use GetSecondConverterOutStandard instead.
	#define	GetK2SecondConverterPulldown			GetSecondConverterPulldown				///< @deprecated		Use GetSecondConverterPulldown instead.
	#define	GetK2SecondDownConvertMode				GetSecondDownConvertMode				///< @deprecated		Use GetSecondDownConvertMode instead.
	#define	GetK2SecondIsoConvertMode				GetSecondIsoConvertMode					///< @deprecated		Use GetSecondIsoConvertMode instead.
	#define	GetK2UCAutoLine21						GetUCAutoLine21							///< @deprecated		Use GetUCAutoLine21 instead.
	#define	GetK2UCPassLine21						GetUCPassLine21							///< @deprecated		Use GetUCPassLine21 instead.
	#define	GetK2UpConvertMode						GetUpConvertMode						///< @deprecated		Use GetUpConvertMode instead.
	#define	GetK2VideoDACMode						GetVideoDACMode							///< @deprecated		Use GetVideoDACMode instead.
	#define	GetK2Xpt1ColorSpaceConverterInputSelect	GetXptColorSpaceConverterInputSelect	///< @deprecated		Use GetXptColorSpaceConverterInputSelect instead.
	#define	GetK2Xpt1CompressionModInputSelect		GetXptCompressionModInputSelect			///< @deprecated		Use GetXptCompressionModInputSelect instead.
	#define	GetK2Xpt1ConversionModInputSelect		GetXptConversionModInputSelect			///< @deprecated		Use GetXptConversionModInputSelect instead.
	#define	GetK2Xpt1CSC1VidInputSelect				GetXptCSC1VidInputSelect				///< @deprecated		Use GetXptCSC1VidInputSelect instead.
	#define	GetK2Xpt1LUTInputSelect					GetXptLUTInputSelect					///< @deprecated		Use GetXptLUTInputSelect instead.
	#define	GetK2Xpt2DuallinkOutInputSelect			GetXptDuallinkOutInputSelect			///< @deprecated		Use GetXptDuallinkOutInputSelect instead.
	#define	GetK2Xpt2FrameBuffer1InputSelect		GetXptFrameBuffer1InputSelect			///< @deprecated		Use GetXptFrameBuffer1InputSelect instead.
	#define	GetK2Xpt2FrameSync1InputSelect			GetXptFrameSync1InputSelect				///< @deprecated		Use GetXptFrameSync1InputSelect instead.
	#define	GetK2Xpt2FrameSync2InputSelect			GetXptFrameSync2InputSelect				///< @deprecated		Use GetXptFrameSync2InputSelect instead.
	#define	GetK2Xpt3AnalogOutInputSelect			GetXptAnalogOutInputSelect				///< @deprecated		Use GetXptAnalogOutInputSelect instead.
	#define	GetK2Xpt3CSC1KeyInputSelect				GetXptCSC1KeyInputSelect				///< @deprecated		Use GetXptCSC1KeyInputSelect instead.
	#define	GetK2Xpt3SDIOut1InputSelect				GetXptSDIOut1InputSelect				///< @deprecated		Use GetXptSDIOut1InputSelect instead.
	#define	GetK2Xpt3SDIOut2InputSelect				GetXptSDIOut2InputSelect				///< @deprecated		Use GetXptSDIOut2InputSelect instead.
	#define	GetK2Xpt4KDCQ1InputSelect				GetXpt4KDCQ1InputSelect					///< @deprecated		Use GetXpt4KDCQ1InputSelect instead.
	#define	GetK2Xpt4KDCQ2InputSelect				GetXpt4KDCQ2InputSelect					///< @deprecated		Use GetXpt4KDCQ2InputSelect instead.
	#define	GetK2Xpt4KDCQ3InputSelect				GetXpt4KDCQ3InputSelect					///< @deprecated		Use GetXpt4KDCQ3InputSelect instead.
	#define	GetK2Xpt4KDCQ4InputSelect				GetXpt4KDCQ4InputSelect					///< @deprecated		Use GetXpt4KDCQ4InputSelect instead.
	#define	GetK2Xpt4MixerBGKeyInputSelect			GetXptMixerBGKeyInputSelect				///< @deprecated		Use GetXptMixerBGKeyInputSelect instead.
	#define	GetK2Xpt4MixerBGVidInputSelect			GetXptMixerBGVidInputSelect				///< @deprecated		Use GetXptMixerBGVidInputSelect instead.
	#define	GetK2Xpt4MixerFGKeyInputSelect			GetXptMixerFGKeyInputSelect				///< @deprecated		Use GetXptMixerFGKeyInputSelect instead.
	#define	GetK2Xpt4MixerFGVidInputSelect			GetXptMixerFGVidInputSelect				///< @deprecated		Use GetXptMixerFGVidInputSelect instead.
	#define	GetK2Xpt5CSC2KeyInputSelect				GetXptCSC2KeyInputSelect				///< @deprecated		Use GetXptCSC2KeyInputSelect instead.
	#define	GetK2Xpt5CSC2VidInputSelect				GetXptCSC2VidInputSelect				///< @deprecated		Use GetXptCSC2VidInputSelect instead.
	#define	GetK2Xpt5FrameBuffer2InputSelect		GetXptFrameBuffer2InputSelect			///< @deprecated		Use GetXptFrameBuffer2InputSelect instead.
	#define	GetK2Xpt5XptLUT2InputSelect				GetXptLUT2InputSelect					///< @deprecated		Use GetXptLUT2InputSelect instead.
	#define	GetK2Xpt6HDMIOutInputSelect				GetXptHDMIOutInputSelect				///< @deprecated		Use GetXptHDMIOutInputSelect instead.
	#define	GetK2Xpt6IICTInputSelect				GetXptIICTInputSelect					///< @deprecated		Use GetXptIICTInputSelect instead.
	#define	GetK2Xpt6SecondConverterInputSelect		GetXptSecondConverterInputSelect		///< @deprecated		Use GetXptSecondConverterInputSelect instead.
	#define	GetK2Xpt6WaterMarkerInputSelect			GetXptWaterMarkerInputSelect			///< @deprecated		Use GetXptWaterMarkerInputSelect instead.
	#define	GetK2Xpt7DuallinkOut2InputSelect		GetXptDuallinkOut2InputSelect			///< @deprecated		Use GetXptDuallinkOut2InputSelect instead.
	#define	GetK2Xpt7IICT2InputSelect				GetXptIICT2InputSelect					///< @deprecated		Use GetXptIICT2InputSelect instead.
	#define	GetK2Xpt7WaterMarker2InputSelect		GetXptWaterMarker2InputSelect			///< @deprecated		Use GetXptWaterMarker2InputSelect instead.
	#define	GetK2Xpt8SDIOut3InputSelect				GetXptSDIOut3InputSelect				///< @deprecated		Use GetXptSDIOut3InputSelect instead.
	#define	GetK2Xpt8SDIOut4InputSelect				GetXptSDIOut4InputSelect				///< @deprecated		Use GetXptSDIOut4InputSelect instead.
	#define	GetK2Xpt8SDIOut5InputSelect				GetXptSDIOut5InputSelect				///< @deprecated		Use GetXptSDIOut5InputSelect instead.
	#define	GetK2Xpt9Mixer2BGKeyInputSelect			GetXptMixer2BGKeyInputSelect			///< @deprecated		Use GetXptMixer2BGKeyInputSelect instead.
	#define	GetK2Xpt9Mixer2BGVidInputSelect			GetXptMixer2BGVidInputSelect			///< @deprecated		Use GetXptMixer2BGVidInputSelect instead.
	#define	GetK2Xpt9Mixer2FGKeyInputSelect			GetXptMixer2FGKeyInputSelect			///< @deprecated		Use GetXptMixer2FGKeyInputSelect instead.
	#define	GetK2Xpt9Mixer2FGVidInputSelect			GetXptMixer2FGVidInputSelect			///< @deprecated		Use GetXptMixer2FGVidInputSelect instead.
	#define	GetK2Xpt10SDIOut1DS2InputSelect			GetXptSDIOut1DS2InputSelect				///< @deprecated		Use GetXptSDIOut1DS2InputSelect instead.
	#define	GetK2Xpt10SDIOut2DS2InputSelect			GetXptSDIOut2DS2InputSelect				///< @deprecated		Use GetXptSDIOut2DS2InputSelect instead.
	#define	GetK2Xpt11DualLinkIn1DSSelect			GetXptDualLinkIn1DSSelect				///< @deprecated		Use GetXptDualLinkIn1DSSelect instead.
	#define	GetK2Xpt11DualLinkIn1Select				GetXptDualLinkIn1Select					///< @deprecated		Use GetXptDualLinkIn1Select instead.
	#define	GetK2Xpt11DualLinkIn2DSSelect			GetXptDualLinkIn2DSSelect				///< @deprecated		Use GetXptDualLinkIn2DSSelect instead.
	#define	GetK2Xpt11DualLinkIn2Select				GetXptDualLinkIn2Select					///< @deprecated		Use GetXptDualLinkIn2Select instead.
	#define	GetK2Xpt12LUT3InputSelect				GetXptLUT3InputSelect					///< @deprecated		Use GetXptLUT3InputSelect instead.
	#define	GetK2Xpt12LUT4InputSelect				GetXptLUT4InputSelect					///< @deprecated		Use GetXptLUT4InputSelect instead.
	#define	GetK2Xpt12LUT5InputSelect				GetXptLUT5InputSelect					///< @deprecated		Use GetXptLUT5InputSelect instead.
	#define	GetK2Xpt13FrameBuffer3InputSelect		GetXptFrameBuffer3InputSelect			///< @deprecated		Use GetXptFrameBuffer3InputSelect instead.
	#define	GetK2Xpt13FrameBuffer4InputSelect		GetXptFrameBuffer4InputSelect			///< @deprecated		Use GetXptFrameBuffer4InputSelect instead.
	#define	GetK2Xpt14SDIOut3DS2InputSelect			GetXptSDIOut3DS2InputSelect				///< @deprecated		Use GetXptSDIOut3DS2InputSelect instead.
	#define	GetK2Xpt14SDIOut4DS2InputSelect			GetXptSDIOut4DS2InputSelect				///< @deprecated		Use GetXptSDIOut4DS2InputSelect instead.
	#define	GetK2Xpt14SDIOut5DS2InputSelect			GetXptSDIOut5DS2InputSelect				///< @deprecated		Use GetXptSDIOut5DS2InputSelect instead.
	#define	GetK2Xpt15DualLinkIn3DSSelect			GetXptDualLinkIn3DSSelect				///< @deprecated		Use GetXptDualLinkIn3DSSelect instead.
	#define	GetK2Xpt15DualLinkIn3Select				GetXptDualLinkIn3Select					///< @deprecated		Use GetXptDualLinkIn3Select instead.
	#define	GetK2Xpt15DualLinkIn4DSSelect			GetXptDualLinkIn4DSSelect				///< @deprecated		Use GetXptDualLinkIn4DSSelect instead.
	#define	GetK2Xpt15DualLinkIn4Select				GetXptDualLinkIn4Select					///< @deprecated		Use GetXptDualLinkIn4Select instead.
	#define	GetK2Xpt16DuallinkOut3InputSelect		GetXptDuallinkOut3InputSelect			///< @deprecated		Use GetXptDuallinkOut3InputSelect instead.
	#define	GetK2Xpt16DuallinkOut4InputSelect		GetXptDuallinkOut4InputSelect			///< @deprecated		Use GetXptDuallinkOut4InputSelect instead.
	#define	GetK2Xpt16DuallinkOut5InputSelect		GetXptDuallinkOut5InputSelect			///< @deprecated		Use GetXptDuallinkOut5InputSelect instead.
	#define	GetK2Xpt17CSC3KeyInputSelect			GetXptCSC3KeyInputSelect				///< @deprecated		Use GetXptCSC3KeyInputSelect instead.
	#define	GetK2Xpt17CSC3VidInputSelect			GetXptCSC3VidInputSelect				///< @deprecated		Use GetXptCSC3VidInputSelect instead.
	#define	GetK2Xpt17CSC4KeyInputSelect			GetXptCSC4KeyInputSelect				///< @deprecated		Use GetXptCSC4KeyInputSelect instead.
	#define	GetK2Xpt17CSC4VidInputSelect			GetXptCSC4VidInputSelect				///< @deprecated		Use GetXptCSC4VidInputSelect instead.
	#define	GetK2XptCSC5KeyInputSelect				GetXptCSC5KeyInputSelect				///< @deprecated		Use GetXptCSC5KeyInputSelect instead.
	#define	GetK2XptCSC5VidInputSelect				GetXptCSC5VidInputSelect				///< @deprecated		Use GetXptCSC5VidInputSelect instead.
	#define	GetK2XptHDMIOutV2Q1InputSelect			GetXptHDMIOutV2Q1InputSelect			///< @deprecated		Use GetXptHDMIOutV2Q1InputSelect instead.
	#define	GetK2XptHDMIOutV2Q2InputSelect			GetXptHDMIOutV2Q2InputSelect			///< @deprecated		Use GetXptHDMIOutV2Q2InputSelect instead.
	#define	GetK2XptHDMIOutV2Q3InputSelect			GetXptHDMIOutV2Q3InputSelect			///< @deprecated		Use GetXptHDMIOutV2Q3InputSelect instead.
	#define	GetK2XptHDMIOutV2Q4InputSelect			GetXptHDMIOutV2Q4InputSelect			///< @deprecated		Use GetXptHDMIOutV2Q4InputSelect instead.
	#define	SetK2AnalogOutHTiming					SetAnalogOutHTiming						///< @deprecated		Use SetAnalogOutHTiming instead.
	#define	SetK2ColorSpaceCustomCoefficients		SetColorSpaceCustomCoefficients			///< @deprecated		Use SetColorSpaceCustomCoefficients instead.
	#define	SetK2ColorSpaceCustomCoefficients12Bit	SetColorSpaceCustomCoefficients12Bit	///< @deprecated		Use SetColorSpaceCustomCoefficients12Bit instead.
	#define	SetK2ColorSpaceMakeAlphaFromKey			SetColorSpaceMakeAlphaFromKey			///< @deprecated		Use SetColorSpaceMakeAlphaFromKey instead.
	#define	SetK2ColorSpaceMatrixSelect				SetColorSpaceMatrixSelect				///< @deprecated		Use SetColorSpaceMatrixSelect instead.
	#define	SetK2ColorSpaceRGBBlackRange			SetColorSpaceRGBBlackRange				///< @deprecated		Use SetColorSpaceRGBBlackRange instead.
	#define	SetK2ColorSpaceUseCustomCoefficient		SetColorSpaceUseCustomCoefficient		///< @deprecated		Use SetColorSpaceUseCustomCoefficient instead.
	#define	SetK2ConversionMode						SetConversionMode						///< @deprecated		Use SetConversionMode instead.
	#define	SetK2ConverterInRate					SetConverterInRate						///< @deprecated		Use SetConverterInRate instead.
	#define	SetK2ConverterInStandard				SetConverterInStandard					///< @deprecated		Use SetConverterInStandard instead.
	#define	SetK2ConverterOutRate					SetConverterOutRate						///< @deprecated		Use SetConverterOutRate instead.
	#define	SetK2ConverterOutStandard				SetConverterOutStandard					///< @deprecated		Use SetConverterOutStandard instead.
	#define	SetK2ConverterPulldown					SetConverterPulldown					///< @deprecated		Use SetConverterPulldown instead.
	#define	SetK2DeinterlaceMode					SetDeinterlaceMode						///< @deprecated		Use SetDeinterlaceMode instead.
	#define	SetK2DownConvertMode					SetDownConvertMode						///< @deprecated		Use SetDownConvertMode instead.
	#define	SetK2EnableConverter					SetEnableConverter						///< @deprecated		Use SetEnableConverter instead.
	#define	SetK2FrameBufferSize					SetFrameBufferSize						///< @deprecated		Use SetFrameBufferSize instead.
	#define	SetK2InputVideoSelect					SetInputVideoSelect						///< @deprecated		Use SetInputVideoSelect instead.
	#define	SetK2IsoConvertMode						SetIsoConvertMode						///< @deprecated		Use SetIsoConvertMode instead.
	#define	SetK2PulldownMode						SetPulldownMode							///< @deprecated		Use SetPulldownMode instead.
	#define	SetK2SDI1OutHTiming						SetSDI1OutHTiming						///< @deprecated		Use SetSDI1OutHTiming instead.
	#define	SetK2SDI2OutHTiming						SetSDI2OutHTiming						///< @deprecated		Use SetSDI2OutHTiming instead.
	#define	SetK2SecondaryVideoFormat				SetSecondaryVideoFormat					///< @deprecated		Use SetSecondaryVideoFormat instead.
	#define	SetK2SecondConverterInStandard			SetSecondConverterInStandard			///< @deprecated		Use SetSecondConverterInStandard instead.
	#define	SetK2SecondConverterOutStandard			SetSecondConverterOutStandard			///< @deprecated		Use SetSecondConverterOutStandard instead.
	#define	SetK2SecondConverterPulldown			SetSecondConverterPulldown				///< @deprecated		Use SetSecondConverterPulldown instead.
	#define	SetK2SecondDownConvertMode				SetSecondDownConvertMode				///< @deprecated		Use SetSecondDownConvertMode instead.
	#define	SetK2SecondIsoConvertMode				SetSecondIsoConvertMode					///< @deprecated		Use SetSecondIsoConvertMode instead.
	#define	SetK2UCAutoLine21						SetUCAutoLine21							///< @deprecated		Use SetUCAutoLine21 instead.
	#define	SetK2UCPassLine21						SetUCPassLine21							///< @deprecated		Use SetUCPassLine21 instead.
	#define	SetK2UpConvertMode						SetUpConvertMode						///< @deprecated		Use SetUpConvertMode instead.
	#define	SetK2VideoDACMode						SetVideoDACMode							///< @deprecated		Use SetVideoDACMode instead.
	#define	SetK2Xpt1ColorSpaceConverterInputSelect	SetXptColorSpaceConverterInputSelect	///< @deprecated		Use SetXptColorSpaceConverterInputSelect instead.
	#define	SetK2Xpt1CompressionModInputSelect		SetXptCompressionModInputSelect			///< @deprecated		Use SetXptCompressionModInputSelect instead.
	#define	SetK2Xpt1ConversionModInputSelect		SetXptConversionModInputSelect			///< @deprecated		Use SetXptConversionModInputSelect instead.
	#define	SetK2Xpt1CSC1VidInputSelect				SetXptCSC1VidInputSelect				///< @deprecated		Use SetXptCSC1VidInputSelect instead.
	#define	SetK2Xpt1LUTInputSelect					SetXptLUTInputSelect					///< @deprecated		Use SetXptLUTInputSelect instead.
	#define	SetK2Xpt2DuallinkOutInputSelect			SetXptDuallinkOutInputSelect			///< @deprecated		Use SetXptDuallinkOutInputSelect instead.
	#define	SetK2Xpt2FrameBuffer1InputSelect		SetXptFrameBuffer1InputSelect			///< @deprecated		Use SetXptFrameBuffer1InputSelect instead.
	#define	SetK2Xpt2FrameSync1InputSelect			SetXptFrameSync1InputSelect				///< @deprecated		Use SetXptFrameSync1InputSelect instead.
	#define	SetK2Xpt2FrameSync2InputSelect			SetXptFrameSync2InputSelect				///< @deprecated		Use SetXptFrameSync2InputSelect instead.
	#define	SetK2Xpt3AnalogOutInputSelect			SetXptAnalogOutInputSelect				///< @deprecated		Use SetXptAnalogOutInputSelect instead.
	#define	SetK2Xpt3CSC1KeyInputSelect				SetXptCSC1KeyInputSelect				///< @deprecated		Use SetXptCSC1KeyInputSelect instead.
	#define	SetK2Xpt3SDIOut1InputSelect				SetXptSDIOut1InputSelect				///< @deprecated		Use SetXptSDIOut1InputSelect instead.
	#define	SetK2Xpt3SDIOut2InputSelect				SetXptSDIOut2InputSelect				///< @deprecated		Use SetXptSDIOut2InputSelect instead.
	#define	SetK2Xpt4KDCQ1InputSelect				SetXpt4KDCQ1InputSelect					///< @deprecated		Use SetXpt4KDCQ1InputSelect instead.
	#define	SetK2Xpt4KDCQ2InputSelect				SetXpt4KDCQ2InputSelect					///< @deprecated		Use SetXpt4KDCQ2InputSelect instead.
	#define	SetK2Xpt4KDCQ3InputSelect				SetXpt4KDCQ3InputSelect					///< @deprecated		Use SetXpt4KDCQ3InputSelect instead.
	#define	SetK2Xpt4KDCQ4InputSelect				SetXpt4KDCQ4InputSelect					///< @deprecated		Use SetXpt4KDCQ4InputSelect instead.
	#define	SetK2Xpt4MixerBGKeyInputSelect			SetXptMixerBGKeyInputSelect				///< @deprecated		Use SetXptMixerBGKeyInputSelect instead.
	#define	SetK2Xpt4MixerBGVidInputSelect			SetXptMixerBGVidInputSelect				///< @deprecated		Use SetXptMixerBGVidInputSelect instead.
	#define	SetK2Xpt4MixerFGKeyInputSelect			SetXptMixerFGKeyInputSelect				///< @deprecated		Use SetXptMixerFGKeyInputSelect instead.
	#define	SetK2Xpt4MixerFGVidInputSelect			SetXptMixerFGVidInputSelect				///< @deprecated		Use SetXptMixerFGVidInputSelect instead.
	#define	SetK2Xpt5CSC2KeyInputSelect				SetXptCSC2KeyInputSelect				///< @deprecated		Use SetXptCSC2KeyInputSelect instead.
	#define	SetK2Xpt5CSC2VidInputSelect				SetXptCSC2VidInputSelect				///< @deprecated		Use SetXptCSC2VidInputSelect instead.
	#define	SetK2Xpt5FrameBuffer2InputSelect		SetXptFrameBuffer2InputSelect			///< @deprecated		Use SetXptFrameBuffer2InputSelect instead.
	#define	SetK2Xpt5XptLUT2InputSelect				SetXptLUT2InputSelect					///< @deprecated		Use SetXptLUT2InputSelect instead.
	#define	SetK2Xpt6HDMIOutInputSelect				SetXptHDMIOutInputSelect				///< @deprecated		Use SetXptHDMIOutInputSelect instead.
	#define	SetK2Xpt6IICTInputSelect				SetXptIICTInputSelect					///< @deprecated		Use SetXptIICTInputSelect instead.
	#define	SetK2Xpt6SecondConverterInputSelect		SetXptSecondConverterInputSelect		///< @deprecated		Use SetXptSecondConverterInputSelect instead.
	#define	SetK2Xpt6WaterMarkerInputSelect			SetXptWaterMarkerInputSelect			///< @deprecated		Use SetXptWaterMarkerInputSelect instead.
	#define	SetK2Xpt7DuallinkOut2InputSelect		SetXptDuallinkOut2InputSelect			///< @deprecated		Use SetXptDuallinkOut2InputSelect instead.
	#define	SetK2Xpt7IICT2InputSelect				SetXptIICT2InputSelect					///< @deprecated		Use SetXptIICT2InputSelect instead.
	#define	SetK2Xpt7WaterMarker2InputSelect		SetXptWaterMarker2InputSelect			///< @deprecated		Use SetXptWaterMarker2InputSelect instead.
	#define	SetK2Xpt8SDIOut3InputSelect				SetXptSDIOut3InputSelect				///< @deprecated		Use SetXptSDIOut3InputSelect instead.
	#define	SetK2Xpt8SDIOut4InputSelect				SetXptSDIOut4InputSelect				///< @deprecated		Use SetXptSDIOut4InputSelect instead.
	#define	SetK2Xpt8SDIOut5InputSelect				SetXptSDIOut5InputSelect				///< @deprecated		Use SetXptSDIOut4InputSelect instead.
	#define	SetK2Xpt9Mixer2BGKeyInputSelect			SetXptMixer2BGKeyInputSelect			///< @deprecated		Use SetXptSDIOut5InputSelect instead.
	#define	SetK2Xpt9Mixer2BGVidInputSelect			SetXptMixer2BGVidInputSelect			///< @deprecated		Use SetXptMixer2BGKeyInputSelect instead.
	#define	SetK2Xpt9Mixer2FGKeyInputSelect			SetXptMixer2FGKeyInputSelect			///< @deprecated		Use SetXptMixer2BGVidInputSelect instead.
	#define	SetK2Xpt9Mixer2FGVidInputSelect			SetXptMixer2FGVidInputSelect			///< @deprecated		Use SetXptMixer2FGKeyInputSelect instead.
	#define	SetK2Xpt10SDIOut1DS2InputSelect			SetXptSDIOut1DS2InputSelect				///< @deprecated		Use SetXptMixer2FGVidInputSelect instead.
	#define	SetK2Xpt10SDIOut2DS2InputSelect			SetXptSDIOut2DS2InputSelect				///< @deprecated		Use SetXptSDIOut1DS2InputSelect instead.
	#define	SetK2Xpt11DualLinkIn1DSSelect			SetXptDualLinkIn1DSSelect				///< @deprecated		Use SetXptSDIOut2DS2InputSelect instead.
	#define	SetK2Xpt11DualLinkIn1Select				SetXptDualLinkIn1Select					///< @deprecated		Use SetXptDualLinkIn1DSSelect instead.
	#define	SetK2Xpt11DualLinkIn2DSSelect			SetXptDualLinkIn2DSSelect				///< @deprecated		Use SetXptDualLinkIn1Select instead.
	#define	SetK2Xpt11DualLinkIn2Select				SetXptDualLinkIn2Select					///< @deprecated		Use SetXptDualLinkIn2DSSelect instead.
	#define	SetK2Xpt12LUT3InputSelect				SetXptLUT3InputSelect					///< @deprecated		Use SetXptDualLinkIn2Select instead.
	#define	SetK2Xpt12LUT4InputSelect				SetXptLUT4InputSelect					///< @deprecated		Use SetXptLUT3InputSelect instead.
	#define	SetK2Xpt12LUT5InputSelect				SetXptLUT5InputSelect					///< @deprecated		Use SetXptLUT4InputSelect instead.
	#define	SetK2Xpt13FrameBuffer3InputSelect		SetXptFrameBuffer3InputSelect			///< @deprecated		Use SetXptLUT5InputSelect instead.
	#define	SetK2Xpt13FrameBuffer4InputSelect		SetXptFrameBuffer4InputSelect			///< @deprecated		Use SetXptFrameBuffer3InputSelect instead.
	#define	SetK2Xpt14SDIOut3DS2InputSelect			SetXptSDIOut3DS2InputSelect				///< @deprecated		Use SetXptFrameBuffer4InputSelect instead.
	#define	SetK2Xpt14SDIOut4DS2InputSelect			SetXptSDIOut4DS2InputSelect				///< @deprecated		Use SetXptSDIOut3DS2InputSelect instead.
	#define	SetK2Xpt14SDIOut5DS2InputSelect			SetXptSDIOut5DS2InputSelect				///< @deprecated		Use SetXptSDIOut4DS2InputSelect instead.
	#define	SetK2Xpt15DualLinkIn3DSSelect			SetXptDualLinkIn3DSSelect				///< @deprecated		Use SetXptSDIOut5DS2InputSelect instead.
	#define	SetK2Xpt15DualLinkIn3Select				SetXptDualLinkIn3Select					///< @deprecated		Use SetXptDualLinkIn3DSSelect instead.
	#define	SetK2Xpt15DualLinkIn4DSSelect			SetXptDualLinkIn4DSSelect				///< @deprecated		Use SetXptDualLinkIn3Select instead.
	#define	SetK2Xpt15DualLinkIn4Select				SetXptDualLinkIn4Select					///< @deprecated		Use SetXptDualLinkIn4DSSelect instead.
	#define	SetK2Xpt16DuallinkOut3InputSelect		SetXptDuallinkOut3InputSelect			///< @deprecated		Use SetXptDualLinkIn4Select instead.
	#define	SetK2Xpt16DuallinkOut4InputSelect		SetXptDuallinkOut4InputSelect			///< @deprecated		Use SetXptDuallinkOut3InputSelect instead.
	#define	SetK2Xpt16DuallinkOut5InputSelect		SetXptDuallinkOut5InputSelect			///< @deprecated		Use SetXptDuallinkOut4InputSelect instead.
	#define	SetK2Xpt17CSC3KeyInputSelect			SetXptCSC3KeyInputSelect				///< @deprecated		Use SetXptDuallinkOut5InputSelect instead.
	#define	SetK2Xpt17CSC3VidInputSelect			SetXptCSC3VidInputSelect				///< @deprecated		Use SetXptCSC3KeyInputSelect instead.
	#define	SetK2Xpt17CSC4KeyInputSelect			SetXptCSC4KeyInputSelect				///< @deprecated		Use SetXptCSC3VidInputSelect instead.
	#define	SetK2Xpt17CSC4VidInputSelect			SetXptCSC4VidInputSelect				///< @deprecated		Use SetXptCSC4KeyInputSelect instead.
	#define	SetK2XptCSC5KeyInputSelect				SetXptCSC5KeyInputSelect				///< @deprecated		Use SetXptCSC4VidInputSelect instead.
	#define	SetK2XptCSC5VidInputSelect				SetXptCSC5VidInputSelect				///< @deprecated		Use SetXptCSC5KeyInputSelect instead.
	#define	SetK2XptHDMIOutV2Q1InputSelect			SetXptHDMIOutV2Q1InputSelect			///< @deprecated		Use SetXptCSC5VidInputSelect instead.
	#define	SetK2XptHDMIOutV2Q2InputSelect			SetXptHDMIOutV2Q2InputSelect			///< @deprecated		Use SetXptHDMIOutV2Q1InputSelect instead.
	#define	SetK2XptHDMIOutV2Q3InputSelect			SetXptHDMIOutV2Q3InputSelect			///< @deprecated		Use SetXptHDMIOutV2Q2InputSelect instead.
	#define	SetK2XptHDMIOutV2Q4InputSelect			SetXptHDMIOutV2Q4InputSelect			///< @deprecated		Use SetXptHDMIOutV2Q4InputSelect instead.
	#define	SetXena2VideoOutputStandard				SetVideoOutputStandard					///< @deprecated		Use SetVideoOutputStandard instead.
	#define	SetXptMixerBGKeyInputSelect				SetXptMixer1BGKeyInputSelect			///< @deprecated		Use SetXptMixer1BGKeyInputSelect instead.
	#define	GetXptMixerBGKeyInputSelect				GetXptMixer1BGKeyInputSelect			///< @deprecated		Use GetXptMixer1BGKeyInputSelect instead.
	#define	SetXptMixerBGVidInputSelect				SetXptMixer1BGVidInputSelect			///< @deprecated		Use SetXptMixer1BGVidInputSelect instead.
	#define	GetXptMixerBGVidInputSelect				GetXptMixer1BGVidInputSelect			///< @deprecated		Use GetXptMixer1BGVidInputSelect instead.
	#define	SetXptMixerFGKeyInputSelect				SetXptMixer1FGKeyInputSelect			///< @deprecated		Use SetXptMixer1FGKeyInputSelect instead.
	#define	GetXptMixerFGKeyInputSelect				GetXptMixer1FGKeyInputSelect			///< @deprecated		Use GetXptMixer1FGKeyInputSelect instead.
	#define	SetXptMixerFGVidInputSelect				SetXptMixer1FGVidInputSelect			///< @deprecated		Use SetXptMixer1FGVidInputSelect instead.
	#define	GetXptMixerFGVidInputSelect				GetXptMixer1FGVidInputSelect			///< @deprecated		Use GetXptMixer1FGVidInputSelect instead.
	#define	SetXptXptLUT2InputSelect				SetXptLUT2InputSelect					///< @deprecated		Use SetXptLUT2InputSelect instead.
	#define	GetXptXptLUT2InputSelect				GetXptLUT2InputSelect					///< @deprecated		Use GetXptLUT2InputSelect instead.
#endif	//	!defined (NTV2_DEPRECATE)

#endif	//	NTV2CARD_H
