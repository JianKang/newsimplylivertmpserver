﻿#pragma once
#include "../../lib.base/Config.h"

class IFrameConsumerManager
{
public:
	static IFrameConsumerManager* getInstance();
	static void releaseInstance();
	virtual bool initialize() = 0;
	virtual bool OutputFrames(unsigned long nChannelID, unsigned char *pVideoBuffer, unsigned long dwVSize, unsigned char *pAudioBuffer, unsigned long dwASize) = 0;
	virtual bool WaitGenlockInterrupt() = 0;
	virtual bool hasPlayout() = 0;
protected:
	IFrameConsumerManager() {}
	virtual ~IFrameConsumerManager() {}
};
