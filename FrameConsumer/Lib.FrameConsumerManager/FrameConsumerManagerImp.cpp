﻿#include "FrameConsumerManagerImp.h"
#include "../Lib.FrameConsumer/IFrameConsumer.h"
#include "../../Lib.Logger/LogWriter.h"
#include "../../lib.base/Config.h"
static TCHAR* LOG_PATH = L"C:\\Logs\\SimplyLiveRTMPServer\\FrameConsumer.log";
FrameConsumerManagerImp::FrameConsumerManagerImp()
{
}

FrameConsumerManagerImp::~FrameConsumerManagerImp()
{
	IFrameConsumer::releaseInstance();
	WriteLogW(L"C:\\Logs\\SimplyLiveRTMPServer\\SimplyLiveRTMPServer.log", InfoLevel, L"FrameConsumerManager exit");
}

bool FrameConsumerManagerImp::initialize()
{
 	m_frameConsumerRef = IFrameConsumer::getInstance();

	if (Config::getInstance()->getOutTypeIsAja())
	{
		string strInput;
		for (int nIndexID = 0; nIndexID < MAX_PAYER; nIndexID++)
		{
			strInput = Config::getInstance()->getInputAddress(nIndexID);
			if (strInput.empty())
				continue;
			sFrameConsumer_Parameter parameter;
			parameter.fpVideoFormat = Config::getInstance()->getVideoFormat();
			parameter.dwBoardID = 0;
			parameter.dwSdiID = nIndexID;
			parameter.fcType = FCT_AJA;

			int nret = m_frameConsumerRef->addChannel(nIndexID, parameter);
			if (nret == 0)
				++m_nbPlayout;
			WriteLogW(LOG_PATH, nret == 0 ? InfoLevel : ErrorLevel, L"addChannel(%d) nret=%d %s in FrameConsumerManagerImp::initialize", nIndexID, nret, (nret == 0) ? L"succeed" : L"failed");
		}
	}
	else
	{
		string strInput;
		string strOut;
		for (int nIndexID = 0; nIndexID < MAX_PAYER; nIndexID++)
		{
			strInput = Config::getInstance()->getInputAddress(nIndexID);
			if (strInput.empty())
				continue;
			strOut = Config::getInstance()->getOutAddress(nIndexID);
			if (strOut.empty())
				continue;
			sFrameConsumer_Parameter parameter;
			parameter.fpVideoFormat = Config::getInstance()->getVideoFormat();
			parameter.dwBoardID = 0;
			parameter.dwSdiID = 0;
			parameter.fcType = FCT_STREAM;
			int nret = m_frameConsumerRef->addChannel(EM_ConsumerChannel_STREAM_0 + nIndexID, parameter);
			WriteLogW(LOG_PATH, nret == 0 ? InfoLevel : ErrorLevel, L"AddBMRChannel(%d)=%d  %s in FrameConsumerManagerImp::initialize", nIndexID, nret, (nret == 0) ? L"Succeed" : L"failed");
		}

	}
	return true;
}

bool FrameConsumerManagerImp::OutputFrames(unsigned long nChannelID, unsigned char* pVideoBuffer, unsigned long dwVSize, unsigned char* pAudioBuffer, unsigned long dwASize)
{
	if (m_frameConsumerRef)
		return m_frameConsumerRef->OutputFrames(nChannelID, pVideoBuffer, dwVSize, pAudioBuffer, dwASize);

	return false;
}

bool FrameConsumerManagerImp::WaitGenlockInterrupt()
{
	if (m_frameConsumerRef)
		return m_frameConsumerRef->WaitGenlockInterrupt();
	return false;
}

bool FrameConsumerManagerImp::hasPlayout()
{
	return m_nbPlayout > 0;
}