﻿#include "IFrameConsumerManager.h"
#include "../../Lib.Base/locker.h"
#include "FrameConsumerManagerImp.h"

static IFrameConsumerManager* g_fpMgr = nullptr;
IFrameConsumerManager* IFrameConsumerManager::getInstance()
{
	static Locker g_lock;
	LockHolder lock(g_lock);
	if (g_fpMgr == nullptr)
		g_fpMgr = new FrameConsumerManagerImp;
	return g_fpMgr;
}

void IFrameConsumerManager::releaseInstance()
{
	if (g_fpMgr)
	{
		delete g_fpMgr;
		g_fpMgr = nullptr;
	}
}