﻿#pragma once
#include "IFrameConsumerManager.h"

class  IFrameConsumer;
class FrameConsumerManagerImp :public IFrameConsumerManager
{
	IFrameConsumer* m_frameConsumerRef = nullptr;
	int				m_nbPlayout = 0;

public:
	FrameConsumerManagerImp();
	~FrameConsumerManagerImp();

	bool initialize() override;
	bool OutputFrames(unsigned long nChannelID, unsigned char *pVideoBuffer, unsigned long dwVSize, unsigned char *pAudioBuffer, unsigned long dwASize) override;
	bool WaitGenlockInterrupt() override;
	bool hasPlayout() override;
};
