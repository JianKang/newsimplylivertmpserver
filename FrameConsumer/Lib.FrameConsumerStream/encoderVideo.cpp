#include "encoderVideo.h"
#include <memory>
#include <Shlobj.h>
#include <direct.h>
#include "../../lib.Base/TimeCount.h"
#include "../../Lib.Logger/LogWriter.h"
#include "../../lib.Base/ConsumerPoolMgr.h"
#include "../../lib.Base/platform.h"
//-----------------------------------------------------------------------------

#define ENCODER_DEFAULT_BFRAME_ADAP	1
#define ENCODER_DEFAULT_BFRAME_BIAS 40
#define ENCODER_DEFAULT_THREADS		0
#define ENCODER_DEFAULT_GOP_SIZE	72
#define ENCODER_DEFAULT_BFRAME		0
#define ENCODER_DEFAULT_QUALITY		1
#define ENCODER_DEFAULT_BITRATE		6000

// buffer size
// 4:4:4 = Nb_pixel x 3
// 4:2:2 = Nb_pixel x 2
// 4:1:1 = Nb_pixel x 1.5

#define ENCODER_BUF_OUT_SIZE		12441600			//3840*2160*2
#define ENCODER_SLIDE_SIZE_PAL		144					// 4.8s
#define ENCODER_SLIDE_SIZE_NTSC_ND	720					// 24s
#define ENCODER_SLIDE_SIZE_NTSC_D	720					// 24s

//-----------------------------------------------------------------------------

CEncoderVideo::CEncoderVideo() :m_b4KMode(FALSE), m_bH264(FALSE), m_enCoder(NULL), m_thread(NULL)
{
	//fp = nullptr;
	m_fp = NULL;
	m_nGopValue = ENCODER_DEFAULT_GOP_SIZE;
	m_nTotalRecord = 0;
	m_Recframes = 0;
	//getCPUfreq(m_freq);
	//m_tickPerMs = m_freq / 1000;
}

//-----------------------------------------------------------------------------

CEncoderVideo::~CEncoderVideo()
{
	m_bContinueAdd = false;
	if (m_thread != NULL)
	{
		WaitForMultipleObjects(1, &m_thread, TRUE, INFINITE);
		CloseHandle(m_thread);
		m_thread = NULL;
	}
}

void CEncoderVideo::SetCallBack(std::function<void(BOOL bVideo, BYTE *pEncBuf, DWORD dwTimeStamp, int nEncSize, BOOL bKeyFrame)> pushFrmCallback)
{
	m_pCallback = pushFrmCallback;
}

void CEncoderVideo::SetLogPath(BOOL b4KMode, TCHAR *pLogPath)
{
	memcpy_s(m_logPath, MAX_PATH * sizeof(TCHAR), pLogPath, MAX_PATH * sizeof(TCHAR));
	m_b4KMode = b4KMode;
}

//-----------------------------------------------------------------------------

DWORD WINAPI CEncoderVideo::ThreadFunction(void * lpParam)
{
	CEncoderVideo * pEncoder = (CEncoderVideo*)lpParam;
	pEncoder->theThread();
	return 0;
}

//-----------------------------------------------------------------------------

void CEncoderVideo::theThread()
{
	VideoFrame *pVideoFrame;
	while (m_bContinueAdd)
	{
		if (!m_encCache.empty())
		{
			{
				LockHolder lock(m_lock_Cache);
				pVideoFrame = m_encCache.front();
				m_encCache.pop();
			}
			encodeFrame(pVideoFrame);
		}
		else
		{
			Sleep(10);
		}
	}
	while (!m_encCache.empty())
	{
		{
			LockHolder lock(m_lock_Cache);
			pVideoFrame = m_encCache.front();
			m_encCache.pop();
		}

		ConsumerPoolMgr::GetInstance()->release(m_cnl, pVideoFrame);
	}
	if (m_enCoder != NULL)
	{
		m_enCoder->EncodeFrame(nullptr);
		m_enCoder->UnInitialize();
		delete m_enCoder;
		m_enCoder = NULL;
	}
}

//-----------------------------------------------------------------------------

bool CEncoderVideo::start(int _cnl, const BMRSetting &bmrSetting)
{
	WriteLogA(m_logPath, InfoLevel, "CEncoderVideo::start Begin.");
	stop();
	m_videoFormat = Config::getInstance()->getVideoFormat();
	m_cnl = _cnl;
	m_nGopValue = bmrSetting.cameraSetting->encoderSetting.dwGopValue;
	m_bitrate = bmrSetting.cameraSetting->compressionSetting.compression;
	//m_seqID = _firstSeqID;
	//m_completedSeqID = -1;
	m_frame = 0;
	m_nTotalRecord = 0;
	m_Recframes = 0;
	m_bH264 = (bmrSetting.cameraSetting[m_cnl].encoderSetting.encoderType == BMREncoderType_H264);
	m_bH264 = TRUE;//
	m_nEncodeDeviceID =  0;
	if (!openVideoEncoder(bmrSetting.cameraSetting[m_cnl].compressionSetting.compression, bmrSetting.cameraSetting[m_cnl].encoderSetting.dwGopValue,
		bmrSetting.cameraSetting[m_cnl].encoderSetting.encoderMode == BMREncoderType_CABAC))
		return false;
	//if (!createDirectory())
	//	return false;
	//openVideoFile();
	m_videoTick = 0;
	m_bContinueAdd = true;
	m_thread = CreateThread(NULL, 0, ThreadFunction, (void*)this, 0, NULL);
	WriteLogA(m_logPath, InfoLevel, "CEncoderVideo::start Succeed. 4KMode (%d) ,H264 (%d) ,QPMode (%d).", m_b4KMode, m_bH264, m_bQPMode);
	return true;
}

bool CEncoderVideo::createDirectory()
{
	// 	char path[MAX_PATH];
	// 	sprintf_s(path, MAX_PATH, "D:\\Data\\Rec_%d", m_cnl);
	// 	if (!PathFileExistsA(path))
	// 	{
	// 		int ret = SHCreateDirectoryExA(nullptr, path, nullptr);
	// 		LogRec::log("CreateDirectory Path %s ,res %d", path, ret);
	// 		if (ret != ERROR_SUCCESS)
	// 		{
	// 			LogRec::log("CreateDirectory Path %s failed, res %d", path, ret);
	// 			Sleep(40);
	// 			ret = SHCreateDirectoryExA(nullptr, path, nullptr);
	// 			if (ret != ERROR_SUCCESS)
	// 			{
	// 				LogRec::log("CreateDirectory Path %s failed again, res %d", path, ret);
	// 				return false;
	// 			}
	// 		}
	// 		LogRec::log("CreateDirectory Path %s success, res %d", path, ret);
	// 	}
	return true;
}

//-----------------------------------------------------------------------------

void CEncoderVideo::openVideoFile()
{
	// 	TCHAR path[200];
	// 	char videoPath[200], videoESPath[200];
	// 	_stprintf_s(path, 200, _T("D:\\Data\\Rec_%d\\Seq_%d"), m_cnl, m_seqID);
	// 	sprintf_s(videoPath, 200, "D:\\Data\\Rec_%d\\Seq_%d\\video.dat", m_cnl, m_seqID);
	// 	printf("openVideoFile %s\n", videoPath);
	// 	CreateDirectory(path, NULL);
	// 	m_fp = NULL;
	// 	int r1 = fopen_s(&m_fp, videoPath, "wb");
	//
	// 	m_fpES = NULL;
	// 	sprintf_s(videoESPath, 200, "D:\\Data\\Rec_%d\\Seq_%d\\video.es", m_cnl, m_seqID);
	// 	int r2 = fopen_s(&m_fpES, videoESPath, "wb");
	//
	// 	LogRec::log("CEncoderVideo::openVideoDataFile( %s) %s ,error code %d", videoPath, m_fp ? "Ok" : "Failed ------!!!!!!!!!!", r1);
	// 	LogRec::log("CEncoderVideo::openVideoESFile( %s) %s, error code %d", videoESPath, m_fpES ? "Ok" : "Failed ----!!!!!!!!!!", r2);
}

//-----------------------------------------------------------------------------

void CEncoderVideo::closeVideoFile()
{
	// 	if (m_fp)
	// 		fclose(m_fp);
	// 	m_fp = NULL;
	//
	// 	if (m_fpES)
	// 		fclose(m_fpES);
	// 	m_fpES = NULL;
	//
	// 	m_completedSeqID = m_seqID;
	// 	m_seqID++;
	//
	// 	char str[400];
	// 	sprintf(str, "CEncoderVideo::closeVideoFile() %d Frame recorded", m_frame);
	// 	LogRec::log(str);
}

//-----------------------------------------------------------------------------

bool CEncoderVideo::saveVideoHeader()
{
	// 	return true;
	// 	unsigned long saveheader = 1;
	// 	m_registryEdit.getVal(TEXT("saveheader"), &saveheader, 1);
	//
	// 	if (saveheader)
	// 	{
	// 		// 		int i_nal;
	// 		// 		x264_nal_t *p_nal;
	// 		// 		x264_encoder_headers(m_handle, &p_nal, &i_nal);
	//
	// 		FILE * fpVideoHdr = NULL;
	// 		char videoHdrPath[200];
	// 		sprintf_s(videoHdrPath, 200, "D:\\Data\\Rec_%d\\video.hdr", m_cnl);
	// 		fopen_s(&fpVideoHdr, videoHdrPath, "wb");
	//
	// 		BYTE seiTemp[629];
	//
	// 		uint32_t sps_size = 23;
	// 		uint32_t pps_size = 4;
	// 		uint32_t sei_size = 629;
	//
	// 		BYTE sps[MAX_PATH];
	// 		BYTE pps[MAX_PATH];
	// 		BYTE *sei = seiTemp;
	//
	// 		m_enCoder->GetHeaders(sps_size, &sps[0], pps_size, &pps[0], fpVideoHdr);
	//
	// 		fwrite(&sps_size, 4, 1, fpVideoHdr);
	// 		fwrite(sps, sps_size, 1, fpVideoHdr);
	// 		fwrite(&pps_size, 4, 1, fpVideoHdr);
	// 		fwrite(pps, pps_size, 1, fpVideoHdr);
	//
	// 		sei[0] = 0x00;
	// 		sei[1] = 0x00;
	// 		sei[2] = 0x02;
	// 		sei[3] = 0x71;
	//
	// 		memset(&sei[25], ' ', sei_size - 26);
	// 		strcpy((char*)&sei[25], "MMR410 ");
	//
	// 		switch (m_videoFormat)
	// 		{
	// 		case ENCODER_VIDEO_FORMAT_1080i_50:	    strcpy((char*)&sei[32], "1080i_50 "); break;
	// 		case ENCODER_VIDEO_FORMAT_1080i_5994:	strcpy((char*)&sei[32], "1080i_59 "); break;
	// 		case ENCODER_VIDEO_FORMAT_1080i_60:	    strcpy((char*)&sei[32], "1080i_60 "); break;
	// 		case ENCODER_VIDEO_FORMAT_720p_25:	    strcpy((char*)&sei[32], "720p_25  "); break;
	// 		case ENCODER_VIDEO_FORMAT_720p_50:	    strcpy((char*)&sei[32], "720p_50  "); break;
	// 		case ENCODER_VIDEO_FORMAT_720p_5994:	strcpy((char*)&sei[32], "720p_59  "); break;
	// 		case ENCODER_VIDEO_FORMAT_720p_60:	    strcpy((char*)&sei[32], "720p_60  "); break;
	// 		case ENCODER_VIDEO_FORMAT_720p_30:	    strcpy((char*)&sei[32], "720p_30  "); break;
	// 		case ENCODER_VIDEO_FORMAT_720p_2997:	strcpy((char*)&sei[32], "720p_29  "); break;
	// 		case ENCODER_VIDEO_FORMAT_SD_PAL:	    strcpy((char*)&sei[32], "SD_PAL   "); break;
	// 		case ENCODER_VIDEO_FORMAT_SD_NTSC:	    strcpy((char*)&sei[32], "SD_NTSC  "); break;
	// 		case ENCODER_VIDEO_FORMAT_1080p_2398:   strcpy((char*)&sei[32], "1080p_23 "); break;
	// 		case ENCODER_VIDEO_FORMAT_1080p_24:		strcpy((char*)&sei[32], "1080p_24 "); break;
	// 		case ENCODER_VIDEO_FORMAT_1080p_25:		strcpy((char*)&sei[32], "1080p_25 "); break;
	// 		case ENCODER_VIDEO_FORMAT_1080p_2997:	strcpy((char*)&sei[32], "1080p_29 "); break;
	// 		case ENCODER_VIDEO_FORMAT_1080p_30:		strcpy((char*)&sei[32], "1080p_30 "); break;
	// 		case ENCODER_VIDEO_FORMAT_720p_2398:	strcpy((char*)&sei[32], "720p_23  "); break;
	// 		case ENCODER_VIDEO_FORMAT_720p_24:		strcpy((char*)&sei[32], "720p_24  "); break;
	// 		}
	//
	// 		fwrite(&sei_size, 4, 1, fpVideoHdr);
	// 		fwrite(sei, sei_size, 1, fpVideoHdr);
	// 		fwrite(&m_frameRate, 4, 1, fpVideoHdr); // 2 is needed to double frame rate;
	// 		fclose(fpVideoHdr);
	// 	}

	return true;
}
void  DumpToH264File(int size, unsigned char * data, bool bclose)
{
	// 	static FILE * pFile = NULL;
	// 	if (bclose)
	// 	{
	// 		if (pFile)
	// 		{
	// 			fclose(pFile);
	// 			pFile = NULL;
	// 			printf("Close es file \n");
	// 		}
	// 		else
	// 		{
	// 			printf("Already closed\n");
	// 		}
	// 		return;
	// 	}
	// 	if (pFile == NULL)
	// 		pFile = fopen("d:\\33.264", "wb");
	//
	// 	fwrite(data, size, 1, pFile);
	// 	printf("Write nal  size %d ----\n", size);
}

//-----------------------------------------------------------------------------

void DumpYUVToFile(char* buffer, int size)
{
	// 	FILE * fpVideoHdr = NULL;
	// 	char videoHdrPath[200];
	// 	static  int index = 1;
	// 	sprintf_s(videoHdrPath, 200, "D:\\Data\\%d.yuv", index++);
	// 	fopen_s(&fpVideoHdr, videoHdrPath, "wb");
	// 	if (fpVideoHdr == NULL)
	// 		return;
	// 	fwrite(buffer, 1, size, fpVideoHdr);
	// 	fclose(fpVideoHdr);
}

void CEncoderVideo::encodeFrame(VideoFrame * video)
{
	CTimeCount ts;
	if (m_enCoder != NULL)
	{
		NVENCSTATUS nvStatus = m_enCoder->EncodeFrame(video->getRaw());
		if (nvStatus != NV_ENC_SUCCESS)
		{
			WriteLogA(m_logPath, ErrorLevel, "CEncoderVideo::encodeFrame (OUTOFMEMORY == 10) failed.: Return value:%d ", nvStatus);
		}
		DWORD dwTime = ts.getCountTime();
		if (dwTime > 33)
		{
			//WriteLogA(m_logPath, WarnLevel, "CEncoderVideo::encodeFrame cost too much time :%d ", dwTime);
		}
		ConsumerPoolMgr::GetInstance()->release(m_cnl, video);
	}
}

std::string CEncoderVideo::GetAppPath()
{
	char strModuleFileName[MAX_PATH];
	char strDriver[MAX_PATH];
	char strPath[MAX_PATH];
	GetModuleFileNameA(nullptr, strModuleFileName, MAX_PATH);
	_splitpath_s(strModuleFileName, strDriver, size(strDriver), strPath, size(strPath), nullptr, 0, nullptr, 0);
	strcat_s(strDriver, size(strDriver), strPath);
	return strDriver;
}

int CEncoderVideo::add(VideoFrame *pVideo, Timecode *tc)
{
	if (!m_bContinueAdd)
	{
		return -1;
	}
	if (0 == m_Recframes++)
	{
		//fp = fopen("d:\\test.yuv", "wb");
		m_tcStart = *tc;
		m_frameIDStart = pVideo->GetFrameID();
		WriteLogA(m_logPath, InfoLevel, "CEncoderVideo::add(%d) frameIDStart(%I64d) startTC(%02d:%02d:%02d:%02d)",
			m_cnl, m_frameIDStart, m_tcStart.hour, m_tcStart.minute, m_tcStart.second, m_tcStart.frame);
	}
	else
		m_tcCurrent = *tc;
	// 	if (fp != nullptr)
	// 	{
	// 		fwrite(pVideo->getRaw(), 1, 1920*1080*3/2, fp);
	// 		fflush(fp);
	// 	}

		//m_scaler.conv_UYVY422_To_YUV420P(m_param.width, m_param.height, m_param.width, m_param.height, video, m_frameCache[m_nextFrameIn]);
	{
		LockHolder lock(m_lock_Cache);
		m_encCache.push(pVideo);
	}
	return 0;
}

//-----------------------------------------------------------------------------

int CEncoderVideo::stop()
{
	//WriteLogA(m_logPath, InfoLevel, "CEncoderVideo::stop() Begin.");
// 	if(fp != nullptr)
// 		fclose(fp);
	m_bContinueAdd = false;

	if (m_thread != NULL)
	{
		WaitForMultipleObjects(1, &m_thread, TRUE, INFINITE);
		CloseHandle(m_thread);
		m_thread = NULL;
	}
	closeVideoFile();
	//WriteLogA(m_logPath, InfoLevel, "CEncoderVideo::stop() End.");
	return 0;
}

int CEncoderVideo::getEncBufSize()
{
	return m_encCache.size();
}

int CEncoderVideo::getSlideSize()
{
	return m_encoderSlideSize;
}

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

bool CEncoderVideo::setParam(int nbitRate, int nGop, BOOL bUseCabc)
{
	memset(&m_param, 0, sizeof(m_param));
	_NV_ENC_PIC_STRUCT picStu = NV_ENC_PIC_STRUCT_FRAME;
	m_param.width = Config::getInstance()->getVideoWidth();
	m_param.height = Config::getInstance()->getVideoHeight();
	m_param.frameRateDen = Config::getInstance()->getFrameRateDen();
	m_param.frameRateNum = Config::getInstance()->getFrameRateNum();
	m_video_gap = (double)1000.00*(double)m_param.frameRateDen / (double)m_param.frameRateNum;

	m_encoderSlideSize = m_nGopValue;
	m_param.gopLength = m_nGopValue;
	m_param.numB = 0;
	m_param.bUseCabc = bUseCabc;
	m_bQPMode = FALSE;
	m_param.endFrameIdx = INT_MAX;
	m_param.bitrate = m_bitrate * 1000;
	m_param.rcMode = m_bQPMode ? NV_ENC_PARAMS_RC_CONSTQP : NV_ENC_PARAMS_RC_CBR;
	m_param.deviceType = NV_ENC_CUDA;
	m_param.codec = m_bH264 ? NV_ENC_H264 : NV_ENC_HEVC;
	m_param.qp = 40;
	m_param.i_quant_factor = DEFAULT_I_QFACTOR;
	m_param.b_quant_factor = DEFAULT_B_QFACTOR;
	m_param.i_quant_offset = DEFAULT_I_QOFFSET;
	m_param.b_quant_offset = DEFAULT_B_QOFFSET;
	m_param.presetGUID = NV_ENC_PRESET_HP_GUID; //NV_ENC_PRESET_LOW_LATENCY_HP_GUID; NV_ENC_PRESET_HP_GUID;
	m_param.pictureStruct = picStu;
	m_param.isYuv444 = 0;
	m_param.deviceID = m_nEncodeDeviceID;
	return true;
}

//-----------------------------------------------------------------------------

bool CEncoderVideo::openVideoEncoder(int nbitRate, int nGop, BOOL bUseCabc)
{
	if (!setParam(nbitRate, nGop, bUseCabc))
	{
		return false;
	}
	m_enCoder = new CNvEncoder;
	char szCardName[MAX_PATH];
	memset(szCardName, 0, MAX_PATH);
	NVENCSTATUS nvStatus = m_enCoder->Initialize(m_param, this, szCardName);

	if (nvStatus != NV_ENC_SUCCESS)
	{
		WriteLogA(m_logPath, ErrorLevel, "CEncoderVideo::openVideoEncoder() encoder Initialize Failed.Video format %ldx%ld  GOP %ld bitrate :%ld fps den %ld, fps num %ld ,CABAC :%ld deviceID: %ld ,GPU Name: %s",
			m_param.width, m_param.height, m_param.gopLength, m_param.bitrate, m_param.frameRateDen, m_param.frameRateNum, m_param.bUseCabc, m_param.deviceID, szCardName);
		return false;
	}

	WriteLogA(m_logPath, InfoLevel, "CEncoderVideo::openVideoEncoder() Succeed.Video format %ldx%ld  GOP %ld bitrate :%ld fps den %ld, fps num %ld ,CABAC :%ld deviceID: %ld ,GPU Name: %s",
		m_param.width, m_param.height, m_param.gopLength, m_param.bitrate, m_param.frameRateDen, m_param.frameRateNum, m_param.bUseCabc, m_param.deviceID, szCardName);

	return true;
}

//-----------------------------------------------------------------------------

void CEncoderVideo::cb(void* bitstreamBufferPtr, DWORD  bitstreamSizeInBytes, DWORD frameIdx, UINT64  outputTimeStamp, int nType)
{
	dumpFrameToFile(nType, frameIdx, outputTimeStamp, bitstreamSizeInBytes, (unsigned char *)bitstreamBufferPtr);
}

//-----------------------------------------------------------------------------

void CEncoderVideo::dumpFrameToFile(int nalType, int dts, int pts, int frameSize, unsigned char * data)
{
	// 	if (m_frame == m_encoderSlideSize)//IDR frame.
	// 	{
	// 		closeVideoFile();
	// 		m_frame = 0;
	// 		ret = 1;
	// 		openVideoFile();
	// 	}
	// 	if (m_fp && m_fpES)
	// 	{
	// 		*(int*)data = 0x01000000; // set the start code
	// 		fwrite(data, frameSize, 1, m_fpES);
	// 	}
	*(int*)data = 0x01000000;
	m_videoTick = ++m_nTotalRecord*m_video_gap;
	cpuTick pushFrameOut;
	if (m_nTotalRecord == 1)
	{
		//dwTC = 0;
		//getTickCount(m_dwStartTC);
		WriteLogA(m_logPath, InfoLevel, "CEncoderVideo::dumpFrameToFile(%d) BackupStreamInfo frameIDStart(%I64d) startTC(%02d:%02d:%02d:%02d) encoderSlideSize(%d)",
			m_cnl, m_frameIDStart, m_tcStart.hour, m_tcStart.minute, m_tcStart.second, m_tcStart.frame, m_encoderSlideSize);
		//CExportClipsMgr::GetInstance()->SetEncSlideSize((CamID)m_cnl, m_encoderSlideSize);
	}
	else
		getTickCount(pushFrameOut);
	m_pCallback(TRUE, data, m_videoTick, frameSize, 0x03 == nalType);
}