#include "FrameConsumerStream.h"
#include "EncoderMgr.h"

CFrameConsumerStream::CFrameConsumerStream()
{
}

CFrameConsumerStream::~CFrameConsumerStream()
{
}

int CFrameConsumerStream::addChannel(DWORD dwCnlID, const sFrameConsumer_Parameter& pCnlParameter)
{
	return CEncoderMgr::GetInstance()->addChannel(dwCnlID);
}

int CFrameConsumerStream::setFormat(DWORD dwCnlID, const FPTVideoFormat fpVideoFormat)
{
	return -1;
}

int CFrameConsumerStream::removeChannel(DWORD dwCnlID)
{
	return CEncoderMgr::GetInstance()->removeChannel(dwCnlID);
}

int CFrameConsumerStream::OutputFrames(DWORD dwCnlID, unsigned char* pVideoBuffer, DWORD dwVSize, unsigned char* pAudioBuffer, DWORD dwASize)
{
	//if (!m_bInit || m_dwCnlID != dwCnlID)
	//	return -1;
	//m_pEncoderMgr->add(pVideoBuffer, dwVSize, pAudioBuffer, dwASize / (16 * 4));
	return 1;
}

int CFrameConsumerStream::OutputFrames(DWORD nChannelID, VideoFrame *pVideo, AudioFrame *pAudio, Timecode *tc)
{
	return CEncoderMgr::GetInstance()->add(nChannelID, pVideo, pAudio, tc);
}

bool CFrameConsumerStream::WaitGenlockInterrupt(DWORD dwCnlID, DWORD _timeout)
{
	return false;
}

int CFrameConsumerStream::startChannel(DWORD dwCnlID, const sFrameConsumer_StartParameter &_param)
{
	return CEncoderMgr::GetInstance()->start(dwCnlID, *_param.bmrSetting);
}

int CFrameConsumerStream::stopChannel(DWORD dwCnlID)
{
	return CEncoderMgr::GetInstance()->stop(dwCnlID);
}