#include "Encoder.h"
#include "../../lib.Base/ConsumerPoolMgr.h"
#define BACKEND_CONFIG "C:\\ProgramData\\SimplyLive.TV\\Vibox\\Backend\\Config.ini"
//-----------------------------------------------------------------------------

CEncoder::CEncoder(int _cnl, BOOL b4KMode):m_nCnl(_cnl),m_bShutdown(FALSE)
{
	wsprintf(m_logPath, _T("C:\\logs\\SimplyLiveRTMPServer\\BMR\\Encoder-Cam%c.log"), _cnl + _T('A'));	
	m_encoderVideo.SetLogPath(b4KMode, m_logPath);
	m_streamingMgr.Initilize(_cnl, &m_encoderVideo, &m_encoderAudio);
	m_hStatusThrd = CreateThread(NULL, 0, Statushread, (void*)this, 0, NULL);
}

//-----------------------------------------------------------------------------

CEncoder::~CEncoder()
{
	m_bShutdown = TRUE;
	if (m_hStatusThrd != nullptr)
	{
		WaitForSingleObject(m_hStatusThrd, INFINITE);
		CloseHandle(m_hStatusThrd);
		m_hStatusThrd = nullptr;
	}	
	//stop();
}

int CEncoder::start(const BMRSetting &bmrSetting)
{
	if (!m_encoderVideo.start(m_nCnl, bmrSetting))
	{
		WriteLogA(m_logPath, ErrorLevel, "CEncoderMgr::start failed.");
		return  -1;
	}
	m_encoderAudio.start(m_nCnl, bmrSetting);
	m_streamingMgr.start(bmrSetting);
	WriteLogA(m_logPath, InfoLevel, "CEncoderMgr::start succeed.");
	return 0;
}

//-----------------------------------------------------------------------------

int CEncoder::stop()
{
	WriteLogA(m_logPath, InfoLevel, "CEncoderMgr::stop Begin.");
	m_encoderVideo.stop();
	m_encoderAudio.stop();
	m_streamingMgr.stop();
	WriteLogA(m_logPath, InfoLevel, "CEncoderMgr::stop End.");
	return 0;
}

int CEncoder::getEncBufSize()
{
	return m_encoderVideo.getEncBufSize();
}

int CEncoder::geUsedBufCount(int nStreamID)
{
	return m_streamingMgr.geUsedBufCount(nStreamID);
}

//-----------------------------------------------------------------------------

int CEncoder::add(VideoFrame *pVideo, AudioFrame *pAudio, Timecode *tc)
{
	if (m_encoderVideo.add(pVideo, tc) < 0)
	{
		return 0xff;
	}
	int sampleCount = pAudio->getDataSize() / (pAudio->GetMonoCnt() * 4);
	if (sampleCount > 0)
		m_encoderAudio.add((unsigned char *)pAudio->getRaw(), sampleCount);
	ConsumerPoolMgr::GetInstance()->release(m_nCnl, pAudio);
	return 0;
}

DWORD WINAPI CEncoder::Statushread(void * lpParam)
{
	CEncoder * pEncoder = (CEncoder*)lpParam;
	pEncoder->StatushreadBC();
	return 0;
}

void CEncoder::StatushreadBC()
{
	BMRNetdriveStatusCameraStreamStatus status;

	while (!m_bShutdown)
	{
		for (int n = 0; n < MAX_STREAM_CNT; ++n)
		{
			m_streamingMgr.GetCurrentStatus(n, status);
			//BMRSettingManager::getInstance().updateStreamStatus(n, m_nCnl, status);
		}
		Sleep(1000);
	}
}