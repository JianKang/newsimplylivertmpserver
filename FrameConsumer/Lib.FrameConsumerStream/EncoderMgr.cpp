#include <winsock2.h>
#include "Encoder.h"
#include "EncoderMgr.h"
#include "../../lib.Base/locker.h"

using namespace std;
static CEncoderMgr *mSInstance = nullptr;
static Locker		g_lock;

CEncoderMgr::CEncoderMgr()
{
	for (int _cnl = 0; _cnl < NB_BMR_RECORDER; ++_cnl)
	{
		m_pEncoder[_cnl] = nullptr;
	}
}

CEncoderMgr::~CEncoderMgr()
{
	for (int _cnl = 0; _cnl < NB_BMR_RECORDER; ++_cnl)
	{
		if (m_pEncoder[_cnl] != nullptr)
		{
			m_pEncoder[_cnl]->stop();
			delete m_pEncoder[_cnl];
			m_pEncoder[_cnl] = nullptr;
		}
	}
}

CEncoderMgr * CEncoderMgr::GetInstance()
{
	LockHolder lock(g_lock);
	if (mSInstance == nullptr)
	{
		mSInstance = new CEncoderMgr();
	}
	return mSInstance;
}

void CEncoderMgr::releaseInstance()
{
	if (mSInstance)
	{
		delete mSInstance;
		mSInstance = nullptr;
	}
}

int CEncoderMgr::addChannel(DWORD dwCnlID)
{
	if (dwCnlID >= EM_ConsumerChannel_MAX || dwCnlID < EM_ConsumerChannel_STREAM_0)
		return -1;
	int cameraID = int(dwCnlID - EM_ConsumerChannel_STREAM_0);
	if (m_pEncoder[cameraID] != nullptr)
		return -2;
	m_pEncoder[cameraID] = new CEncoder(cameraID, Config::getInstance()->is4K());
	return 0;
}

int CEncoderMgr::removeChannel(DWORD dwCnlID)
{
	if (dwCnlID >= EM_ConsumerChannel_MAX || dwCnlID < EM_ConsumerChannel_STREAM_0)
		return -1;
	int cameraID = int(dwCnlID - EM_ConsumerChannel_STREAM_0);
	if (m_pEncoder[cameraID] != nullptr)
	{
		m_pEncoder[cameraID]->stop();
		delete m_pEncoder[cameraID];
		m_pEncoder[cameraID] = nullptr;
	}
	return 0;
}

int CEncoderMgr::start(DWORD dwCnlID, const BMRSetting &bmrSetting)
{
	if (dwCnlID >= EM_ConsumerChannel_MAX || dwCnlID < EM_ConsumerChannel_STREAM_0)
		return -1;
	int cameraID = int(dwCnlID - EM_ConsumerChannel_STREAM_0);
	if (m_pEncoder[cameraID] == nullptr)
		return -1;
	return m_pEncoder[cameraID]->start(bmrSetting);
}

int CEncoderMgr::add(DWORD dwCnlID, VideoFrame *pVideo, AudioFrame *pAudio, Timecode *tc)
{
	if (dwCnlID >= EM_ConsumerChannel_MAX || dwCnlID < EM_ConsumerChannel_STREAM_0)
		return 0xff;
	int cameraID = int(dwCnlID - EM_ConsumerChannel_STREAM_0);
	if (m_pEncoder[cameraID] == nullptr)
		return 0xff;
	return m_pEncoder[cameraID]->add(pVideo, pAudio, tc);
}

int CEncoderMgr::stop(DWORD dwCnlID)
{
	if (dwCnlID >= EM_ConsumerChannel_MAX || dwCnlID < EM_ConsumerChannel_STREAM_0)
		return -1;
	int cameraID = int(dwCnlID - EM_ConsumerChannel_STREAM_0);
	if (m_pEncoder[cameraID] == nullptr)
		return -1;
	return m_pEncoder[cameraID]->stop();
}

int CEncoderMgr::getEncBufSize(DWORD dwCnlID)
{
	if (dwCnlID >= EM_ConsumerChannel_MAX || dwCnlID < EM_ConsumerChannel_STREAM_0)
		return -1;
	int cameraID = int(dwCnlID - EM_ConsumerChannel_STREAM_0);
	if (m_pEncoder[cameraID] == nullptr)
		return -1;
	return m_pEncoder[cameraID]->getEncBufSize();
}

int CEncoderMgr::geUsedBufCount(DWORD dwCnlID, int nStreamID)
{
	if (dwCnlID >= EM_ConsumerChannel_MAX || dwCnlID < EM_ConsumerChannel_STREAM_0)
		return -1;
	int cameraID = int(dwCnlID - EM_ConsumerChannel_STREAM_0);
	if (m_pEncoder[cameraID] == nullptr)
		return -1;
	return m_pEncoder[cameraID]->geUsedBufCount(nStreamID);
}