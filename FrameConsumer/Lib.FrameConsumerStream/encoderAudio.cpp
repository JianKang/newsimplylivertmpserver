#include "encoderAudio.h"
#include <direct.h>
#include <io.h>
#include <Shlobj.h>

#define ENCODER_SLIDE_SIZE			225					// 4.8s*48000/1024
#define DEFAULT_TNS     0
#define ENCODER_BUF_IN_SIZE		    32768			// 1024 samples of 4 bytes x 2 cnl x 4 (4 slices)
#define ENCODER_BUF_OUT_SIZE		4096

//-----------------------------------------------------------------------------

CEncoderAudio::CEncoderAudio()
{
	m_handle = NULL;
	m_fp = NULL;
	m_bufferIn = NULL;
	m_bufferOut = NULL;
	m_nSteroCount = 3;
	m_nBuffInSize = 32768* m_nSteroCount;
	m_audio_gap = (double)1024.00 * 1000.00 / (double)48000.00;
	m_bufferIn = (unsigned char *)malloc(m_nBuffInSize);
	m_bufferOut = (unsigned char *)malloc(ENCODER_BUF_OUT_SIZE);
}

//-----------------------------------------------------------------------------

CEncoderAudio::~CEncoderAudio()
{
	if (m_bufferIn)
		free(m_bufferIn);
	m_bufferIn = NULL;

	if (m_bufferOut)
		free(m_bufferOut);
	m_bufferOut = NULL;
}

//-----------------------------------------------------------------------------

void CEncoderAudio::start(int _cnl, const BMRSetting &bmrSetting)
{
	m_cnl = _cnl;
	//m_seqID = _firstSeqID;
	//m_completedSeqID = -1;
	m_frame = 0;
	m_frameNum = 0;
	m_nextSampleRead = 0;
	m_nextSampleWrite = 0;
	m_audioTick = 0;
	m_nSteroCount = bmrSetting.cameraSetting[_cnl].audioSetting.LowNum / 2;
	m_nBuffInSize = ENCODER_BUF_IN_SIZE* m_nSteroCount;
	openAudioEncoder();
	//LogRec::log("CEncoderAudio::::start nStreamParam(%d, %d)", nStreamParam[0], nStreamParam[1]);
}

//-----------------------------------------------------------------------------

void CEncoderAudio::stop()
{
	LockHolder lock(m_lock_Audio);

	if (m_handle)
	{
		int bytesWritten = 0;
		unsigned long nbSample = popSampleFromBuf(true);
		while (nbSample || bytesWritten)
		{
			//printf("Audio remaining sample %ld\n", nbSample);
			bytesWritten = faacEncEncode(m_handle, (int32_t *)m_slice, nbSample * 2 * m_nSteroCount, m_bufferOut, ENCODER_BUF_OUT_SIZE);
			nbSample = popSampleFromBuf(true);

			//printf("Audio Delayed bytesWritten %ld\n", bytesWritten);

			if (bytesWritten)
				dumpFrameToFile(bytesWritten);
		}

		faacEncClose(m_handle);
	}

	m_handle = NULL;
}

//-----------------------------------------------------------------------------

void CEncoderAudio::openAudioEncoder()
{
	unsigned long samplesInput, maxBytesOutput;
	m_handle = faacEncOpen(48000, 2 * m_nSteroCount, &samplesInput, &maxBytesOutput);

	faacEncConfigurationPtr myFormat;
	myFormat = faacEncGetCurrentConfiguration(m_handle);
	myFormat->aacObjectType = LOW;
	myFormat->mpegVersion = MPEG4;
	myFormat->useLfe = 0;
	myFormat->useTns = DEFAULT_TNS;
	myFormat->inputFormat = FAAC_INPUT_FLOAT;
	myFormat->bitRate = 96000;
	myFormat->bandWidth = 0;
	myFormat->outputFormat = 1; //1:ADTS,0: raw data . by default ,it's 1.

	if (!faacEncSetConfiguration(m_handle, myFormat))
	{
		//printf("Unsupported output format!\n");
	}
}

//-----------------------------------------------------------------------------

void CEncoderAudio::fillSlice(int * data, int size, int offset)
{
	for (int i = 0; i < size; i++)
	{
		int s = data[i];
		m_slice[i + offset] = (float)s / 65536;
	}
}

//-----------------------------------------------------------------------------

int CEncoderAudio::add(unsigned char * audio, long sampleCount)
{
	LockHolder lock(m_lock_Audio);

	if (audio == NULL || m_handle == NULL)
	{
		//LogRec::log("Audio buffer is empty or sample count is 0, frame ID %d ",m_frame,sampleCount);
		return 0;
	}
	pushSampleToBuf(audio, sampleCount);

	unsigned long nbSample = popSampleFromBuf(false);
	//if (nbSample == 0 )
		//LogRec::log("Audio sample is 0 after popSampleFromBuf , frame ID %d ", m_frame);
	while (nbSample)
	{
		int bytesWritten = faacEncEncode(m_handle, (int32_t *)m_slice, nbSample * 2 * m_nSteroCount, m_bufferOut, ENCODER_BUF_OUT_SIZE);
		nbSample = popSampleFromBuf(false);

		if (bytesWritten)
		{
			//LogRec::log("write audio data to file , frame ID %d ,size %d", m_frame,bytesWritten);
			dumpFrameToFile(bytesWritten);
		}
	}
	return 0;
}


void CEncoderAudio::SetCallBack(std::function<void(BOOL bVideo, BYTE *pEncBuf, DWORD dwTimeStamp, int nEncSize, BOOL bKeyFrame)> pushFrmCallback)
{
	m_pCallback = pushFrmCallback;
}

//-----------------------------------------------------------------------------

void CEncoderAudio::pushSampleToBuf(unsigned char * audio, long sampleCount)
{
	unsigned long sampleSize = sampleCount * 8 * m_nSteroCount;
	unsigned long distanceToEndOfBuf = m_nBuffInSize - m_nextSampleWrite;
	if (distanceToEndOfBuf > sampleSize)			// split the write
	{
		memcpy(&m_bufferIn[m_nextSampleWrite], audio, sampleSize);
		m_nextSampleWrite += sampleSize;
	}
	else
	{
		memcpy(&m_bufferIn[m_nextSampleWrite], audio, distanceToEndOfBuf);
		memcpy(m_bufferIn, &audio[distanceToEndOfBuf], sampleSize - distanceToEndOfBuf);
		m_nextSampleWrite = (sampleSize - distanceToEndOfBuf);
	}
}

//-----------------------------------------------------------------------------

int CEncoderAudio::popSampleFromBuf(bool _force)
{
	int ret = 0;
	unsigned long remainingbytes = (m_nextSampleWrite >= m_nextSampleRead) ? (m_nextSampleWrite - m_nextSampleRead) : (m_nextSampleWrite + m_nBuffInSize - m_nextSampleRead);
	unsigned long remainingSample = remainingbytes / (8 * m_nSteroCount);

	if (remainingSample >= 1024)
	{
		if ((m_nextSampleRead + (1024 * 8 * m_nSteroCount)) <= m_nBuffInSize)
			fillSlice((int*)&m_bufferIn[m_nextSampleRead], 2048 * m_nSteroCount, 0);
		else
		{
			unsigned long step1 = m_nBuffInSize - m_nextSampleRead;
			unsigned long step2 = (8 * 1024 * m_nSteroCount) - step1;
			fillSlice((int*)&m_bufferIn[m_nextSampleRead], step1 / 4, 0);
			fillSlice((int*)&m_bufferIn, step2 / 4, step1);
		}
		m_nextSampleRead += (1024 * 8 * m_nSteroCount);
		if (m_nextSampleRead >= m_nBuffInSize)
			m_nextSampleRead -= m_nBuffInSize;
		ret = 1024;
	}
	else
	{
		if (_force && remainingbytes)
		{
			if (m_nextSampleWrite > m_nextSampleRead)
				fillSlice((int*)&m_bufferIn[m_nextSampleRead], remainingbytes / 4, 0);
			else
			{
				unsigned long step1 = m_nBuffInSize - m_nextSampleRead;
				unsigned long step2 = remainingbytes - step1;
				fillSlice((int*)&m_bufferIn[m_nextSampleRead], step1 / 4, 0);
				fillSlice((int*)&m_bufferIn, step2 / 4, step1);
			}
			m_nextSampleRead = m_nextSampleWrite = 0;
			ret = remainingSample;
		}
		else
			ret = 0;
	}
	return ret;
}

//-----------------------------------------------------------------------------

int CEncoderAudio::dumpFrameToFile(int size)
{
	if (m_fp)
		fwrite(m_bufferOut, size, 1, m_fp);
	m_audioTick += m_audio_gap;
	m_pCallback(FALSE, m_bufferOut, m_audioTick, size, FALSE);
	++m_frame;
	return 0;
}