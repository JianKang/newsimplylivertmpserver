#pragma once
#include <winsock2.h>
#include "encoderVideo.h"
#include "encoderAudio.h"
#include "../../BMRLib/StreamingMgr/StreamingMgr.h"

//-----------------------------------------------------------------------------

class CEncoder
{
public:
	CEncoder(int _cnl, BOOL b4KMode);
	~CEncoder();
	int	 start(const BMRSetting &bmrSetting);
	int  add(VideoFrame *pVideo, AudioFrame *pAudio, Timecode *tc);
	int  stop();

	int  getEncBufSize();
	int  geUsedBufCount(int nStreamID);
private:
	CEncoderVideo	m_encoderVideo;
	CEncoderAudio   m_encoderAudio;
	int             m_nCnl;
	CStreamingMgr   m_streamingMgr;
	TCHAR			m_logPath[MAX_PATH];
	HANDLE	m_hStatusThrd;
	static DWORD WINAPI  Statushread(void * lpParam);
	void StatushreadBC();
	BOOL m_bShutdown;
};

//-----------------------------------------------------------------------------
