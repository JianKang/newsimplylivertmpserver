#pragma once

#include <stdio.h>
#include <functional>
#include <queue>
#include "../../Lib.base/BMRServiceDefine.h"
#include "../../BMRLib/NvEncoderLib/INvCallBack.h"
#include "../../BMRLib/NvEncoderLib/NvEncoder.h"
#include "../../lib.Base/AudioFrame.h"
#include "../../lib.Base/CompVideoFrame.h"
#include "../../lib.Base/ObjectPoolEx.h"

//-----------------------------------------------------------------------------

class CEncoderVideo :public INvCallBack
{
	//FILE *fp;
	int				m_nGopValue;
	int64_t			m_Recframes;
	EncodeConfig	m_param;
	unsigned long	m_bitrate;
	//int				m_seqID;
	//int				m_completedSeqID;
	FILE	      * m_fp;
	FPTVideoFormat				m_videoFormat;
	int             m_cnl;
	unsigned long   m_frame;
	volatile bool   m_bContinueAdd;
	HANDLE			m_thread;
	unsigned long  m_dumpUncompressed;
	unsigned long  m_sd_ratio = 0; //0=>4:3(720:576,720:480) ,1=>16:9(720:404)
	unsigned long		m_nTotalRecord;
	Locker				m_lock_Cache;
	std::queue<VideoFrame*>	m_encCache;
	unsigned long m_encoderSlideSize;
	bool setParam(int nbitRate, int nGop, BOOL bUseCabc);
	bool saveVideoHeader();
	void openVideoFile();
	void closeVideoFile();
	void  dumpFrameToFile(int naltype, int dts, int pts, int frameSize, unsigned char * data);
	bool openVideoEncoder(int nbitRate, int nGop, BOOL bUseCabc);
	bool createDirectory();

	static DWORD WINAPI ThreadFunction(void * lpParam);

	void theThread();

	void encodeFrame(VideoFrame * video);
	string	GetAppPath();
	CNvEncoder *m_enCoder;
	void cb(void* bitstreamBufferPtr, DWORD  bitstreamSizeInBytes, DWORD frameIdx, UINT64  outputTimeStamp, int nType);
	BOOL m_b4KMode;
	BOOL m_bH264;
	BOOL m_bQPMode;
	//UINT64					m_dwStartTC;
	//UINT64					m_freq;
	//UINT64	                m_tickPerMs;
	Timecode				m_tcStart;
	UINT64	                m_frameIDStart;
	Timecode				m_tcCurrent;
	int						m_nEncodeDeviceID;
	double m_video_gap;
	double m_videoTick;
	TCHAR					m_logPath[MAX_PATH];
	std::function<void(BOOL bVideo, BYTE *pEncBuf, DWORD dwTimeStamp, int nEncSize, BOOL bKeyFrame)> m_pCallback;
public:
	CEncoderVideo();
	~CEncoderVideo();
	void SetCallBack(std::function<void(BOOL bVideo, BYTE *pEncBuf, DWORD dwTimeStamp, int nEncSize, BOOL bKeyFrame)> pushFrmCallback);
	void SetLogPath(BOOL b4KMode, TCHAR *pLogPath);
	bool start(int _cnl, const BMRSetting &bmrSetting);
	int  add(VideoFrame *pVideo, Timecode *tc);
	int  stop();
	int  getEncBufSize();
	int  getSlideSize();
};

//-----------------------------------------------------------------------------
