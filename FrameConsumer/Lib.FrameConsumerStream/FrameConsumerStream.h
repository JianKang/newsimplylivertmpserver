#pragma once
#include "Encoder.h"
#include "../Lib.FrameConsumer/IFrameConsumerChannel.h"

class CFrameConsumerStream :public IFrameConsumerChannel
{
public:
	CFrameConsumerStream();
	virtual ~CFrameConsumerStream();

	int addChannel(DWORD dwCnlID, const sFrameConsumer_Parameter& pCnlParameter) override;
	int startChannel(DWORD dwCnlID, const sFrameConsumer_StartParameter &_param) override;
	int setFormat(DWORD dwCnlID, const FPTVideoFormat fpVideoFormat) override;
	int OutputFrames(DWORD dwCnlID, unsigned char* pVideoBuffer, DWORD dwVSize, unsigned char* pAudioBuffer, DWORD dwASize) override;
	bool WaitGenlockInterrupt(DWORD dwCnlID, DWORD _timeout) override;
	int stopChannel(DWORD dwCnlID) override;
	int removeChannel(DWORD dwCnlID) override;
	virtual int OutputFrames(DWORD nChannelID, VideoFrame *pVideo, AudioFrame *pAudio, Timecode *tc) override;
};
