#pragma once
#include <queue>
#include <stdio.h>
#include "faac.h"
#include <functional>
#include "../../Lib.base/BMRServiceDefine.h"
#include "../../lib.Base/AudioFrame.h"
#include "../../lib.base/CompVideoFrame.h"
#include "../../lib.Base/ObjectPoolEx.h"

//-----------------------------------------------------------------------------

class CEncoderAudio
{
	long long		m_frameNum;
	//int			    m_seqID;
	//int				m_completedSeqID;
	FILE	      * m_fp;
	int             m_cnl;
	unsigned long   m_frame;
	faacEncHandle	m_handle;
	unsigned char * m_bufferIn;
	float			m_slice[1024 * 2 * 3];
	unsigned char * m_bufferOut;
	unsigned long   m_nextSampleRead;
	unsigned long   m_nextSampleWrite;		// where to write the next sample in the buffer
	unsigned long   m_nSteroCount;
	unsigned long   m_nBuffInSize;
	void openAudioEncoder();
	void pushSampleToBuf(unsigned char * audio, long sampleCount);
	int  popSampleFromBuf(bool _force);
	int  dumpFrameToFile(int size);
	void fillSlice(int * data, int size, int offset);
	double m_audio_gap;
	double m_audioTick;
	std::function<void(BOOL bVideo, BYTE *pEncBuf, DWORD dwTimeStamp, int nEncSize, BOOL bKeyFrame)> m_pCallback;
	Locker	m_lock_Audio;
public:
	CEncoderAudio();
	~CEncoderAudio();

	void start(int _cnl, const BMRSetting &bmrSetting);
	void stop();
	int add(unsigned char * audio, long sampleCount);
	void SetCallBack(std::function<void(BOOL bVideo, BYTE *pEncBuf, DWORD dwTimeStamp, int nEncSize, BOOL bKeyFrame)> pushFrmCallback);
};

//-----------------------------------------------------------------------------
