#pragma once
class CEncoder;
class CEncoderMgr
{
	CEncoder	*m_pEncoder[NB_BMR_RECORDER];
	CEncoderMgr();
	~CEncoderMgr();
public:
	static CEncoderMgr * GetInstance();
	static void		     releaseInstance();

	int  addChannel(DWORD dwCnlID);
	int  removeChannel(DWORD dwCnlID);
	int  start(DWORD dwCnlID, const BMRSetting &bmrSetting);
	int  add(DWORD dwCnlID, VideoFrame *pVideo, AudioFrame *pAudio, Timecode *tc);
	int  stop(DWORD dwCnlID);

	//For debug info
	int  getEncBufSize(DWORD dwCnlID);
	int  geUsedBufCount(DWORD dwCnlID,int nStreamID);
};

