#include <tchar.h>
#include "StreamingMgr.h"

CStreamingMgr::CStreamingMgr()
{
}

CStreamingMgr::~CStreamingMgr()
{
	stop();
}

void CStreamingMgr::Initilize(int nCamID, CEncoderVideo *pEncoderVideo, CEncoderAudio *pEncoderAudio)
{
	m_nCnl = nCamID;
	for (int n = 0; n < MAX_STREAM_CNT; ++n)
	{
		m_RTMPWrapper[n].Initialize(m_nCnl,n);
	}
	pEncoderVideo->SetCallBack(std::bind(&CStreamingMgr::PushFrmCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5));
	pEncoderAudio->SetCallBack(std::bind(&CStreamingMgr::PushFrmCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5));
}

void CStreamingMgr::start(const BMRSetting &bmrSetting)
{
	for (int n = 0; n < MAX_STREAM_CNT; ++n)
	{
		m_nStreamType[n] = bmrSetting.netdriveSetting[n].type;
		if (m_nStreamType[n] == BMRNetdriveType_RTMP && bmrSetting.netdriveSetting[n].subNetdrive[0].keyCamera[m_nCnl].buttonStatus == ButtonStatus_selected)
			m_RTMPWrapper[n].Start(bmrSetting);
	}
}

void CStreamingMgr::stop()
{
	for (int n = 0; n < MAX_STREAM_CNT; ++n)
	{
		if (m_nStreamType[n] == BMRNetdriveType_RTMP)
			m_RTMPWrapper[n].Stop();
	}
}

void CStreamingMgr::setBufferCount(unsigned long nBufCount)
{
	for (int n = 0; n < MAX_STREAM_CNT; ++n)
	{
		m_RTMPWrapper[n].setBufferCount(nBufCount);
	}
}

void CStreamingMgr::GetCurrentStatus(int nNetDrive, BMRNetdriveStatusCameraStreamStatus &status)
{
	status.nStreamStatus = BMRStreamStatus_stopped;	
	if (m_nStreamType[nNetDrive] == BMRNetdriveType_RTMP)
	{
		m_RTMPWrapper[nNetDrive].GetCurrentStatus(status);
	}
	else
	{
		status.nRecordFrames = 0;
		status.nDropFrames = 0;
		status.nBufTotal = 0;
		status.nBufFrames = 0;
	}
}

void CStreamingMgr::PushFrmCallback(BOOL bVideo, BYTE *pEncBuf, double dwTimeStamp, int nEncSize, BOOL bKeyFrame)
{
	for (UINT nIndex = 0; nIndex < MAX_STREAM_CNT; ++nIndex)
	{
		if (m_nStreamType[nIndex] == BMRNetdriveType_RTMP)
			m_RTMPWrapper[nIndex].AddEncodedData(bVideo, pEncBuf, dwTimeStamp, nEncSize, bKeyFrame);
	}
}

int CStreamingMgr::geUsedBufCount(int nStreamID)
{
	if (nStreamID >= MAX_STREAM_CNT || nStreamID < 0)
		return -1;
	if (m_nStreamType[nStreamID] == BMRNetdriveType_RTMP)
		return m_RTMPWrapper[nStreamID].geUsedBufCount();
	else
		return -1;
}
