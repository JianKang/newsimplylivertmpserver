#pragma once

#include "RTMPWrapper.h"

#include "../../FrameConsumer/Lib.FrameConsumerStream/encoderAudio.h"
#include "../../FrameConsumer/Lib.FrameConsumerStream/encoderVideo.h"

class CStreamingMgr
{
public:
	CStreamingMgr();
	~CStreamingMgr();
	void Initilize(int nCamID, CEncoderVideo *pEncoderVideo, CEncoderAudio *pEncoderAudio);
	void start(const BMRSetting &bmrSetting);
	void stop();
	void setBufferCount(unsigned long nBufCount);
	void GetCurrentStatus(int nNetDrive,BMRNetdriveStatusCameraStreamStatus &status);
	void PushFrmCallback(BOOL bVideo, BYTE *pEncBuf, double dwTimeStamp, int nEncSize, BOOL bKeyFrame);
	int  geUsedBufCount(int nStreamID);
private:
	CRTMPWrapper	m_RTMPWrapper[MAX_STREAM_CNT];
	BMRNetdriveType	m_nStreamType[MAX_STREAM_CNT];
	int m_nCnl;
};
