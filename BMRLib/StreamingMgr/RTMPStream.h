#pragma once
#include <stdio.h>
#include <stdlib.h>
#include "rtmp.h"
#include "rtmp_sys.h"
#include "log.h"
#include "amf.h"
#include "../../lib.base/BMRaudioFrame.h"
#include "../../lib.base/ObjectPoolEx.h"
#include "../../lib.base/CompVideoFrame.h"
#include <functional>

#define MAX_VIDEO_FILESIZE (1024 * 1024 * 50)       //  40M
#define MAX_AUDIO_FILESIZE (1024 * 1024 * 3)       //  2M

#include "../../Lib.Logger/LogWriter.h"
#define __STDC_CONSTANT_MACROS

// NALU单元
typedef struct _NaluUnit
{
	int type;
	int size;
	unsigned char* data;
} NaluUnit;

typedef struct _RTMPMetadata
{
	// video, must be h264 type
	bool	 bHasVideo;
	unsigned int nWidth;
	unsigned int nHeight;
	unsigned int nFrameRate; // fps
	unsigned int nVideoDataRate; // bps
	unsigned int nSpsLen;
	unsigned char Sps[1024];
	unsigned int nPpsLen;
	unsigned char Pps[1024];

	// audio, must be aac type  
	bool            bHasAudio;
	unsigned int    nAudioSampleRate;
	unsigned int    nAudioSampleSize;
	unsigned int    nAudioChannels;
	char            pAudioSpecCfg;
	unsigned int    nAudioSpecCfgLen;
	unsigned int	nAudioObjType;
} RTMPMetadata, *LPRTMPMetadata;

typedef enum {
	TYPE_READ_AUDIO = 0,
	TYPE_READ_VIDEO = 1,
	TYPE_READ_CLEARALL = 2,
} OPERATION_TYPE;

class CRTMPStream
{
public:
	CRTMPStream(void);
	~CRTMPStream(void);
public:
	void SetLogPath(TCHAR* path);
	void SetCallBack(std::function<void(OPERATION_TYPE emType, BMRaudioFrame * &fBMRaudioFrame, CompVideoFrame * &fVideoFrame)> readCallback);
	void SetObjectPool(ObjectPoolEx<CompVideoFrame> *videoPool,ObjectPoolEx<BMRaudioFrame>* audioPool);
	void ResetState();
	bool Connect(char* url);
	BOOL StartStream(int bitrate, DWORD dwAudioCnlCnt);
	void StopStream();
	BOOL CheckHasError(){ return m_bHasError; }
	BOOL CheckUsrStop(){ return m_bExitThread; }
	DWORD GetBandWith();
	void generateVGenlock();
	void generateAudioGenlock();
private:
	HANDLE	m_hSemaphoreV;
	HANDLE	m_hSemaphoreA;
	static BOOL m_bSetLog;
	ObjectPoolEx<BMRaudioFrame>	*mobjAudioPool;
	ObjectPoolEx<CompVideoFrame>	*mobjVideoPool;
	RTMPPacket m_packet;
	std::function<void(OPERATION_TYPE emType, BMRaudioFrame * &fBMRaudioFrame, CompVideoFrame * &fVideoFrame)> m_pActionCallBack;

	BOOL _SendMetaAVData(int bitrate, DWORD dwAudioCnlCnt, CompVideoFrame *fVideoFrame);

	// 送缓存中读取一个NALU包
	bool ReadOneNaluFromBuf(NaluUnit& nalu, unsigned char* pVideoBuf, unsigned int nVideoBufSize, unsigned int &posRead);
	// 发送数据
	int SendPacket(unsigned int nPacketType, unsigned char* data, unsigned int size, unsigned int nTimestamp);
	bool SendAACData(unsigned char* buf, int len, int timestamp);
	bool ReadAACData(unsigned char* buf, int& len,double &dTime);
	// 发送MetaData
	bool _SendMetadata(LPRTMPMetadata lpMetaData, DWORD dwAudioCnlCnt);
	// 发送H264数据帧
	bool SendH264Packet(unsigned char* data, unsigned int size, bool bIsKeyFrame, unsigned int nTimeStamp);
	int _getNoADTSframe(unsigned char* buffer, int* data_size);
	BOOL m_bExitThread;
	//DWORD m_dwRTMPRetryTimes;
	//DWORD m_dwRTMPDelayBetweenRetry;
	int m_bitRate;
	char  m_serverAddress[MAX_PATH];
// 	static VOID CALLBACK SendVideoCB(
// 		_In_ PVOID lpParameter,
// 		_In_ BOOLEAN TimerOrWaitFired
// 	);
// 	static VOID CALLBACK SendAudioCB(
// 		_In_ PVOID lpParameter,
// 		_In_ BOOLEAN TimerOrWaitFired
// 	);
	static DWORD WINAPI SendVideoCB(void* lpParam);
	static DWORD WINAPI SendAudioCB(void* lpParam);
	void ProcessVideo();
	void SendOneVideoFrm(uint32_t uStartTime);
	void ProcessAudio();
	void SendOneAudioFrm();
	
private:
	RTMP* m_pRtmp;
	unsigned int m_nAudioBufSize;
	unsigned int m_nAudioPos;
	unsigned char* m_pOneAudioBuffer;
	TCHAR         m_logPath[MAX_PATH];
	BOOL m_bHasError;
	static DWORD WINAPI threadRTMPStarter(void* lpParam);
	CRITICAL_SECTION m_videoCS;
	CRITICAL_SECTION m_audioCS;
	CRITICAL_SECTION m_rtmpCS;
	unsigned int m_video_gap = 0;
	unsigned int m_audio_gap = 0;
	unsigned int m_audioInx = 0;
	unsigned int m_audioTick = 0;
	unsigned int m_audioPreTick = 0;
	HANDLE m_hVideoTimerRender = NULL;
	HANDLE m_hAudioTimerRender = NULL;
	unsigned int m_videoTick = 0;
	unsigned int m_videoFrameInx = 0;
	__int64 m_videoStartTime = 0;
	__int64 m_audioStartTime = 0;
	__int64 m_nTotalBytes = 0;
	unsigned int m_connectTick = 0;
	RTMPMetadata m_metaData;
	FILE *m_Rtmpfp;
	BYTE *m_pTempVBuf;
	char* m_pAudioBody;
	unsigned char* m_pVideoBody;

};
