#pragma once
#include <queue>
#include "RTMPStream.h"
#include "../../Lib.base/BMRServiceDefine.h"

class CRTMPWrapper
{
public:
	CRTMPWrapper();
	~CRTMPWrapper();
	void Initialize(int _cnl, int nIndex);
	void Start(const BMRSetting &bmrSetting);
	void AddEncodedData(BOOL bVideo, BYTE *pData, double dwTimeStamp, int nSize, BOOL bKeyFrame = FALSE);
	void Stop();
	void GetCurrentStatus(BMRNetdriveStatusCameraStreamStatus &status);
	void setBufferCount(unsigned long nBufUse);
	int geUsedBufCount();
private:
	int                                     m_nCnl;
	UINT                                    m_nIndex;
	unsigned long                           m_nSetBufCount;
	DWORD                                   m_dwRetryTimes;
	BMRStreamStatus                         m_nStreamStatus;
	char                                    m_szErrorMsg[MAX_PATH];
	static const int nbMaxCaptureVPoolSize = 500;
	static const int nbMaxCaptureAPoolSize = 800;
	static const int nbMax4KVideoSize = 512*1024*3;
	static const int nbMaxHDVideoSize = 200 * 1024;
	ObjectPoolEx<CompVideoFrame>                 m_captureVideoPool;
	ObjectPoolEx<BMRaudioFrame>                m_captureAudioPool;
	BMRSetting                              m_bmrSetting;

	queue<CompVideoFrame*>                       m_videoList;
	queue<BMRaudioFrame*>                      m_audioList;

	CRTMPStream*                            m_pRTMPStream = nullptr;
	TCHAR                                   m_logPath[MAX_PATH];
	void RTMP_ReadFrmCallback(OPERATION_TYPE emType, BMRaudioFrame *&fBMRaudioFrame, CompVideoFrame *&fVideoFrame);
	void ClearAll();
	HANDLE                                  m_threadRtmp;
	static DWORD WINAPI PushRTMPStreamThread(void* lpParam);
	void PushRTMPStream();
	void InitVideoMem();
	BOOL                                    m_bExitThread;
	Locker                                  m_mutexAudio;
	Locker                                  m_mutexVideo;

	unsigned long m_nbCacheFrames;
	unsigned long m_nRecordFrames;
};
