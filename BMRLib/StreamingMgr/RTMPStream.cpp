#include "RTMPStream.h"
#include <atlbase.h>
#include <io.h>
#include "SpsDecode.h"
#ifdef WIN32
#include <windows.h>
#endif
#include "../../Lib.Base/TimeCount.h"

#ifdef WIN32
#pragma comment(lib,"WS2_32.lib")
#pragma comment(lib,"winmm.lib")
#endif

#define SENDTIME 0

enum
{
	FLV_CODECID_H264 = 7,
	AUDIO_CODECID_AAC = 10,
};

int InitSockets()
{
#ifdef WIN32
	WORD version;
	WSADATA wsaData;
	version = MAKEWORD(2, 2);
	return (WSAStartup(version, &wsaData) == 0);
#else
	return TRUE;
#endif
}

inline void CleanupSockets()
{
#ifdef WIN32
	WSACleanup();
#endif
}

char* put_byte(char* output, uint8_t nVal)
{
	output[0] = nVal;
	return output + 1;
}

char* put_be16(char* output, uint16_t nVal)
{
	output[1] = nVal & 0xff;
	output[0] = nVal >> 8;
	return output + 2;
}

char* put_be24(char* output, uint32_t nVal)
{
	output[2] = nVal & 0xff;
	output[1] = nVal >> 8;
	output[0] = nVal >> 16;
	return output + 3;
}

char* put_be32(char* output, uint32_t nVal)
{
	output[3] = nVal & 0xff;
	output[2] = nVal >> 8;
	output[1] = nVal >> 16;
	output[0] = nVal >> 24;
	return output + 4;
}

char* put_be64(char* output, uint64_t nVal)
{
	output = put_be32(output, nVal >> 32);
	output = put_be32(output, nVal);
	return output;
}

char* put_amf_string(char* c, const char* str)
{
	uint16_t len = strlen(str);
	c = put_be16(c, len);
	memcpy(c, str, len);
	return c + len;
}

char* put_amf_double(char* c, double d)
{
	*c++ = AMF_NUMBER; /* type: Number */
	{
		unsigned char *ci, *co;
		ci = (unsigned char *)&d;
		co = (unsigned char *)c;
		co[0] = ci[7];
		co[1] = ci[6];
		co[2] = ci[5];
		co[3] = ci[4];
		co[4] = ci[3];
		co[5] = ci[2];
		co[6] = ci[1];
		co[7] = ci[0];
	}
	return c + 8;
}

char * put_amf_bool(char *c, bool b)
{
	*c++ = AMF_BOOLEAN;
	{
		unsigned char *ci, *co;
		ci = (unsigned char *)&b;
		co = (unsigned char *)c;
		co[0] = ci[0];
	}
	return c + 1;
}

BOOL CRTMPStream::m_bSetLog = FALSE;

CRTMPStream::CRTMPStream(void) :m_pRtmp(nullptr), m_hVideoTimerRender(nullptr), m_hAudioTimerRender(nullptr), m_Rtmpfp(nullptr)
{
	//InitSockets();
	m_pOneAudioBuffer = new unsigned char[4096];
	m_pTempVBuf = new unsigned char[8*1024*1024];
	m_pAudioBody = new char[1024 * 1024];
	m_pVideoBody = new unsigned  char[8 * 1024 * 1024];
	m_hSemaphoreV = CreateSemaphore(nullptr, 0, LONG_MAX, nullptr);
	m_hSemaphoreA = CreateSemaphore(nullptr, 0, LONG_MAX, nullptr);
	InitializeCriticalSectionAndSpinCount(&m_videoCS, 4000);
	InitializeCriticalSectionAndSpinCount(&m_audioCS, 4000);
	InitializeCriticalSectionAndSpinCount(&m_rtmpCS, 4000);	
	RTMPPacket_Reset(&m_packet);
	RTMPPacket_Alloc(&m_packet, 8 * 1024 * 1024);
}

CRTMPStream::~CRTMPStream(void)
{
	StopStream();
	//WSACleanup();
	if (m_pOneAudioBuffer)
	{
		delete[] m_pOneAudioBuffer;
		m_pOneAudioBuffer = nullptr;
	}
	if (m_pTempVBuf)
	{
		delete[] m_pTempVBuf;
		m_pTempVBuf = nullptr;
	}
	RTMP_Free(m_pRtmp);
	m_pRtmp = nullptr;
	if (m_Rtmpfp != nullptr)
	{
		fclose(m_Rtmpfp);
		m_Rtmpfp = nullptr;
	}
	delete[]m_pAudioBody;
	delete[]m_pVideoBody;
	if (nullptr != m_hSemaphoreV)
	{
		CloseHandle(m_hSemaphoreV);
		m_hSemaphoreV = nullptr;
	}	
	if (m_hSemaphoreA != m_hSemaphoreA)
	{
		CloseHandle(m_hSemaphoreA);
		m_hSemaphoreA = nullptr;
	}
	RTMPPacket_Free(&m_packet);
	DeleteCriticalSection(&m_videoCS);
	DeleteCriticalSection(&m_audioCS);
	DeleteCriticalSection(&m_rtmpCS);
}

void CRTMPStream::SetLogPath(TCHAR* path)
{
	memcpy_s(m_logPath, MAX_PATH, path, MAX_PATH);
	if (!m_bSetLog)
	{
		//m_bSetLog = TRUE;
		//m_Rtmpfp = fopen("C:\\logs\\SimplyLiveRTMPServer\\RTMP.log", "wb");
		//RTMP_LogSetOutput(m_Rtmpfp);
	}
}

void CRTMPStream::SetCallBack(std::function<void(OPERATION_TYPE emType, BMRaudioFrame * &fBMRaudioFrame, CompVideoFrame * &fVideoFrame)> readCallback)
{
	m_pActionCallBack = readCallback;
}

void CRTMPStream::SetObjectPool(ObjectPoolEx<CompVideoFrame> *videoPool, ObjectPoolEx<BMRaudioFrame>* audioPool)
{
	mobjVideoPool = videoPool;
	mobjAudioPool = audioPool;
}

void CRTMPStream::ResetState()
{

}

bool CRTMPStream::Connect(char* url)
{
	WriteLogA(m_logPath, InfoLevel, "RTMP gonna to connect to server ,url %s", url);

	CCritSecLock lock(m_rtmpCS);
	StopStream();
	memset(m_serverAddress, 0, MAX_PATH);
	memcpy_s(m_serverAddress, MAX_PATH, url, strlen(url));

	if (m_pRtmp == nullptr)
	{
		m_pRtmp = RTMP_Alloc();
		RTMP_Init(m_pRtmp);
	}

	if (RTMP_SetupURL(m_pRtmp, (char*)url) < 0)
	{
		WriteLogA(m_logPath, ErrorLevel, "RTMP_SetupURL failed ,url %s", url);
		return FALSE;
	}
	RTMP_EnableWrite(m_pRtmp);
	if (RTMP_Connect(m_pRtmp, NULL) <= 0)
	{
		WriteLogA(m_logPath, ErrorLevel, "RTMP_Connect failed ,url %s", url);
		return FALSE;
	}
	if (RTMP_ConnectStream(m_pRtmp, 0) < 0)
	{
		WriteLogA(m_logPath, ErrorLevel, "RTMP_OpenStream failed ,url %s", url);
		return FALSE;
	}
	m_connectTick = RTMP_GetTime();

	WriteLogA(m_logPath, InfoLevel, "RTMP connect succeed ,url %s.", url); 
	return TRUE;
}

BOOL CRTMPStream::StartStream(int bitrate,DWORD dwAudioCnlCnt)
{
	WriteLogA(m_logPath, InfoLevel, "CRTMPStream::StartStream. ");

	m_videoFrameInx = 0;
	m_videoTick = 0;
	m_audioTick = 0;
	m_audioInx = 0;
	m_bHasError = FALSE;
	m_bExitThread = FALSE;
	CompVideoFrame *fVideoFrame = nullptr;
	BMRaudioFrame * fBMRaudioFrame = nullptr;
	m_pActionCallBack(TYPE_READ_CLEARALL, fBMRaudioFrame, fVideoFrame);
	int nTryTime = 0;
	BOOL bFindFrame = FALSE;
	int nVFramesDropped = 0;
	while (nTryTime++ < 500)
	{
		m_pActionCallBack(TYPE_READ_VIDEO, fBMRaudioFrame, fVideoFrame);
		if (fVideoFrame != nullptr)
		{
			if (fVideoFrame->getIsIDR())
			{
				bFindFrame = TRUE;
				break;
			}
			mobjVideoPool->release(fVideoFrame);
			++nVFramesDropped;
		}		
		Sleep(20);
	}
	if (!bFindFrame)
	{
		WriteLogA(m_logPath, ErrorLevel, "CRTMPStream::StartStream failed for no video frames.VDropped:%d", nVFramesDropped);
		return FALSE;
	}
	int nAFramesDropped = (int)((float(25.00) / float(16.00))*float(nVFramesDropped));
	int nDroped = 0;
	while (nDroped < nAFramesDropped)
	{
		m_pActionCallBack(TYPE_READ_AUDIO, fBMRaudioFrame, fVideoFrame);
		if (fBMRaudioFrame == nullptr)		
			Sleep(1); 
		else
		{
			mobjAudioPool->release(fBMRaudioFrame); nDroped++;
		}
	}

	WriteLogA(m_logPath, InfoLevel, "CRTMPStream::StartStream find the first key frame at loop :%d VFrameID : %I64d VDropped:%d ADropped : %d .",
		nTryTime, fVideoFrame->GetFrameID(), nVFramesDropped, nAFramesDropped);

	m_bHasError = !_SendMetaAVData(bitrate, dwAudioCnlCnt,fVideoFrame);
	mobjVideoPool->release(fVideoFrame);
	if (m_bHasError)
		return FALSE;

	m_audio_gap = 1024 * 1000 / 48000;
	int t1 = GetTickCount();
	//BOOL bRes = CreateTimerQueueTimer(&m_hVideoTimerRender, NULL, SendVideoCB, (void*)this, 0, m_video_gap, WT_EXECUTEDEFAULT);
	//BOOL bRes = CreateTimerQueueTimer(&m_hVideoTimerRender, NULL, SendVideoCB, (void*)this, 0, 0, WT_EXECUTEONLYONCE);
	//BOOL bRes = CreateTimerQueueTimer(&m_hAudioTimerRender, NULL, SendAudioCB, (void*)this, 0, m_audio_gap, WT_EXECUTEDEFAULT);
	m_hVideoTimerRender = CreateThread(NULL, 0, SendVideoCB, (void*)this, 0, NULL);
	m_hAudioTimerRender = CreateThread(NULL, 0, SendAudioCB, (void*)this, 0, NULL);
	m_nTotalBytes = 0;
	m_videoStartTime = GetTickCount();
	m_audioStartTime = GetTickCount();
	WriteLogA(m_logPath, InfoLevel, "CRTMPStream::StartStream succeed");
	return TRUE;
}
void CRTMPStream::StopStream()
{
	m_bExitThread = TRUE;
	WriteLogA(m_logPath, InfoLevel, "CRTMPStream::StopStream Start.");
// 	if (m_hVideoTimerRender != nullptr)
// 	{
// 		DeleteTimerQueueTimer(NULL, m_hVideoTimerRender, INVALID_HANDLE_VALUE);
// 		m_hVideoTimerRender = nullptr;
// 	}
// 	if (m_hAudioTimerRender != nullptr)
// 	{
// 		DeleteTimerQueueTimer(NULL, m_hAudioTimerRender, INVALID_HANDLE_VALUE);
// 		m_hAudioTimerRender = nullptr;
// 	}
	if (m_hVideoTimerRender != nullptr)
	{
		WaitForSingleObject(m_hVideoTimerRender, INFINITE);
		m_hVideoTimerRender = nullptr;
	}	
	if (m_hAudioTimerRender != nullptr)
	{
		WaitForSingleObject(m_hAudioTimerRender, INFINITE);
		m_hVideoTimerRender = nullptr;
	}
	if (m_pRtmp != NULL)
	{
		RTMP_Close(m_pRtmp);
		RTMP_Free(m_pRtmp);
		m_pRtmp = nullptr;
	}
	m_nTotalBytes = 0;
	WriteLogA(m_logPath, InfoLevel, "CRTMPStream::StopStream End.");
}

DWORD CRTMPStream::GetBandWith()
{
	return 0;//m_nTotalBytes*8*1000 / ((GetTickCount() - m_videoStartTime)*1024);
}

void CRTMPStream::generateVGenlock()
{
	ReleaseSemaphore(m_hSemaphoreV, 1, nullptr);
}

void CRTMPStream::generateAudioGenlock()
{
	ReleaseSemaphore(m_hSemaphoreA, 1, nullptr);
}

int CRTMPStream::SendPacket(unsigned int nPacketType, unsigned char* data, unsigned int size, unsigned int nTimestamp)
{
	CCritSecLock lock(m_rtmpCS);

	m_nTotalBytes += size;

	m_packet.m_packetType = nPacketType;
	m_packet.m_nChannel = 0x04;
	m_packet.m_headerType = RTMP_PACKET_SIZE_LARGE;
	m_packet.m_nTimeStamp = nTimestamp;// == -1 ? RTMP_GetTime() : 0;
	m_packet.m_nInfoField2 = m_pRtmp->m_stream_id;
	m_packet.m_nBodySize = size;
	memcpy(m_packet.m_body, data, size);

#ifdef _DEBUG
//	WriteLogW(L"C:\\logs\\SimplyLiveRTMPServer\\SendPacket.txt", InfoLevel, L"SendPacket: Type %d size %d (AV:%d,%d)nTimestamp %d", nPacketType, size, m_audioInx, m_videoFrameInx, nTimestamp);
#endif

	int nRet = RTMP_SendPacket(m_pRtmp, &m_packet, false);
	if (nRet <=0 )
	{
		WriteLogW(m_logPath, ErrorLevel, L"!!!!!!!!!!!!CRTMPStream::SendPacket failed.Type(%d)", nPacketType);
	}
	return nRet;
}


bool CRTMPStream::SendAACData(unsigned char* buf, int len, int timestamp)
{
	if (len > 0)
	{
		m_pAudioBody[0] = 0xAF;
		m_pAudioBody[1] = 0x01;

		memcpy(&m_pAudioBody[2], buf, len);
		int ret = SendPacket(RTMP_PACKET_TYPE_AUDIO, (unsigned char*)m_pAudioBody, len + 2, timestamp);
		return ret > 0;
	}
	return false;
}

bool CRTMPStream::_SendMetadata(LPRTMPMetadata lpMetaData, DWORD dwAudioCnlCnt)
{
	char body[1024] = { 0 };

	char * p = (char *)body;
	p = put_byte(p, AMF_STRING);
	p = put_amf_string(p, "@setDataFrame");

	p = put_byte(p, AMF_STRING);
	p = put_amf_string(p, "onMetaData");

	p = put_byte(p, AMF_OBJECT);
	p = put_amf_string(p, "copyright");
	p = put_byte(p, AMF_STRING);
	p = put_amf_string(p, "simplylive");

	p = put_amf_string(p, "duration");
	p = put_amf_double(p, 0);

	p = put_amf_string(p, "width");
	p = put_amf_double(p, lpMetaData->nWidth);

	p = put_amf_string(p, "height");
	p = put_amf_double(p, lpMetaData->nHeight);

	p = put_amf_string(p, "videodatarate");
	p = put_amf_double(p, lpMetaData->nVideoDataRate);

	p = put_amf_string(p, "framerate");
	if (lpMetaData->nFrameRate == 29)
		p = put_amf_double(p, 29.97);
	else if (lpMetaData->nFrameRate == 59)
		p = put_amf_double(p, 59.94);
	else
		p = put_amf_double(p, lpMetaData->nFrameRate);

	p = put_amf_string(p, "videocodecid");
	p = put_amf_double(p, FLV_CODECID_H264);// FLV_CODECID_H264
	
	p = put_amf_string(p, "audiodatarate");
	p = put_amf_double(p, 333.0078125);

	p = put_amf_string(p, "audiosamplerate");
	p = put_amf_double(p, 48000);

	p = put_amf_string(p, "audiosamplesize");
	p = put_amf_double(p, 16);

	p = put_amf_string(p, "stereo");
	p = put_amf_bool(p, false);

	p = put_amf_string(p, "audiocodecid");
	p = put_amf_double(p, 10);

	p = put_amf_string(p, "encoder");
	p = put_byte(p, AMF_STRING);
	p = put_amf_string(p, "Lavf57.37.101");
	
	p = put_amf_string(p, "filesize");
	p = put_amf_double(p, 0);
	
	p = put_amf_string(p, "");
	p = put_byte(p, AMF_OBJECT_END);

	int index = p - body;
	int nRes = 0;
	nRes = SendPacket(RTMP_PACKET_TYPE_INFO, (unsigned char*)body, index, 0);
	if (nRes <= 0)
	{
		WriteLogA(m_logPath, ErrorLevel, "Send MetaData SendPacket RTMP_PACKET_TYPE_INFO failed.");
		return false;
	}
	int i = 0;
	body[i++] = 0x17; // 1:keyframe  7:AVC
	body[i++] = 0x00; // AVC sequence header

	body[i++] = 0x00;
	body[i++] = 0x00;
	body[i++] = 0x00; // fill in 0;

					  // AVCDecoderConfigurationRecord.
	body[i++] = 0x01; // configurationVersion
	body[i++] = lpMetaData->Sps[1]; // AVCProfileIndication
	body[i++] = lpMetaData->Sps[2]; // profile_compatibility
	body[i++] = lpMetaData->Sps[3]; // AVCLevelIndication 
	body[i++] = 0xff; // lengthSizeMinusOne  

					  // sps nums
	body[i++] = 0xE1; //&0x1f
					  // sps data length
	body[i++] = lpMetaData->nSpsLen >> 8;
	body[i++] = lpMetaData->nSpsLen & 0xff;
	// sps data
	memcpy(&body[i], lpMetaData->Sps, lpMetaData->nSpsLen);
	i = i + lpMetaData->nSpsLen;

	// pps nums
	body[i++] = 0x01; //&0x1f
					  // pps data length 
	body[i++] = lpMetaData->nPpsLen >> 8;
	body[i++] = lpMetaData->nPpsLen & 0xff;
	// sps data
	memcpy(&body[i], lpMetaData->Pps, lpMetaData->nPpsLen);
	i = i + lpMetaData->nPpsLen;

	nRes = SendPacket(RTMP_PACKET_TYPE_VIDEO, (unsigned char*)body, i, 0);

	if (nRes <= 0)
	{
		WriteLogA(m_logPath, ErrorLevel, "Send MetaData SendPacket RTMP_PACKET_TYPE_VIDEO failed.");
		return false;
	}
	
	unsigned char audiobody[7];

	//AF 00 + AAC RAW data
	audiobody[0] = 0xAF;
	audiobody[1] = 0x00;
	//just for 48000
	audiobody[2] = 0x11;
	switch (dwAudioCnlCnt)
	{
	case 2:
		audiobody[3] = 0x90;
		break;
	case 4:
		audiobody[3] = 0xA0;
		break;	
	default:
		audiobody[3] = 0xB0;
	}
	audiobody[4] = 0x56;
	audiobody[5] = 0xe5;
	audiobody[6] = 0x00;

	nRes = SendPacket(RTMP_PACKET_TYPE_AUDIO, audiobody, 7, 0);

	if (nRes <= 0)
	{
		WriteLogA(m_logPath, ErrorLevel, "Send MetaData RTMP_PACKET_TYPE_AUDIO RTMP_PACKET_TYPE_AUDIO failed.");
		return false;
	}	
	
	return true;
}

bool CRTMPStream::SendH264Packet(unsigned char* data, unsigned int size, bool bIsKeyFrame, unsigned int nTimeStamp)
{
	if (data == NULL && size < 11)
	{
		return false;
	}

	int i = 0;
	if (bIsKeyFrame)
	{
		m_pVideoBody[i++] = 0x17;// 1:Iframe  7:AVC
	}
	else
	{
		m_pVideoBody[i++] = 0x27;// 2:Pframe  7:AVC
	}
	m_pVideoBody[i++] = 0x01;// AVC NALU
	m_pVideoBody[i++] = 0x00;
	m_pVideoBody[i++] = 0x00;
	m_pVideoBody[i++] = 0x00;

	// NALU size
	m_pVideoBody[i++] = size >> 24;
	m_pVideoBody[i++] = size >> 16;
	m_pVideoBody[i++] = size >> 8;
	m_pVideoBody[i++] = size & 0xff;;

	// NALU data
	memcpy(&m_pVideoBody[i], data, size);

	int bRet = SendPacket(RTMP_PACKET_TYPE_VIDEO, m_pVideoBody, i + size, nTimeStamp);

	return bRet>0;
}
// VOID CALLBACK CRTMPStream::SendVideoCB(
// 	_In_ PVOID lpParameter,
// 	_In_ BOOLEAN TimerOrWaitFired
// )
// {
// 	CRTMPStream* pthis = (CRTMPStream*)lpParameter;
// 	//pthis->SendOneVideoFrm();
// 	pthis->ProcessVideo();
// }

DWORD WINAPI CRTMPStream::SendVideoCB(void* lpParam)
{
	CRTMPStream* p = (CRTMPStream*)lpParam;
	p->ProcessVideo();
	return 0;
}

void CRTMPStream::ProcessVideo()
{
	WriteLogA(m_logPath, InfoLevel, "CRTMPStream::ProcessVideo() Enter.");
	DWORD dwRes = 0;
	uint32_t uStartTime = RTMP_GetTime();
	while (!m_bExitThread && !m_bHasError)
	{
		dwRes = WaitForSingleObject(m_hSemaphoreV, 30);
		if (dwRes != WAIT_OBJECT_0 )		
			continue;
		SendOneVideoFrm(uStartTime);
	}
	WriteLogA(m_logPath, InfoLevel, "CRTMPStream::ProcessVideo() Leave. ExitThread (%d) HasError (%d)", m_bExitThread, m_bHasError);
}


void CRTMPStream::SendOneVideoFrm(uint32_t uStartTime)
{
	//CCritSecLock lock(m_videoCS);

	//m_videoTick += m_video_gap;
	//m_videoTick = RTMP_GetTime() - m_connectTick;
	NaluUnit naluUnit;
	bool bKeyframe = 0;
	int m_audioInx = 0;
	int nStampValue = m_video_gap;
	
	CompVideoFrame *fVideoFrame = nullptr;
	BMRaudioFrame * fBMRaudioFrame = nullptr;
	m_pActionCallBack(TYPE_READ_VIDEO, fBMRaudioFrame, fVideoFrame);
	if (fVideoFrame == nullptr)
	{
		return;
	}
	int nSize = fVideoFrame->getRawSize();
	memcpy(m_pTempVBuf, fVideoFrame->getRaw(), nSize);
	double dTime = fVideoFrame->getTimeStamp();
	mobjVideoPool->release(fVideoFrame);
// 	int nAjust = 0;
// 	if (m_video_gap == 33)
// 		nAjust = 1;

	unsigned int posRead = 0;
	while (true)
	{
		if (ReadOneNaluFromBuf(naluUnit, m_pTempVBuf, nSize, posRead))
		{
			bKeyframe = (naluUnit.type == 0x05) ? TRUE : FALSE;

			if (naluUnit.type == 1 || naluUnit.type == 5)
			{
// 				if (m_videoFrameInx++ % 3 == 2)
// 					nStampValue += nAjust;
// 				m_videoTick += nStampValue;
// 				m_videoTick = RTMP_GetTime() - uStartTime;
				++m_videoFrameInx;
				if (!SendH264Packet(naluUnit.data, naluUnit.size, bKeyframe, dTime))
				{
					WriteLogA(m_logPath, ErrorLevel, "SendOneVideoFrm Send h264 failed ,frame index %d,size %d,type %d, timestamp %.2f videoTotalSend %d", 
						m_videoFrameInx, naluUnit.size,naluUnit.type, dTime, m_videoFrameInx);
					m_bHasError = TRUE;
				}
				else
				{
					if (m_videoFrameInx % 300 == 0)
						WriteLogA(m_logPath, InfoLevel, "SendOneVideoFrm Send h264 out ,frame index %d,timestamp %.2f",m_videoFrameInx, dTime);
				}
				break;
			}
		}
		else
		{
			WriteLogA(m_logPath, ErrorLevel, "SendOneVideoFrm ReadOneNaluFromBuf failed ,frame index %d,size %d,type %d, timestamp %d videoTotalSend %d",
				m_videoFrameInx, naluUnit.size,naluUnit.type, m_videoTick, m_videoFrameInx);
		}
	}	
}		



// VOID CALLBACK CRTMPStream::SendAudioCB(
// 	_In_ PVOID lpParameter,
// 	_In_ BOOLEAN TimerOrWaitFired
// )
// {
// 	CRTMPStream* pthis = (CRTMPStream*)lpParameter;
// 	pthis->SendOneAudioFrm();
// }

DWORD WINAPI CRTMPStream::SendAudioCB(void* lpParam)
{
	CRTMPStream* p = (CRTMPStream*)lpParam;
	p->ProcessAudio();
	return 0;
}

void CRTMPStream::ProcessAudio()
{
	WriteLogA(m_logPath, InfoLevel, "CRTMPStream::ProcessAudio() Enter.");
	
	DWORD dwRes = 0;
	while (!m_bExitThread && !m_bHasError)
	{
		dwRes = WaitForSingleObject(m_hSemaphoreA, 21);
		if (dwRes != WAIT_OBJECT_0)
			continue;
		SendOneAudioFrm();
	}
	WriteLogA(m_logPath, InfoLevel, "CRTMPStream::ProcessAudio() Leave. ExitThread (%d) HasError (%d)", m_bExitThread, m_bHasError);
}

void CRTMPStream::SendOneAudioFrm()
{
	//CCritSecLock lock(m_audioCS);
	int audiolen = 0;
	//if (m_audioPreTick == 0)
	//	m_audioPreTick = RTMP_GetTime();
	//int nStampValue = m_audio_gap;
	double dTime = 0.00;
	if (ReadAACData(m_pOneAudioBuffer, audiolen,dTime))
	{
		//if (m_audioInx++ % 3 == 2)
		//	nStampValue += 1;
		//m_audioTick += nStampValue;
		++m_audioInx;
		if (SendAACData(m_pOneAudioBuffer, audiolen, dTime))
		{
			if (m_audioInx % 1125 == 0)
				WriteLogA(m_logPath, InfoLevel, "SendOneAudioFrm succeed  ,audio index %d audio timestamp %.2f ", m_audioInx, dTime);
		}
		else
		{
			m_bHasError = TRUE;
			WriteLogA(m_logPath, ErrorLevel, "SendOneAudioFrm failed ,audio index %d audio timestamp %.2f audioTotalSend %d", m_audioInx, dTime, m_audioInx);
		}
	}
}

BOOL CRTMPStream::_SendMetaAVData(int bitrate, DWORD dwAudioCnlCnt, CompVideoFrame *fVideoFrame)
{
	WriteLogA(m_logPath, InfoLevel, "RTMP gonna to push MetaAVData.");

	m_bitRate = bitrate;
	m_audioInx = 0, m_audioTick = 0;

	RTMPMetadata metaData2;

	memset(&m_metaData, 0, sizeof(RTMPMetadata));
	memset(&metaData2, 0, sizeof(RTMPMetadata));
	NaluUnit naluUnit;
	unsigned char * pVideoBuf = fVideoFrame->getRaw();
	int nVideoBufSize = fVideoFrame->getRawSize();
	unsigned long nTimeStamp = fVideoFrame->getTimeStamp();
	// 读取SPS帧
	unsigned int posRead = 0;
	if (!ReadOneNaluFromBuf(naluUnit, pVideoBuf, nVideoBufSize, posRead))
	{		
		WriteLogA(m_logPath, InfoLevel, "SendMetaAVData failed due to read SPS failed.");
		return FALSE;
	}
	m_metaData.nSpsLen = naluUnit.size;
	memcpy(m_metaData.Sps, naluUnit.data, naluUnit.size);
	metaData2.nSpsLen = naluUnit.size;
	memcpy(metaData2.Sps, naluUnit.data, naluUnit.size);

	// 读取PPS帧
	if(!ReadOneNaluFromBuf(naluUnit, pVideoBuf, nVideoBufSize, posRead))
	{
		WriteLogA(m_logPath, InfoLevel, "SendMetaAVData failed due to read PPS failed.");
		return FALSE;
	}
	m_metaData.nPpsLen = naluUnit.size;
	memcpy(m_metaData.Pps, naluUnit.data, naluUnit.size);

	metaData2.nPpsLen = naluUnit.size;
	memcpy(metaData2.Pps, naluUnit.data, naluUnit.size);

	// 解码SPS,获取视频图像宽、高信息
	int width = 0, height = 0;
	int frameRate = 0;
	h264_decode_sps(metaData2.Sps, metaData2.nSpsLen, width, height, frameRate);

	h264_decode_sps2(m_metaData.Sps, m_metaData.nSpsLen, width, height);
	m_metaData.nWidth = width;
	m_metaData.nHeight = height;
	m_metaData.nFrameRate = frameRate;
	m_metaData.nVideoDataRate = bitrate;

	WriteLogA(m_logPath, InfoLevel, "SendMetaAVData Video Width %d, Height %d,frame Rate %d", m_metaData.nWidth, m_metaData.nHeight, m_metaData.nFrameRate);

	// 发送MetaData
	if (!_SendMetadata(&m_metaData, dwAudioCnlCnt))
	{
		WriteLogA(m_logPath, ErrorLevel, "SendMetaAVData  failed ");
		return FALSE;
	}

	m_video_gap = 40;
	switch (m_metaData.nFrameRate)
	{
	case 59:
	case 60:
	case 50:
	case 25:
	case 30: m_video_gap = 1000 / m_metaData.nFrameRate;
		break;
	case 29: m_video_gap = 1000 / 30;
		break;
	default:
		break;
	}

	if (ReadOneNaluFromBuf(naluUnit, pVideoBuf, nVideoBufSize, posRead))
	{
		BOOL bKeyframe = (naluUnit.type == 0x05) ? TRUE : FALSE;
		if (naluUnit.type == 1 || naluUnit.type == 5)
		{
			++m_videoFrameInx;
			if (!SendH264Packet(naluUnit.data, naluUnit.size, bKeyframe, nTimeStamp))
			{
				WriteLogA(m_logPath, ErrorLevel, "SendMetaAVData Send h264 failed ,frame inx %d,size %d,type %d, timestamp %.2f videoTotalSend %d",
					m_videoFrameInx, naluUnit.size,	naluUnit.type, nTimeStamp, m_videoFrameInx);
				return FALSE;
			}
		}
		else
		{
			WriteLogA(m_logPath, ErrorLevel, "SendMetaAVData ReadOneNaluFromBuf failed ,NalType is not correct : %d", naluUnit.type);
			return FALSE;
		}
	}
	else
	{
		WriteLogA(m_logPath, ErrorLevel, "SendMetaAVData ReadOneNaluFromBuf Failed.,frame rate %d video gap %d ", m_metaData.nFrameRate, m_video_gap);
		return FALSE;
	}

	WriteLogA(m_logPath, InfoLevel, "SendMetaAVData succeeded ,frame rate %d video gap %d ", m_metaData.nFrameRate, m_video_gap);

	return TRUE;
}


int CRTMPStream::_getNoADTSframe(unsigned char* buffer, int* data_size)
{
	int size = 0;
	while (TRUE)
	{
		//Sync words
		if ((buffer[0] == 0xff) && ((buffer[1] & 0xf0) == 0xf0))
		{
			size |= ((buffer[3] & 0x03) << 11);     //high 2 bit
			size |= buffer[4] << 3;                //middle 8 bit
			size |= ((buffer[5] & 0xe0) >> 5);        //low 3bit
			break;
		}
		++buffer;
	}
	*data_size = size;
	return 0;
}
bool CRTMPStream::ReadAACData(unsigned char* buf, int& len, double &dTime)
{
	CompVideoFrame *fVideoFrame = nullptr;
	BMRaudioFrame * fBMRaudioFrame = nullptr;
	m_pActionCallBack(TYPE_READ_AUDIO, fBMRaudioFrame, fVideoFrame);
	if (fBMRaudioFrame != nullptr)
	{
		/*
		int pos = 7;
		_getNoADTSframe((unsigned char*)fBMRaudioFrame->getRaw(), &len);	
		len -= 7;
		memcpy_s(buf, 4096, fBMRaudioFrame->getRaw() + pos, len);
		mobjAudioPool->release(fBMRaudioFrame);
		*/
		unsigned char* pAudioBuf = (unsigned char*)fBMRaudioFrame->getRaw();
		dTime = fBMRaudioFrame->getTimeStamp();
		int pos = 0;
		unsigned char* buffer = (unsigned char*)(pAudioBuf + pos);

		_getNoADTSframe(buffer, &len);
		pos += 7;
		len -= 7; //remove the header len
		memcpy_s(buf, 4096, pAudioBuf + pos, len);
		pos += len;
		mobjAudioPool->release(fBMRaudioFrame);
		return true;
	}
	return false;
}

bool CRTMPStream::ReadOneNaluFromBuf(NaluUnit& nalu, unsigned char* pVideoBuf, unsigned int nVideoBufSize, unsigned int &posRead)
{
	int i = posRead;
	while (i < nVideoBufSize)
	{
		if (pVideoBuf[i++] == 0x00 &&
			pVideoBuf[i++] == 0x00 &&
			pVideoBuf[i++] == 0x00 &&
			pVideoBuf[i++] == 0x01
			)
		{
			int pos = i;
			while (pos < nVideoBufSize)
			{
				if (pVideoBuf[pos++] == 0x00 &&
					pVideoBuf[pos++] == 0x00 &&
					pVideoBuf[pos++] == 0x00 &&
					pVideoBuf[pos++] == 0x01
					)
				{
					break;
				}
			}
			if (pos == nVideoBufSize)
			{
				nalu.size = pos - i;
			}
			else
			{
				nalu.size = (pos - 4) - i;
			}
			nalu.type = pVideoBuf[i] & 0x1f;
			nalu.data = &pVideoBuf[i];

			posRead = pos - 4;
			return TRUE;
		}
	}
	return FALSE;
}
