#include <mbstring.h>
#include "RTMPWrapper.h"

CRTMPWrapper::CRTMPWrapper() :m_nStreamStatus(BMRStreamStatus_stopped), m_dwRetryTimes(0), m_threadRtmp(nullptr)
{
	m_nbCacheFrames = 0;
	m_nRecordFrames = 0;
	m_bExitThread = TRUE;
	m_nSetBufCount = nbMaxCaptureVPoolSize;
	m_captureVideoPool.initialize<CompVideoFrame>(nbMaxCaptureVPoolSize);
	InitVideoMem();
	m_captureAudioPool.initialize<BMRaudioFrame>(nbMaxCaptureAPoolSize);
	m_pRTMPStream = new CRTMPStream();
}

CRTMPWrapper::~CRTMPWrapper()
{
}

void CRTMPWrapper::Initialize(int _cnl, int nIndex)
{
	m_nCnl = _cnl;
	m_nIndex = nIndex;
	wsprintf(m_logPath, _T("C:\\logs\\SimplyLiveRTMPServer\\BMR\\Cam%c-RTMP-%d.log"), _cnl + _T('A'), nIndex);
	m_pRTMPStream->SetLogPath(m_logPath);
	m_pRTMPStream->SetCallBack(std::bind(&CRTMPWrapper::RTMP_ReadFrmCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
	m_pRTMPStream->SetObjectPool(&m_captureVideoPool, &m_captureAudioPool);
}

void CRTMPWrapper::Start(const BMRSetting &bmrSetting)
{
	m_bExitThread = TRUE;
	m_nbCacheFrames = 0;
	m_nRecordFrames = 0;
	memcpy(&m_bmrSetting, &bmrSetting, sizeof(bmrSetting));

	if (m_threadRtmp)
	{
		DWORD ret = WaitForSingleObject(&m_threadRtmp, INFINITE);
		CloseHandle(m_threadRtmp);
		m_threadRtmp = nullptr;
	}
	m_bExitThread = FALSE;
	m_threadRtmp = CreateThread(NULL, 0, PushRTMPStreamThread, (void*)this, 0, NULL);
}

void CRTMPWrapper::AddEncodedData(BOOL bVideo, BYTE *pData, double dwTimeStamp, int nDataSize, BOOL bKeyFrame)
{
	if (m_bExitThread)
		return;
	if (bVideo)
	{
		++m_nRecordFrames;
		if (geUsedBufCount() >= m_nSetBufCount)
		{
			WriteLogA(m_logPath, ErrorLevel, "CRTMPWrapper::AddEncodedData failed, The buffer count set by user is full.(%d)", m_nSetBufCount);
			return;
		}
		CompVideoFrame *pVFrame = nullptr;
		m_captureVideoPool.getNew(pVFrame);
		if (pVFrame != nullptr)
		{
			pVFrame->setBufferSize(nDataSize);
			pVFrame->SetFrameID(m_nRecordFrames);
			pVFrame->setIsIDR(bKeyFrame);
			memcpy(pVFrame->getRaw(), pData, nDataSize);
			pVFrame->setTimeStamp(dwTimeStamp);
			{
				LockHolder locker(m_mutexVideo);
				m_videoList.push(pVFrame);
			}
			++m_nbCacheFrames;
			m_pRTMPStream->generateVGenlock();
		}
		else
		{
			WriteLogA(m_logPath, ErrorLevel, "CEncoderVideo::AddEncodedData  getNew failed.RTMP will lost video frame: FrameId(%d).", m_nRecordFrames);
		}
	}
	else
	{
		BMRaudioFrame *pAFrame = nullptr;
		m_captureAudioPool.getNew(pAFrame);
		if (pAFrame != nullptr)
		{
			pAFrame->setBufferSize(nDataSize);
			memcpy(pAFrame->getRaw(), pData, nDataSize);
			pAFrame->setTimeStamp(dwTimeStamp);
			{
				LockHolder locker(m_mutexAudio);
				m_audioList.push(pAFrame);
			}
			m_pRTMPStream->generateAudioGenlock();
		}
		else
		{
			WriteLogA(m_logPath, ErrorLevel, "CEncoderAudio::dumpFrameToFile  getNew failed.RTMP will lost audio frame.");
		}
	}
}

void CRTMPWrapper::Stop()
{
	if (!m_bExitThread)
	{
		WriteLogA(m_logPath, InfoLevel, "CRTMPWrapper::Stop() Start.");

		m_nbCacheFrames = 0;
		m_nRecordFrames = 0;
		m_bExitThread = TRUE;

		if (m_threadRtmp)
		{
			DWORD ret = WaitForSingleObject(&m_threadRtmp, INFINITE);
			CloseHandle(m_threadRtmp);
			m_threadRtmp = nullptr;
		}
		ClearAll();
		WriteLogA(m_logPath, InfoLevel, "CRTMPWrapper::Stop() End.");
	}
}

void CRTMPWrapper::GetCurrentStatus(BMRNetdriveStatusCameraStreamStatus &status)
{
	status.nRecordFrames = m_nRecordFrames;
	status.nDropFrames = m_nRecordFrames - m_nbCacheFrames;
	status.nBandWidth = 0;
	status.nPinResponseTime = 0;
	status.nCurrentRetry = 0;
	status.nBufTotal = m_nSetBufCount;
	status.nBufFrames = geUsedBufCount();

	if (!m_bExitThread)
	{
		status.nStreamStatus = m_nStreamStatus;
		status.nCurrentRetry = m_dwRetryTimes;
	}
	else
	{
		status.nStreamStatus = BMRStreamStatus_stopped;
		memset(m_szErrorMsg, 0, MAX_PATH);
	}
	memcpy_s(status.szLastMsg, MAX_PATH, m_szErrorMsg, MAX_PATH);
}

void CRTMPWrapper::setBufferCount(unsigned long nBufUse)
{
	if (nBufUse > nbMaxCaptureVPoolSize)
		nBufUse = nbMaxCaptureVPoolSize;
	m_nSetBufCount = nBufUse;
}

int CRTMPWrapper::geUsedBufCount()
{
	return m_videoList.size();
}

void CRTMPWrapper::RTMP_ReadFrmCallback(OPERATION_TYPE emType, BMRaudioFrame *&fBMRaudioFrame, CompVideoFrame *&fVideoFrame)
{
	switch (emType)
	{
	case TYPE_READ_AUDIO:
	{
		fBMRaudioFrame = nullptr;
		LockHolder locker(m_mutexAudio);
		if (!m_audioList.empty())
		{
			fBMRaudioFrame = m_audioList.front();
			m_audioList.pop();
		}
	}
	break;
	case TYPE_READ_VIDEO:
	{
		fVideoFrame = nullptr;
		LockHolder locker(m_mutexVideo);
		if (!m_videoList.empty())
		{
			fVideoFrame = m_videoList.front();
			m_videoList.pop();
		}
	}
	break;
	case TYPE_READ_CLEARALL:
		ClearAll();
		break;
	}
}

void CRTMPWrapper::ClearAll()
{
	//
	BMRaudioFrame *fBMRaudioFrame = nullptr;
	CompVideoFrame *fVideoFrame = nullptr;
	{
		LockHolder locker(m_mutexAudio);
		while (!m_audioList.empty())
		{
			fBMRaudioFrame = m_audioList.front();
			m_audioList.pop();
			m_captureAudioPool.release(fBMRaudioFrame);
		}
	}

	{
		LockHolder locker(m_mutexVideo);
		while (!m_videoList.empty())
		{
			fVideoFrame = m_videoList.front();
			m_videoList.pop();
			m_captureVideoPool.release(fVideoFrame);
		}
	}
}

//-----------------------------------------------------------------------------

DWORD WINAPI CRTMPWrapper::PushRTMPStreamThread(void* lpParam)
{
	CRTMPWrapper* p = (CRTMPWrapper*)lpParam;
	p->PushRTMPStream();
	// 	if (p->m_sliceDone != nullptr)
	// 	{
	// 		p->m_sliceDone->vSliceDone(0xFFFFFFFE, p->m_nIndex);
	// 	}
	SetEvent(p->m_threadRtmp);
	return 0;
}

void CRTMPWrapper::PushRTMPStream()
{
	WriteLogA(m_logPath, InfoLevel, "CRTMPWrapper::PushRTMPStream() Start.");
	byte *address = m_bmrSetting.netdriveSetting[m_nIndex].subNetdrive[0].keyCamera[m_nCnl].pathValue;

	if (_mbsnicmp((unsigned char *)address, (unsigned char *)"rtmp", 4) != 0)
	{
		if (strlen((char*)address) == 0)
		{
			WriteLogA(m_logPath, ErrorLevel, "CRTMPWrapper::PushRTMPStream() getNetworkDriveValString failed, incorrect address(empty),nIndex :%d", m_nIndex);
			return;
		}
		WriteLogA(m_logPath, ErrorLevel, "CRTMPWrapper::PushRTMPStream() getNetworkDriveValString failed, incorrect address(%s).nIndex :%d", address, m_nIndex);
		return;
	}

	unsigned long bitrate = m_bmrSetting.cameraSetting[m_nCnl].compressionSetting.compression;
	if (bitrate < 1000 || bitrate>25000)
		bitrate = 6000;

	DWORD dwRTMPRetryTimes = m_bmrSetting.cameraSetting[m_nCnl].streamSetting.dwRetryTimes;
	DWORD dwRTMPDelayBetweenRetry = m_bmrSetting.cameraSetting[m_nCnl].streamSetting.dwDealyBetweenRetry;
	DWORD dwAudioCnlCnt = m_bmrSetting.cameraSetting[m_nCnl].audioSetting.LowNum;
	if (dwAudioCnlCnt != 2 && dwAudioCnlCnt != 4 && dwAudioCnlCnt != 6)
	{
		WriteLogA(m_logPath, ErrorLevel, "CRTMPWrapper::PushRTMPStream() dwAudioCnlCnt(%d) Error.Set to 6", dwAudioCnlCnt);
		dwAudioCnlCnt = 6;
	}
	if (dwRTMPRetryTimes == 0)
		dwRTMPRetryTimes = 1;
	WriteLogA(m_logPath, InfoLevel, "CRTMPWrapper::PushRTMPStream() Address(%s).Parameter RTMPRetryTimes %d RTMPDelayBetweenRetry %d dwAudioCnlCnt(%d)",
		address, dwRTMPRetryTimes, dwRTMPDelayBetweenRetry, dwAudioCnlCnt);

	m_dwRetryTimes = 0;
	BOOL bError = TRUE;
	memset(m_szErrorMsg, 0, MAX_PATH);
	m_nStreamStatus = BMRStreamStatus_stopped;
	while (m_dwRetryTimes < dwRTMPRetryTimes && bError && !m_bExitThread)
	{
		if (bError && !m_bExitThread && m_dwRetryTimes != 0)
		{
			memcpy_s(m_szErrorMsg, MAX_PATH, "ReConnect to server.", strlen("ReConnect to server.") + 1);
			Sleep(dwRTMPDelayBetweenRetry * 1000);
		}
		m_nStreamStatus = BMRStreamStatus_connecting;
		memcpy_s(m_szErrorMsg, MAX_PATH, "Connecting to server.", strlen("Connecting to server.") + 1);
		if (m_bExitThread)
		{
			break;
		}
		if (!m_pRTMPStream->Connect((char*)address))
		{
			WriteLogA(m_logPath, ErrorLevel, "CRTMPWrapper::PushRTMPStream() Connect failed.Streaming time %d", m_dwRetryTimes);
			memcpy_s(m_szErrorMsg, MAX_PATH, "Connect to server failed.", strlen("Connect to server failed.") + 1);
			++m_dwRetryTimes; m_nStreamStatus = BMRStreamStatus_error;
			continue;
		}
		if (m_bExitThread)
		{
			break;
		}
		if (!m_pRTMPStream->StartStream(bitrate, dwAudioCnlCnt))
		{
			WriteLogA(m_logPath, ErrorLevel, "CRTMPWrapper::PushRTMPStream() StartStream failed.Streaming time %d", m_dwRetryTimes);
			memcpy_s(m_szErrorMsg, MAX_PATH, "StartStream to server failed.", strlen("StartStream to server failed.") + 1);
			++m_dwRetryTimes; m_nStreamStatus = BMRStreamStatus_error;
			continue;
		}
		m_nStreamStatus = BMRStreamStatus_streaming;
		memcpy_s(m_szErrorMsg, MAX_PATH, "Streaming.", strlen("Streaming.") + 1);
		bError = m_pRTMPStream->CheckHasError();
		while (!bError && !m_bExitThread)
		{
			msleep(1000);
			bError = m_pRTMPStream->CheckHasError();
		}

		++m_dwRetryTimes; m_nStreamStatus = BMRStreamStatus_error;
		memcpy_s(m_szErrorMsg, MAX_PATH, "Send packet to server failed.", strlen("Send packet to server failed.") + 1);

		WriteLogA(m_logPath, InfoLevel, "CRTMPWrapper::PushRTMPStream() finished.Streaming times %d.HasError %d, UsrStop :%d", m_dwRetryTimes, bError, m_bExitThread);
	}
	m_pRTMPStream->StopStream();
	ClearAll();
	WriteLogA(m_logPath, InfoLevel, "PushRTMPStream Stop.");
}

void CRTMPWrapper::InitVideoMem()
{
	DWORD dwSize = nbMaxHDVideoSize;
	if (Config::getInstance()->is4K())
		dwSize = nbMax4KVideoSize;
	CompVideoFrame *pVFrame = nullptr;
	for (int n = 0; n < nbMaxCaptureVPoolSize; ++n)
	{
		m_captureVideoPool.getNew(pVFrame);
		if (pVFrame != nullptr)
		{
			pVFrame->setBufferSize(dwSize);
			m_captureVideoPool.release(pVFrame);
		}
	}
}