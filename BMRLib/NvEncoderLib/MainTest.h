﻿#pragma once
#include "INvCallBack.h"

class CWriteFrameTest :public INvCallBack
{
public:
	CWriteFrameTest();
	~CWriteFrameTest();
	void cb(void* bitstreamBufferPtr, DWORD  bitstreamSizeInBytes, DWORD frameIdx, UINT64  outputTimeStamp, int nType);

	FILE *fpDst;
	DWORD dwFrames;
	DWORD dwIndex;

	DWORD m_dwType[8];
};