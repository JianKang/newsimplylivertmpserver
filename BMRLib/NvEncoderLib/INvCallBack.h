#pragma once

class INvCallBack
{
public:
	virtual void cb(void* bitstreamBufferPtr, DWORD  bitstreamSizeInBytes, DWORD frameIdx, UINT64  outputTimeStamp, int nType) = 0;
};
