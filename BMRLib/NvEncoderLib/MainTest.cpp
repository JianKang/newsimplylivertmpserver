#include "NvEncoder.h"
#include "MainTest.h"
#include <iostream>

CWriteFrameTest::CWriteFrameTest()
{
	dwFrames = 0;
	dwIndex = 0;
	fpDst = NULL;
	char szName[MAX_PATH];
	sprintf_s(szName, "E:\\TestFiles\\DST_video_%d.h264", dwIndex);
	fpDst = fopen(szName, "wb");
	for (int n = 0; n < 8;++n)
	{
		m_dwType[n] = 0;
	}

}

CWriteFrameTest::~CWriteFrameTest()
{
	if (fpDst)
		fclose(fpDst);
}

void CWriteFrameTest::cb(void* bitstreamBufferPtr, DWORD  bitstreamSizeInBytes, DWORD frameIdx, UINT64  outputTimeStamp, int nType)
{
	if (dwFrames == 600)
	{
		++dwIndex;
		dwFrames = 0;
		if (fpDst)
			fclose(fpDst);
		char szName[MAX_PATH];
		sprintf_s(szName, "E:\\TestFiles\\DST_video_%d.h264", dwIndex);
		fpDst = fopen(szName, "wb");		
	}

	fwrite(bitstreamBufferPtr, bitstreamSizeInBytes, 1, fpDst);

	if (nType == 0xff)
	{
		nType = 7;
	}
	m_dwType[nType]++;
	//if (nType == 3 || nType == 6 )
	dwFrames++;

}


int main(int argc, char **argv)
{
	FILE *fpSrc = nullptr;

	fpSrc = fopen("E:\\TestFiles\\HeavyHand_1080p.yuv", "rb");
	//fpSrc = fopen("d:\\Clips\\video_UYVYHD.yuv", "rb");
	DWORD dwFrames = 0;
	CNvEncoder nvEncoder;
	BYTE *pBuf = new BYTE[2 * 1920 * 1080];
	CWriteFrameTest tt;

	EncodeConfig stencodeConfig;
	memset(&stencodeConfig, 0, sizeof(EncodeConfig));
	stencodeConfig.endFrameIdx = INT_MAX;
	stencodeConfig.bitrate = 6000 * 1000;
	stencodeConfig.rcMode = NV_ENC_PARAMS_RC_CONSTQP;
	stencodeConfig.gopLength = 1;
	stencodeConfig.deviceType = NV_ENC_CUDA;
	stencodeConfig.codec = NV_ENC_HEVC;
	stencodeConfig.frameRateNum = 25;
	stencodeConfig.frameRateDen = 1;
	stencodeConfig.qp = 28;
	stencodeConfig.i_quant_factor = DEFAULT_I_QFACTOR;
	stencodeConfig.b_quant_factor = DEFAULT_B_QFACTOR;
	stencodeConfig.i_quant_offset = DEFAULT_I_QOFFSET;
	stencodeConfig.b_quant_offset = DEFAULT_B_QOFFSET;
	stencodeConfig.presetGUID = NV_ENC_PRESET_DEFAULT_GUID;
	stencodeConfig.pictureStruct = NV_ENC_PIC_STRUCT_FIELD_TOP_BOTTOM; NV_ENC_PIC_STRUCT_FIELD_TOP_BOTTOM; NV_ENC_PIC_STRUCT_FIELD_BOTTOM_TOP; NV_ENC_PIC_STRUCT_FRAME;
	stencodeConfig.isYuv444 = 0;
	stencodeConfig.deviceID = 0;
	stencodeConfig.width = 1280;
	stencodeConfig.height = 720;

	NVENCSTATUS sts = NV_ENC_ERR_NO_ENCODE_DEVICE;// nvEncoder.Initialize(stencodeConfig, &tt);
	if (sts != NV_ENC_SUCCESS)
	{
		printf("nvEncoder.Initialize Failed.\n");
	}

	//for (int n = 0; n < 76; ++n)
	for (int n = 0; n < 1800; ++n)
	{
		fread(pBuf, 1920*1080*2, 1, fpSrc);
		sts = nvEncoder.EncodeFrame(pBuf);
		if (sts != NV_ENC_SUCCESS)
		{
			printf("nvEncoder.EncodeFrame Failed.\n");
		}
	}
	
	nvEncoder.EncodeFrame(nullptr);

	nvEncoder.UnInitialize();
	
	std::cout << "Index :" << tt.dwIndex << ", Frames : " << tt.dwFrames << std::endl;

	if (fpSrc)
		fclose(fpSrc);
	return 0;
}
