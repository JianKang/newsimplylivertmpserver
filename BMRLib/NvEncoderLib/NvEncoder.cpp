////////////////////////////////////////////////////////////////////////////
//
// Copyright 1993-2014 NVIDIA Corporation.  All rights reserved.
//
// Please refer to the NVIDIA end user license agreement (EULA) associated
// with this source code for terms and conditions that govern your use of
// this software. Any use, reproduction, disclosure, or distribution of
// this software and related documentation outside the terms of the EULA
// is strictly prohibited.
//
////////////////////////////////////////////////////////////////////////////

#include "nvCPUOPSys.h"
#include "nvEncodeAPI.h"
#include "nvUtils.h"
#include "NvEncoder.h"
#include "nvFileIO.h"
#include <new>

#define BITSTREAM_BUFFER_SIZE 2 * 1024 * 1024

CNvEncoder::CNvEncoder()
{
    m_pNvHWEncoder = new CNvHWEncoder;
    m_pDevice = NULL;
    m_cuContext = NULL;
    m_uEncodeBufferCount = 0;
	memset(&m_encodeConfig, 0, sizeof(m_encodeConfig));
    memset(&m_stEOSOutputBfr, 0, sizeof(m_stEOSOutputBfr));
    memset(&m_stEncodeBuffer, 0, sizeof(m_stEncodeBuffer));
}

CNvEncoder::~CNvEncoder()
{
    if (m_pNvHWEncoder)
    {
        delete m_pNvHWEncoder;
        m_pNvHWEncoder = NULL;
    }
}

NVENCSTATUS CNvEncoder::Initialize(EncodeConfig	&encodeConfig,INvCallBack* pWriteCallBack,char *szCardName)
{
	memcpy(&m_encodeConfig, &encodeConfig, sizeof(EncodeConfig));
	NVENCSTATUS nvStatus = _InitCuda(m_encodeConfig.deviceID, szCardName);
	if (nvStatus != NV_ENC_SUCCESS)
		return nvStatus;
	nvStatus = m_pNvHWEncoder->Initialize(m_pDevice, pWriteCallBack,NV_ENC_DEVICE_TYPE_CUDA);
	if (nvStatus != NV_ENC_SUCCESS)
		return nvStatus;
	m_encodeConfig.presetGUID = m_pNvHWEncoder->GetPresetGUID(m_encodeConfig.encoderPreset, m_encodeConfig.codec);
	nvStatus = m_pNvHWEncoder->CreateEncoder(&m_encodeConfig);
	if (nvStatus != NV_ENC_SUCCESS)
		return nvStatus;

	m_encodeConfig.maxWidth = m_encodeConfig.maxWidth ? m_encodeConfig.maxWidth : m_encodeConfig.width;
	m_encodeConfig.maxHeight = m_encodeConfig.maxHeight ? m_encodeConfig.maxHeight : m_encodeConfig.height;

	int numMBs = ((m_encodeConfig.maxHeight + 15) >> 4) * ((m_encodeConfig.maxWidth + 15) >> 4);
	int NumIOBuffers;
	if (numMBs >= 32768) //4kx2k
		NumIOBuffers = MAX_ENCODE_QUEUE / 8;
	else if (numMBs >= 16384) // 2kx2k
		NumIOBuffers = MAX_ENCODE_QUEUE / 4;
	else if (numMBs >= 8160) // 1920x1080
		NumIOBuffers = MAX_ENCODE_QUEUE / 2;
	else
		NumIOBuffers = MAX_ENCODE_QUEUE;
	m_uEncodeBufferCount = NumIOBuffers;

	m_uPicStruct = m_encodeConfig.pictureStruct;

	nvStatus = _AllocateIOBuffers(m_encodeConfig.width, m_encodeConfig.height, m_encodeConfig.isYuv444);

	int lumaPlaneSize, chromaPlaneSize;
	uint32_t  chromaFormatIDC = (m_encodeConfig.isYuv444 ? 3 : 1);
	lumaPlaneSize = m_encodeConfig.maxWidth * m_encodeConfig.maxHeight;
	chromaPlaneSize = (chromaFormatIDC == 3) ? lumaPlaneSize : (lumaPlaneSize >> 2);

	//yuv[0] = new(std::nothrow) uint8_t[lumaPlaneSize];
	//yuv[1] = new(std::nothrow) uint8_t[chromaPlaneSize];
	//yuv[2] = new(std::nothrow) uint8_t[chromaPlaneSize];

	return nvStatus;
}

NVENCSTATUS CNvEncoder::EncodeFrame_UYVY(BYTE *pBufferUYVY)
{
	return _EncodeFrame_UYVY(pBufferUYVY, false, m_encodeConfig.width, m_encodeConfig.height);	
}
NVENCSTATUS CNvEncoder::EncodeFrame(BYTE *pBufferYUV420p)
{
	if (pBufferYUV420p == nullptr)
	{
		return _EncodeFrame(NULL, true, m_encodeConfig.width, m_encodeConfig.height);
	}

	EncodeFrameConfig stEncodeFrame;
	memset(&stEncodeFrame, 0, sizeof(stEncodeFrame));

	stEncodeFrame.yuv[0] = pBufferYUV420p;
	stEncodeFrame.yuv[1] = pBufferYUV420p + m_encodeConfig.width*m_encodeConfig.height;
	stEncodeFrame.yuv[2] = pBufferYUV420p + m_encodeConfig.width*m_encodeConfig.height+ m_encodeConfig.width*m_encodeConfig.height/4;

	stEncodeFrame.stride[0] = m_encodeConfig.width;
	stEncodeFrame.stride[1] = (m_encodeConfig.isYuv444) ? m_encodeConfig.width : m_encodeConfig.width / 2;
	stEncodeFrame.stride[2] = (m_encodeConfig.isYuv444) ? m_encodeConfig.width : m_encodeConfig.width / 2;
	stEncodeFrame.width = m_encodeConfig.width;
	stEncodeFrame.height = m_encodeConfig.height;
	
	//_ConvertUYVYToYUV420P(pBufferIn,m_encodeConfig.width, m_encodeConfig.height, m_encodeConfig.isYuv444);
	
	return _EncodeFrame(&stEncodeFrame, false, m_encodeConfig.width, m_encodeConfig.height);	
}

NVENCSTATUS CNvEncoder::GetHeaders(uint32_t &sps_size, BYTE * sps, uint32_t &pps_size, BYTE * pps, FILE * fpVideoHdr)
{
	uint32_t outSize = 0;
	char tmpHeader[256];
	NV_ENC_SEQUENCE_PARAM_PAYLOAD payload = { 0 };
	payload.version = NV_ENC_SEQUENCE_PARAM_PAYLOAD_VER;

	payload.spsppsBuffer = tmpHeader;
	payload.inBufferSize = sizeof(tmpHeader);
	payload.outSPSPPSPayloadSize = &outSize;
	
	NVENCSTATUS nvStatus = m_pNvHWEncoder->NvEncGetSequenceParams(&payload);
	if (nvStatus != NV_ENC_SUCCESS)
		return nvStatus;

	
	sps_size = 4;

	while (tmpHeader[sps_size] != 0x00
		|| tmpHeader[sps_size + 1] != 0x00
		|| tmpHeader[sps_size + 2] != 0x00
		|| tmpHeader[sps_size + 3] != 0x01)
	{
		sps_size += 1;
		if (sps_size >= outSize)
		{
			return NV_ENC_ERR_GENERIC;
		}
	}

	pps_size = outSize - sps_size;


	sps_size -= 4;
	pps_size -= 4;
	memcpy(sps, tmpHeader + 4, sps_size);
	memcpy(pps, tmpHeader + sps_size + 4 + 4, pps_size);
	/*
	char *sps = tmpHeader;
	unsigned int i_sps = 4;

	while (tmpHeader[i_sps] != 0x00
		|| tmpHeader[i_sps + 1] != 0x00
		|| tmpHeader[i_sps + 2] != 0x00
		|| tmpHeader[i_sps + 3] != 0x01)
	{
		i_sps += 1;
		if (i_sps >= outSize)
		{
			return nvStatus;
		}
	}

	char *pps = tmpHeader + i_sps;
	unsigned int i_pps = outSize - i_sps;

	headerPacket.Clear();
	BufferOutputSerializer headerOut(headerPacket);

	headerOut.OutputByte(0x17);
	headerOut.OutputByte(0);
	headerOut.OutputByte(0);
	headerOut.OutputByte(0);
	headerOut.OutputByte(0);
	headerOut.OutputByte(1);
	headerOut.Serialize(sps + 5, 3);
	headerOut.OutputByte(0xff);
	headerOut.OutputByte(0xe1);
	headerOut.OutputWord(htons(i_sps - 4));
	headerOut.Serialize(sps + 4, i_sps - 4);

	headerOut.OutputByte(1);
	headerOut.OutputWord(htons(i_pps - 4));
	headerOut.Serialize(pps + 4, i_pps - 4);

	DataPacket packet;

	packet.size = headerPacket.Num();
	packet.lpPacket = headerPacket.Array();

	fwrite(packet.lpPacket, packet.size, 1, fpVideoHdr);
	*/
	return nvStatus;
}

NVENCSTATUS CNvEncoder::UnInitialize()
{
	return	_Deinitialize(m_encodeConfig.deviceType);
}



NVENCSTATUS CNvEncoder::_InitCuda(uint32_t deviceID, char *szCardName)
{
    CUresult cuResult;
    CUdevice device;
    CUcontext cuContextCurr;
    int  deviceCount = 0;
    int  SMminor = 0, SMmajor = 0;

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
    typedef HMODULE CUDADRIVER;
#else
    typedef void *CUDADRIVER;
#endif
    CUDADRIVER hHandleDriver = 0;
    cuResult = cuInit(0, __CUDA_API_VERSION, hHandleDriver);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuInit error:0x%x\n", cuResult);
#ifdef _DEBUG		
		assert(0);
#else
#endif
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }

    cuResult = cuDeviceGetCount(&deviceCount);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuDeviceGetCount error:0x%x\n", cuResult);
#ifdef _DEBUG		
		assert(0);
#else
#endif
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }
	if (deviceCount <= 0)
	{
		PRINTERR("cuDeviceGetCount deviceCount is zero.\n");
#ifdef _DEBUG		
		assert(0);
#else
#endif
		return NV_ENC_ERR_NO_ENCODE_DEVICE;
	}

    // If dev is negative value, we clamp to 0
    if ((int)deviceID < 0)
        deviceID = 0;

    if (deviceID >(unsigned int)deviceCount - 1)
    {
        //PRINTERR("Invalid Device Id = %d\n", deviceID);
		deviceID = 0;
        //return NV_ENC_ERR_INVALID_ENCODERDEVICE;
    }

    cuResult = cuDeviceGet(&device, deviceID);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuDeviceGet error:0x%x\n", cuResult);
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }
	CUdevprop major;
	cuResult = cuDeviceGetProperties(&major, device);
	if (cuResult != CUDA_SUCCESS)
	{
		PRINTERR("cuDeviceGetAttribute error:0x%x\n", cuResult);
		return NV_ENC_ERR_NO_ENCODE_DEVICE;
	}
	cuResult = cuDeviceGetName(szCardName, MAX_PATH, device);
	if (cuResult != CUDA_SUCCESS)
	{
		PRINTERR("cuDeviceGetName error:0x%x\n", cuResult);
#ifdef _DEBUG		
		assert(0);
#else
#endif
		return NV_ENC_ERR_NO_ENCODE_DEVICE;
	}
    cuResult = cuDeviceComputeCapability(&SMmajor, &SMminor, deviceID);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuDeviceComputeCapability error:0x%x\n", cuResult);
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }

    if (((SMmajor << 4) + SMminor) < 0x30)
    {
        PRINTERR("GPU %d does not have NVENC capabilities exiting\n", deviceID);
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }

    cuResult = cuCtxCreate((CUcontext*)(&m_pDevice), 0, device);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuCtxCreate error:0x%x\n", cuResult);
#ifdef _DEBUG		
		assert(0);
#else
#endif
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }

    cuResult = cuCtxPopCurrent(&cuContextCurr);
    if (cuResult != CUDA_SUCCESS)
    {
        PRINTERR("cuCtxPopCurrent error:0x%x\n", cuResult);
#ifdef _DEBUG		
		assert(0);
#else
#endif
        return NV_ENC_ERR_NO_ENCODE_DEVICE;
    }
    return NV_ENC_SUCCESS;
}

NVENCSTATUS CNvEncoder::_AllocateIOBuffers(uint32_t uInputWidth, uint32_t uInputHeight, uint32_t isYuv444)
{
    NVENCSTATUS nvStatus = NV_ENC_SUCCESS;

    m_EncodeBufferQueue.Initialize(m_stEncodeBuffer, m_uEncodeBufferCount);
    for (uint32_t i = 0; i < m_uEncodeBufferCount; i++)
    {
        nvStatus = m_pNvHWEncoder->NvEncCreateInputBuffer(uInputWidth, uInputHeight, &m_stEncodeBuffer[i].stInputBfr.hInputSurface, isYuv444);
        if (nvStatus != NV_ENC_SUCCESS)
            return nvStatus;

        m_stEncodeBuffer[i].stInputBfr.bufferFmt = isYuv444 ? NV_ENC_BUFFER_FORMAT_YUV444_PL : NV_ENC_BUFFER_FORMAT_NV12_PL;
        m_stEncodeBuffer[i].stInputBfr.dwWidth = uInputWidth;
        m_stEncodeBuffer[i].stInputBfr.dwHeight = uInputHeight;

        //Allocate output surface
		if (m_encodeConfig.enableMEOnly)
        {
            uint32_t encodeWidthInMbs = (uInputWidth + 15) >> 4;
            uint32_t encodeHeightInMbs = (uInputHeight + 15) >> 4;
            uint32_t dwSize = encodeWidthInMbs * encodeHeightInMbs * 64;
            nvStatus = m_pNvHWEncoder->NvEncCreateMVBuffer(dwSize, &m_stEncodeBuffer[i].stOutputBfr.hBitstreamBuffer);
            if (nvStatus != NV_ENC_SUCCESS)
            {
                PRINTERR("nvEncCreateMVBuffer error:0x%x\n", nvStatus);
                return nvStatus;
            }
            m_stEncodeBuffer[i].stOutputBfr.dwBitstreamBufferSize = dwSize;
        }
        else
        {
            nvStatus = m_pNvHWEncoder->NvEncCreateBitstreamBuffer(BITSTREAM_BUFFER_SIZE, &m_stEncodeBuffer[i].stOutputBfr.hBitstreamBuffer);
            if (nvStatus != NV_ENC_SUCCESS)
                return nvStatus;
            m_stEncodeBuffer[i].stOutputBfr.dwBitstreamBufferSize = BITSTREAM_BUFFER_SIZE;
        }

#if defined (NV_WINDOWS)
        nvStatus = m_pNvHWEncoder->NvEncRegisterAsyncEvent(&m_stEncodeBuffer[i].stOutputBfr.hOutputEvent);
        if (nvStatus != NV_ENC_SUCCESS)
            return nvStatus;
		if (m_encodeConfig.enableMEOnly)
        {
            m_stEncodeBuffer[i].stOutputBfr.bWaitOnEvent = false;
        }
        else
            m_stEncodeBuffer[i].stOutputBfr.bWaitOnEvent = true;
#else
        m_stEncodeBuffer[i].stOutputBfr.hOutputEvent = NULL;
#endif
    }

    m_stEOSOutputBfr.bEOSFlag = TRUE;

#if defined (NV_WINDOWS)
    nvStatus = m_pNvHWEncoder->NvEncRegisterAsyncEvent(&m_stEOSOutputBfr.hOutputEvent);
    if (nvStatus != NV_ENC_SUCCESS)
        return nvStatus; 
#else
    m_stEOSOutputBfr.hOutputEvent = NULL;
#endif

    return NV_ENC_SUCCESS;
}

NVENCSTATUS CNvEncoder::_ReleaseIOBuffers()
{
    for (uint32_t i = 0; i < m_uEncodeBufferCount; i++)
    {
        m_pNvHWEncoder->NvEncDestroyInputBuffer(m_stEncodeBuffer[i].stInputBfr.hInputSurface);
        m_stEncodeBuffer[i].stInputBfr.hInputSurface = NULL;

		if (m_encodeConfig.enableMEOnly)
        {
            m_pNvHWEncoder->NvEncDestroyMVBuffer(m_stEncodeBuffer[i].stOutputBfr.hBitstreamBuffer);
            m_stEncodeBuffer[i].stOutputBfr.hBitstreamBuffer = NULL;
        }
        else
        {
            m_pNvHWEncoder->NvEncDestroyBitstreamBuffer(m_stEncodeBuffer[i].stOutputBfr.hBitstreamBuffer);
            m_stEncodeBuffer[i].stOutputBfr.hBitstreamBuffer = NULL;
        }
    }
    return NV_ENC_SUCCESS;
}

NVENCSTATUS CNvEncoder::_FlushEncoder()
{
    NVENCSTATUS nvStatus = m_pNvHWEncoder->NvEncFlushEncoderQueue(m_stEOSOutputBfr.hOutputEvent);
    if (nvStatus != NV_ENC_SUCCESS)
    {
#ifdef _DEBUG		
		assert(0);
#else
#endif
        return nvStatus;
    }

    EncodeBuffer *pEncodeBufer = m_EncodeBufferQueue.GetPending();
    while (pEncodeBufer)
    {
		m_pNvHWEncoder->ProcessOutput(pEncodeBufer);
        pEncodeBufer = m_EncodeBufferQueue.GetPending();
    }

#if defined(NV_WINDOWS)
    if (WaitForSingleObject(m_stEOSOutputBfr.hOutputEvent, 500) != WAIT_OBJECT_0)
    {
#ifdef _DEBUG		
		assert(0);
#else
#endif
        nvStatus = NV_ENC_ERR_GENERIC;
    }
#endif

    return nvStatus;
}

NVENCSTATUS CNvEncoder::_Deinitialize(uint32_t devicetype)
{
    NVENCSTATUS nvStatus = NV_ENC_SUCCESS;

	_ReleaseIOBuffers();

    nvStatus = m_pNvHWEncoder->NvEncDestroyEncoder();

    if (m_pDevice)
    {
        switch (devicetype)
        {
        case NV_ENC_CUDA:
            CUresult cuResult = CUDA_SUCCESS;
            cuResult = cuCtxDestroy((CUcontext)m_pDevice);
            if (cuResult != CUDA_SUCCESS)
                PRINTERR("cuCtxDestroy error:0x%x\n", cuResult);
        }

        m_pDevice = NULL;
    }

	// 	for (int i = 0; i < 3; i++)
	// 	{
	// 		if (yuv[i])
	// 		{
	// 			delete[] yuv[i];
	// 		}
	// 	}
    return nvStatus;
}

NVENCSTATUS CNvEncoder::_EncodeFrame(EncodeFrameConfig *pEncodeFrame, bool bFlush, uint32_t width, uint32_t height)
{
    NVENCSTATUS nvStatus = NV_ENC_SUCCESS;
    uint32_t lockedPitch = 0;
    EncodeBuffer *pEncodeBuffer = NULL;

    if (bFlush)
    {
		_FlushEncoder();
        return NV_ENC_SUCCESS;
    }

    if (!pEncodeFrame)
    {
        return NV_ENC_ERR_INVALID_PARAM;
    }

    pEncodeBuffer = m_EncodeBufferQueue.GetAvailable();
    if(!pEncodeBuffer)
    {
		m_pNvHWEncoder->ProcessOutput(m_EncodeBufferQueue.GetPending());
        pEncodeBuffer = m_EncodeBufferQueue.GetAvailable();
    }

    unsigned char *pInputSurface;
    
    nvStatus = m_pNvHWEncoder->NvEncLockInputBuffer(pEncodeBuffer->stInputBfr.hInputSurface, (void**)&pInputSurface, &lockedPitch);
	if (nvStatus != NV_ENC_SUCCESS)
        return nvStatus;

    if (pEncodeBuffer->stInputBfr.bufferFmt == NV_ENC_BUFFER_FORMAT_NV12_PL)
    {
        unsigned char *pInputSurfaceCh = pInputSurface + (pEncodeBuffer->stInputBfr.dwHeight*lockedPitch);
        _convertYUVpitchtoNV12(pEncodeFrame->yuv[0], pEncodeFrame->yuv[1], pEncodeFrame->yuv[2], pInputSurface, pInputSurfaceCh, width, height, width, lockedPitch);
    }
    else
    {
        unsigned char *pInputSurfaceCb = pInputSurface + (pEncodeBuffer->stInputBfr.dwHeight * lockedPitch);
        unsigned char *pInputSurfaceCr = pInputSurfaceCb + (pEncodeBuffer->stInputBfr.dwHeight * lockedPitch);
		_convertYUVpitchtoYUV444(pEncodeFrame->yuv[0], pEncodeFrame->yuv[1], pEncodeFrame->yuv[2], pInputSurface, pInputSurfaceCb, pInputSurfaceCr, width, height, width, lockedPitch);
    }
    nvStatus = m_pNvHWEncoder->NvEncUnlockInputBuffer(pEncodeBuffer->stInputBfr.hInputSurface);
    if (nvStatus != NV_ENC_SUCCESS)
        return nvStatus;

    nvStatus = m_pNvHWEncoder->NvEncEncodeFrame(pEncodeBuffer, NULL, width, height, (NV_ENC_PIC_STRUCT)m_uPicStruct);
    return nvStatus;
}

NVENCSTATUS CNvEncoder::_EncodeFrame_UYVY(unsigned char *pUYVY, bool bFlush, uint32_t width, uint32_t height)
{
	NVENCSTATUS nvStatus = NV_ENC_SUCCESS;
	uint32_t lockedPitch = 0;
	EncodeBuffer *pEncodeBuffer = NULL;

	if (bFlush)
	{
		_FlushEncoder();
		return NV_ENC_SUCCESS;
	}

	pEncodeBuffer = m_EncodeBufferQueue.GetAvailable();
	if (!pEncodeBuffer)
	{
		m_pNvHWEncoder->ProcessOutput(m_EncodeBufferQueue.GetPending());
		pEncodeBuffer = m_EncodeBufferQueue.GetAvailable();
	}

	unsigned char *pInputSurface;

	nvStatus = m_pNvHWEncoder->NvEncLockInputBuffer(pEncodeBuffer->stInputBfr.hInputSurface, (void**)&pInputSurface, &lockedPitch);
	if (nvStatus != NV_ENC_SUCCESS)
		return nvStatus;
	unsigned char *pInputSurfaceCh = pInputSurface + (pEncodeBuffer->stInputBfr.dwHeight*lockedPitch);
	_ConvertUYVYpitchtoNV12(pUYVY, pInputSurface, pInputSurfaceCh, width, height, lockedPitch);
// 	if (pEncodeBuffer->stInputBfr.bufferFmt == NV_ENC_BUFFER_FORMAT_NV12_PL)
// 	{
// 		unsigned char *pInputSurfaceCh = pInputSurface + (pEncodeBuffer->stInputBfr.dwHeight*lockedPitch);
// 		_convertYUVpitchtoNV12(pEncodeFrame->yuv[0], pEncodeFrame->yuv[1], pEncodeFrame->yuv[2], pInputSurface, pInputSurfaceCh, width, height, width, lockedPitch);
// 	}
// 	else
// 	{
// 		unsigned char *pInputSurfaceCb = pInputSurface + (pEncodeBuffer->stInputBfr.dwHeight * lockedPitch);
// 		unsigned char *pInputSurfaceCr = pInputSurfaceCb + (pEncodeBuffer->stInputBfr.dwHeight * lockedPitch);
// 		_convertYUVpitchtoYUV444(pEncodeFrame->yuv[0], pEncodeFrame->yuv[1], pEncodeFrame->yuv[2], pInputSurface, pInputSurfaceCb, pInputSurfaceCr, width, height, width, lockedPitch);
// 	}
	nvStatus = m_pNvHWEncoder->NvEncUnlockInputBuffer(pEncodeBuffer->stInputBfr.hInputSurface);
	if (nvStatus != NV_ENC_SUCCESS)
		return nvStatus;

	nvStatus = m_pNvHWEncoder->NvEncEncodeFrame(pEncodeBuffer, NULL, width, height, (NV_ENC_PIC_STRUCT)m_uPicStruct);
	return nvStatus;
}


void CNvEncoder::_ConvertUYVYToYUV420P(BYTE *pBufferIn,uint32_t width, uint32_t height, uint32_t isYuv444)
{
	// 	if (isYuv444)
	// 	{
	// 		nvReadFile(hInputYUVFile, yuvInput[0], width * height, &numBytesRead, NULL);
	// 		nvReadFile(hInputYUVFile, yuvInput[1], width * height, &numBytesRead, NULL);
	// 		nvReadFile(hInputYUVFile, yuvInput[2], width * height, &numBytesRead, NULL);
	// 	}
	// 	else
	// 	{
	// 		unsigned char *pY = yuv[0];
	// 		unsigned char *pU = yuv[1];
	// 		unsigned char *pV = yuv[2];
	// 
	// 		for (int n = 0; n < width * height / 4; ++n)
	// 		{
	// 
	// 			*pY++ = pBufferIn[1];
	// 			*pY++ = pBufferIn[3];
	// 			*pU++ = (pBufferIn[0] + pBufferIn[4])/2;
	// 			*pV++ = (pBufferIn[2] + pBufferIn[6]) / 2;
	// 
	// 			*pY++ = pBufferIn[5];
	// 			*pY++ = pBufferIn[7];
	// 
	// 			pBufferIn += 8;
	// 		}
	// 	}
}

void CNvEncoder::_ConvertUYVYpitchtoNV12(BYTE *pBufferIn, unsigned char *nv12_luma, unsigned char *nv12_chroma, uint32_t width, uint32_t height, int dstStride)
{
	//dstStride = width;
	//UYVY_2_yuv420_pitch(pBufferIn,nv12_luma,nv12_chroma,width,height,dstStride);	
	return;
	BYTE*pYSrc = pBufferIn;

		for (int h=0;h<height;h++)
		{
			for (int w = 0,yw=0; w < width*2; yw+=2)
			{
				if (h % 2 == 0)
				{
					//U	pYSrc[0]
					nv12_chroma[dstStride*h/2 + yw] = pYSrc[h*width*2+2];
					//V	pYSrc[2]
					nv12_chroma[dstStride*h / 2 + yw + 1] = pYSrc[h*width * 2+w + 2];
				}

				nv12_luma[dstStride*h+ yw] = pYSrc[h*width * 2 + w+1];//Y
				nv12_luma[dstStride*h + yw + 1] = pYSrc[h*width * 2 + w + 3];//Y				
				w += 4;
			}
		}
}

void CNvEncoder::_convertYUVpitchtoNV12(unsigned char *yuv_luma, unsigned char *yuv_cb, unsigned char *yuv_cr,
	unsigned char *nv12_luma, unsigned char *nv12_chroma,
	int width, int height, int srcStride, int dstStride)
{
	int y;
	int x;
	if (srcStride == 0)
		srcStride = width;
	if (dstStride == 0)
		dstStride = width;

	for (y = 0; y < height; y++)
	{
		memcpy(nv12_luma + (dstStride*y), yuv_luma + (srcStride*y), width);
	}

	for (y = 0; y < height / 2; y++)
	{
		for (x = 0; x < width; x = x + 2)
		{
			nv12_chroma[(y*dstStride) + x] = yuv_cb[((srcStride / 2)*y) + (x >> 1)];
			nv12_chroma[(y*dstStride) + (x + 1)] = yuv_cr[((srcStride / 2)*y) + (x >> 1)];
		}
	}
}

void CNvEncoder::_convertYUVpitchtoYUV444(unsigned char *yuv_luma, unsigned char *yuv_cb, unsigned char *yuv_cr,
	unsigned char *surf_luma, unsigned char *surf_cb, unsigned char *surf_cr, int width, int height, int srcStride, int dstStride)
{
	int h;

	for (h = 0; h < height; h++)
	{
		memcpy(surf_luma + dstStride * h, yuv_luma + srcStride * h, width);
		memcpy(surf_cb + dstStride * h, yuv_cb + srcStride * h, width);
		memcpy(surf_cr + dstStride * h, yuv_cr + srcStride * h, width);
	}
}

