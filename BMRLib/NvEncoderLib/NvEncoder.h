#include "NvHWEncoder.h"

#define MAX_ENCODE_QUEUE 32
#define FRAME_QUEUE 240

#define SET_VER(configStruct, type) {configStruct.version = type##_VER;}

template<class T>
class CNvQueue {
    T** m_pBuffer;
    unsigned int m_uSize;
    unsigned int m_uPendingCount;
    unsigned int m_uAvailableIdx;
    unsigned int m_uPendingndex;
public:
    CNvQueue(): m_pBuffer(NULL), m_uSize(0), m_uPendingCount(0), m_uAvailableIdx(0),
                m_uPendingndex(0)
    {
    }

    ~CNvQueue()
    {
        delete[] m_pBuffer;
    }

    bool Initialize(T *pItems, unsigned int uSize)
    {
        m_uSize = uSize;
        m_uPendingCount = 0;
        m_uAvailableIdx = 0;
        m_uPendingndex = 0;
        m_pBuffer = new T *[m_uSize];
        for (unsigned int i = 0; i < m_uSize; i++)
        {
            m_pBuffer[i] = &pItems[i];
        }
        return true;
    }


    T * GetAvailable()
    {
        T *pItem = NULL;
        if (m_uPendingCount == m_uSize)
        {
            return NULL;
        }
        pItem = m_pBuffer[m_uAvailableIdx];
        m_uAvailableIdx = (m_uAvailableIdx+1)%m_uSize;
        m_uPendingCount += 1;
        return pItem;
    }

    T* GetPending()
    {
        if (m_uPendingCount == 0) 
        {
            return NULL;
        }

        T *pItem = m_pBuffer[m_uPendingndex];
        m_uPendingndex = (m_uPendingndex+1)%m_uSize;
        m_uPendingCount -= 1;
        return pItem;
    }
};

typedef struct _EncodeFrameConfig
{
    uint8_t  *yuv[3];
    uint32_t stride[3];
    uint32_t width;
    uint32_t height;
}EncodeFrameConfig;

struct DataPacket
{
	LPBYTE lpPacket;
	UINT size;
};

#define NV_ENC_CUDA 2

class CNvEncoder
{
public:
    CNvEncoder();
    virtual ~CNvEncoder();
	NVENCSTATUS				 Initialize(EncodeConfig &encodeConfig, INvCallBack* pWriteCallBack,char* szCardName);
	NVENCSTATUS              EncodeFrame(BYTE *pBufferYUV420p);
	NVENCSTATUS              EncodeFrame_UYVY(BYTE *pBufferUYVY);
	NVENCSTATUS				 GetHeaders(uint32_t &sps_size, BYTE * sps, uint32_t &pps_size, BYTE * pps, FILE * fpVideoHdr);
	NVENCSTATUS				 UnInitialize();

protected:
    CNvHWEncoder           *m_pNvHWEncoder;
    uint32_t                m_uEncodeBufferCount;
    uint32_t                m_uPicStruct;
    void*                   m_pDevice;
    CUcontext               m_cuContext;
    EncodeBuffer            m_stEncodeBuffer[MAX_ENCODE_QUEUE];
    CNvQueue<EncodeBuffer>  m_EncodeBufferQueue;
    EncodeOutputBuffer      m_stEOSOutputBfr; 
	EncodeConfig			m_encodeConfig;
	uint8_t *yuv[3];
protected:
    NVENCSTATUS             _Deinitialize(uint32_t devicetype);
	NVENCSTATUS             _InitCuda(uint32_t deviceID, char *szCardName);
	NVENCSTATUS             _AllocateIOBuffers(uint32_t uInputWidth, uint32_t uInputHeight, uint32_t isYuv444);
	NVENCSTATUS             _EncodeFrame(EncodeFrameConfig *pEncodeFrame, bool bFlush = false, uint32_t width = 0, uint32_t height = 0);
	NVENCSTATUS             _EncodeFrame_UYVY(unsigned char *pUYVY, bool bFlush = false, uint32_t width = 0, uint32_t height = 0);
	NVENCSTATUS             _ReleaseIOBuffers();
	NVENCSTATUS             _FlushEncoder();
	void					_ConvertUYVYpitchtoNV12(BYTE *pBufferIn, 
		unsigned char *nv12_luma, unsigned char *nv12_chroma, uint32_t width, uint32_t height, int dstStride);
	void					_ConvertUYVYToYUV420P(BYTE *pBufferIn, uint32_t width, uint32_t height, uint32_t isYuv444);
	void _convertYUVpitchtoNV12(unsigned char *yuv_luma, unsigned char *yuv_cb, unsigned char *yuv_cr,
		unsigned char *nv12_luma, unsigned char *nv12_chroma,
		int width, int height, int srcStride, int dstStride);
	void _convertYUVpitchtoYUV444(unsigned char *yuv_luma, unsigned char *yuv_cb, unsigned char *yuv_cr,
		unsigned char *surf_luma, unsigned char *surf_cb, unsigned char *surf_cr, int width, int height, int srcStride, int dstStride);
};

// NVEncodeAPI entry point
typedef NVENCSTATUS (NVENCAPI *MYPROC)(NV_ENCODE_API_FUNCTION_LIST*); 
