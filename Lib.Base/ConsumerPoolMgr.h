#pragma once
#include "audioFrame.h"
#include "videoFrame.h"
#include "ObjectPoolEx.h"
#include "../Lib.Base/BMRServiceDefine.h"


class ConsumerPoolMgr
{
	ConsumerPoolMgr();
	~ConsumerPoolMgr();
	ObjectPoolEx<VideoFrame>                m_captureVideoPool4K[NB_BMR_RECORDER];
	ObjectPoolEx<VideoFrame>                m_captureVideoPoolHD[NB_BMR_RECORDER];
	ObjectPoolEx<VideoFrame>                m_captureVideoPool720[NB_BMR_RECORDER];
	ObjectPoolEx<AudioFrame>                m_captureAudioPool[NB_BMR_RECORDER];
public:
	static ConsumerPoolMgr * GetInstance();
	void initialize(ChannelMask channelMask, FPTVideoFormat _fpFormat, int _size);
	void getNew(int _cnl, VideoFrame* & _elem, FPTVideoFormat fpFormat);
	void getNew(int _cnl, AudioFrame* & _elem);
	void release(int _cnl, VideoFrame* _elem);
	void release(int _cnl, AudioFrame* _elem);
	uint32_t getVideoPoolSize(int _cnl, FPTVideoFormat _fpFormat);
	uint32_t getAudioPoolSize(int _cnl);
};
