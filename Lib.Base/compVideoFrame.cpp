#include "CompVideoFrame.h"

CompVideoFrame::CompVideoFrame()
{
	m_totalSize = sizeof(header);
	m_frame = nullptr;
	m_nFrameID = 0;
	m_tag = 0;
	m_nTimeStamp = 0;
}

CompVideoFrame::~CompVideoFrame()
{
	if (m_frame != nullptr)
		_aligned_free(m_frame);
}

unsigned char* CompVideoFrame::getRaw() const
{
	return &m_frame[sizeof(header)];
}

unsigned long CompVideoFrame::getRawSize() const
{
	if (m_frame)
	{
		header * s = (header *)m_frame;
		return s->rawSize;
	}
	return 0;
}

unsigned char* CompVideoFrame::getRawWithSize() const
{
	return &m_frame[sizeof(header) - sizeof(unsigned long)];
}

unsigned char* CompVideoFrame::getRawWithHeader() const
{
	return m_frame;
}

unsigned long CompVideoFrame::getRawWithHeaderSize() const
{
	if (m_frame)
	{
		header * s = (header *)m_frame;
		return s->rawSize + sizeof(header);
	}
	return 0;
}

int CompVideoFrame::setBufferSize(unsigned long _rawSize)
{
	if (m_totalSize - sizeof(header) < _rawSize
		|| (m_totalSize - sizeof(header) > _rawSize && m_totalSize > 4 * 1024 * 1024))
	{
		if (m_frame != nullptr)
		{
			_aligned_free(m_frame);
		}
		m_totalSize = _rawSize + sizeof(header);
		m_frame = (unsigned char *)_aligned_malloc(m_totalSize, 4096);
		if (m_frame == nullptr)
		{
			m_totalSize = sizeof(header);
			return -1;
		}
	}
	header * s = (header *)m_frame;
	if (s)
		s->rawSize = _rawSize;
	return 0;
}

void CompVideoFrame::SetFrameID(UINT64  nFrameID)
{
	m_nFrameID = nFrameID;
}

UINT64 CompVideoFrame::GetFrameID() const
{
	return m_nFrameID;
}

void CompVideoFrame::setTag(UINT64 _tag)
{
	m_tag = _tag;
}

UINT64 CompVideoFrame::getTag() const
{
	return  m_tag;
}

void CompVideoFrame::setTC(const Timecode& _tc)
{
	if (m_frame)
	{
		header * s = (header *)m_frame;
		s->tc = _tc;
	}
}

Timecode CompVideoFrame::getTC() const
{
	if (m_frame)
	{
		header * s = (header *)m_frame;
		return s->tc;
	}
	return Timecode::make_tc(0, 0, 0, 0);
}

void CompVideoFrame::setTimeStamp(double lTimeStamp)
{
	m_nTimeStamp = lTimeStamp;
}

double CompVideoFrame::getTimeStamp() const
{
	return m_nTimeStamp;
}

void CompVideoFrame::setIsIDR(bool bIsIDRFrame)
{
	if (m_frame)
	{
		header * s = (header *)m_frame;
		s->m_bIsIDRFrame = bIsIDRFrame;
	}
}

bool CompVideoFrame::getIsIDR() const
{
	if (m_frame)
	{
		header * s = (header *)m_frame;
		return s->m_bIsIDRFrame;
	}
	return false;
}

void CompVideoFrame::setQscale(int _qScale)
{
	if (m_frame)
	{
		header * s = (header *)m_frame;
		s->qScale = _qScale;
	}
}

int  CompVideoFrame::getQscale()
{
	if (m_frame)
	{
		header * s = (header *)m_frame;
		return s->qScale;
	}
	return 0;
}


void CompVideoFrame::setFieldSize(int _field0, int _field1)
{
	if (m_frame)
	{
		header * s = (header *)m_frame;
		s->fieldSize[0] = _field0;
		s->fieldSize[1] = _field1;
	}
}

int  CompVideoFrame::getFieldSize(int _idx)
{
	if (m_frame)
	{
		header * s = (header *)m_frame;
		return s->fieldSize[_idx];
	}
	return 0;
}
