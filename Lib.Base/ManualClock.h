#pragma once
#include "eventClock.h"
#include "IMasterClock.h"

class ManualClock : public IMasterClock
{
public:
	ManualClock();
	~ManualClock();
	void start(int _frameRate) override;
	void setEventClock() override;
	bool waitEventClock() override;
	void stop() override;

protected:
	void callBack() override;
	EventClock m_event;
};
