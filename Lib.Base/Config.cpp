#include "Config.h"
#include "locker.h"
#include <Shlwapi.h>

using namespace std;
static Config *mSInstance = nullptr;
static Locker			g_lock;

Config::Config()
{
	load();
}

Config::~Config()
{
}

Config * Config::getInstance()
{
	LockHolder lock(g_lock);
	if (mSInstance == nullptr)
	{
		mSInstance = new Config();
	}
	return mSInstance;
}

void Config::releaseInstance()
{
	if (mSInstance)
	{
		delete mSInstance;
		mSInstance = nullptr;
	}
}

FPTVideoFormat Config::getVideoFormat() const
{
	return m_videoFormat;
}

int Config::getFrameRateNum() const
{
	switch (m_videoFormat)
	{
	case FP_FORMAT_1080i_5000: return 25000;
	case FP_FORMAT_1080i_5994: return 30000;
	case FP_FORMAT_1080i_6000: return 30000;
	case FP_FORMAT_1080p_2400: return 24000;
	case FP_FORMAT_1080p_2500: return 25000;
	case FP_FORMAT_1080p_2997: return 30000;
	case FP_FORMAT_1080p_3000: return 30000;
	case FP_FORMAT_1080p_5000: return 50000;
	case FP_FORMAT_1080p_5994: return 60000;
	case FP_FORMAT_1080p_6000: return 60000;
	case FP_FORMAT_720p_2500: return 25000;
	case FP_FORMAT_720p_2997: return 30000;
	case FP_FORMAT_720p_5000: return 50000;
	case FP_FORMAT_720p_5994: return 60000;
	case FP_FORMAT_720p_6000: return 60000;
	case FP_FORMAT_4Kp_2400: return 24000;
	case FP_FORMAT_4Kp_2500: return 25000;
	case FP_FORMAT_4Kp_2997: return 30000;
	case FP_FORMAT_4Kp_3000: return 30000;
	case FP_FORMAT_4Kp_5000: return 50000;
	case FP_FORMAT_4Kp_5994: return 60000;
	case FP_FORMAT_4Kp_6000: return 60000;
	case FP_FORMAT_UNKNOWN:
	default: return 25000;
	}
}

int Config::getFrameRateDen() const
{
	switch (m_videoFormat)
	{
	case FP_FORMAT_1080i_5000: return 1000;
	case FP_FORMAT_1080i_5994: return 1001;
	case FP_FORMAT_1080i_6000: return 1000;
	case FP_FORMAT_1080p_2400: return 1000;
	case FP_FORMAT_1080p_2500: return 1000;
	case FP_FORMAT_1080p_2997: return 1001;
	case FP_FORMAT_1080p_3000: return 1000;
	case FP_FORMAT_1080p_5000: return 1000;
	case FP_FORMAT_1080p_5994: return 1001;
	case FP_FORMAT_1080p_6000: return 1000;
	case FP_FORMAT_720p_2500: return 1000;
	case FP_FORMAT_720p_2997: return 1001;
	case FP_FORMAT_720p_5000: return 1000;
	case FP_FORMAT_720p_5994: return 1001;
	case FP_FORMAT_720p_6000: return 1000;
	case FP_FORMAT_4Kp_2400: return 1000;
	case FP_FORMAT_4Kp_2500: return 1000;
	case FP_FORMAT_4Kp_2997: return 1001;
	case FP_FORMAT_4Kp_3000: return 1000;
	case FP_FORMAT_4Kp_5000: return 1000;
	case FP_FORMAT_4Kp_5994: return 1001;
	case FP_FORMAT_4Kp_6000: return 1000;
	case FP_FORMAT_UNKNOWN:
	default: return 1000;
	}
}


int Config::getFramesPerSec() const
{
	switch (m_videoFormat)
	{
	case FP_FORMAT_1080i_5000: return 25;
	case FP_FORMAT_1080i_5994: return 30;
	case FP_FORMAT_1080i_6000: return 30;
	case FP_FORMAT_1080p_2400: return 24;
	case FP_FORMAT_1080p_2500: return 25;
	case FP_FORMAT_1080p_2997: return 30;
	case FP_FORMAT_1080p_3000: return 30;
	case FP_FORMAT_1080p_5000: return 50;
	case FP_FORMAT_1080p_5994: return 60;
	case FP_FORMAT_1080p_6000: return 60;
	case FP_FORMAT_720p_2500: return 25;
	case FP_FORMAT_720p_2997: return 30;
	case FP_FORMAT_720p_5000: return 50;
	case FP_FORMAT_720p_5994: return 60;
	case FP_FORMAT_720p_6000: return 60;
	case FP_FORMAT_4Kp_2400: return 24;
	case FP_FORMAT_4Kp_2500: return 25;
	case FP_FORMAT_4Kp_2997: return 30;
	case FP_FORMAT_4Kp_3000: return 30;
	case FP_FORMAT_4Kp_5000: return 50;
	case FP_FORMAT_4Kp_5994: return 60;
	case FP_FORMAT_4Kp_6000: return 60;
	case FP_FORMAT_UNKNOWN:
	default: return 25;
	}
}


float Config::getInterval() const
{
	switch (m_videoFormat)
	{
	case FP_FORMAT_1080i_5000: return 40;
	case FP_FORMAT_1080i_5994: return 33.37;
	case FP_FORMAT_1080i_6000: return 33.33;
	case FP_FORMAT_1080p_2400: return 41.67;
	case FP_FORMAT_1080p_2500: return 40;
	case FP_FORMAT_1080p_2997: return 33.37;
	case FP_FORMAT_1080p_3000: return 33.33;
	case FP_FORMAT_1080p_5000: return 20;
	case FP_FORMAT_1080p_5994: return 16.68;
	case FP_FORMAT_1080p_6000: return 16.67;
	case FP_FORMAT_720p_2500: return 40;
	case FP_FORMAT_720p_2997: return 33.37;
	case FP_FORMAT_720p_5000: return 20;
	case FP_FORMAT_720p_5994: return 16.68;
	case FP_FORMAT_720p_6000: return 16.67;
	case FP_FORMAT_4Kp_2400: return 41.67;
	case FP_FORMAT_4Kp_2500: return 40;
	case FP_FORMAT_4Kp_2997: return 33.37;
	case FP_FORMAT_4Kp_3000: return 33.33;
	case FP_FORMAT_4Kp_5000: return 20;
	case FP_FORMAT_4Kp_5994: return 16.68;
	case FP_FORMAT_4Kp_6000: return 16.67;
	case FP_FORMAT_UNKNOWN:
	default: return 40;
	}
}
bool Config::is4K() const
{
	return (m_videoFormat >= FP_FORMAT_4Kp_2500 && m_videoFormat <= FP_FORMAT_4Kp_6000) || (FP_FORMAT_4Kp_2400 == m_videoFormat);
}

bool Config::isProgressive() const
{
	return !(m_videoFormat == FP_FORMAT_1080i_5994 || m_videoFormat == FP_FORMAT_1080i_6000 || m_videoFormat == FP_FORMAT_1080i_5000);
}

int Config::getOutTypeIsAja() const
{
	return m_outType == 0;
}

std::string Config::getInputAddress(unsigned int nIndex) const
{
	if (nIndex >= MAX_PAYER)
		return "";
	return m_camInputAddress[nIndex];
}

std::string	Config::getOutAddress(unsigned int nIndex) const
{
	if (nIndex >= MAX_PAYER)
		return "";
	return m_camOutAddress[nIndex];
}

ChannelMask Config::getPGMChannelMask() const
{
	return m_PGMChannelMask;
}

ChannelMask Config::getRecChannelMask() const
{
	return m_recChannelMask;
}

bool Config::is720p() const
{
	return m_videoFormat >= FP_FORMAT_720p_2500 && m_videoFormat <= FP_FORMAT_720p_6000;
}

bool Config::isHD() const
{
	return (m_videoFormat >= FP_FORMAT_1080i_5000 && m_videoFormat <= FP_FORMAT_1080p_6000) || (FP_FORMAT_1080p_2400 == m_videoFormat);
}

bool Config::isUsingGpuDecode() const
{
	return m_isUsingGpuDecode;
}

void Config::load()
{
	CHAR strModuleFileName[MAX_PATH] = {};
	GetModuleFileNameA(GetModuleHandleA("SimplyLiveRTMPServer_DLG.exe"), strModuleFileName, MAX_PATH);
	PathRemoveFileSpecA(strModuleFileName);
	CHAR szConfigFile[MAX_PATH];
	sprintf_s(szConfigFile, MAX_PATH, "%s\\Config.ini", strModuleFileName);
	m_videoFormat = FPTVideoFormat(GetPrivateProfileIntA("GENERAL_CONFIG", "Video_Format", FP_FORMAT_1080i_5000, szConfigFile));
	if (m_videoFormat >= FP_FORMAT_MAX || m_videoFormat <= FP_FORMAT_UNKNOWN)
		m_videoFormat = FP_FORMAT_1080i_5000;	
	m_outType = GetPrivateProfileIntA("CH_CONFIG_OUT", "Type", 0, szConfigFile);

	CHAR szCameraName[MAX_PATH];
	CHAR szCHName[MAX_PATH];
	CHAR strValue[MAX_PATH];
	string strTmep;
	for (int n = 0; n < MAX_PAYER; ++n)
	{
		sprintf_s(szCameraName, MAX_PATH, "Cam%c", 'A' + n);
		sprintf_s(szCHName, "CH_%d", n);
		GetPrivateProfileStringA("CH_CONFIG_IN", szCHName, szCameraName, strValue, MAX_PATH, szConfigFile);
		strTmep = strValue;
		if (strTmep.empty())
			continue;
		m_recChannelMask.set(n);
		m_camInputAddress[n] = strTmep;
	}
	if (m_outType == 1)//RTMP
	{
		for (int n = 0; n < MAX_PAYER; ++n)
		{
			sprintf_s(szCHName, "CH_%d", n);
			GetPrivateProfileStringA("CH_CONFIG_OUT", szCHName, "", strValue, MAX_PATH, szConfigFile);
			strTmep = strValue;
			if (strTmep.empty())
				continue;
			m_PGMChannelMask.set(n);
			m_camOutAddress[n] = strTmep;
		}
	}
	else
		m_PGMChannelMask = m_recChannelMask;

	m_isUsingGpuDecode = GetPrivateProfileIntA("DEC_TYPE", "USE_GPU_DEC", 0, szConfigFile) == 0?false:true;
}

int Config::getVideoWidth() const
{
	if ((m_videoFormat >= FP_FORMAT_1080i_5000 && m_videoFormat <= FP_FORMAT_1080p_6000) || (FP_FORMAT_1080p_2400 == m_videoFormat))
		return 1920;
	if (m_videoFormat >= FP_FORMAT_720p_2500 && m_videoFormat <= FP_FORMAT_720p_6000)
		return 1280;
	if ((m_videoFormat >= FP_FORMAT_4Kp_2500 && m_videoFormat <= FP_FORMAT_4Kp_6000) || (FP_FORMAT_4Kp_2400 == m_videoFormat))
		return 3840;
	return 0;
}

int Config::getVideoHeight() const
{
	if ((m_videoFormat >= FP_FORMAT_1080i_5000 && m_videoFormat <= FP_FORMAT_1080p_6000) || (FP_FORMAT_1080p_2400 == m_videoFormat))
		return 1080;
	if (m_videoFormat >= FP_FORMAT_720p_2500 && m_videoFormat <= FP_FORMAT_720p_6000)
		return 720;
	if ((m_videoFormat >= FP_FORMAT_4Kp_2500 && m_videoFormat <= FP_FORMAT_4Kp_6000) || (FP_FORMAT_4Kp_2400 == m_videoFormat))
		return 2160;
	return 0;
}
