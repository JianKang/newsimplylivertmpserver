#pragma once

#include <windows.h>

typedef UINT64 cpuFreq;
typedef UINT64 cpuTick;

void getCPUfreq(cpuFreq& _freq);
void getTickCount(cpuTick& _tick);

