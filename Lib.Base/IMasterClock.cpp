#include "IMasterClock.h"
#include "AutoClock.h"
#include "ManualClock.h"

IMasterClock::IMasterClock()
{
}

IMasterClock* IMasterClock::CreateInstance(bool bManualClock)
{
	if (bManualClock)
		return new ManualClock();
	return new AutoClock();
}

void IMasterClock::ReleaseInstance(IMasterClock*& pInstance)
{
	if (pInstance)
	{
		delete pInstance;
		pInstance = nullptr;
	}
}