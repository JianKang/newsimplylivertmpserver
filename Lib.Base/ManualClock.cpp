#include "ManualClock.h"
ManualClock::ManualClock()
{
}

ManualClock::~ManualClock()
{
}

void ManualClock::start(int _frameRate)
{
	startThread();
}

void ManualClock::setEventClock()
{
	m_event.raiseEvent();
}

bool ManualClock::waitEventClock()
{
	return m_event.waitEvent(100);
}

void ManualClock::stop()
{
	stopThread(1000);
}

void ManualClock::callBack()
{
	while (isRunning())
	{
		//InterruptManager::WaitGenlockInterrupt();
		setEventClock();
	}
}