#include "ConsumerPoolMgr.h"
#include "locker.h"
#include "VFrame_1280_720.h"
#include "VFrame_1920_1080.h"
#include "VFrame_3840_2160.h"

using namespace std;

ConsumerPoolMgr::ConsumerPoolMgr()
{
}

ConsumerPoolMgr::~ConsumerPoolMgr()
{
}

ConsumerPoolMgr * ConsumerPoolMgr::GetInstance()
{
	static ConsumerPoolMgr *gSInstance = nullptr;
	static Locker g_lock;
	LockHolder lock(g_lock);
	if (gSInstance == nullptr)
	{
		gSInstance = new ConsumerPoolMgr();
	}
	return gSInstance;
}

void ConsumerPoolMgr::initialize(ChannelMask channelMask, FPTVideoFormat _fpFormat, int _size)
{
	switch (_fpFormat)
	{
	case FP_FORMAT_1080i_5000:
	case FP_FORMAT_1080i_5994:
	case FP_FORMAT_1080i_6000:
	case FP_FORMAT_1080p_2400:
	case FP_FORMAT_1080p_2500:
	case FP_FORMAT_1080p_2997:
	case FP_FORMAT_1080p_3000:
	case FP_FORMAT_1080p_5000:
	case FP_FORMAT_1080p_5994:
	case FP_FORMAT_1080p_6000:
	{
		for (int _cnl = 0; _cnl < NB_BMR_RECORDER; ++_cnl)
		{
			if (channelMask[_cnl])
				m_captureVideoPoolHD[_cnl].initialize<VFrame_1920_1080>(_size);
		}
		break;
	}
	case FP_FORMAT_4Kp_3000:
	case FP_FORMAT_4Kp_5000:
	case FP_FORMAT_4Kp_6000:
	case FP_FORMAT_4Kp_2400:
	case FP_FORMAT_4Kp_2500:
	case FP_FORMAT_4Kp_2997:
	case FP_FORMAT_4Kp_5994:
	{
		for (int _cnl = 0; _cnl < NB_BMR_RECORDER; ++_cnl)
		{
			if (channelMask[_cnl])
				m_captureVideoPool4K[_cnl].initialize<VFrame_3840_2160>(_size);
		}
		break;
	}
	case FP_FORMAT_720p_5000:
	case FP_FORMAT_720p_5994:
	case FP_FORMAT_720p_2500:
	case FP_FORMAT_720p_2997:
	case FP_FORMAT_720p_6000:
	{
		for (int _cnl = 0; _cnl < NB_BMR_RECORDER; ++_cnl)
		{
			if (channelMask[_cnl])
				m_captureVideoPool720[_cnl].initialize<VFrame_1280_720>(_size);
		}
		break;
	}
	case FP_FORMAT_UNKNOWN:
	case FP_FORMAT_MAX:
	default:
		break;
	}
	for (int _cnl = 0; _cnl < NB_BMR_RECORDER; ++_cnl)
	{
		if (channelMask[_cnl])
			m_captureAudioPool[_cnl].initialize<AudioFrame>(_size);
	}
}

void ConsumerPoolMgr::getNew(int _cnl, VideoFrame* & _elem, FPTVideoFormat fpFormat)
{
	switch (fpFormat)
	{
	case FP_FORMAT_1080i_5000:
	case FP_FORMAT_1080i_5994:
	case FP_FORMAT_1080i_6000:
	case FP_FORMAT_1080p_2400:
	case FP_FORMAT_1080p_2500:
	case FP_FORMAT_1080p_2997:
	case FP_FORMAT_1080p_3000:
	case FP_FORMAT_1080p_5000:
	case FP_FORMAT_1080p_5994:
	case FP_FORMAT_1080p_6000:
		m_captureVideoPoolHD[_cnl].getNew(_elem);
		break;
	case FP_FORMAT_4Kp_3000:
	case FP_FORMAT_4Kp_5000:
	case FP_FORMAT_4Kp_6000:
	case FP_FORMAT_4Kp_2400:
	case FP_FORMAT_4Kp_2500:
	case FP_FORMAT_4Kp_2997:
	case FP_FORMAT_4Kp_5994:
		m_captureVideoPool4K[_cnl].getNew(_elem);
		break;
	case FP_FORMAT_720p_5000:
	case FP_FORMAT_720p_5994:
	case FP_FORMAT_720p_2500:
	case FP_FORMAT_720p_2997:
	case FP_FORMAT_720p_6000:
		m_captureVideoPool720[_cnl].getNew(_elem);
		break;
	case FP_FORMAT_UNKNOWN:
	case FP_FORMAT_MAX:
	default:
		_elem = nullptr;
		break;
	}
	if (_elem)
		_elem->setFormat(fpFormat);
}

void ConsumerPoolMgr::getNew(int _cnl, AudioFrame* & _elem)
{
	m_captureAudioPool[_cnl].getNew(_elem);
}

void ConsumerPoolMgr::release(int _cnl, AudioFrame* _elem)
{
	if (_elem != nullptr)
		m_captureAudioPool[_cnl].release(_elem);
}

void ConsumerPoolMgr::release(int _cnl, VideoFrame* _elem)
{
	if (_elem == nullptr)
		return;
	switch (_elem->getFormat())
	{
	case FP_FORMAT_1080i_5000:
	case FP_FORMAT_1080i_5994:
	case FP_FORMAT_1080i_6000:
	case FP_FORMAT_1080p_2400:
	case FP_FORMAT_1080p_2500:
	case FP_FORMAT_1080p_2997:
	case FP_FORMAT_1080p_3000:
	case FP_FORMAT_1080p_5000:
	case FP_FORMAT_1080p_5994:
	case FP_FORMAT_1080p_6000:
		m_captureVideoPoolHD[_cnl].release(_elem);
		break;
	case FP_FORMAT_4Kp_3000:
	case FP_FORMAT_4Kp_5000:
	case FP_FORMAT_4Kp_6000:
	case FP_FORMAT_4Kp_2400:
	case FP_FORMAT_4Kp_2500:
	case FP_FORMAT_4Kp_2997:
	case FP_FORMAT_4Kp_5994:
		m_captureVideoPool4K[_cnl].release(_elem);
		break;
	case FP_FORMAT_720p_5000:
	case FP_FORMAT_720p_5994:
	case FP_FORMAT_720p_2500:
	case FP_FORMAT_720p_2997:
	case FP_FORMAT_720p_6000:
		m_captureVideoPool720[_cnl].release(_elem);
		break;
	case FP_FORMAT_UNKNOWN:
	case FP_FORMAT_MAX:
	default:
		break;
	}
}

uint32_t ConsumerPoolMgr::getVideoPoolSize(int _cnl, FPTVideoFormat _fpFormat)
{
	switch (_fpFormat)
	{
	case FP_FORMAT_1080p_2400:
	case FP_FORMAT_1080p_2500:
	case FP_FORMAT_1080p_2997:
	case FP_FORMAT_1080p_3000:
	case FP_FORMAT_1080p_5000:
	case FP_FORMAT_1080p_5994:
	case FP_FORMAT_1080p_6000:
	case FP_FORMAT_1080i_5000:
	case FP_FORMAT_1080i_5994:
	case FP_FORMAT_1080i_6000:
		return m_captureVideoPoolHD[_cnl].size();
	case FP_FORMAT_4Kp_3000:
	case FP_FORMAT_4Kp_5000:
	case FP_FORMAT_4Kp_6000:
	case FP_FORMAT_4Kp_2400:
	case FP_FORMAT_4Kp_2500:
	case FP_FORMAT_4Kp_2997:
	case FP_FORMAT_4Kp_5994:
		return m_captureVideoPool4K[_cnl].size();
	case FP_FORMAT_720p_5000:
	case FP_FORMAT_720p_5994:
	case FP_FORMAT_720p_2500:
	case FP_FORMAT_720p_2997:
	case FP_FORMAT_720p_6000:
		return m_captureVideoPool720[_cnl].size();
	case FP_FORMAT_UNKNOWN:
	default:
		return 0;
	}
}

uint32_t ConsumerPoolMgr::getAudioPoolSize(int _cnl)
{
	return m_captureAudioPool[_cnl].size();
}