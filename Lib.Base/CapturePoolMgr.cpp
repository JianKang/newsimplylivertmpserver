#include "CapturePoolMgr.h"
#include "locker.h"
#include "VFrame_1280_720.h"
#include "VFrame_1920_1080.h"
#include "VFrame_3840_2160.h"

using namespace std;

CapturePoolMgr::CapturePoolMgr()
{
}

CapturePoolMgr::~CapturePoolMgr()
{
}

CapturePoolMgr * CapturePoolMgr::GetInstance()
{
	static CapturePoolMgr *gSInstance = nullptr;
	static Locker g_lock;
	LockHolder lock(g_lock);
	if (gSInstance == nullptr)
	{
		gSInstance = new CapturePoolMgr();
	}
	return gSInstance;
}

void CapturePoolMgr::initialize(FPTVideoFormat _fpFormat, int _size)
{
	switch (_fpFormat)
	{
	case FP_FORMAT_1080i_5000:
	case FP_FORMAT_1080i_5994:
	case FP_FORMAT_1080i_6000:
	case FP_FORMAT_1080p_2400:
	case FP_FORMAT_1080p_2500:
	case FP_FORMAT_1080p_2997:
	case FP_FORMAT_1080p_3000:
	case FP_FORMAT_1080p_5000:
	case FP_FORMAT_1080p_5994:
	case FP_FORMAT_1080p_6000:
	{
		m_captureVideoPoolHD.initialize<VFrame_1920_1080>(_size);
		break;
	}
	case FP_FORMAT_4Kp_3000:
	case FP_FORMAT_4Kp_5000:
	case FP_FORMAT_4Kp_6000:
	case FP_FORMAT_4Kp_2400:
	case FP_FORMAT_4Kp_2500:
	case FP_FORMAT_4Kp_2997:
	case FP_FORMAT_4Kp_5994:
		m_captureVideoPool4K.initialize<VFrame_3840_2160>(_size);
		break;
	case FP_FORMAT_720p_5000:
	case FP_FORMAT_720p_5994:
	case FP_FORMAT_720p_2500:
	case FP_FORMAT_720p_2997:
	case FP_FORMAT_720p_6000:
		m_captureVideoPool720.initialize<VFrame_1280_720>(_size);
		break;
	case FP_FORMAT_UNKNOWN:
	case FP_FORMAT_MAX:
	default:
		break;
	}
	m_captureAudioPool.initialize<AudioFrame>(2*_size);
}

void CapturePoolMgr::getNew(VideoFrame* & _elem, FPTVideoFormat fpFormat)
{
	switch (fpFormat)
	{
	case FP_FORMAT_1080i_5000:
	case FP_FORMAT_1080i_5994:
	case FP_FORMAT_1080i_6000:
	case FP_FORMAT_1080p_2400:
	case FP_FORMAT_1080p_2500:
	case FP_FORMAT_1080p_2997:
	case FP_FORMAT_1080p_3000:
	case FP_FORMAT_1080p_5000:
	case FP_FORMAT_1080p_5994:
	case FP_FORMAT_1080p_6000:
		m_captureVideoPoolHD.getNew(_elem);
		break;
	case FP_FORMAT_4Kp_3000:
	case FP_FORMAT_4Kp_5000:
	case FP_FORMAT_4Kp_6000:
	case FP_FORMAT_4Kp_2400:
	case FP_FORMAT_4Kp_2500:
	case FP_FORMAT_4Kp_2997:
	case FP_FORMAT_4Kp_5994:
		m_captureVideoPool4K.getNew(_elem);
		break;
	case FP_FORMAT_720p_5000:
	case FP_FORMAT_720p_5994:
	case FP_FORMAT_720p_2500:
	case FP_FORMAT_720p_2997:
	case FP_FORMAT_720p_6000:
		m_captureVideoPool720.getNew(_elem);
		break;
	case FP_FORMAT_UNKNOWN:
	case FP_FORMAT_MAX:
	default:
		_elem = nullptr;
		break;
	}
	if (_elem)
		_elem->setFormat(fpFormat);
}

void CapturePoolMgr::getNew(AudioFrame* & _elem)
{
	m_captureAudioPool.getNew(_elem);
}

void CapturePoolMgr::release(AudioFrame* _elem)
{
	//WriteLogA(L"C:\\logs\\SimplyLiveRTMPServer\\releaseFrame.log", ErrorLevel, "need release audio one 0x%x", _elem);
	if (_elem != nullptr)
		m_captureAudioPool.release(_elem);
}

void CapturePoolMgr::release(VideoFrame* _elem)
{
	if (_elem == nullptr)
		return;
	switch (_elem->getFormat())
	{
	case FP_FORMAT_1080i_5000:
	case FP_FORMAT_1080i_5994:
	case FP_FORMAT_1080i_6000:
	case FP_FORMAT_1080p_2400:
	case FP_FORMAT_1080p_2500:
	case FP_FORMAT_1080p_2997:
	case FP_FORMAT_1080p_3000:
	case FP_FORMAT_1080p_5000:
	case FP_FORMAT_1080p_5994:
	case FP_FORMAT_1080p_6000:
		m_captureVideoPoolHD.release(_elem);
		break;
	case FP_FORMAT_4Kp_3000:
	case FP_FORMAT_4Kp_5000:
	case FP_FORMAT_4Kp_6000:
	case FP_FORMAT_4Kp_2400:
	case FP_FORMAT_4Kp_2500:
	case FP_FORMAT_4Kp_2997:
	case FP_FORMAT_4Kp_5994:
		m_captureVideoPool4K.release(_elem);
		break;
	case FP_FORMAT_720p_5000:
	case FP_FORMAT_720p_5994:
	case FP_FORMAT_720p_2500:
	case FP_FORMAT_720p_2997:
	case FP_FORMAT_720p_6000:
		m_captureVideoPool720.release(_elem);
		break;
	case FP_FORMAT_UNKNOWN:
	case FP_FORMAT_MAX:
	default:
		break;
	}
}

uint32_t CapturePoolMgr::getVideoPoolSize(FPTVideoFormat _fpFormat)
{
	switch (_fpFormat)
	{
	case FP_FORMAT_1080p_2400:
	case FP_FORMAT_1080p_2500:
	case FP_FORMAT_1080p_2997:
	case FP_FORMAT_1080p_3000:
	case FP_FORMAT_1080p_5000:
	case FP_FORMAT_1080p_5994:
	case FP_FORMAT_1080p_6000:
	case FP_FORMAT_1080i_5000:
	case FP_FORMAT_1080i_5994:
	case FP_FORMAT_1080i_6000:
		return m_captureVideoPoolHD.size();
	case FP_FORMAT_4Kp_3000:
	case FP_FORMAT_4Kp_5000:
	case FP_FORMAT_4Kp_6000:
	case FP_FORMAT_4Kp_2400:
	case FP_FORMAT_4Kp_2500:
	case FP_FORMAT_4Kp_2997:
	case FP_FORMAT_4Kp_5994:
		return m_captureVideoPool4K.size();
	case FP_FORMAT_720p_5000:
	case FP_FORMAT_720p_5994:
	case FP_FORMAT_720p_2500:
	case FP_FORMAT_720p_2997:
	case FP_FORMAT_720p_6000:
		return m_captureVideoPool720.size();
	case FP_FORMAT_UNKNOWN:
	default:
		return 0;
	}
}

uint32_t CapturePoolMgr::getAudioPoolSize()
{
	return m_captureAudioPool.size();
}