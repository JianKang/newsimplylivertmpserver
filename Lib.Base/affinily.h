#pragma once

#include <Windows.h>

class Affinily
{
	void	* m_token;
	unsigned long long  m_systemAffinityMask;

	int		getToken();
	bool	setPrivilege(bool _enable);
	void	changeOtherProcess(UINT64 _mask);

public:
	Affinily();
	~Affinily();

	void start(int _nbCoreReserved);
	void stop();
};
