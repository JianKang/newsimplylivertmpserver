#include "AudioPlayer.h"
#include <stdio.h>
//#include "..\LogRec\logRec.h"

#define REFTIMES_PER_SEC  10000000
#define SAFE_RELEASE(punk)   if ((punk) != NULL) { (punk)->Release(); (punk) = NULL; }

const CLSID CLSID_MMDeviceEnumerator = __uuidof(MMDeviceEnumerator);
const IID IID_IMMDeviceEnumerator = __uuidof(IMMDeviceEnumerator);
const IID IID_IAudioClient = __uuidof(IAudioClient);
const IID IID_IAudioRenderClient = __uuidof(IAudioRenderClient);

AudioPlayer::AudioPlayer()
{
	m_bStarted = false;
}

AudioPlayer::~AudioPlayer()
{
	_Unit();
}

bool AudioPlayer::Init()
{
	HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
	REFERENCE_TIME hnsRequestedDuration = REFTIMES_PER_SEC;

	m_pEnumerator = NULL;
	m_pDevice = NULL;
	m_pAudioClient = NULL;


	CoCreateInstance(CLSID_MMDeviceEnumerator, NULL, CLSCTX_ALL, IID_IMMDeviceEnumerator, (void**)&m_pEnumerator);
	m_pEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &m_pDevice);
	if (!m_pDevice)
	{
		return false;
	}
	hr = m_pDevice->Activate(IID_IAudioClient, CLSCTX_ALL, NULL, (void**)&m_pAudioClient);
	if (FAILED(hr))
	{
		printf("m_pDevice->Activate failed, hr %u, last error %d",hr ,GetLastError());
		return false;
	}
	m_pwfx = NULL;
	WAVEFORMATEXTENSIBLE ex;
	WAVEFORMATEX* closeMatch;
	WAVEFORMATEX* format = (WAVEFORMATEX *)&ex;
	m_count = 0;
	format->nChannels = 2;
	format->wBitsPerSample = 32;
	format->nBlockAlign = format->wBitsPerSample / 8 * format->nChannels;
	format->nSamplesPerSec = 48000;
	format->cbSize = sizeof(WAVEFORMATEXTENSIBLE) - sizeof(WAVEFORMATEX);
	format->nAvgBytesPerSec = format->nSamplesPerSec * format->wBitsPerSample / 8 * format->nChannels;
	format->wFormatTag = WAVE_FORMAT_EXTENSIBLE;
	ex.SubFormat = KSDATAFORMAT_SUBTYPE_PCM;
	ex.dwChannelMask = 3;
	ex.Samples.wReserved = format->wBitsPerSample;
	ex.Samples.wSamplesPerBlock = format->wBitsPerSample;
	ex.Samples.wValidBitsPerSample = format->wBitsPerSample;
	hr = m_pAudioClient->IsFormatSupported(AUDCLNT_SHAREMODE_SHARED, format, &closeMatch);
	bool bUseClosedMatchFormat = false;
	if (FAILED(hr))
	{
		printf("AUDCLNT_E_UNSUPPORTED_FORMAT not supported, hr %u , error %d ", hr, GetLastError());
		if (hr != S_FALSE)
		{
			printf("FORMAT is not supported and failed , hr %u , error %d ", hr, GetLastError());
			return false;
		}
		printf(" we will use closed match format ,channels %d , bits per sample %d ,sample per sec %d",closeMatch->nChannels,closeMatch->wBitsPerSample,closeMatch->nSamplesPerSec);
		bUseClosedMatchFormat = true;
	}

	if(!bUseClosedMatchFormat)
		hr = m_pAudioClient->Initialize(AUDCLNT_SHAREMODE_SHARED, 0, hnsRequestedDuration, 0, format, NULL);
	else
		hr = m_pAudioClient->Initialize(AUDCLNT_SHAREMODE_SHARED, 0, hnsRequestedDuration, 0, closeMatch, NULL);

	if (FAILED(hr))
	{
		printf("m_pAudioClient->Initialize failed,hr 0x%x , error %d",hr,GetLastError());
		return false;
	}

	int errorcode = hr;
	m_bufferFrameCount = 0;
	hr = m_pAudioClient->GetBufferSize(&m_bufferFrameCount); // Get the actual size of the allocated buffer.
//	printf("m_bufferFrameCount %ld, res 0x%x ,error code  0x%x of audio init", m_bufferFrameCount, hr, errorcode);

	m_pRenderClient = NULL;
	hr = m_pAudioClient->GetService(IID_IAudioRenderClient, (void**)&m_pRenderClient);
	if (FAILED(hr) || m_pRenderClient == NULL)
	{
		printf("m_pAudioClient->GetService failed, hr:%d, last error:%d", hr, GetLastError());
		return false;
	}
	m_bInit = true;
	return true;
}

bool AudioPlayer::_Unit()
{
	if(m_pAudioClient)
		m_pAudioClient->Stop();
	CoTaskMemFree(m_pwfx);
	SAFE_RELEASE(m_pEnumerator);
	SAFE_RELEASE(m_pDevice);
	SAFE_RELEASE(m_pAudioClient);
	SAFE_RELEASE(m_pRenderClient);
	return true;
}

bool AudioPlayer::AddPreviewAudio(LPBYTE pAudioData, DWORD nbFrames)
{
	if (!m_bInit ||  !m_bStarted || m_pRenderClient == NULL || pAudioData == NULL)
	{
		if (m_pRenderClient == NULL || pAudioData == NULL)
		{
			//printf("Skip AddPreviewAudio one time since the pointer is nullptr, nbFrames(%u)", nbFrames);
		}
		return false;
	}

	HRESULT hr;
	DWORD flags = 0;
	BYTE* pData = NULL;

	UINT32 numFramesPadding;
	hr = m_pAudioClient->GetCurrentPadding(&numFramesPadding); // See how much buffer space is available.
	UINT32 numFramesAvailable = m_bufferFrameCount - numFramesPadding;
	//printf("<%ld>numFramesAvailable %ld\n", m_count, numFramesAvailable);

	if (numFramesAvailable < nbFrames)
		return true;

	hr = m_pRenderClient->GetBuffer(nbFrames, &pData); // Grab the entire buffer for the initial fill operation.
	if (FAILED(hr) || pData == NULL)
	{
		printf("m_pRenderClient->GetBuffer failed, nbFrames(%u),res %d ,error code %d", nbFrames,hr,GetLastError());
		if (hr == AUDCLNT_E_DEVICE_INVALIDATED)
		{
			_Unit();
			Init();
		}
		return false;
	}
	//printf("Playing:%s Add audio buffer !!!!!!!!!! \n", bStarted ? "Yes" : "No");
	memcpy(pData, pAudioData, nbFrames * 8); // Load the initial data into the shared buffer.
	m_pRenderClient->ReleaseBuffer(nbFrames, flags);

	if (++m_count == 1)
	{
		m_pAudioClient->Start(); // Start playing.
	//	printf("Start playing Audio  !!!!!!!!!!!!!!!!!!!!!!\n");
	}
	return true;
}

bool AudioPlayer::Start()
{
//	printf("Start Audio monitor !!!!!!!!!!!!!!!!!!!!!!\n");
	m_bStarted = true;
	Init();
	return true;
}

bool AudioPlayer::Stop()
{
//	printf("Stop Audio monitor !!!!!!!!!!!!!!!!!!!!!!\n");
	m_bStarted = false;
	m_count = 0;
	return true;
}