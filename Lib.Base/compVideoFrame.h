#pragma once
#include <windows.h>
#include "timecode.h"
#include <memory>
#include "videoFrame.h"
//|-----------|
//|IDR 1 byte |
//|-----------|
//|TC 4 byte  |
//-------------
//|size 4 byte|   <------ this rawSize is the rawSize of "Data" below
//|-----------|
//|           |
//|	Data      |
//|			  |
//|			  |
//|			  |
//|-----------|
class CompVideoFrame
{
	UINT64         m_nFrameID;
	unsigned char* m_frame;
	unsigned long  m_totalSize;
	UINT64	       m_tag;
	double		   m_nTimeStamp;
public:
	struct header
	{
		bool			m_bIsIDRFrame;
		Timecode	    tc;
		int				qScale;
		int				fieldSize[2];
		unsigned long   rawSize;
	};

	CompVideoFrame();
	virtual ~CompVideoFrame();

	unsigned char* getRaw() const;
	unsigned long getRawSize() const;
	unsigned char* getRawWithSize() const;
	unsigned char* getRawWithHeader() const;
	unsigned long getRawWithHeaderSize() const;

	int setBufferSize(unsigned long _rawSize);

	void   SetFrameID(UINT64  nFrameID);
	UINT64 GetFrameID() const;

	void   setTag(UINT64 _tag);
	UINT64 getTag() const;

	void     setTC(const Timecode& _tc);
	Timecode getTC() const;

	void   setTimeStamp(double lTimeStamp);
	double getTimeStamp() const;

	void setIsIDR(bool bIsIDRFrame);
	bool getIsIDR() const;

	void setQscale(int _qScale);
	int  getQscale();

	void setFieldSize(int _field0, int _field1);
	int  getFieldSize(int _idx);
};

typedef shared_ptr<CompVideoFrame> pCVframe;
