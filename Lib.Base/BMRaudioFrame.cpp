#include "BMRaudioFrame.h"
#include <malloc.h>

BMRaudioFrame ::BMRaudioFrame ()
	:m_totalSize(1920) //16 audio channels , and simple bit depth is 4 BYTES
{
	m_raw = (short*)malloc(m_totalSize);
	m_dataSize = m_totalSize;
}
void BMRaudioFrame ::reAlloc(UINT32 size)
{
	if (m_raw)
		free(m_raw);

	m_raw = (short*)malloc(size);
	m_totalSize = size;
}
BMRaudioFrame ::~BMRaudioFrame ()
{
	free(m_raw);
}

bool BMRaudioFrame ::isMute()
{
	for (int i = 0; i < m_dataSize; i++)
	{
		if (m_raw[i])
			return false;
	}
	return true;
}

void BMRaudioFrame ::setToMute()
{
	memset(m_raw, 0, m_totalSize);
}

void BMRaudioFrame ::SetFrameID(UINT64  nFrameID)
{
	m_nFrameID = nFrameID;
}

UINT64 BMRaudioFrame ::GetFrameID()
{
	return m_nFrameID;
}