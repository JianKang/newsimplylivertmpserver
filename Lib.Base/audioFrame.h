#pragma once
#include <windows.h>
#include <memory>
#include <atomic>

class AudioFrame
{
	UINT64             m_tag;
	short*             m_raw;
	UINT64             m_nFrameID;
	unsigned long      m_totalSize;
	unsigned long      m_dataSize;
	UINT32             m_monoCnt;
	UINT32             m_sampleCnt;
	UINT64			   ptsFrame;
	void reAlloc(UINT32 size);

public:
	static void  get2AudioCnl(int sample, long* srcBuffer, long* dstBuffer, int cn1ID, int cn2ID);

	AudioFrame();
	~AudioFrame();

	short*			getRaw() const;
	void			setToMute() const;
	void			SetFrameID(UINT64  nFrameID);
	void			SetMonoCnt(UINT32 cnt);
	void			SetSampleCnt(UINT32  sampleCnt);
	UINT32			GetMonoCnt() const;
	UINT64			GetFrameID() const;
	unsigned long	getBufferTotalSize() const;
	unsigned long	getSampleCount() const;
	bool			isMute() const;
	unsigned long	getDataSize() const;
	void			setBufferSize(UINT32 size);
	void			trimStero(int steroID);
	UINT64			getTag() const;
	void			setTag(UINT64 _tag);


	void setPts(int64_t pts)
	{
		ptsFrame = pts;
	}
	int64_t getPts()
	{
		return ptsFrame;
	}
};

typedef std::shared_ptr<AudioFrame> pAframe;
