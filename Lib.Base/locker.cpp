#include "locker.h"

Locker::Locker()
{
	InitializeCriticalSectionAndSpinCount(&m_locker, 4000);
}

Locker::~Locker()
{
	DeleteCriticalSection(&m_locker);
}

void Locker::lock()
{
	EnterCriticalSection(&m_locker);
}

void Locker::unlock()
{
	LeaveCriticalSection(&m_locker);
}

LockHolder::LockHolder(Locker& _locker) : m_locker(_locker)
{
	m_locker.lock();
}

LockHolder::~LockHolder()
{
	m_locker.unlock();
}

void LockHolder::lock() const
{
	m_locker.lock();
}

void LockHolder::unlock() const
{
	m_locker.unlock();
}