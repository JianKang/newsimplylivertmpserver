#pragma once
#include <windows.h>
#include <memory>
#include "timecode.h"
#include <atomic>
#include "../Lib.Base/config.h"

class VideoFrame
{
	UINT64		   m_nFrameID = 0;
	Timecode	   m_tc;
	bool		   m_validTC = false;
	FPTVideoFormat m_fpFormat = FP_FORMAT_1080i_5000;
	UINT32		   m_dataSize = 0;
	atomic_long    m_iProtectedCnt = 0;
	bool			m_isBlackFrame;
	double			m_pts;
protected:
	bool           m_is422 = true;
public:
	VideoFrame();
	virtual ~VideoFrame();

	virtual unsigned long getWidth() = 0;
	virtual unsigned long getHeight() = 0;
	virtual unsigned char* getRaw() = 0;
	virtual unsigned long getRawSize() = 0;
	int setToBlack(bool _flagOnly = false);
	void resetBlackFlag() { m_isBlackFrame = false; };
	bool isBlackFrame() { return m_isBlackFrame; };
	void SetFrameID(UINT64  nFrameID);
	UINT64 GetFrameID() const;
	void setTC(const Timecode& _tc);
	Timecode getTC() const;
	void setFormat(const FPTVideoFormat &fpFormat);
	FPTVideoFormat getFormat() const;
	void setTCValid(bool _valid);
	bool getTCValid() const;
	bool isProtected() const;
	void setProtected(bool _bProtected);
	void setPts(double pts);
	double getPts() const;
};

typedef shared_ptr<VideoFrame> pVFrame;
