#include "AutoClock.h"
#include "platform.h"

AutoClock::AutoClock()
{
	m_tickPerFrame = 0;
	getCPUfreq(m_freq);
}

AutoClock::~AutoClock()
{
}

void AutoClock::start(int _frameRate)
{
	m_tickPerFrame = m_freq / _frameRate;
	startThread();
}

void AutoClock::stop()
{
	stopThread(1000);
}

void AutoClock::callBack()
{
	cpuTick tickIn, tickOut;
	getTickCount(tickIn);

	while (isRunning())
	{
		Sleep(1);
		getTickCount(tickOut);

		UINT64 diff = tickOut - tickIn;
		if (diff >= m_tickPerFrame)
		{
			tickIn += m_tickPerFrame;
			setEventClock();
			Sleep(1);
		}
	}
}

void AutoClock::setEventClock()
{
	m_event.raiseEvent();
}

bool AutoClock::waitEventClock()
{
	return m_event.waitEvent();
}