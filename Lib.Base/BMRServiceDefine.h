﻿#pragma once
#include <windows.h>
#include <bitset>
#define MAX_STREAM_CNT 1
#define NB_BMR_RECORDER 8
enum EM_ConsumerChannel
{
	EM_ConsumerChannel_STREAM_0,
	EM_ConsumerChannel_STREAM_1,
	EM_ConsumerChannel_STREAM_2,
	EM_ConsumerChannel_STREAM_3,
	EM_ConsumerChannel_STREAM_4,
	EM_ConsumerChannel_STREAM_5,
	EM_ConsumerChannel_STREAM_6,
	EM_ConsumerChannel_STREAM_7,
	EM_ConsumerChannel_MAX
};


#pragma pack(push,1)
enum BMRStreamStatus : byte
{
	BMRStreamStatus_stopped,
	BMRStreamStatus_connecting,
	BMRStreamStatus_streaming,
	BMRStreamStatus_error
};

enum ButtonStatus : byte
{
	ButtonStatus_invalid,
	ButtonStatus_unselected,
	ButtonStatus_selected,
	ButtonStatus_Streaming
};

enum BMREncoderType : byte
{
	BMREncoderType_H264,
	BMREncoderType_H265,
};

enum BMREncoderModeType : byte
{
	BMREncoderType_CAVLC,
	BMREncoderType_CABAC,
};

enum BMRExportStatus : byte
{
	ExportStatus_Stopped,
	ExportStatus_ERROR,
	ExportStatus_PROGRESS,
	ExportStatus_FINISH,
};

enum BMREncodeRCModeType : byte
{
	RC_CONSTQP = 0x0,                  /**< Constant QP mode */
	RC_VBR = 1,                        /**< Variable bitrate mode */
	RC_CBR = 2,                        /**< Constant bitrate mode */
	RC_VBR_MINQP = 4,                  /**< Variable bitrate mode with MinQP */
	RC_2_PASS_QUALITY = 8,             /**< Multi pass encoding optimized for image quality and works only with low latency mode */
	RC_2_PASS_FRAMESIZE_CAP = 16,      /**< Multi pass encoding optimized for maintaining frame size and works only with low latency mode */
	RC_2_PASS_VBR = 32                 /**< Multi pass VBR */
};

enum ExportFileType : byte
{
	FILE_MP4 = 0,
	FILE_MXF = 1,
	FILE_TS = 2,
	FILE_RAW_DNX = 3,
	FILE_RAW_YUVAndWav = 4,
};

enum BMRNetdriveType : byte
{
	BMRNetdriveType_None,
	BMRNetdriveType_RTMP,
	BMRNetdriveType_HLS,
	BMRNetdriveType_FILE,
};

struct BMRNetdriveStatusCameraStreamStatus
{
	BMRStreamStatus nStreamStatus;
	int nCurrentRetry;
	int nBandWidth;
	int nPinResponseTime;
	int nRecordFrames;
	int nDropFrames;
	int nBufFrames;
	int nBufTotal;
	byte szLastMsg[MAX_PATH];
};

struct BMRNetdriveStatusCamera
{
	ButtonStatus buttonStatus;
	byte pathValue[MAX_PATH];
	BMRNetdriveStatusCameraStreamStatus StreamStatusInfo;
};

struct BMRCompressionSetting
{
	bool isCustomized;
	int compression;
};

struct BMRAudioSetting
{
	byte LowNum;	
	byte LowBandwidthChannel[6];
	byte HighNum;	
	byte HighBandwidthChannel[8];
	byte isMonitoringAudio;
	int nAudioGain;
};

enum LogoPosition : byte
{
	top_Left = 0,
	top_Right = 1,
	bottom_left = 2,
	bottom_right = 3,
	Custom = 4,
};

struct BMRLogoSetting
{
	bool isLogoEnabled;
	bool isFullLogo;
	int logo_TopRightX;
	int logo_TopRightY;
	DWORD logoWidth;
	DWORD logoHeight;
	LogoPosition logoPosition;
};

struct BMREncoderSetting
{
	BMREncoderModeType encoderMode;
	int dwGopValue;
	BMREncoderType encoderType;
	BMREncodeRCModeType encodeRCModeType;
};

struct BMRStreamSetting
{
	int dwRetryTimes;
	int dwDealyBetweenRetry;
	int dwBufferCount;
};

struct BMRTimeCodeSetting
{
	bool isTimecodeEnabled;
};

struct ExportClipStatus
{
	BMRExportStatus		stauts;
	ExportFileType	fileType;
	byte cameraIndex;
	byte netDriveID;// 0 1 2 3  0xff:REST Export. 0xFE:debug Export.
	float progrcess;
	UINT64	nStart;
	UINT64	nStop;
	UINT64	nCurrent;
	DWORD audioCount;	//
	DWORD pathLen;				//[4 byte]
	DWORD errorLen;				//[4 byte]
	DWORD taskGUIDLen;			//[4 byte]
	char audioFileMap[16];		//[audioCount byte]
	byte szClipName[MAX_PATH];
	byte szErrorMsg[MAX_PATH];
	char taskGUID[MAX_PATH];	//[taskGUIDLen byte]	
};

struct BMRCameraStatus
{
	bool canStartRecOnCam;
	bool canStopRecOnCam;
	bool isRecordingOnCam;
	byte name[MAX_PATH];	
	BMRStreamSetting streamSetting;
	BMREncoderSetting encoderSetting;
	BMRLogoSetting logoSetting;
	BMRAudioSetting audioSetting;
	BMRTimeCodeSetting timeCodeSetting;
	BMRCompressionSetting compressionSetting;
};

struct BMRNetdriveCamerasStatus
{
	ExportFileType fileType;
	BMRNetdriveStatusCamera keyCamera[NB_BMR_RECORDER];
};

struct BMRNetdriveStatus
{
	BMRNetdriveType type;
	BMRNetdriveCamerasStatus subNetdrive[2];
};

struct BMRSetting
{
	BMRCameraStatus cameraSetting[NB_BMR_RECORDER];
	BMRNetdriveStatus netdriveSetting[MAX_STREAM_CNT];

	BMRSetting()
	{
		memset(this, 0, sizeof(BMRSetting));
		for (int _cnl = 0; _cnl < NB_BMR_RECORDER; ++_cnl)
		{
			cameraSetting[_cnl].compressionSetting.compression = 6000;
			cameraSetting[_cnl].streamSetting.dwBufferCount = 500;
			cameraSetting[_cnl].streamSetting.dwRetryTimes = 5;
			cameraSetting[_cnl].streamSetting.dwDealyBetweenRetry = 5;
			cameraSetting[_cnl].encoderSetting.encoderType = BMREncoderType_H264;
			cameraSetting[_cnl].encoderSetting.encodeRCModeType = RC_CBR;
			cameraSetting[_cnl].audioSetting.LowNum = 2;
			cameraSetting[_cnl].audioSetting.HighNum = 2;
			cameraSetting[_cnl].audioSetting.isMonitoringAudio = 0;
			for (int n = 0; n < cameraSetting[_cnl].audioSetting.LowNum;++n)
			{
				cameraSetting[_cnl].audioSetting.LowBandwidthChannel[n] = n;
			}
			for (int n = 0; n < cameraSetting[_cnl].audioSetting.HighNum; ++n)
			{
				cameraSetting[_cnl].audioSetting.HighBandwidthChannel[n] = n;
			}			
		}
	}
};

#pragma pack(pop)
