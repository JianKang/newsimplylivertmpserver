#include "VideoFrame.h"

VideoFrame::VideoFrame()
{
	m_isBlackFrame = false;
}

VideoFrame::~VideoFrame()
{
}

int VideoFrame::setToBlack(bool _flagOnly)
{
	if (_flagOnly)
		m_isBlackFrame = true;
	else
	{
		unsigned long size = getRawSize() / 4;
		unsigned long* dest = (unsigned long *)getRaw();

		for (unsigned long i = 0; i < size; i++)
			*dest++ = 0x10801080;
	}

	return 0;
}

void VideoFrame::SetFrameID(UINT64  nFrameID)
{
	m_nFrameID = nFrameID;
}

UINT64 VideoFrame::GetFrameID() const
{
	return m_nFrameID;
}

void VideoFrame::setTC(const Timecode& _tc)
{
	m_tc = _tc;
}

Timecode VideoFrame::getTC() const
{
	return m_tc;
}

void VideoFrame::setFormat(const FPTVideoFormat &fpFormat)
{
	m_fpFormat = fpFormat;
}

FPTVideoFormat VideoFrame::getFormat() const
{
	return m_fpFormat;
}

void VideoFrame::setTCValid(bool _valid)
{
	m_validTC = _valid;
}

bool VideoFrame::getTCValid() const
{
	return m_validTC;
}

bool VideoFrame::isProtected() const
{
	return m_iProtectedCnt > 0;
}

void VideoFrame::setProtected(bool _bProtected)
{
	if (_bProtected)
		++m_iProtectedCnt;
	else
	{
		if (m_iProtectedCnt > 0)
			--m_iProtectedCnt;
	}
}

void VideoFrame::setPts(double pts)
{
	m_pts = pts;
}

double VideoFrame::getPts() const
{
	return m_pts;
}
