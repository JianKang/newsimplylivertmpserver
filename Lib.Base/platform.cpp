#include "platform.h"

void getCPUfreq(cpuFreq& _freq)
{
	LARGE_INTEGER l;
	QueryPerformanceFrequency(&l);
	QueryPerformanceFrequency((LARGE_INTEGER*)&_freq);
}

void getTickCount(cpuTick& _tick)
{
	QueryPerformanceCounter((LARGE_INTEGER*)&_tick);
}