#pragma once

#include <Windows.h>

class Locker
{
	CRITICAL_SECTION	 m_locker;

public:
	Locker();
	~Locker();

	void lock();
	void unlock();
};

class LockHolder
{
	Locker& m_locker;

	LockHolder& operator=(const LockHolder&) = delete;
public:
	explicit LockHolder(Locker& _locker);
	~LockHolder();

	void lock() const;
	void unlock() const;
};
