#pragma once

#include "VideoFrame.h"

class VFrame_480_270 : public VideoFrame
{
	static const unsigned long m_width = 480;
	static const unsigned long m_height = 270;

	unsigned char * m_pFrame = nullptr;
	int32_t			frameSize = 0;

public:
	VFrame_480_270(bool is422 = true)
	{
		m_is422 = is422;
		frameSize = m_width * m_height;
		if (m_is422)
			frameSize *= 2;
		else
			frameSize = frameSize * 3 / 2;
		m_pFrame = static_cast <unsigned char *> (_aligned_malloc(frameSize, 1024));
	}

	~VFrame_480_270()
	{
		if (m_pFrame)
			_aligned_free(m_pFrame);
	}

	unsigned long getWidth() override
	{
		return m_width;
	}

	unsigned long getHeight() override
	{
		return m_height;
	}

	unsigned char* getRaw() override
	{
		return m_pFrame;
	}

	unsigned long getRawSize() override
	{
		return frameSize;
	}
};

typedef shared_ptr<VFrame_480_270> pVFrame_480_270;
