#pragma once
#include "platform.h"
#include "../Lib.Base/eventClock.h"
#include "../Lib.Base/SemaphoreClock.h"
#include "IMasterClock.h"

class AutoClock : public IMasterClock
{
	cpuFreq m_freq;
	UINT64 m_tickPerFrame;
	SemaphoreClock m_event;

	void callBack() override;
	void setEventClock() override; //Don't call this....

public:
	AutoClock();
	~AutoClock();
	void start(int _frameRate) override;
	bool waitEventClock() override;
	void stop() override;
};
