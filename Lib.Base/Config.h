#pragma once
#include <bitset>
#include <string>
using namespace std;
#define  MAX_PAYER 8
using ChannelMask = std::bitset<32>;

enum FPTVideoFormat
{
	FP_FORMAT_UNKNOWN = 0,
	FP_FORMAT_1080i_5000,
	FP_FORMAT_1080i_5994,
	FP_FORMAT_1080i_6000,
	FP_FORMAT_1080p_2500,
	FP_FORMAT_1080p_2997,
	FP_FORMAT_1080p_3000,
	FP_FORMAT_1080p_5000,
	FP_FORMAT_1080p_5994,
	FP_FORMAT_1080p_6000,
	FP_FORMAT_720p_2500,
	FP_FORMAT_720p_2997,
	FP_FORMAT_720p_5000,
	FP_FORMAT_720p_5994,
	FP_FORMAT_720p_6000,
	FP_FORMAT_4Kp_2500,
	FP_FORMAT_4Kp_2997,
	FP_FORMAT_4Kp_3000,
	FP_FORMAT_4Kp_5000,
	FP_FORMAT_4Kp_5994,
	FP_FORMAT_4Kp_6000,
	FP_FORMAT_1080p_2400,
	FP_FORMAT_4Kp_2400,
	FP_FORMAT_MAX
};

class Config
{
	FPTVideoFormat				m_videoFormat = FP_FORMAT_UNKNOWN;
	int							m_outType;
	string                      m_camInputAddress[MAX_PAYER];
	string                      m_camOutAddress[MAX_PAYER];
	ChannelMask				   m_recChannelMask;
	ChannelMask                m_PGMChannelMask;
	bool						m_isUsingGpuDecode;
protected:
	Config();
	~Config();
public:

	static Config * getInstance();
	static void				 releaseInstance();
	FPTVideoFormat			 getVideoFormat() const;
	int						 getFrameRateNum() const;
	int						 getFrameRateDen() const;
	int						 getVideoWidth() const;
	int						 getVideoHeight() const;
	int						 getFramesPerSec() const;
	float					 getInterval() const;
	bool					 is4K() const;
	bool					 isProgressive() const;
	int						 getOutTypeIsAja() const;
	string					 getInputAddress(unsigned int nIndex) const;
	string					 getOutAddress(unsigned int nIndex) const;
	ChannelMask				 getPGMChannelMask() const;
	ChannelMask				 getRecChannelMask() const;
	bool					 is720p() const;
	bool					 isHD() const;
	bool					 isUsingGpuDecode() const;
private:
	void load();
};
