#pragma once

struct videoFrameInfo //Used for import and RTMP IN
{
	unsigned char *pData;
	int nDataSize;
	int nBufferSize = 0;
	int64_t ptsFrame;
	videoFrameInfo()
	{
		nDataSize	= 0;
		nBufferSize = 0;
		pData = nullptr;
	}
	void setBufferSize(int nNewBufferSize)
	{
		pData = new unsigned char[nNewBufferSize];
		nBufferSize = nNewBufferSize;
	}
	void setPts(int64_t pts)
	{
		ptsFrame =  pts;
	}
	int64_t getPts()
	{
		return ptsFrame;
	}
};

