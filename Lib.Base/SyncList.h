#pragma once
#include <list>
#include "locker.h"
using std::list;

template<class T>
class SyncList
{
	list<T> m_list;
	Locker	m_lock;

public:
	SyncList() {}

	~SyncList() {}

	void push_back(const T& _val)
	{
		LockHolder lock(m_lock);
		m_list.push_back(_val);
	}

	void push_front(const T& _val)
	{
		LockHolder lock(m_lock);
		m_list.push_front(_val);
	}

	void pop_front()
	{
		LockHolder lock(m_lock);
		m_list.pop_front();
	}

	void pop_back()
	{
		LockHolder lock(m_lock);
		m_list.pop_back();
	}

	T front()
	{
		LockHolder lock(m_lock);
		return m_list.front();
	}

	uint32_t size()
	{
		LockHolder lock(m_lock);
		return m_list.size();
	}

	bool empty()
	{
		LockHolder lock(m_lock);
		return m_list.empty();
	}

	void clear()
	{
		LockHolder lock(m_lock);
		return m_list.clear();
	}

	bool find(const T& item)
	{
		LockHolder lock(m_lock);
		return any_of(begin(m_list), end(m_list), [&](const T& obj) { return item == obj; });
	}

	bool remove(const T& item)
	{
		LockHolder lock(m_lock);
		m_list.remove(item);
		return true;
	}
};
