#pragma once

class EasyThread
{
	void* m_thread;
	unsigned long m_threadId = 0;
	volatile bool m_exitRequested;

	static unsigned long __stdcall theThread(void* lpParam);

protected:
	virtual void callBack() = 0;

	void startThread();
	void stopThread(unsigned long _timeOut = 0xFFFFFFFF);

	bool isRunning() const
	{
		return !m_exitRequested;
	}

public:
	EasyThread();
	virtual ~EasyThread();
};
