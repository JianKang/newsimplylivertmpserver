#include "easyThread.h"
#include <windows.h>
#include <stdio.h>

EasyThread::EasyThread()
{
	m_thread = nullptr;
	m_exitRequested = false;
}

EasyThread::~EasyThread()
{
}

void EasyThread::startThread()
{
	m_exitRequested = false;
	m_thread = CreateThread(nullptr, 0, theThread, (void*)this, 0, &m_threadId);
}

void EasyThread::stopThread(unsigned long _timeOut)
{
	m_exitRequested = true;
	if (m_thread)
	{
		WaitForMultipleObjects(1, &m_thread, TRUE, _timeOut);
		CloseHandle(m_thread);
		m_thread = nullptr;
	}
}

unsigned long __stdcall EasyThread::theThread(void* lpParam)
{
	EasyThread* pEasyThread = (EasyThread *)lpParam;
	pEasyThread->callBack();
	return 0;
}