#pragma once
#include <Windows.h>
#include "CommonHelper.h"

struct ID
{
#define ID_LENGTH 256
	DWORD idSize;
	char  id[ID_LENGTH];

	ID()
	{
		memset(this, 0, sizeof(ID));
	}

	ID(char *pId, DWORD size)
	{
		assign(pId, size);
	}

	bool assign(char *pId, DWORD size)
	{
		memset(this, 0, sizeof(ID));

		if (pId != nullptr && size <= ID_LENGTH)
		{
			memcpy_s(id, ID_LENGTH, pId, size);
			idSize = size;
			return true;
		}
		idSize = 0;
		return false;
	}

	bool assign(char c, DWORD number)
	{
		memset(this, 0, sizeof(ID));

		if (number <= ID_LENGTH)
		{
			memset(id, c, number);
			idSize = number;
			return true;
		}
		idSize = 0;
		return false;
	}

	int compare(const ID& value) const
	{
		return memcmp(id, value.id, max(idSize, value.idSize));
	}

	bool empty() const
	{
		return idSize == 0;
	}

	bool operator==(const ID& value) const
	{
		return compare(value) == 0;
	}

	bool operator!=(const ID& value) const
	{
		return compare(value) != 0;
	}

	bool operator < (const ID& cmp) const
	{
		return compare(cmp) < 0;
	}

	static ID make_ID(char *pId, DWORD size)
	{
		ID id(pId, size);
		return id;
	}

	void clear()
	{
		memset(this, 0, sizeof(ID));
		idSize = 0;
	}

	string getIDString()
	{
		return ::getIDString(id, idSize);
	}
};
