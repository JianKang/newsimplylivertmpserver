﻿#pragma once
#include "../Lib.Base/easyThread.h"

class IMasterClock : public EasyThread
{
protected:
	IMasterClock();
	virtual ~IMasterClock() {}
public:
	static IMasterClock* CreateInstance(bool bManualClock);
	static void ReleaseInstance(IMasterClock*& pInstance);

	virtual void start(int _frameRate) = 0;
	virtual void setEventClock() = 0;
	virtual bool waitEventClock() = 0;
	virtual void stop() = 0;
};
