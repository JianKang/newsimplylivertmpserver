#pragma once
#include <windows.h>
#include <mmdeviceapi.h>
#include <AudioClient.h>

class AudioPlayer
{
public:
	AudioPlayer();
	~AudioPlayer();
public:
	bool Init();
	bool Start();
	bool AddPreviewAudio(LPBYTE pAudioData, DWORD size);
	bool Stop();
private:
	IMMDeviceEnumerator* m_pEnumerator;
	IMMDevice* m_pDevice;
	IAudioClient* m_pAudioClient;
	IAudioRenderClient* m_pRenderClient;
	UINT32 m_bufferFrameCount;
	WAVEFORMATEX* m_pwfx;
	int m_count;
	bool m_bStarted;
	bool _Unit();
	bool m_bInit = false;
};
