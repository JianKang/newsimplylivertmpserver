#pragma once
#include <windows.h>
#include <memory>

class BMRaudioFrame 
{
	short*        m_raw;
	UINT64        m_nFrameID;
	unsigned long m_totalSize;
	unsigned long m_dataSize;
	double		  m_nTimeStamp;
	void reAlloc(UINT32 size);

public:
	BMRaudioFrame ();
	~BMRaudioFrame ();

	short* getRaw()
	{
		return m_raw;
	};

	void setToMute();

	void SetFrameID(UINT64  nFrameID);

	UINT64 GetFrameID();

	unsigned long getBufferTotalSize()
	{
		return m_totalSize;
	};

	bool isMute();

	unsigned long getDataSize()
	{
		return m_dataSize;
	};

	void setBufferSize(UINT32 size)
	{
		m_dataSize = size;
		if (m_dataSize > m_totalSize)
		{
			reAlloc(size);
		}
	}

	void setTimeStamp(double lTimeStamp)
	{
		m_nTimeStamp = lTimeStamp;
	}
	double getTimeStamp()
	{
		return m_nTimeStamp;
	}
};

typedef std::shared_ptr<BMRaudioFrame > pBMRAframe;
