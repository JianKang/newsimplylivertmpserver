#pragma once
#include "audioFrame.h"
#include "VideoFrame.h"
#include "ObjectPoolEx.h"

class CapturePoolMgr
{
	CapturePoolMgr();
	~CapturePoolMgr();
	ObjectPoolEx<VideoFrame>                m_captureVideoPool4K;
	ObjectPoolEx<VideoFrame>                m_captureVideoPoolHD;
	ObjectPoolEx<VideoFrame>                m_captureVideoPool720;
	ObjectPoolEx<AudioFrame>                m_captureAudioPool;

public:
	static CapturePoolMgr * GetInstance();
	void initialize(FPTVideoFormat _fpFormat, int _size);
	void getNew(VideoFrame* & _elem, FPTVideoFormat fpFormat);
	void getNew(AudioFrame* & _elem);
	void release(AudioFrame* _elem);
	void release(VideoFrame* _elem);
	uint32_t getVideoPoolSize(FPTVideoFormat _fpFormat);
	uint32_t getAudioPoolSize();
};
