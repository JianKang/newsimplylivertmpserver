#pragma once
#include "../lib.base/VideoFrame.h"
#include "../lib.base/audioFrame.h"

__interface IFPInputCallBack
{
	void cb(DWORD _channelID, VideoFrame* pFrameVideo = nullptr,AudioFrame* pFrameAudio = nullptr);
};
