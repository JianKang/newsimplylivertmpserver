#pragma once
#include <string>
#include <map>
#include <future>
#include <deque>
using namespace std;
#include "../Lib.Base/locker.h"
#include "../Lib.Base/platform.h"
#include <windows.h>
#include "INaluUnitCallBack.h"
#include "LoopBuffer.h"

class RTMPDataDecoder
{
public:
	RTMPDataDecoder(INaluUnitCallBack *pCallback, TCHAR *szLogFile);
	~RTMPDataDecoder();

	void startWork();
	void AddRawRTMPData(BYTE *pData, double absoluteTimestamp, uint32_t dataLength,uint32_t processedLength, uint32_t totalLength, bool bAudio);
	void stopWork();
private:
	Locker m_lockerQueue;
	deque<uint32_t> m_audioQueue;
	TCHAR *m_szLogFile;
	void resetPos();
	INaluUnitCallBack * m_pCallback;
	bool m_bStopDec;
	future<void> m_decVideoResult;
	void _decRTMPVideoThread();

	future<void> m_decAudioResult;
	void _decRTMPAudioThread();

	CLoopBuffer m_bufVideo;
	CLoopBuffer m_bufAudio;

	void ProcessAudioData(BYTE *pData, double absoluteTimestamp, uint32_t dataLength, uint32_t processedLength, uint32_t totalLength);
	void ProcessVideoData(BYTE *pData, double absoluteTimestamp, uint32_t dataLength, uint32_t processedLength, uint32_t totalLength);

	//unsigned __int64 m_nAudioPushPos;
	//unsigned __int64 m_nVideoPushPos;
	//unsigned __int64 m_nAudioFetchPos;
	//unsigned __int64 m_nVideoFetchPos;
	//unsigned __int64 m_nAudioFetchTotal;
	//unsigned __int64 m_nAudioPushTotal;
	//unsigned __int64 m_nVideoFetchTotal;
	//unsigned __int64 m_nVideoPushTotal;

	//Locker m_videoCS;
	//Locker m_audioCS;
	//const int MAX_AUDIO_SIZE = 4 * 1024 * 1024;
	//const int MAX_VIDEO_SIZE = 10 * 1024 * 1024;
	const int TEMP_VIDEO_SIZE = 2*1024 * 1024;
	const int TEMP_AUDIO_SIZE = 50*1024;
	bool	m_bGetAudioMeta;
	bool	m_bGetVideoMeta;
	DWORD	m_dwVideoUnitPos;
	DWORD	m_dwAudioUnitPos;
	BYTE *m_pbufVideoUnit;
	BYTE *m_pbufVideo;
	BYTE *m_pbufAudioUnit;

	RTMP_RAW_Unit m_naluCbDataV;
	RTMP_RAW_Unit m_naluCbDataA;
	//BYTE *m_pbufAudio;
	//BYTE *m_pbufVideo;
	BYTE *m_pbufVideoTEMP;
	BYTE *m_pbufAudioTEMP;

	int _fetchRawRTMPData(uint8_t* buf, int size, bool bAudio);
	//int _fetchVideoData(uint8_t* buf, int size);
	//int _fetchAudioData(uint8_t* buf, int size);

	BYTE headerVideo[4] = { 0x00,0x00,0x00,0x01 };

	cpuFreq m_freq;
	UINT64 m_tickPerFrame;
	cpuTick m_lastTick;
	UINT64	m_GotFrameID;
};

