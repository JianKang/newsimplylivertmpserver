#include "RTMPCore.h"
#include "netio/netio.h"
#include "configuration/configfile.h"
#include "protocols/protocolmanager.h"
#include "application/clientapplicationmanager.h"
#include "protocols/protocolfactorymanager.h"
#include "protocols/defaultprotocolfactory.h"
#include "../lib.Base/locker.h"
#include <string>

//This is a structure that holds the state
//between config re-reads/re-runs
struct RunningStatus
{
	// startup parameters
	Variant commandLine;

	//Configuration file
	ConfigFile *pConfigFile;

	//default protocol factory
	DefaultProtocolFactory * pProtocolFactory;

	//should we run again?
	bool run;

	//is this a daemon already?
	bool daemon;

	RunningStatus()
	{
		pConfigFile = NULL;
		pProtocolFactory = NULL;
		run = false;
		daemon = false;
	}
};

static CRTMPCore *mSInstance = nullptr;
static Locker			g_lock;
static RunningStatus gRs;

#ifdef COMPILE_STATIC
#ifdef HAS_APP_ADMIN
extern "C" BaseClientApplication *GetApplication_admin(Variant configuration);
#endif
#ifdef HAS_APP_APPLESTREAMINGCLIENT
extern "C" BaseClientApplication *GetApplication_applestreamingclient(Variant configuration);
#endif
#ifdef HAS_APP_APPSELECTOR
extern "C" BaseClientApplication *GetApplication_appselector(Variant configuration);
#endif
#ifdef HAS_APP_FLVPLAYBACK
extern "C" BaseClientApplication *GetApplication_flvplayback(Variant configuration);
#endif
#ifdef HAS_APP_PROXYPUBLISH
extern "C" BaseClientApplication *GetApplication_proxypublish(Variant configuration);
#endif
#ifdef HAS_APP_SAMPLEFACTORY
extern "C" BaseClientApplication *GetApplication_samplefactory(Variant configuration);
extern "C" BaseProtocolFactory *GetFactory_samplefactory(Variant configuration);
#endif
#ifdef HAS_APP_STRESSTEST
extern "C" BaseClientApplication *GetApplication_stresstest(Variant configuration);
#endif
#ifdef HAS_APP_VPTESTS
extern "C" BaseClientApplication *GetApplication_vptests(Variant configuration);
#endif
#ifdef HAS_APP_VMAPP
extern "C" BaseClientApplication *GetApplication_vmapp(Variant configuration);
#endif

BaseClientApplication *SpawnApplication(Variant configuration) {
	if (false) {
	}
#ifdef HAS_APP_ADMIN
	else if (configuration[CONF_APPLICATION_NAME] == "admin") {
		return GetApplication_admin(configuration);
	}
#endif
#ifdef HAS_APP_APPLESTREAMINGCLIENT
	else if (configuration[CONF_APPLICATION_NAME] == "applestreamingclient") {
		return GetApplication_applestreamingclient(configuration);
	}
#endif
#ifdef HAS_APP_APPSELECTOR
	else if (configuration[CONF_APPLICATION_NAME] == "appselector") {
		return GetApplication_appselector(configuration);
	}
#endif
#ifdef HAS_APP_FLVPLAYBACK
	else if (configuration[CONF_APPLICATION_NAME] == "flvplayback") {
		return GetApplication_flvplayback(configuration);
	}
#endif
#ifdef HAS_APP_PROXYPUBLISH
	else if (configuration[CONF_APPLICATION_NAME] == "proxypublish") {
		return GetApplication_proxypublish(configuration);
	}
#endif
#ifdef HAS_APP_SAMPLEFACTORY
	else if (configuration[CONF_APPLICATION_NAME] == "samplefactory") {
		return GetApplication_samplefactory(configuration);
	}
#endif
#ifdef HAS_APP_STRESSTEST
	else if (configuration[CONF_APPLICATION_NAME] == "stresstest") {
		return GetApplication_stresstest(configuration);
	}
#endif
#ifdef HAS_APP_VPTESTS
	else if (configuration[CONF_APPLICATION_NAME] == "vptests") {
		return GetApplication_vptests(configuration);
	}
#endif
#ifdef HAS_APP_VMAPP
	else if (configuration[CONF_APPLICATION_NAME] == "vmapp") {
		return GetApplication_vmapp(configuration);
	}
#endif
	else {
		return NULL;
	}
}

BaseProtocolFactory *SpawnFactory(Variant configuration) {
	if (false) {
	}
#ifdef HAS_APP_SAMPLEFACTORY
	else if (configuration[CONF_APPLICATION_NAME] == "samplefactory") {
		return GetFactory_samplefactory(configuration);
	}
#endif
	else {
		return NULL;
	}
}
#endif

void QuitSignalHandler(void)
{
	IOHandlerManager::SignalShutdown();
}

CRTMPCore * CRTMPCore::getInstance()
{
	LockHolder lock(g_lock);
	if (mSInstance == nullptr)
	{
		mSInstance = new CRTMPCore();
	}
	return mSInstance;
}

bool CRTMPCore::AddChannel(uint32_t dwChlID, std::string strCameraName, IRTMPDataReceiver *pCallBack)
{
	if (mSInstance->m_bRunning)
	{
		INFO("CRTMPCore add camera(%s).", strCameraName.c_str());
		ClientApplicationManager::addCamera(strCameraName, pCallBack);
		return true;
	}
	char strModuleFileName[MAX_PATH];
	char strDriver[MAX_PATH];
	char strPath[MAX_PATH];
	GetModuleFileNameA(nullptr, strModuleFileName, MAX_PATH);
	_splitpath_s(strModuleFileName, strDriver, size(strDriver), strPath, size(strPath), nullptr, 0, nullptr, 0);
	strcat_s(strDriver, size(strDriver), strPath);
	std::string strtemp = strDriver;
	strtemp += "SimplyLive.lua";
	gRs.commandLine["arguments"]["configFile"] = strtemp;

	Logger::Init();
	if ((bool)gRs.commandLine["arguments"]["--use-implicit-console-appender"]) {
		Variant dummy;
		dummy[CONF_LOG_APPENDER_NAME] = "implicit console appender";
		dummy[CONF_LOG_APPENDER_TYPE] = CONF_LOG_APPENDER_TYPE_CONSOLE;
		dummy[CONF_LOG_APPENDER_COLORED] = (bool)true;
		dummy[CONF_LOG_APPENDER_LEVEL] = (uint32_t)6;
		ConsoleLogLocation * pLogLocation = new ConsoleLogLocation(dummy);
		pLogLocation->SetLevel(_FINEST_);
		Logger::AddLogLocation(pLogLocation);
	}

	INFO("Reading configuration from %s", STR(gRs.commandLine["arguments"]["configFile"]));
	gRs.pConfigFile = new ConfigFile(SpawnApplication, SpawnFactory);
	string configFilePath = gRs.commandLine["arguments"]["configFile"];
	string fileName;
	string extension;
	splitFileName(configFilePath, fileName, extension);
	if (lowerCase(extension) == "lua")
	{
#ifdef HAS_LUA
		if (!gRs.pConfigFile->LoadLuaFile(configFilePath,
			(bool)gRs.commandLine["arguments"]["--daemon"]))
		{
			FATAL("Unable to load file %s", STR(configFilePath));
			return false;
		}
#else
#endif
	}

	INFO("Configure logger");
	if (!gRs.pConfigFile->ConfigLogAppenders()) {
		FATAL("Unable to configure log appenders");
		return false;
	}

	INFO("Initialize I/O handlers manager: %s", NETWORK_REACTOR);
	IOHandlerManager::Initialize();

	INFO("Configure modules");
	if (!gRs.pConfigFile->ConfigModules()) {
		FATAL("Unable to configure modules");
		return false;
	}

	INFO("Plug in the default protocol factory");
	gRs.pProtocolFactory = new DefaultProtocolFactory();
	if (!ProtocolFactoryManager::RegisterProtocolFactory(gRs.pProtocolFactory)) {
		FATAL("Unable to register default protocols factory");
		return false;
	}

	INFO("Configure factories");
	if (!gRs.pConfigFile->ConfigFactories()) {
		FATAL("Unable to configure factories");
		return false;
	}

	INFO("Configure acceptors");
	if (!gRs.pConfigFile->ConfigAcceptors()) {
		FATAL("Unable to configure acceptors");
		return false;
	}

	INFO("Configure instances");
	if (!gRs.pConfigFile->ConfigInstances()) {
		FATAL("Unable to configure instances");
		return false;
	}

	INFO("Start I/O handlers manager: %s", NETWORK_REACTOR);
	IOHandlerManager::Start();

	INFO("Configure applications");
	if (!gRs.pConfigFile->ConfigApplications()) {
		FATAL("Unable to configure applications");
		return false;
	}
	INFO("CRTMPCore add camera(%s).", strCameraName.c_str());
	ClientApplicationManager::addCamera(strCameraName, pCallBack);

	INFO("Install the quit signal");
	installQuitSignal(QuitSignalHandler);

	mSInstance->m_bRunning = true;
	mSInstance->m_rtmpCoreResult = async(launch::async, &CRTMPCore::_rtmpCoreThread, mSInstance);
	return true;
}

void CRTMPCore::releaseInstance()
{
	LockHolder lock(g_lock);
	if (mSInstance)
	{
		delete mSInstance;
		mSInstance = nullptr;
	}
}

CRTMPCore::CRTMPCore()
{
	m_bRunning = false;
	InitNetworking();
}

CRTMPCore::~CRTMPCore()
{
	m_bRunning = false;
	if (m_rtmpCoreResult.valid())
		m_rtmpCoreResult.wait();
}

void CRTMPCore::_rtmpCoreThread()
{
	//INFO("\n%s", STR(gRs.pConfigFile->GetServicesInfo()));
	//INFO("GO! GO! GO! (%u)", (uint32_t)_getpid());
	INFO("CRTMPCore CoreThread is running.");
	while (IOHandlerManager::Pulse() && m_bRunning)
	{
		IOHandlerManager::DeleteDeadHandlers();
		ProtocolManager::CleanupDeadProtocols();
	}
	Cleanup();
}

void CRTMPCore::Cleanup()
{
	WARN("Shutting down protocols manager");
	ProtocolManager::Shutdown();
	ProtocolManager::CleanupDeadProtocols();

	WARN("Shutting down I/O handlers manager");
	IOHandlerManager::ShutdownIOHandlers();
	IOHandlerManager::DeleteDeadHandlers();
	IOHandlerManager::Shutdown();

	WARN("Unregister and delete default protocol handler");
	ProtocolFactoryManager::UnRegisterProtocolFactory(gRs.pProtocolFactory);
	delete gRs.pProtocolFactory;
	gRs.pProtocolFactory = NULL;

	WARN("Shutting down applications");
	ClientApplicationManager::Shutdown();

	WARN("Delete the configuration");
	delete gRs.pConfigFile;
	gRs.pConfigFile = NULL;

	WARN("Doing final OpenSSL cleanup");
	CleanupSSL();

	WARN("CRTMPCore Shutting down.");
	Logger::Free(true);
}