#pragma once
#include <string>
#include <future>
#include <stdint.h>
#include "../lib.RTMPCore/IRTMPDataReceiver.h"

class CRTMPCore
{
public:
	static CRTMPCore * getInstance();
	bool	AddChannel(uint32_t dwChlID, std::string strCameraName, IRTMPDataReceiver *pCallBack);
	static void		   releaseInstance();
protected:
	CRTMPCore();
	~CRTMPCore();

	static void		   Cleanup();

	bool m_bRunning;
	std::future<void> m_rtmpCoreResult;
	void _rtmpCoreThread();
};

