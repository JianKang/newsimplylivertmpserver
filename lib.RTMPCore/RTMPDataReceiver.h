#pragma once
#include <string>
#include <map>
#include <future>
using namespace std;
#include "../Lib.Base/locker.h"
#include "../Lib.Base/FrameList.h"
#include "../Lib.Base/BMRaudioFrame.h"
#include "../Lib.Base/compVideoFrame.h"
#include "../Lib.Base/SemaphoreClock.h"

#include "../Lib.Base/ObjectPoolEx.h"
#include <windows.h>
#include "INaluUnitCallBack.h"
#include "RTMPDataDecoder.h"
#include "IRTMPDataReceiver.h"
#include "IDecodedDataCallBack.h"
#include "LoopBuffer.h"
#include "../DecodeH264/DecodeH264.h"

struct AVFrame;
struct AVCodecContext;
struct AVCodec;
struct AVCodecParserContext;

class CRTMPDataReceiver : public INaluUnitCallBack,public IRTMPDataReceiver
{
public:
	CRTMPDataReceiver(IDecodedDataCallBack *pCallBack,TCHAR *szLogFile);
	~CRTMPDataReceiver();
	//IRTMPDataReceiver
	bool  InitChannel(uint32_t dwChannelID,string strName) override;
	void  RemoveChannel() override;
	void  streamStart(uint32_t dwChlID, string strName) override;
	void  addRTMPData(uint32_t dwChlID, string strName, uint8_t *pData, uint32_t dataLength,
		uint32_t processedLength, uint32_t totalLength, double absoluteTimestamp, bool isAudio) override;
	void  streamStop(uint32_t dwChlID, string strName) override;
	//INaluUnitCallBack
	void InitAACDecoder(int nCount) override;
	void cb(RTMP_RAW_Unit &naluUnit) override;
protected:
	TCHAR  *m_szLogFile;
	string m_strCamName;
	void  _stop();
	IDecodedDataCallBack* m_pCallBack;
	RTMPDataDecoder m_rtmpDatadec;

	AVFrame *pFrameVideo_ = NULL;
	AVCodecContext *codec_ContextVideo = NULL;
	AVCodecParserContext *pCodecParserVideoCtx = NULL;
	AVCodec *videoCodec = NULL;

	DecodeH264* m_pVideoDecoder;
	AVFrame *pFrameAudio_ = NULL;
	AVCodecContext *codec_ContextAudio = NULL;
	AVCodecParserContext *pCodecParserAudioCtx = NULL;
	AVCodec *audioCodec = NULL;
	DWORD m_nChannelCount;
	bool m_bStopVideoDec;
	bool m_bStopAudioDec;

	future<void> m_decVideoResult;
	void _decVideoThread();

	future<void> m_decAudioResult;
	void _decAudioThread();

	CLoopBuffer m_loopBufferVideo;
	CLoopBuffer m_loopBufferAudio;

	const int MAX_AUDIO_SIZE = 4 * 1024 * 1024;
	const int MAX_VIDEO_SIZE = 10 * 1024 * 1024;
	const int TEMP_AUDIO_SIZE = 1024;

	BYTE *m_pbufVideoTEMP;
	BYTE *m_pbufAudioTEMP;

	void PushAudio(BYTE *pAudio, int nSize);
	void PushVideo(BYTE *pVideo, int nSize);

	int _fetchVideoData(uint8_t* buf, int size);
	int _fetchAudioData(uint8_t* buf, int size);

	BYTE headerVideo[4] = { 0x00,0x00,0x00,0x01 };
	BYTE headerAudio[7] = { 0xFF,0xF1,0x00,0x00,0x00,0x00,0xFC };

	const static DWORD                        m_dwVideoPoolSize = 150;
	const static DWORD                        m_dwAudioPoolSize = 250;
	FrameList<CompVideoFrame>              m_decVideoList;
	FrameList<BMRaudioFrame>               m_decAudioList;

	SemaphoreClock	m_audioClock;
	SemaphoreClock	m_videoClock;
};

