#pragma once
#include <stdint.h>
#include <functional>
#include <future>
#include <queue>

#include "../lib.Base/audioFrame.h"
#include "../lib.Base/VideoFrame.h"
#include "../lib.Base/SemaphoreClock.h"
#include "IFPInputCallBack.h"
#include "LoopBuffer.h"
#include "IDecodedDataCallBack.h"
#include "../lib.Base/FrameList.h"
#include "../Lib.Base/platform.h"

extern "C"
{
	#include "libswresample/swresample.h"
};
class CMediaStream : public IDecodedDataCallBack
{
public:
	CMediaStream(DWORD dwCnlID, const string &strUrl, IFPInputCallBack* _pGetFrameCB, TCHAR *pLogPath);
	~CMediaStream();

	//IDecodedDataCallBack
	void start() override;
	void cbVideo(AVFrame *pVFrame_) override;
	void cbVideo(VideoFrame *_uncompFrame) override;
	void cbAudio(AVFrame *pVFrame_,uint32_t aac_sample_hz_, uint8_t aac_channels_) override;
	void cbAudio(const uint8_t*pdata, int len, uint32_t ts, uint32_t aac_sample_hz_, uint8_t aac_channels_) override;
	void frameConsumed() override;
	void stop() override;
private:
	double m_startAPts;
	double m_startVPts;
	double m_dStepValue;
	bool	m_syncCompelted;
	int checkHasEnoughAudio();
	void InitAudioConverter(uint32_t aac_sample_hz_, uint8_t aac_channels_, AVSampleFormat format);
	bool m_bStreaming = false;
	int _PushData_once();
	bool compareFloat(float a, float b);
	IFPInputCallBack* m_pGetFrameCB;
	future<void> m_syncResult;
	void _syncAVThread();
	bool m_bStop = false;
	TCHAR	 *m_szLogFile;


	DWORD	m_dwChannelID;
	uint64_t m_nAudioIndex = 0;
	uint64_t m_nPushedFrames = 0;
	uint64_t m_nGotVFrames = 0;
	const int *m_samplesArr = nullptr;
	int m_samplesArr_len = 0;

	SemaphoreClock m_clock;	
	CLoopBuffer m_loopBuffer;
	int ReadData_16chs_32bits(int *pDestBuffer, int nperChannelSample);

	//FrameList<VideoFrame>  m_VideoFrameList;
	struct SwsContext *m_img_convert_ctx;
	AVFrame *m_pAVFrameYUV = nullptr;

	//FrameList<AudioFrame>                 m_audioFrameList;
	struct SwrContext *au_convert_ctx = nullptr;
	int m_out_nb_samples = 0;
	int m_out_sample_rate = 0;
	int m_out_channels = 0;
	int m_nChannelCnt = 0;
	int m_in_nb_samples = 0;
	int m_in_sample_rate = 0;
	AVSampleFormat m_out_sample_fmt = AV_SAMPLE_FMT_S32;
	int m_nAudioBufferSize = 0;
	uint8_t*m_pAudioDestBuffer = nullptr;

	Locker	m_videoLocker;
	Locker	m_audioLocker;
	std::queue<VideoFrame *>		m_queueVideo;
	std::queue<AudioFrame *>		m_queueAudio;
	UINT64	m_curLoop;
	DWORD	m_dwMaxQueFrames = 15;
	const DWORD	m_dwAudioBufSize = 10*1024*1024;
	cpuFreq m_freq;
	UINT64 m_tickPerFrame;
	cpuTick m_lastTick;

	UINT64 m_audioSampleCnt;
	UINT64 m_audioSampleSize;
};
