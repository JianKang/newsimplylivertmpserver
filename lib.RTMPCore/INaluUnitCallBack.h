#pragma once

typedef struct _RTMP_RAW_Unit
{
	bool bAddHeader;
	bool bAudio;
	int type;
	int size;
	double pts;
	unsigned char* data;	
} RTMP_RAW_Unit;

__interface INaluUnitCallBack
{
	void InitAACDecoder(int nCount);
	void cb(RTMP_RAW_Unit &naluUnit);
};
