#pragma once
#include <stdint.h>
struct AVFrame;
class VideoFrame;

class IDecodedDataCallBack
{
public:
	IDecodedDataCallBack() {};
	virtual ~IDecodedDataCallBack() {}
	virtual void start() = 0;
	virtual void cbVideo(AVFrame *pVFrame_) = 0;
	virtual void cbVideo(VideoFrame *_uncompFrame) = 0;
	virtual void cbAudio(AVFrame *pVFrame_,uint32_t aac_sample_hz_, uint8_t aac_channels_) = 0;
	virtual void cbAudio(const uint8_t*pdata, int len, uint32_t ts, uint32_t aac_sample_hz_, uint8_t aac_channels_) = 0;
	virtual void frameConsumed() = 0;
	virtual void stop() = 0;	
};
