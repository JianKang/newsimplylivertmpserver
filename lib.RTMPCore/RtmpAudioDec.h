#pragma once
#include <stdio.h>

#define __STDC_CONSTANT_MACROS

#ifdef _WIN32
//Windows
extern "C"
{
	#include "libavformat/avformat.h"
	#include "libswresample/swresample.h"
	#include "libavutil/opt.h"
};
#else
//Linux...
#ifdef __cplusplus
extern "C"
{
#endif
#include <libavformat/avformat.h>
#ifdef __cplusplus
};
#endif
#endif
//#include "LoopBuffer.h"
#include "../../lib.Base/audioFrame.h"
#include "../../lib.Base/FrameList.h"

#define MAX_AUDIO_FRAME_SIZE 192000 // 1 second of 48khz 32bit audio
#define MAX_AUDIO_CNT     96

#include <list>
#include <future>

class CRtmpAudioDec
{

	AVCodecContext  *m_pDec_ctx = nullptr;
	AVCodec *m_pAVCodec = nullptr;
	AVFrame *m_pAVFrame = nullptr;

	bool m_bStop = false;
	int m_nChannelCnt = 0;
	char m_out_filename[255];
	uint8_t*m_pDestBuffer = nullptr;
	
	int m_out_nb_samples = 0;
	int m_out_sample_rate = 0;
	int m_out_channels = 0;

	int m_in_nb_samples = 0;
	int m_in_sample_rate = 0;
	AVSampleFormat m_out_sample_fmt = AV_SAMPLE_FMT_S32;
public:
	UINT64 m_nReadedSize = 0;

public:
	CRtmpAudioDec();
	~CRtmpAudioDec();

	bool isCurrentStream(int _streamIdx) {
		return m_streamIdx == _streamIdx;
	}
	struct SwrContext *au_convert_ctx = nullptr;
	int buildAudioDec(int _idx, AVCodecID id, int sample_rate, int channels, AVSampleFormat sample_fmt, TCHAR *pLogPath);
	int buildAudioDec(int _idx, AVCodecContext*_pCtx,TCHAR *pLogPath);
	int ReadData_16chs_32bits(int *pDestBuffer, int nperChannelSample);
	bool IsDataEnough(int nperChannelSample);
	int GetAudioChannelCnt() { return m_nChannelCnt; }
	UINT64 nDecodedCnt = 0;
	int m_nPktCount = 0;
	int m_nBufferSize = 0;
	int m_streamIdx = -1;
	int Decode_Audio(const AVPacket *pkt);
	bool Flush_data(const AVPacket *pkt);
	//CLoopBuffer m_loopBuffer;
	FrameList<AudioFrame>                 m_audioFrameList;
	void CloseDec();

	TCHAR *m_szLogFile;
	Locker locker;
	void addPacket(AVPacket *pkt);
	std::list<AVPacket *>	lst_audio_buffer_;
	future<void> m_hdecthread;
	void _decThread();
};

