#include "MediaStream.h"
#include "../../lib.Base/CapturePoolMgr.h"
#include "../../lib.Base/TimeCount.h"
#include "AudioSampleHeader.h"
#include "../../lib.Logger/LogWriter.h"
#include "../Lib.Base/Config.h"
#include "../Lib.Base/VFrame_1280_720.h"
#include "../Lib.Base/VFrame_1920_1080.h"
#include "../Lib.Base/VFrame_3840_2160.h"

#define __STDC_CONSTANT_MACROS

#ifdef _WIN32
//Windows
extern "C"
{
#include "libavformat/avformat.h"
#include "libswresample/swresample.h"
#include "libavutil/opt.h"
#include "libswscale/swscale.h" 
#include "libavutil/imgutils.h"  
};
#else
//Linux...
#ifdef __cplusplus
extern "C"
{
#endif
#include <libavformat/avformat.h>
#ifdef __cplusplus
};
#endif
#endif

CMediaStream::CMediaStream(DWORD dwCnlID, const string &strUrl, IFPInputCallBack* _pGetFrameCB, TCHAR *pLogPath):m_pGetFrameCB(_pGetFrameCB)
{
	m_curLoop = 0;
	m_samplesArr = FFMPEG_AUDIOSAMPLE::getFrameSamples(Config::getInstance()->getVideoFormat(), m_samplesArr_len);
	m_img_convert_ctx = nullptr;
	au_convert_ctx = nullptr;
	av_log_set_level(AV_LOG_QUIET);
	m_dwChannelID = dwCnlID;
	m_szLogFile = pLogPath;
	m_nAudioIndex = 0;
	m_nPushedFrames = 0;
	m_nGotVFrames = 0;
	m_lastTick = 0;
	m_startAPts = 0;
	m_startVPts = 0;
	m_audioSampleCnt = 0;
	m_audioSampleSize = 0;
	m_syncCompelted = false;
	m_loopBuffer.InitBuffer(m_dwAudioBufSize);

	m_dStepValue = (double)Config::getInstance()->getFrameRateDen() * (double)1000 / (double)Config::getInstance()->getFrameRateNum();
	getCPUfreq(m_freq);
	m_dwMaxQueFrames = Config::getInstance()->getFramesPerSec();
	m_tickPerFrame = m_freq / Config::getInstance()->getFramesPerSec();
	m_lastTick = 0;
	m_pAVFrameYUV = av_frame_alloc();
	m_bStop = false;
	m_syncResult = async(launch::async, &CMediaStream::_syncAVThread, this);
}

CMediaStream::~CMediaStream()
{
	m_bStop = true;
	if (m_syncResult.valid())
		m_syncResult.wait();
	m_loopBuffer.UnInitBuffer();
}

void CMediaStream::start()
{
	m_bStreaming = true;
	m_nAudioIndex = 0;
	m_nPushedFrames = 0;
	m_nGotVFrames = 0;
	m_lastTick = 0;
	m_startAPts = 0;
	m_startVPts = 0;
	m_audioSampleCnt = 0;
	m_audioSampleSize = 0;
	m_syncCompelted = true;
	//if (Config::getInstance()->getRtmpInNeedSync())
	//	m_syncCompelted = false;
	m_loopBuffer.RestBufferPos();
	WriteLogA(m_szLogFile, InfoLevel, "CMediaStream::start() Done.");
}

bool CMediaStream::compareFloat(float a, float b)
{
#define EPSILON 0.01
	float dd = fabs(a - b);
	if (fabs(a - b) <= EPSILON)
		return true;
	return false;
}

void CMediaStream::_syncAVThread()
{
	WriteLogA(m_szLogFile, InfoLevel, "CMediaStream::_syncAVThread start.");
	while (!m_bStop)
	{
		if (!m_clock.waitEvent())
			continue;
		if (m_bStreaming)
		{
			checkHasEnoughAudio();
			_PushData_once();
		}
	}
	WriteLogA(m_szLogFile, InfoLevel, "CMediaStream::_syncAVThread stop.");
}

void CMediaStream::cbVideo(AVFrame * pVFrame_)
{
	if (m_nGotVFrames == 0)
	{
		m_startVPts = pVFrame_->pts;
		WriteLogA(m_szLogFile, InfoLevel, "CMediaStream::cbVideo startVPts(%.2f).", m_startVPts);
	}
	VideoFrame *_uncompFrame = nullptr;
	CapturePoolMgr::GetInstance()->getNew(_uncompFrame, Config::getInstance()->getVideoFormat());
	if (_uncompFrame == nullptr)
	{
		WriteLogA(m_szLogFile, WarnLevel, "CMediaStream::cbVideo Video Frame List is empty.");
		return;
	}
	if (m_img_convert_ctx == nullptr)
	{
		m_img_convert_ctx = sws_getContext(pVFrame_->width, pVFrame_->height, (AVPixelFormat)pVFrame_->format,
			pVFrame_->width, pVFrame_->height, AV_PIX_FMT_UYVY422, SWS_BICUBIC, nullptr, nullptr, nullptr);
	}
	av_image_fill_arrays(m_pAVFrameYUV->data, m_pAVFrameYUV->linesize, _uncompFrame->getRaw(),
		AV_PIX_FMT_UYVY422, pVFrame_->width, pVFrame_->height, 1);

	sws_scale(m_img_convert_ctx, (const unsigned char* const*)pVFrame_->data, pVFrame_->linesize, 0, pVFrame_->height,
		m_pAVFrameYUV->data, m_pAVFrameYUV->linesize);

	_uncompFrame->setPts(pVFrame_->pts);
	LockHolder lock(m_videoLocker);
	m_queueVideo.push(_uncompFrame);

	++m_nGotVFrames;
}

void CMediaStream::cbVideo(VideoFrame *_uncompFrame)
{
	//WriteLogA(m_szLogFile, InfoLevel, "CMediaStream::cbVideo pts(%.2f) GotVFrames(%I64d)", pts, m_nGotVFrames);
	if (m_nGotVFrames == 0)
	{
		m_startVPts = _uncompFrame->getPts();
		WriteLogA(m_szLogFile, InfoLevel, "CMediaStream::cbVideo startVPts(%.2f).", m_startVPts);
	}	
	LockHolder lock(m_videoLocker);
	m_queueVideo.push(_uncompFrame);
	++m_nGotVFrames;
}

void CMediaStream::cbAudio(AVFrame * pVFrame_, uint32_t aac_sample_hz_, uint8_t aac_channels_)
{
	//WriteLogA(m_szLogFile, InfoLevel, "CMediaStream::cbAudio DonePTS(%I64d)", pVFrame_->pts);
	if (m_audioSampleCnt == 0)
	{
		m_startAPts = pVFrame_->pts;
		WriteLogA(m_szLogFile, InfoLevel, "CMediaStream::cbAudio startAPts(%.2f).", m_startAPts);
	}
	if (au_convert_ctx == nullptr)
		InitAudioConverter(aac_sample_hz_, aac_channels_, (AVSampleFormat)pVFrame_->format);
	if (aac_channels_ != m_nChannelCnt)
	{
		WriteLogA(m_szLogFile, WarnLevel, "CMediaStream::cbAudio receive audio channels(%d).src channels (%d)", aac_channels_, m_nChannelCnt);
		InitAudioConverter(aac_sample_hz_, aac_channels_, (AVSampleFormat)pVFrame_->format);;
	}

	int dst_lineSize = 0;
	if (m_nAudioBufferSize <= 0)
	{
		m_out_nb_samples = av_rescale_rnd(swr_get_delay(au_convert_ctx, m_in_sample_rate) + pVFrame_->nb_samples,
			m_out_sample_rate, m_in_sample_rate, AV_ROUND_UP);

		av_samples_alloc(&m_pAudioDestBuffer, &dst_lineSize, m_out_channels, m_out_nb_samples, m_out_sample_fmt, 1);
	}
	int nbConvert = swr_convert(au_convert_ctx, &m_pAudioDestBuffer, m_out_nb_samples, (const uint8_t **)pVFrame_->data, pVFrame_->nb_samples);
	if (m_nAudioBufferSize <= 0)
	{
		m_nAudioBufferSize = av_samples_get_buffer_size(&dst_lineSize, m_out_channels, nbConvert, m_out_sample_fmt, 1);
	}
	if (m_nAudioBufferSize > 0)
	{
		int nRes = m_loopBuffer.WriteBuffer((LPBYTE)m_pAudioDestBuffer, m_nAudioBufferSize);
		if (nRes != m_nAudioBufferSize)
			WriteLogA(m_szLogFile, ErrorLevel, "CMediaStream::cbAudio WriteBuffer size(%d) SrcSize(%d)", nRes, m_nAudioBufferSize);
	}
	m_audioSampleCnt += m_out_nb_samples;
	m_audioSampleSize += m_nAudioBufferSize;
	//WriteLogA(m_szLogFile, InfoLevel, "CMediaStream::cbAudio size(%d) audioSampleSize(%I64d) sampleCount(%I64d)",
	//	m_nAudioBufferSize, m_audioSampleSize, m_audioSampleCnt);
}

void CMediaStream::cbAudio(const uint8_t * pdata, int len, uint32_t ts, uint32_t aac_sample_hz_, uint8_t aac_channels_)
{
	if (au_convert_ctx == nullptr)
	{
		m_nChannelCnt = aac_channels_;
		uint64_t in_channel_layout = av_get_default_channel_layout(aac_channels_);
		uint64_t out_channel_layout = in_channel_layout;

		m_out_sample_rate = 48000;
		m_in_sample_rate = aac_sample_hz_;
		m_out_nb_samples = 0;

		m_out_channels = av_get_channel_layout_nb_channels(out_channel_layout);
		if (out_channel_layout == 0)
			m_out_channels = aac_channels_;
		m_out_sample_fmt = AV_SAMPLE_FMT_S32;

		au_convert_ctx = swr_alloc();
		av_opt_set_int(au_convert_ctx, "ich", aac_channels_, 0);
		av_opt_set_int(au_convert_ctx, "in_sample_rate", aac_sample_hz_, 0);
		av_opt_set_sample_fmt(au_convert_ctx, "in_sample_fmt", AV_SAMPLE_FMT_S16, 0);//??

		av_opt_set_int(au_convert_ctx, "och", aac_channels_, 0);
		av_opt_set_int(au_convert_ctx, "out_sample_rate", m_out_sample_rate, 0);
		av_opt_set_sample_fmt(au_convert_ctx, "out_sample_fmt", m_out_sample_fmt, 0);

		swr_init(au_convert_ctx);
	}
	int nSrcSamples = len / (sizeof(int16_t) * aac_channels_);

	int dst_lineSize = 0;
	if (m_nAudioBufferSize <= 0)
	{
		m_out_nb_samples = av_rescale_rnd(swr_get_delay(au_convert_ctx, m_in_sample_rate) + nSrcSamples,
			m_out_sample_rate, m_in_sample_rate, AV_ROUND_UP);

		av_samples_alloc(&m_pAudioDestBuffer, &dst_lineSize, m_out_channels, m_out_nb_samples, m_out_sample_fmt, 1);
	}
	int nbConvert = swr_convert(au_convert_ctx, &m_pAudioDestBuffer, m_out_nb_samples, (const uint8_t **)&pdata, nSrcSamples);
	if (m_nAudioBufferSize <= 0)
		m_nAudioBufferSize = av_samples_get_buffer_size(&dst_lineSize, m_out_channels, nbConvert, m_out_sample_fmt, 1);

	if (m_nAudioBufferSize > 0)
	{
		int nRes = m_loopBuffer.WriteBuffer((LPBYTE)m_pAudioDestBuffer, m_nAudioBufferSize);
		if (nRes != m_nAudioBufferSize)
			WriteLogA(m_szLogFile, ErrorLevel, "CMediaStream::cbAudio WriteBuffer size(%d) SrcSize(%d)", nRes, m_nAudioBufferSize);

	}
}

void CMediaStream::frameConsumed()
{
	++m_curLoop;
	m_clock.raiseEvent();
}

void CMediaStream::stop()
{
	m_bStreaming = false;
	while (!m_queueAudio.empty())
	{
		LockHolder lock(m_audioLocker);
		AudioFrame* _aFrame = m_queueAudio.front();
		m_queueAudio.pop();
		CapturePoolMgr::GetInstance()->release(_aFrame);
	}

	while (!m_queueVideo.empty())
	{
		LockHolder lock(m_videoLocker);
		VideoFrame *_uncompFrame = m_queueVideo.front();
		CapturePoolMgr::GetInstance()->release(_uncompFrame);
		m_queueVideo.pop();
	}
}

int CMediaStream::checkHasEnoughAudio()
{
	if (m_audioSampleCnt <= 0)
		return -1;
	int offset = m_nAudioIndex%m_samplesArr_len;
	int nNeedAudioSize_perChannels = m_samplesArr[offset] * 4;
	int nChannelCount = m_nChannelCnt;
	int nNeedSize = nChannelCount*nNeedAudioSize_perChannels;
	int nBufSize = m_loopBuffer.GetSizeOfDataList();
	if (nNeedSize > nBufSize)
	{
		WriteLogA(m_szLogFile, WarnLevel, "CMediaStream::checkHasEnoughAudio IsDataEnough need(%d),audioBufSize(%d),dwLoop(%I64d)",
			nNeedSize, nBufSize, m_curLoop);
		return -2;
	}
	AudioFrame* _aFrame = nullptr;
	CapturePoolMgr::GetInstance()->getNew(_aFrame);
	if (nullptr == _aFrame)
	{
		WriteLogA(m_szLogFile, WarnLevel, "CMediaStream::checkHasEnoughAudio getNew audio failed(%I64d). dwLoop(%I64d)", m_audioSampleCnt, m_curLoop);
		LockHolder lock(m_audioLocker);
		int nSize = m_queueAudio.size();
		for (int n = 0; n < nSize / 2; ++n)
		{
			_aFrame = m_queueAudio.front();
			m_queueAudio.pop();
			CapturePoolMgr::GetInstance()->release(_aFrame);
		}
		return -3;
	}
	int*pAudioDest = (int*)_aFrame->getRaw();
	_aFrame->setBufferSize(nNeedAudioSize_perChannels * 16);
	double pts = m_startAPts + m_dStepValue*(double)m_nAudioIndex;
	_aFrame->setPts(pts);
	memset(pAudioDest, 0, nNeedAudioSize_perChannels * 16);
	for (int i = 0; i < 1; i++)
	{
		ReadData_16chs_32bits(pAudioDest, nNeedAudioSize_perChannels);
		pAudioDest += m_nChannelCnt;
	}
	LockHolder lock(m_audioLocker);
	m_queueAudio.push(_aFrame);
	++m_nAudioIndex;
	return 0;
}

void CMediaStream::InitAudioConverter(uint32_t aac_sample_hz_, uint8_t aac_channels_, AVSampleFormat format)
{
	if (au_convert_ctx != nullptr)
		swr_free(&au_convert_ctx);
	m_nChannelCnt = aac_channels_;
	uint64_t in_channel_layout = av_get_default_channel_layout(aac_channels_);
	uint64_t out_channel_layout = in_channel_layout;

	m_out_sample_rate = 48000;
	m_in_sample_rate = aac_sample_hz_;
	m_out_nb_samples = 0;

	m_out_channels = av_get_channel_layout_nb_channels(out_channel_layout);
	if (out_channel_layout == 0)
		m_out_channels = aac_channels_;
	m_out_sample_fmt = AV_SAMPLE_FMT_S32;

	au_convert_ctx = swr_alloc();
	av_opt_set_int(au_convert_ctx, "ich", aac_channels_, 0);
	av_opt_set_int(au_convert_ctx, "in_sample_rate", aac_sample_hz_, 0);
	av_opt_set_sample_fmt(au_convert_ctx, "in_sample_fmt", format, 0);//??

	av_opt_set_int(au_convert_ctx, "och", aac_channels_, 0);
	av_opt_set_int(au_convert_ctx, "out_sample_rate", m_out_sample_rate, 0);
	av_opt_set_sample_fmt(au_convert_ctx, "out_sample_fmt", m_out_sample_fmt, 0);

	swr_init(au_convert_ctx);
	m_nAudioBufferSize = 0;
}

int CMediaStream::_PushData_once()
{
	if (m_audioSampleCnt <= 0 || m_nGotVFrames <= 0)
		return -1;
	AudioFrame* _aFrame = nullptr;
	VideoFrame* _uncompFrame = nullptr;
	int nAudioSize = m_queueAudio.size();
	int nVideoSize = m_queueVideo.size();

	if (nAudioSize == 0 || nVideoSize == 0)
	{
		WriteLogA(m_szLogFile, WarnLevel, "CMediaStream::_PushData_once failed.Video(%d) Audio(%d) dwLoop(%I64d)", nVideoSize, nAudioSize, m_curLoop);
		return -1;
	}
	if (!m_syncCompelted)
	{
		double pts = 0;
		WriteLogA(m_szLogFile, InfoLevel, "CMediaStream::_PushData_once.VideoPts(%.2f) AudioPts(%.2f) dwLoop(%I64d)",
			m_startVPts, m_startAPts, m_curLoop);
		if (m_startVPts > m_startAPts)
		{
			LockHolder lock(m_audioLocker);
			while (!m_queueAudio.empty())
			{
				_aFrame = m_queueAudio.front();
				pts = _aFrame->getPts();
				if (pts >= m_startVPts)
				{
					m_syncCompelted = true;
					WriteLogA(m_szLogFile, InfoLevel, "CMediaStream::_PushData_once.Audio Sync completed.pts(%.2f)", pts);
					break;
				}
				else
				{
					WriteLogA(m_szLogFile, InfoLevel, "CMediaStream::_PushData_once.Drop Audio pts(%.2f)", pts);
					m_queueAudio.pop();
					CapturePoolMgr::GetInstance()->release(_aFrame);
				}
			}
		}
		else
		{
			LockHolder lock(m_videoLocker);
			while (!m_queueVideo.empty())
			{
				_uncompFrame = m_queueVideo.front();
				pts = _uncompFrame->getPts();
				if (pts >= m_startAPts)
				{
					m_syncCompelted = true;
					WriteLogA(m_szLogFile, InfoLevel, "CMediaStream::_PushData_once.Video Sync completed.Current(%.2f)", pts);
					break;
				}
				else
				{
					WriteLogA(m_szLogFile, InfoLevel, "CMediaStream::_PushData_once.Drop Video pts(%.2f)", pts);
					CapturePoolMgr::GetInstance()->release(_uncompFrame);
					m_queueVideo.pop();
				}
			}
		}
	}
	if (!m_syncCompelted)
		return -1;
	if (nAudioSize >= m_dwMaxQueFrames && nVideoSize >= m_dwMaxQueFrames)
	{
		{
			LockHolder lock(m_videoLocker);
			_uncompFrame = m_queueVideo.front();
			m_queueVideo.pop();
		}
		{
			LockHolder lock(m_audioLocker);
			_aFrame = m_queueAudio.front();
			m_queueAudio.pop();
		}
		CapturePoolMgr::GetInstance()->release(_uncompFrame);
		CapturePoolMgr::GetInstance()->release(_aFrame);

		WriteLogA(m_szLogFile, WarnLevel, "CMediaStream::_PushData_once Video(%d) Audio(%d) RemoveTop(%I64d) dwLoop(%I64d)",
			nVideoSize, nAudioSize, m_nPushedFrames, m_curLoop);
	}
	{
		LockHolder lock(m_videoLocker);
		_uncompFrame = m_queueVideo.front();
		m_queueVideo.pop();
	}
	{
		LockHolder lock(m_audioLocker);
		_aFrame = m_queueAudio.front();
		m_queueAudio.pop();
	}
	m_pGetFrameCB->cb(m_dwChannelID, _uncompFrame,/*nullptr,nullptr,*/_aFrame);
	if (++m_nPushedFrames % 1000 == 0)
	{
		nAudioSize = m_queueAudio.size();
		nVideoSize = m_queueVideo.size();
		WriteLogA(m_szLogFile, InfoLevel, "CMediaStream::_PushData_once VideoSize(%d) AudioSize(%d) PushedFrames(%I64d)",
			nVideoSize, nAudioSize, m_nPushedFrames);
	}
	return 0;
}

int CMediaStream::ReadData_16chs_32bits(int *pDestBuffer, int nperChannelSample)
{
	unsigned char*pp = (unsigned char *)pDestBuffer;
	int *pDest = pDestBuffer;

	int nNeedAudioSize = m_nChannelCnt;
	for (int i = 0; i < nperChannelSample / 4; i++)
	{
		int nReadSize = m_loopBuffer.ReadBuffer((unsigned char *)pDest, nNeedAudioSize * 4);
		if (nReadSize != (nNeedAudioSize) * 4)
			WriteLogA(m_szLogFile, WarnLevel, "CMediaStream::ReadData_16chs_32bits error nReadSize(%d) nNeedAudioSize(%d).", nReadSize, nNeedAudioSize * 4);
		pDest += 16;
	}
	return nNeedAudioSize;
}
