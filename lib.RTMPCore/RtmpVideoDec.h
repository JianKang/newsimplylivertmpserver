#pragma once
#include <stdio.h>

#define __STDC_CONSTANT_MACROS

#ifdef _WIN32
//Windows
extern "C"
{
#include "libavformat/avformat.h"
#include "libswresample/swresample.h"
#include "libswscale/swscale.h"  
#include "libavutil/pixfmt.h"  
#include "libavutil/imgutils.h"  
};
#else
//Linux...
#ifdef __cplusplus
extern "C"
{
#endif
#include <libavformat/avformat.h>
#ifdef __cplusplus
};
#endif
#endif
#include <list>
#include <future>
#include "../../lib.Base/FrameList.h"
#include "../../lib.Logger/LogWriter.h"
#include "../../lib.Base/FrameInfo.h"
#include "../../lib.Base/locker.h"

class CRtmpVideoDec
{
	bool m_bInited = false;
public:
	CRtmpVideoDec();
	~CRtmpVideoDec();

	AVCodecContext  *m_pDec_ctx = nullptr;
	AVCodec *m_pAVCodec = nullptr;
	AVCodecParserContext *pCodecParserCtx = NULL;
	bool m_bStop = false;
	int m_nBufferSize = 0;
	int m_streamIdx = -1;
	int m_decodedCnt = 0;
	char m_out_filename[255];
	AVFrame *m_pAVFrame = nullptr;
	AVFrame *m_pAVFrameYUV = nullptr;

	bool isCurrentStream(int _streamIdx) {
		return m_streamIdx == _streamIdx;
	}
	int m_nWidth = 0;
	int m_nHeight = 0;
	int buildVideoDec(int _idx, AVCodecID id, int width,int height, TCHAR *pLogPath);
	int buildVideoDec(int _idx, AVCodecContext*_pCtx, TCHAR *pLogPath);

	int Decode_video(const AVPacket *pkt);

	bool Flush_data(const AVPacket *pkt);
	FrameList<videoFrameInfo>                 m_VideoFrameList;
	struct SwsContext *m_img_convert_ctx;
	void CloseDec();

	Locker locker;
	void addPacket(AVPacket *pkt);
	std::list<AVPacket *>	lst_video_buffer_;
	future<void> m_hdecthread;
	void _decThread();

	TCHAR *m_szLogFile;

};

