#pragma once
#include <stdint.h>
#include <string>
using namespace std;

class IRTMPDataReceiver
{
public:
	IRTMPDataReceiver() {}
	virtual ~IRTMPDataReceiver() {}
	virtual bool  InitChannel(uint32_t dwChannelID, string strName) = 0;
	virtual void  RemoveChannel() = 0;
	virtual void  streamStart(uint32_t dwChlID, string strName) = 0;
	virtual void  addRTMPData(uint32_t dwChlID, string strName, uint8_t *pData, uint32_t dataLength,
		uint32_t processedLength, uint32_t totalLength, double absoluteTimestamp, bool isAudio) = 0;
	virtual void  streamStop(uint32_t dwChlID, string strName) = 0;
};

