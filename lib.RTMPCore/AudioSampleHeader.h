#pragma once
#include "../Lib.Base/config.h"

namespace FFMPEG_AUDIOSAMPLE
{
	struct AVRational {
		int num; ///< numerator
		int den; ///< denominator
	};
	typedef struct {
		char describe[20];
		AVRational time_base;
		int samples_per_frame[6];
	} MXFSamplesPerFrame;


	typedef struct {
		FPTVideoFormat videoFormat;
		char describe[24];
		AVRational time_base_JK;
		AVRational time_base;
		int samples_per_frame[6];
	} FPT_VIDEOFORMAT_INFO;

	static const FPT_VIDEOFORMAT_INFO mxf_spfx[] = {
		{ FP_FORMAT_UNKNOWN   ,"FP_FORMAT_UNKNOWN   ",{ 0,     0 },{ 0   ,  0 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_1080i_5000,"FP_FORMAT_1080i_5000",{ 1000, 25000 },{ 1   , 25 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_1080i_5994,"FP_FORMAT_1080i_5994",{ 1001, 30000 },{ 1001, 30000 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_1080i_6000,"FP_FORMAT_1080i_6000",{ 1000, 30000 },{ 1   , 30 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_1080p_2400,"FP_FORMAT_1080p_2400",{ 1000, 24000 },{ 1   , 24 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_1080p_2500,"FP_FORMAT_1080p_2500",{ 1000, 25000 },{ 1   , 25 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_1080p_2997,"FP_FORMAT_1080p_2997",{ 1001, 30000 },{ 1001, 30000 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_1080p_3000,"FP_FORMAT_1080p_3000",{ 1000, 30000 },{ 1   , 30 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_1080p_5000,"FP_FORMAT_1080p_5000",{ 1000, 50000 },{ 1   , 50 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_1080p_5994,"FP_FORMAT_1080p_5994",{ 1001, 60000 },{ 1001, 60000 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_1080p_6000,"FP_FORMAT_1080p_6000",{ 1000, 60000 },{ 1   , 60 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_720p_2500 ,"FP_FORMAT_720p_2500 ",{ 1000, 25000 },{ 1   , 25 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_720p_2997 ,"FP_FORMAT_720p_2997 ",{ 1001, 30000 },{ 1001, 30000 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_720p_5000 ,"FP_FORMAT_720p_5000 ",{ 1000, 50000 },{ 1   , 50 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_720p_5994 ,"FP_FORMAT_720p_5994 ",{ 1001, 60000 },{ 1001, 60000 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_720p_6000 ,"FP_FORMAT_720p_6000 ",{ 1000, 60000 },{ 1   , 60 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_4Kp_2400  ,"FP_FORMAT_4Kp_2400  ",{ 1000, 24000 },{ 1   , 24 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_4Kp_2500  ,"FP_FORMAT_4Kp_2500  ",{ 1000, 25000 },{ 1   , 25 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_4Kp_2997  ,"FP_FORMAT_4Kp_2997  ",{ 1001, 30000 },{ 1001, 30000 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_4Kp_3000  ,"FP_FORMAT_4Kp_3000  ",{ 1000, 30000 },{ 1   , 30 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_4Kp_5000  ,"FP_FORMAT_4Kp_5000  ",{ 1000, 50000 },{ 1   , 50 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_4Kp_5994  ,"FP_FORMAT_4Kp_5994  ",{ 1001, 60000 },{ 1001, 60000 },{ 1920, 0,    0,    0,    0,    0 } },
		{ FP_FORMAT_4Kp_6000  ,"FP_FORMAT_4Kp_6000  ",{ 1000, 60000 },{ 1   , 60 },{ 1920, 0,    0,    0,    0,    0 } }
	};

	static const MXFSamplesPerFrame mxf_spf[] = {
		{ "FILM 23.976",{ 1001, 24000 },{ 2002, 0,    0,    0,    0,    0 } },		// FILM 23.976
		{ "FILM 24    ",{ 1   , 24 },{ 2000, 0,    0,    0,    0,    0 } },			// FILM 24
		{ "NTSC 29.97 ",{ 1001, 30000 },{ 1602, 1602, 1601, 1602, 1601, 0 } },		// NTSC 29.97
		{ "NTSC 59.94 ",{ 1001, 60000 },{ 801,  801,  801,  801,  800,  0 } },		// NTSC 59.94
		{ "PAL  25    ",{ 1   , 25 },{ 1920, 0,    0,    0,    0,    0 } },			// PAL 25
		{ "PAL  50    ",{ 1   , 50 },{ 960,  0,    0,    0,    0,    0 } },			// PAL 50
		{ "PAL  50    ",{ 1   , 50 },{ 960,  0,    0,    0,    0,    0 } },			// PAL 50
	};

	inline const char*getVideoFormatString(FPTVideoFormat _fpvf)
	{
		if (_fpvf > FP_FORMAT_MAX)return "Unknown";
		for (int i = 0; i < FP_FORMAT_MAX; i++)
		{
			if (_fpvf == mxf_spfx[i].videoFormat)
				return mxf_spfx[i].describe;
		}
		return mxf_spfx[0].describe;
	}
	inline int getFrameRateNum(FPTVideoFormat _fpvf)
	{
		for (int i = 0; i < FP_FORMAT_MAX; i++)
		{
			if (_fpvf == mxf_spfx[i].videoFormat)
				return mxf_spfx[i].time_base.num;
		}
		return mxf_spfx[0].time_base.num;
	}
	inline int getFrameRateDen(FPTVideoFormat _fpvf)
	{
		for (int i = 0; i < FP_FORMAT_MAX; i++)
		{
			if (_fpvf == mxf_spfx[i].videoFormat)
				return mxf_spfx[i].time_base.den;
		}
		return mxf_spfx[0].time_base.den;
	}
	inline const int * getFrameSamples(int _den, int _num, int &arrLen)
	{
		for (int i = 0; i < sizeof(mxf_spf) / sizeof(MXFSamplesPerFrame); i++)
		{
			if (mxf_spf[i].time_base.den == _den && mxf_spf[i].time_base.num == _num)
			{
				const int* arr = mxf_spf[i].samples_per_frame;
				for (arrLen = 0; arrLen < 6; arrLen++)
				{
					if (arr[arrLen] == 0)
					{
						break;
					}
				}
				return arr;
			}
		}
		return mxf_spf[0].samples_per_frame;
	}
	inline const int *getFrameSamples(FPTVideoFormat _fpvf, int &arrLen)
	{
		return getFrameSamples(getFrameRateDen(_fpvf), getFrameRateNum(_fpvf), arrLen);
	}
};