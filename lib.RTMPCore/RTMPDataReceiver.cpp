#include "RTMPDataReceiver.h"
#include "../Lib.Base/locker.h"
#include "../Lib.Base/platform.h"
#include "IDecodedDataCallBack.h"
#include "IFPInputCallBack.h"
#include "../lib.Logger/LogWriter.h"
#include "../lib.Base/TimeCount.h"
#include "../Lib.Base/CapturePoolMgr.h"

extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswresample/swresample.h"
#include "libswscale/swscale.h"  
#include "libavutil/pixfmt.h"  
#include "libavutil/imgutils.h"  
};

#pragma comment(lib,"avcodec.lib")
#pragma comment(lib,"avformat.lib")
#pragma comment(lib,"avutil.lib")
#pragma comment(lib,"swresample.lib")
#pragma comment(lib,"swscale.lib")


CRTMPDataReceiver::CRTMPDataReceiver(IDecodedDataCallBack *pCallBack, TCHAR *szLogFile):
	m_rtmpDatadec(this,szLogFile),
	m_pCallBack(pCallBack),
	m_szLogFile(szLogFile)
{
	av_register_all();
	avcodec_register_all();
	pFrameVideo_ = NULL;
	codec_ContextVideo = NULL;
	pCodecParserVideoCtx = NULL;
	videoCodec = NULL;
	pFrameAudio_ = NULL;
	codec_ContextAudio = NULL;
	pCodecParserAudioCtx = NULL;
	audioCodec = NULL;
	m_pbufVideoTEMP = nullptr;
	m_pbufAudioTEMP = nullptr;
	m_pVideoDecoder = nullptr;
	if (Config::getInstance()->isUsingGpuDecode())		
		m_pVideoDecoder = new DecodeH264();
}


CRTMPDataReceiver::~CRTMPDataReceiver()
{
	_stop();
	m_loopBufferVideo.UnInitBuffer();
	m_loopBufferAudio.UnInitBuffer();
	if (m_pbufVideoTEMP)
		delete[]m_pbufVideoTEMP;
	if (m_pbufAudioTEMP)
		delete[]m_pbufAudioTEMP;
	if(pCodecParserVideoCtx)
		av_parser_close(pCodecParserVideoCtx);
	if(pFrameVideo_)
		av_frame_free(&pFrameVideo_);
	if (pFrameVideo_)
	{
		avcodec_close(codec_ContextVideo);
		av_free(codec_ContextVideo);
	}
	if (pCodecParserAudioCtx)
		av_parser_close(pCodecParserAudioCtx);
	if (pFrameAudio_)
		av_frame_free(&pFrameAudio_);
	if (codec_ContextAudio)
	{
		avcodec_close(codec_ContextAudio);
		av_free(codec_ContextAudio);
	}
	if (m_pVideoDecoder)
	{
		delete m_pVideoDecoder;
		m_pVideoDecoder = nullptr;
	}
}

bool CRTMPDataReceiver::InitChannel(uint32_t dwChannelID, std::string strName)
{
	m_strCamName = strName;
	int nDecWidth = Config::getInstance()->getVideoWidth();
	int nDecHeight = Config::getInstance()->getVideoHeight();
	if (nDecHeight == 1080)
		nDecHeight = 1088;
	m_pbufVideoTEMP = new BYTE[nDecWidth*nDecHeight*2];
	m_decVideoList.initialize<CompVideoFrame>(m_dwVideoPoolSize);
	m_decAudioList.initialize<BMRaudioFrame>(m_dwAudioPoolSize);
	m_loopBufferVideo.InitBuffer(MAX_VIDEO_SIZE);
	m_loopBufferAudio.InitBuffer(MAX_AUDIO_SIZE);

	_stop();
	if (m_pVideoDecoder != nullptr)
	{
		if (m_pVideoDecoder->Init(dwChannelID,nDecWidth,nDecHeight) < 0)
		{
			WriteLogA(m_szLogFile, ErrorLevel, "CRTMPDataReceiver::InitChannel init video decoder failed.");
			return false;
		}
		WriteLogA(m_szLogFile, InfoLevel, "CRTMPDataReceiver::InitChannel dwChannelID(%d) strName(%s) success.", dwChannelID, strName.c_str());
		return true;
	}	

	// find the video encoder 
	videoCodec = avcodec_find_decoder(CODEC_ID_H264);
	if (!videoCodec)
	{
		WriteLogA(m_szLogFile, ErrorLevel, "CRTMPDataReceiver::InitChannel find video decoder failed.");
		return false;
	}

	codec_ContextVideo = avcodec_alloc_context3(videoCodec);

	codec_ContextVideo->time_base.num = 1;
	codec_ContextVideo->frame_number = 1;
	codec_ContextVideo->codec_type = AVMEDIA_TYPE_VIDEO;
	codec_ContextVideo->bit_rate = 600000;
	codec_ContextVideo->time_base.den = 25;
	codec_ContextVideo->width = 1920;
	codec_ContextVideo->height = 1080;
	int nRet = avcodec_open2(codec_ContextVideo, videoCodec, nullptr);
	if (nRet < 0)
	{
		WriteLogA(m_szLogFile, ErrorLevel, "CRTMPDataReceiver::InitChannel open video decoder failed(%d).", nRet);
		return false;
	}
	pCodecParserVideoCtx = av_parser_init(CODEC_ID_H264);
	if (!pCodecParserVideoCtx)
	{
		WriteLogA(m_szLogFile, ErrorLevel, "CRTMPDataReceiver::InitChannel av_parser_init failed(%d).", nRet);
		return false;
	}
	pFrameVideo_ = av_frame_alloc();
	WriteLogA(m_szLogFile, InfoLevel, "CRTMPDataReceiver::InitChannel dwChannelID(%d) strName(%s) success.", dwChannelID, strName.c_str());
	return true;
}

void CRTMPDataReceiver::RemoveChannel()
{
	WriteLogA(m_szLogFile, InfoLevel, "CRTMPDataReceiver::RemoveChannel.");
	_stop();
}

void CRTMPDataReceiver::streamStart(uint32_t dwChlID, string strName)
{
	WriteLogA(m_szLogFile, InfoLevel, "CRTMPDataReceiver::streamStart dwChannelID(%d) strName(%s).", dwChlID, strName.c_str());
	if (m_strCamName.compare(strName) == 0)
	{
		_stop();
		m_bStopVideoDec = false;
		m_pCallBack->start();
		m_rtmpDatadec.startWork();
		m_decVideoResult = async(launch::async, &CRTMPDataReceiver::_decVideoThread, this);
	}
}

void CRTMPDataReceiver::addRTMPData(uint32_t dwChlID, string strName, uint8_t *pData, uint32_t dataLength,
	uint32_t processedLength, uint32_t totalLength, double absoluteTimestamp, bool isAudio)
{	
	m_rtmpDatadec.AddRawRTMPData(pData, absoluteTimestamp, dataLength, processedLength, totalLength, isAudio);
}


void CRTMPDataReceiver::streamStop(uint32_t dwChlID, string strName)
{
	WriteLogA(m_szLogFile, InfoLevel, "CRTMPDataReceiver::streamStop dwChannelID(%d) strName(%s).", dwChlID, strName.c_str());
	if (m_strCamName.compare(strName) == 0)
	{
		_stop();
		m_pCallBack->stop();
	}	
}

void CRTMPDataReceiver::InitAACDecoder(int nCount)
{
	WriteLogA(m_szLogFile, InfoLevel, "CRTMPDataReceiver::InitAACDecoder AudioCount(%d).", nCount);

	m_bStopAudioDec = true;
	if (m_decAudioResult.valid())
		m_decAudioResult.wait();

	m_loopBufferAudio.RestBufferPos();
	m_nChannelCount = nCount;
	if (m_pbufAudioTEMP == nullptr)
		m_pbufAudioTEMP = new BYTE[TEMP_AUDIO_SIZE];
	
	if (pCodecParserAudioCtx)
	{
		av_parser_close(pCodecParserAudioCtx);
		pCodecParserAudioCtx = nullptr;
	}
	if (codec_ContextAudio)
	{
		avcodec_close(codec_ContextAudio);
		av_free(codec_ContextAudio);
		codec_ContextAudio = nullptr;
	}

	audioCodec = avcodec_find_decoder(AV_CODEC_ID_AAC);
	codec_ContextAudio = avcodec_alloc_context3(audioCodec);
	codec_ContextAudio->sample_rate = 48000;
	codec_ContextAudio->channels = nCount;
	codec_ContextAudio->frame_size = 1024;
	codec_ContextAudio->bits_per_coded_sample = 16;
	codec_ContextAudio->profile = 1;
	codec_ContextAudio->channel_layout = av_get_default_channel_layout(nCount);
	int nRet = avcodec_open2(codec_ContextAudio, audioCodec, nullptr);
	if (nRet != 0)
	{
		WriteLogA(m_szLogFile, ErrorLevel, "CRTMPDataReceiver::InitAACDecoder avcodec_open2 failed(%d).", nRet);
		return ;
	}
	
	pCodecParserAudioCtx = av_parser_init(AV_CODEC_ID_AAC);
	if (!pCodecParserAudioCtx)
	{
		WriteLogA(m_szLogFile, ErrorLevel, "CRTMPDataReceiver InitAACDecoder av_parser_init failed(%d).", nRet);
		return ;
	}
	
	if(pFrameAudio_ == nullptr)
		pFrameAudio_ = av_frame_alloc();

	m_bStopAudioDec = false;
	m_decAudioResult = async(launch::async, &CRTMPDataReceiver::_decAudioThread, this);

	WriteLogA(m_szLogFile, InfoLevel, "CRTMPDataReceiver::InitAACDecoder init audio decoder success.");

}

void CRTMPDataReceiver::_decVideoThread()
{
	WriteLogA(m_szLogFile, InfoLevel, "CRTMPDataReceiver::_decVideoThread decoder (%s)start.", m_pVideoDecoder == nullptr?"CPU":"GPU");
	__int64 nSentframes = 0;	
	int cur_size = 0;
	int ret = 0;
	unsigned char *cur_ptr;
	AVPacket packet;
	av_init_packet(&packet);
	CompVideoFrame *videoFrame = nullptr;
	int nOutLen = 0;
	int got_picture = 0;

	CTimeCount ts;
	CTimeCount tsTotal;
	double		pts = 0;
	while (!m_bStopVideoDec)
	{
		//if (m_loopBufferVideo.GetSizeOfDataList() >= TEMP_VIDEO_SIZE)
		if (!m_videoClock.waitEvent())
			continue;
		m_decVideoList.pop_front_data(videoFrame);
		if(videoFrame != nullptr)
		{			
			++nSentframes;
			
			pts = videoFrame->getTimeStamp();
			cur_size = videoFrame->getRawSize();
			cur_ptr = videoFrame->getRaw();

			if (m_pVideoDecoder != nullptr)
			{
				tsTotal.reset();
				ts.reset();
				ret = m_pVideoDecoder->DecodeVideoH264((char*)cur_ptr, cur_size,pts, false);
				float fDec = ts.getCountTime();
				if (ret < 0)
				{
					WriteLogA(m_szLogFile, ErrorLevel, "CRTMPDataReceiver::_decVideoThread DecodeVideoH264 error.");
					m_decVideoList.push_back_empty(videoFrame);
					continue;
				}
				m_decVideoList.push_back_empty(videoFrame);
				VideoFrame *_uncompFrame = nullptr;
				CapturePoolMgr::GetInstance()->getNew(_uncompFrame, Config::getInstance()->getVideoFormat());
				if (_uncompFrame == nullptr)
				{
					WriteLogA(m_szLogFile, WarnLevel, "CRTMPDataReceiver::_decVideoThread Video Frame List is empty.");
					continue;
				}
				ret = m_pVideoDecoder->GetDecodedData(_uncompFrame->getRaw(), nOutLen, pts);
				if (ret == 0)
					m_pCallBack->cbVideo(_uncompFrame);
				else
					CapturePoolMgr::GetInstance()->release(_uncompFrame);

				float fTotal = tsTotal.getCountTime();
				if (fTotal >= Config::getInstance()->getInterval())
					WriteLogA(m_szLogFile, WarnLevel, "CRTMPDataReceiver::_decVideoThread DecodeVideoH264 fTotal(%.2f) decTime(%.2f).", fTotal,fDec);
			}
			else
			{
				got_picture = 0;
				//cur_size = _fetchVideoData(m_pbufVideoTEMP, TEMP_VIDEO_SIZE);			
				//cur_ptr = m_pbufVideoTEMP;
				packet.size = cur_size;
				packet.data = cur_ptr ;

				if (cur_size > 0)
				{
					/*
					int len = av_parser_parse2(
						pCodecParserVideoCtx, codec_ContextVideo,
						&packet.data, &packet.size,
						cur_ptr, cur_size,
						AV_NOPTS_VALUE, AV_NOPTS_VALUE, AV_NOPTS_VALUE);

					cur_ptr += len;
					cur_size -= len;

					if (packet.size == 0)
						continue;
					
					//Some Info from AVCodecParserContext
					printf("[Packet]Size:%6d\t", packet.size);
					switch (pCodecParserVideoCtx->pict_type)
					{
					case AV_PICTURE_TYPE_I: printf("*********Type:I\t"); break;
					case AV_PICTURE_TYPE_P: printf("Type:P\t"); break;
					case AV_PICTURE_TYPE_B: printf("Type:B\t"); break;
					default: printf("Type:Other\t"); break;
					}
					printf("Number:%4d\n", pCodecParserVideoCtx->output_picture_number);
					*/
					ret = avcodec_decode_video2(codec_ContextVideo, pFrameVideo_, &got_picture, &packet);					
					if (ret < 0)
					{
						WriteLogA(m_szLogFile, ErrorLevel, "CRTMPDataReceiver::_decVideoThread nDecodedframes(%d) error(%x).", nSentframes, ret);
						m_decVideoList.push_back_empty(videoFrame);
						continue;
					}
					m_decVideoList.push_back_empty(videoFrame);
					if (got_picture)
					{
						pFrameVideo_->pts = packet.pts;
						m_pCallBack->cbVideo(pFrameVideo_);
					}
				}
			}
			
		}
	}

	WriteLogA(m_szLogFile, InfoLevel, "CRTMPDataReceiver::_decVideoThread stop.");
}


void CRTMPDataReceiver::_decAudioThread()
{
	WriteLogA(m_szLogFile, InfoLevel, "CRTMPDataReceiver::_decAudioThread start.");
	__int64 nDecodedframes = 0;
	int cur_size = 0;
	int ret = 0;
	unsigned char *cur_ptr;
	AVPacket packet;
	av_init_packet(&packet);
	BMRaudioFrame *aduioFrame = nullptr;
	double	pts;
	while (!m_bStopAudioDec)
	{
		if (!m_audioClock.waitEvent())
			continue;
		//if (m_loopBufferAudio.GetSizeOfDataList() >= TEMP_AUDIO_SIZE)
		m_decAudioList.pop_front_data(aduioFrame);
		if (aduioFrame != nullptr)
		{
			int got_picture = 0;
			//cur_size = _fetchAudioData(m_pbufAudioTEMP, TEMP_AUDIO_SIZE);
			//cur_ptr = m_pbufAudioTEMP;
			packet.size = cur_size = aduioFrame->getDataSize();
			packet.data = cur_ptr = (unsigned char *)aduioFrame->getRaw();
			packet.pts = pts = aduioFrame->getTimeStamp();
			/*
			ret = avcodec_decode_audio4(codec_ContextAudio, pFrameAudio_, &got_picture, &packet);
			m_decAudioList.push_back_empty(aduioFrame);
			if (ret < 0)
			{
				WriteLogA(m_szLogFile, ErrorLevel, "CRTMPDataReceiver::_decAudioThread nDecodedframes(%d) error(%x).", nDecodedframes, ret);
				got_picture = false;
			}
			if (got_picture)
			{
				++nDecodedframes;
				pFrameAudio_->pts = packet.pts = pts;
				m_pCallBack->cbAudio(pFrameAudio_, 48000, m_nChannelCount);
			}
			*/
			while (cur_size > 0)
			{				
				int len = av_parser_parse2(
					pCodecParserAudioCtx, codec_ContextAudio,
					&packet.data, &packet.size,
					cur_ptr, cur_size,
					AV_NOPTS_VALUE, AV_NOPTS_VALUE, AV_NOPTS_VALUE);

				cur_ptr += len;
				cur_size -= len;

				if (packet.size == 0)
					continue;
				ret = avcodec_decode_audio4(codec_ContextAudio, pFrameAudio_, &got_picture, &packet);
				if (ret < 0)
				{
					WriteLogA(m_szLogFile, ErrorLevel, "CRTMPDataReceiver::_decAudioThread nDecodedframes(%d) error(%x).", nDecodedframes, ret);
					break;
				}
				if (got_picture)
				{
					++nDecodedframes;
					pFrameAudio_->pts = packet.pts;
					m_pCallBack->cbAudio(pFrameAudio_, 48000, m_nChannelCount);
				}			
			}
			m_decAudioList.push_back_empty(aduioFrame);
		}
	}

	WriteLogA(m_szLogFile, InfoLevel, "CRTMPDataReceiver::_decAudioThread stop.");
}

int CRTMPDataReceiver::_fetchAudioData(uint8_t* buf, int size)
{
	return m_loopBufferAudio.ReadBuffer(buf, size);
}
void CRTMPDataReceiver::PushAudio(BYTE *pAudio, int nSize)
{
	m_loopBufferAudio.WriteBuffer(pAudio, nSize);
}


int CRTMPDataReceiver::_fetchVideoData(uint8_t* buf, int size)
{
	return m_loopBufferVideo.ReadBuffer(buf, size);
}

void CRTMPDataReceiver::PushVideo(BYTE *pVideo, int nSize)
{
	m_loopBufferVideo.WriteBuffer(pVideo, nSize);
}

void CRTMPDataReceiver::cb(RTMP_RAW_Unit &naluUnit)
{
	if (naluUnit.bAudio)
	{
		memset(headerAudio, 0, 7);
		headerAudio[0] += 0xFF; /* 8b: syncword */

		headerAudio[1] += 0xF0; /* 4b: syncword */
						 /* 1b: mpeg id = 0 */
						 /* 2b: layer = 0 */
		headerAudio[1] += 1; /* 1b: protection absent */

		headerAudio[2] += ((1 << 6) & 0xC0); /* 2b: profile */
		headerAudio[2] += ((3 << 2) & 0x3C); /* 4b: sampling_frequency_index */
											 /* 1b: private = 0 */
		headerAudio[2] += ((m_nChannelCount >> 2) & 0x1); /* 1b: channel_configuration */

		headerAudio[3] += ((m_nChannelCount << 6) & 0xC0); /* 2b: channel_configuration */
													/* 1b: original */
													/* 1b: home */
													/* 1b: copyright_id */
													/* 1b: copyright_id_start */
		headerAudio[3] += (((naluUnit.size + 7) >> 11) & 0x3); /* 2b: aac_frame_length */

		headerAudio[4] += (((naluUnit.size + 7) >> 3) & 0xFF); /* 8b: aac_frame_length */

		headerAudio[5] += (((naluUnit.size + 7) << 5) & 0xE0); /* 3b: aac_frame_length */
		headerAudio[5] += ((0x7FF >> 6) & 0x1F); /* 5b: adts_buffer_fullness */

		//headerAudio[6] += ((0x7FF << 2) & 0x3F); /* 6b: adts_buffer_fullness */
		//										   /* 2b: num_raw_data_blocks */
		headerAudio[6] += 0xFC;
		/*
 		static FILE * fp = nullptr;
 		//if (fp == nullptr)
 		//	fopen_s(&fp, "E:\\decAAC.aac", "wb");		
 		if (fp != nullptr)		
 			fwrite(headerAudio, 7, 1, fp);
 		if (fp != nullptr)
 			fwrite(naluUnit.data, naluUnit.size, 1, fp);

		PushAudio(headerAudio, 7);
		PushAudio(naluUnit.data, naluUnit.size);
		*/
		//
		BMRaudioFrame *aduioFrame = nullptr;
		m_decAudioList.pop_front_empty(aduioFrame);
		if (aduioFrame == nullptr)
		{
			WriteLogA(m_szLogFile, ErrorLevel, "CRTMPDataReceiver::cb pop_front_empty audio  failed.");
			return;
		}
		aduioFrame->setTimeStamp(naluUnit.pts);
		aduioFrame->setBufferSize(7 + naluUnit.size);
		memcpy(aduioFrame->getRaw(), headerAudio, 7);
		memcpy((unsigned char*)aduioFrame->getRaw() + 7, naluUnit.data, naluUnit.size);
		m_decAudioList.push_back_data(aduioFrame);
		m_audioClock.raiseEvent();
	}	
	else
	{	
		CompVideoFrame *videoFrame = nullptr;
		m_decVideoList.pop_front_empty(videoFrame);
		if (videoFrame == nullptr)
		{
			WriteLogA(m_szLogFile, ErrorLevel, "CRTMPDataReceiver::cb pop_front_empty video failed.");
			return ;
		}	
		videoFrame->setTimeStamp(naluUnit.pts);
		if (naluUnit.bAddHeader)
		{
			videoFrame->setBufferSize(4 + naluUnit.size);
			memcpy(videoFrame->getRaw(), headerVideo, 4);
			memcpy(videoFrame->getRaw() + 4, naluUnit.data, naluUnit.size);
		}
		else
		{
			videoFrame->setBufferSize(naluUnit.size);
			memcpy(videoFrame->getRaw(), naluUnit.data, naluUnit.size);
		}
		m_decVideoList.push_back_data(videoFrame);
		m_videoClock.raiseEvent();
	}
}

void CRTMPDataReceiver::_stop()
{
	WriteLogA(m_szLogFile, InfoLevel, "CRTMPDataReceiver::_stop() .");

	m_bStopVideoDec = true;
	m_bStopAudioDec = true;
	m_rtmpDatadec.stopWork();

	if (m_decVideoResult.valid())
		m_decVideoResult.wait();
	if (m_decAudioResult.valid())
		m_decAudioResult.wait();	
	m_loopBufferVideo.RestBufferPos();
	m_loopBufferAudio.RestBufferPos();

	CompVideoFrame*pVideoFrame = nullptr;
	while (m_decVideoList.pop_front_data(pVideoFrame))
	{
		m_decVideoList.push_back_empty(pVideoFrame);
	}

	BMRaudioFrame*pAudioFrame = nullptr;
	while (m_decAudioList.pop_front_data(pAudioFrame))
	{
		m_decAudioList.push_back_empty(pAudioFrame);
	}
}
