#include "RtmpVideoDec.h"

CRtmpVideoDec::CRtmpVideoDec()
{
	m_VideoFrameList.initialize<videoFrameInfo>(200);//
}

CRtmpVideoDec::~CRtmpVideoDec()
{
}

int CRtmpVideoDec::buildVideoDec(int _idx, AVCodecContext*_pCtx, TCHAR *pLogPath)
{
	m_pDec_ctx = _pCtx;
	m_szLogFile = pLogPath;
	m_pAVCodec = avcodec_find_decoder(_pCtx->codec_id);
	avcodec_open2(m_pDec_ctx, m_pAVCodec, nullptr);
	m_streamIdx = _idx;
	m_nHeight = m_pDec_ctx->height;
	m_nWidth = m_pDec_ctx->width;
	//int bufSize = m_nWidth * m_nHeight * 3 / 2;
	//int bytes_num = avpicture_get_size(AV_PIX_FMT_YUV420P, m_nWidth, m_nHeight);
	m_pAVFrame = av_frame_alloc();// avcodec_alloc_frame();
	m_pAVFrameYUV = av_frame_alloc();

	m_img_convert_ctx = sws_getContext(_pCtx->width, _pCtx->height, _pCtx->pix_fmt,
		_pCtx->width, _pCtx->height, AV_PIX_FMT_UYVY422, SWS_BICUBIC, nullptr, nullptr, nullptr);
	m_bStop = false;
	m_decodedCnt = 0;
	videoFrameInfo*pFrame = nullptr;
	while (m_VideoFrameList.pop_front_data(pFrame))
	{
		m_VideoFrameList.push_back_empty(pFrame);
	}
	m_bInited = true;

	m_hdecthread = async(launch::async, &CRtmpVideoDec::_decThread, this);
	return 0;
}
int CRtmpVideoDec::buildVideoDec(int _idx, AVCodecID id, int width, int height, TCHAR *pLogPath)
{
	av_register_all();
	m_szLogFile = pLogPath;
	
	m_pAVCodec = avcodec_find_decoder(id);
	if (!m_pAVCodec) {
		return -1;
	}
	m_pDec_ctx = avcodec_alloc_context3(m_pAVCodec);
	
	m_pDec_ctx->bit_rate = 6000000;
	//m_pDec_ctx->refcounted_frames = 0;
	m_pDec_ctx->codec_type = AVMEDIA_TYPE_VIDEO;
	m_pDec_ctx->codec_id = AV_CODEC_ID_H264;
	m_pDec_ctx->width = width;
	m_pDec_ctx->height = height;

	m_pDec_ctx->coded_width = width;
	m_pDec_ctx->coded_height = (height == 1080? 1088: height);
	m_pDec_ctx->pix_fmt = AV_PIX_FMT_YUV420P;
	//m_pDec_ctx->extradata = nullptr;
	//m_pDec_ctx->extradata_size = 0;
	
	//m_pDec_ctx->has_b_frames = 2;

	// If this is ever increased, look at |av_context_->thread_safe_callbacks| and
	// make it possible to disable the thread checker in the frame buffer pool.
	m_pDec_ctx->thread_count = 13;
	m_pDec_ctx->thread_type = 3;
	//m_pDec_ctx->active_thread_type = 1;
	//m_pDec_ctx->refs = 4;
	//m_pDec_ctx->delay = 12;
	//m_pDec_ctx->profile = 100;
	//m_pDec_ctx->level = 40;
	//m_pDec_ctx->sample_aspect_ratio.den = 1;
	//m_pDec_ctx->sample_aspect_ratio.num = 1; 
	//m_pDec_ctx->time_base.num = 1;
	//m_pDec_ctx->time_base.den = 50;

	m_pDec_ctx->framerate.num = 0;
	m_pDec_ctx->framerate.den = 1;

	m_pDec_ctx->pkt_timebase.num = 1;
	m_pDec_ctx->pkt_timebase.den = 1000;
	//m_pDec_ctx->bits_per_raw_sample = 8;
	
	// Function used by FFmpeg to get buffers to store decoded frames in.
	//m_pDec_ctx->get_buffer2 = nullptr;
	// |get_buffer2| is called with the context, there |opaque| can be used to get
	// a pointer |this|.
	//m_pDec_ctx->opaque = nullptr;


	int res = avcodec_open2(m_pDec_ctx, m_pAVCodec, nullptr);
	if (res < 0) {
		return -1;
	}

	m_streamIdx = _idx;
	m_nHeight = m_pDec_ctx->height;
	m_nWidth = m_pDec_ctx->width;
	//int bufSize = m_nWidth * m_nHeight * 3 / 2;
	//int bytes_num = avpicture_get_size(AV_PIX_FMT_YUV420P, m_nWidth, m_nHeight);
	m_pAVFrame = av_frame_alloc();// avcodec_alloc_frame();
	m_pAVFrameYUV = av_frame_alloc();

	m_img_convert_ctx = sws_getContext(width, height, AV_PIX_FMT_YUV420P,
		width, height, AV_PIX_FMT_UYVY422, SWS_BICUBIC, nullptr, nullptr, nullptr);
	m_bStop = false;
	m_decodedCnt = 0;
	videoFrameInfo*pFrame = nullptr;
	while (m_VideoFrameList.pop_front_data(pFrame))
	{
		m_VideoFrameList.push_back_empty(pFrame);
	}
	m_bInited = true;

	m_hdecthread = async(launch::async, &CRtmpVideoDec::_decThread, this);
	return 0;
}

int CRtmpVideoDec::Decode_video(const AVPacket *pkt)
{
	int bytesRemaining = 0;
	int bytesDecoded = 0;
	int got_picture = 0;

	uint8_t  *rawData = 0;

	bytesRemaining = pkt->size;
	rawData = pkt->data;
	videoFrameInfo *pFrameInfo = nullptr;
	static int pktcnt = 0;
	pktcnt++;
	while (bytesRemaining > 0)
	{
		if (m_bStop)
			return 0;
		bytesDecoded = avcodec_decode_video2(m_pDec_ctx, m_pAVFrame, &got_picture, pkt);
		if (bytesDecoded < 0)
			return -1;

		bytesRemaining -= bytesDecoded;
		rawData += bytesDecoded;
		if (got_picture)
		{			
			if (!m_VideoFrameList.pop_front_empty(pFrameInfo))
				return -2;
			pFrameInfo->setPts(pkt->pts);
			av_image_fill_arrays(m_pAVFrameYUV->data, m_pAVFrameYUV->linesize, pFrameInfo->pData,
				AV_PIX_FMT_UYVY422, m_nWidth, m_nHeight, 1);
			pFrameInfo->nDataSize = m_nWidth*m_nHeight * 2;

			sws_scale(m_img_convert_ctx, (const unsigned char* const*)m_pAVFrame->data, m_pAVFrame->linesize, 0, m_nHeight,
				m_pAVFrameYUV->data, m_pAVFrameYUV->linesize);
			m_VideoFrameList.push_back_data(pFrameInfo);
			m_decodedCnt++;
			return 1;
		}
	}
	return 0;
}

void CRtmpVideoDec::addPacket(AVPacket * pkt)
{
	LockHolder lock(locker);
	lst_video_buffer_.push_back(pkt);
}

bool CRtmpVideoDec::Flush_data(const AVPacket *pkt)
{
	int bytesDecoded = 0;
	int got_picture = 0;

	videoFrameInfo *pFrameInfo = nullptr;
	//while (!m_bStop)
	{
		bytesDecoded = avcodec_decode_video2(m_pDec_ctx, m_pAVFrame, &got_picture, pkt);
		if (bytesDecoded <= 0 || !got_picture)
			return true;

		while (!m_VideoFrameList.pop_front_empty(pFrameInfo))
		{
			if (m_bStop) return true;
			Sleep(10);
		}

		av_image_fill_arrays(m_pAVFrameYUV->data, m_pAVFrameYUV->linesize, pFrameInfo->pData,
			AV_PIX_FMT_UYVY422, m_nWidth, m_nHeight, 1);
		pFrameInfo->nDataSize = m_nWidth*m_nHeight * 2;

		sws_scale(m_img_convert_ctx, (const unsigned char* const*)m_pAVFrame->data, m_pAVFrame->linesize, 0, m_nHeight,
			m_pAVFrameYUV->data, m_pAVFrameYUV->linesize);

		m_VideoFrameList.push_back_data(pFrameInfo);
	}
	return false;
}

void CRtmpVideoDec::CloseDec()
{
	m_bStop = true;
	if (m_hdecthread.valid())
		m_hdecthread.wait();
	if (m_bInited)
	{
		sws_freeContext(m_img_convert_ctx);
		//av_frame_unref(m_pAVFrameYUV);
		//av_frame_unref(m_pAVFrame);

		av_free(m_pAVFrameYUV);
		av_free(m_pAVFrame);
		//avcodec_close(m_pDec_ctx);
	}
	m_bInited = false;
}

void CRtmpVideoDec::_decThread()
{
	AVPacket * pkt = nullptr;
	
	while (!m_bStop)
	{
		if (lst_video_buffer_.empty())
		{
			Sleep(10);
			continue;
		}
		{
			LockHolder lock(locker);
			pkt = lst_video_buffer_.front();
			lst_video_buffer_.pop_front();
		}
		int nRet = Decode_video(pkt);
		av_free_packet(pkt);
		if (nRet < 0)
		{
			printf("---->Decode video failed.\n");
			WriteLogA(m_szLogFile, ErrorLevel, "CRtmpVideoDec::_decThread Decode_video failed(%d).", nRet);
			break;
		}
	}
	while (!lst_video_buffer_.empty())
	{

		pkt = lst_video_buffer_.front();
		lst_video_buffer_.pop_front();
		av_free_packet(pkt);
	}
	WriteLogA(m_szLogFile, WarnLevel, "CRtmpVideoDec::_decThread exit.");
}
