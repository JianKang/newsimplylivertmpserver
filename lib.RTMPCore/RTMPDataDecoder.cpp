#include "RTMPDataDecoder.h"
#include "../lib.Logger/LogWriter.h"
#include "../lib.base/Config.h"

RTMPDataDecoder::RTMPDataDecoder(INaluUnitCallBack *pCallback,TCHAR *szLogFile):
	m_pCallback(pCallback),
	m_szLogFile(szLogFile),
	m_pbufVideoTEMP(nullptr),
	m_pbufVideoUnit(nullptr),
	m_pbufAudioTEMP(nullptr),
	m_pbufAudioUnit(nullptr)
{
	m_bGetAudioMeta = false;
	m_bGetVideoMeta = false;
	//m_pbufVideoTEMP = new BYTE[TEMP_VIDEO_SIZE];
	m_pbufVideoUnit = new BYTE[TEMP_VIDEO_SIZE];
	m_pbufVideo = new BYTE[TEMP_VIDEO_SIZE];
	//m_pbufAudioTEMP = new BYTE[TEMP_AUDIO_SIZE];
	m_pbufAudioUnit = new BYTE[TEMP_AUDIO_SIZE];
	//m_bufVideo.InitBuffer(MAX_VIDEO_SIZE);
	//m_bufAudio.InitBuffer(MAX_AUDIO_SIZE);
	getCPUfreq(m_freq);
	m_tickPerFrame = m_freq / Config::getInstance()->getFramesPerSec();
	m_lastTick	= 0;
	m_GotFrameID = 0;
	resetPos();
}


RTMPDataDecoder::~RTMPDataDecoder()
{
	stopWork();
	if (m_pbufVideoTEMP)
		delete[]m_pbufVideoTEMP;
	if (m_pbufAudioTEMP)
		delete[]m_pbufAudioTEMP;
	if (m_pbufVideoUnit)
		delete[]m_pbufVideoUnit;
	if (m_pbufAudioUnit)
		delete[]m_pbufAudioUnit;
	m_bufVideo.UnInitBuffer();
	m_bufAudio.UnInitBuffer();
}


void RTMPDataDecoder::startWork()
{
	resetPos();
	m_bStopDec = false;
	//m_decAudioResult = async(launch::async, &RTMPDataDecoder::_decRTMPAudioThread, this);
	//m_decVideoResult = async(launch::async, &RTMPDataDecoder::_decRTMPVideoThread, this);
	WriteLogA(m_szLogFile, InfoLevel, "RTMPDataDecoder::startWork success.");
}

void RTMPDataDecoder::stopWork()
{
	m_bStopDec = true;

	if (m_decAudioResult.valid())
		m_decAudioResult.wait();	
	if (m_decVideoResult.valid())
		m_decVideoResult.wait();
	{
		LockHolder locker(m_lockerQueue);
		m_audioQueue.clear();
	}	
	
	WriteLogA(m_szLogFile, InfoLevel, "RTMPDataDecoder::stopWork.");
}

void RTMPDataDecoder::resetPos()
{
	m_bGetAudioMeta = false;
	m_bGetVideoMeta = false;
	m_bufVideo.RestBufferPos();
	m_bufAudio.RestBufferPos();
	m_dwVideoUnitPos = 0;
	m_dwAudioUnitPos = 0;
	m_lastTick		 = 0;
	m_GotFrameID = 0;
	LockHolder locker(m_lockerQueue);
	m_audioQueue.clear();
}

/*
void RTMP264Decoder::_readVideoThread()
{
	
	FILE *fp_in;
	char filepath_in[] = "e:\\recv.264";;
	//Input File
	fp_in = fopen(filepath_in, "rb");
	if (!fp_in) {
		printf("Could not open input stream\n");
		return;
	}
	int in_buffer_size = 128;
	unsigned char in_buffer[1024] = { 0 };
	int cur_size = 0;

	int ntime = 0;
	while (true) 
	{
		in_buffer_size = rand() / 128;
		if (in_buffer_size < 10)
			in_buffer_size = 10;
		cur_size = fread(in_buffer, 1, in_buffer_size, fp_in);
		if (cur_size == 0)
			break;
		AddVideo(in_buffer, in_buffer_size);
		if(ntime++%100 == 0)
			Sleep(10);
	}		
}
*/
void RTMPDataDecoder::_decRTMPVideoThread()
{
	WriteLogA(m_szLogFile, InfoLevel, "RTMPDataDecoder::_decRTMPVideoThread start.");
	/*
	FILE *fp_in;
	char filepath_in[] = "e:\\recv.264";
	//Input File
	fp_in = fopen(filepath_in, "rb");
	if (!fp_in) {
		printf("Could not open input stream\n");
		return;
	}

	BYTE szTEMP[10240];
	int nSrcsize = 0;
	if (fp_in != nullptr)
	{
		fseek(fp_in, 0, SEEK_END);
		nSrcsize = ftell(fp_in);
		rewind(fp_in);
		printf("src file size(%d) \n", nSrcsize);
		while (nSrcsize > 0)
		{
			int size  = fread(szTEMP,1 , 10240, fp_in);
			AddVideo(szTEMP, size);
			nSrcsize -= size;
		}		
		fclose(fp_in);
	}
	*/

	FILE * fp = nullptr;
	//if (fp == nullptr)
	//	fopen_s(&fp, "E:\\decVideo.264", "wb");
	BYTE *pTemp = 0;
	bool bGetMeta = false;
	bool bHasError = false;
	int nDealedLeftBytes = 0;
	DWORD nNALUsize = 0;
	DWORD nNALUStillNeed = 0;
	DWORD nFetchSize = TEMP_VIDEO_SIZE;
	RTMP_RAW_Unit nalu;
	nalu.bAudio = false;
	while (!m_bStopDec && !bHasError)
	{
		nFetchSize = TEMP_VIDEO_SIZE - nDealedLeftBytes;
		if (m_bufVideo.GetSizeOfDataList() > nFetchSize)
		{			
			if (nFetchSize == _fetchRawRTMPData(m_pbufVideoTEMP+ nDealedLeftBytes, nFetchSize,false))
			{					
				pTemp = m_pbufVideoTEMP;
				for (int n = 0; n < TEMP_VIDEO_SIZE;)
				{
					if (nNALUStillNeed != 0)
					{
						nalu.bAddHeader = false;
						nalu.type = 0;
						nalu.size = nNALUStillNeed;
						nalu.data = pTemp;
						m_pCallback->cb(nalu);
						if (fp != nullptr)
							fwrite(pTemp, nNALUStillNeed, 1, fp);
						n += nNALUStillNeed;
						nNALUStillNeed = 0;
					}
					if (!bGetMeta)
					{
						if (pTemp[n++] == 0x17 &&
							pTemp[n++] == 0x00 &&
							pTemp[n++] == 0x00 &&
							pTemp[n++] == 0x00 &&
							pTemp[n++] == 0x00)
						{
							//MetaData info
							n += 6;

							WORD nSpsLen = pTemp[n]<<8 & 0xff00 | pTemp[n+1];
							n += sizeof(WORD);

							nalu.bAddHeader = true;
							nalu.type = 0;
							nalu.size = nSpsLen;
							nalu.data = pTemp + n;
							m_pCallback->cb(nalu);

							if (fp != nullptr)
								fwrite(headerVideo, 4, 1, fp);
							if (fp != nullptr)
								fwrite(pTemp + n, nSpsLen, 1, fp);

							n += nSpsLen;

							n += 1;
							WORD nPpsLen = pTemp[n] << 8 & 0xff00 | pTemp[n + 1];
							n += sizeof(WORD);
							nalu.bAddHeader = true;
							nalu.type = 0;
							nalu.size = nPpsLen;
							nalu.data = pTemp + n;
							m_pCallback->cb(nalu);

							if (fp != nullptr)
								fwrite(headerVideo, 4, 1, fp);
							if (fp != nullptr)
								fwrite(pTemp + n, nPpsLen, 1, fp);

							n += nPpsLen;
							bGetMeta = true;
						}
						else
						{
							WriteLogA(m_szLogFile, FatalLevel, "RTMPDataDecoder::_decRTMPVideoThread get meta data error.");
							bHasError = true;
							break;
						}
					}

					if ((n + 9) >= TEMP_VIDEO_SIZE)
					{
						nDealedLeftBytes = (TEMP_VIDEO_SIZE - n);
						memcpy(m_pbufVideoTEMP, m_pbufVideoTEMP + n, nDealedLeftBytes);
						WriteLogA(m_szLogFile, InfoLevel, "RTMPDataDecoder::_decRTMPVideoThread current DealedLeftBytes(%d).", nDealedLeftBytes);
						break;
					}
					else
						nDealedLeftBytes = 0;
					//
					if ((pTemp[n] == 0x17 || pTemp[n] == 0x27) &&
						pTemp[n + 1] == 0x01 &&
						pTemp[n + 2] == 0x00 &&
						pTemp[n + 3] == 0x00 &&
						pTemp[n + 4] == 0x00)
					{
						n += 5;
						nNALUsize = pTemp[n] << 24 & 0xff000000 | pTemp[n + 1] << 16 & 0x00ff0000 | pTemp[n+2] << 8 & 0x0000ff00 | pTemp[n + 3] & 0x000000ff;
						n += sizeof(DWORD);
						if (fp != nullptr)
							fwrite(headerVideo, 4, 1, fp);

						nalu.bAddHeader = true;
						nalu.type = 0;
						nalu.size = 0;
						nalu.data = nullptr;
						m_pCallback->cb(nalu);
					}
					else
					{
						WriteLogA(m_szLogFile, FatalLevel, "RTMPDataDecoder::_decRTMPVideoThread error.(%d-%d-%d-%d-%d).",
							pTemp[n], pTemp[n + 1], pTemp[n + 2], pTemp[n + 3], pTemp[n + 4]);
						bHasError = true;
						break;
					}

					if ((n + nNALUsize) >= TEMP_VIDEO_SIZE)
					{
						nNALUStillNeed = nNALUsize - (TEMP_VIDEO_SIZE - n);
						if (fp != nullptr)
							fwrite(pTemp + n, TEMP_VIDEO_SIZE - n, 1, fp);

						nalu.bAddHeader = false;
						nalu.type = 0;
						nalu.size = TEMP_VIDEO_SIZE - n;
						nalu.data = pTemp + n;
						m_pCallback->cb(nalu);						
						n += (TEMP_VIDEO_SIZE - n);
						//WriteLogA(m_szLogFile, InfoLevel, "RTMPDataDecoder::_decRTMPVideoThread NALU is not enough,next time need(%d).", nNALUStillNeed);
					}
					else
					{
						if (fp != nullptr)
							fwrite(pTemp + n, nNALUsize, 1, fp);

						nalu.bAddHeader = false;
						nalu.type = 0;
						nalu.size = nNALUsize;
						nalu.data = pTemp + n;
						m_pCallback->cb(nalu);

						n += nNALUsize;
					}
				}
			}
		}
		else
		{
			Sleep(10);
		}
	}
	if (fp != nullptr)
		fclose(fp);
	WriteLogA(m_szLogFile, InfoLevel, "RTMPDataDecoder::_decRTMPVideoThread stop.");
}

void RTMPDataDecoder::_decRTMPAudioThread()
{
	WriteLogA(m_szLogFile, InfoLevel, "RTMPDataDecoder::_decRTMPAudioThread start.");
	BYTE *pTemp = 0;
	bool bGetMeta = false;
	bool bHasError = false;
	int nDealedLeftBytes = 0;
	DWORD nFetchSize = TEMP_AUDIO_SIZE;
	RTMP_RAW_Unit nalu;
	nalu.bAudio = true;
	int nAudioChannels = 0;
	int pos = 0;
	int nCurSampleSize = 0;
	int nCurLeft = 0;
	while (!m_bStopDec && !bHasError)
	{
		nFetchSize = TEMP_AUDIO_SIZE - nDealedLeftBytes;
		if (m_bufAudio.GetSizeOfDataList() >= (nFetchSize+2048))
		{			
			if (nFetchSize == _fetchRawRTMPData(m_pbufAudioTEMP + nDealedLeftBytes, nFetchSize, true))
			{
				pTemp = m_pbufAudioTEMP;
				int nIndex = 0;
				if (!bGetMeta)
				{
					if (pTemp[nIndex] == 0xAF && pTemp[nIndex + 1] == 0x00)
					{
						if (pTemp[nIndex + 2] != 0x11)//ONLY 48000
						{
							WriteLogA(m_szLogFile, FatalLevel, "RTMPDataDecoder::_decRTMPAudioThread ONLY support 48000HZ (%d).", pTemp[nIndex + 2]);
							bHasError = true;
							continue;
						}

						switch (pTemp[nIndex + 3])
						{
						case 0x90:nAudioChannels = 2; break;
						case 0xA0:nAudioChannels = 4; break;
						case 0xB0:nAudioChannels = 6; break;
						default:
							break;
						}
						m_pCallback->InitAACDecoder(nAudioChannels);

						//MetaData info							
						nIndex += 7;
						pTemp += nIndex;
						bGetMeta = true;
					}
					else
					{
						WriteLogA(m_szLogFile, FatalLevel, "RTMPDataDecoder::_decRTMPAudioThread the first two bytes(%x,%x) are expected(0xAF,0x00).",
							pTemp[nIndex], pTemp[nIndex + 1]);
						bHasError = true;
						continue;
					}
				}
				pos = 0;
				nCurLeft = (TEMP_AUDIO_SIZE - nIndex-2);
				//WriteLogA(m_szLogFile, InfoLevel, "RTMPDataDecoder::_decRTMPAudioThread enter while (%d)", nCurLeft);
				while (pos <= nCurLeft)
				{
					//WriteLogA(m_szLogFile, InfoLevel, "RTMPDataDecoder::_decRTMPAudioThread need(%d)", nCurSampleSize);
					if (pTemp[pos] != 0xAF || pTemp[pos + 1] != 0x01)
					{
						WriteLogA(m_szLogFile, FatalLevel, "RTMPDataDecoder::_decRTMPAudioThread header bytes(%x,%x) are not correct!!!!",
							pTemp[pos], pTemp[pos + 1]);
						break;
					}
					if (m_audioQueue.empty())
					{
						WriteLogA(m_szLogFile, WarnLevel, "RTMPDataDecoder::_decRTMPAudioThread audioQueue is empty pos(%d) nCurLeft(%d)!!!!", pos, nCurLeft);
						Sleep(10);
						continue;
					}
					{
						LockHolder locker(m_lockerQueue);
						nCurSampleSize = m_audioQueue.front();
						m_audioQueue.pop_front();
					}
					if ((nCurSampleSize + pos) <= nCurLeft)
					{
						nalu.bAddHeader = true;
						nalu.type = 0;
						nalu.size = nCurSampleSize-2;
						nalu.data = pTemp + pos+2;
						m_pCallback->cb(nalu);

						pos += nCurSampleSize;
						//WriteLogA(m_szLogFile, InfoLevel, "RTMPDataDecoder::_decRTMPAudioThread get(%d),pos(%d)", nCurSampleSize, pos);
					}
					else
					{
						nDealedLeftBytes = nCurLeft +2- pos;
						memcpy(m_pbufAudioTEMP, pTemp + pos, nDealedLeftBytes);
						LockHolder locker(m_lockerQueue);
						m_audioQueue.push_front(nCurSampleSize);
						//WriteLogA(m_szLogFile, InfoLevel, "RTMPDataDecoder::_decRTMPAudioThread letf(%d),pos(%d)", nDealedLeftBytes, pos);
						break;
					}
				}
			}
		}
		else
		{
			Sleep(10);
		}
	}

	WriteLogA(m_szLogFile, InfoLevel, "RTMPDataDecoder::_decRTMPAudioThread stop.");
}

void RTMPDataDecoder::ProcessAudioData(BYTE *pData, double absoluteTimestamp, uint32_t dataLength, uint32_t processedLength, uint32_t totalLength)
{
	int pos = 0;
	m_naluCbDataA.bAudio = true;
	m_naluCbDataA.pts = absoluteTimestamp;
	if ((processedLength + dataLength) == totalLength)
	{
//		WriteLogA(m_szLogFile, InfoLevel, "RTMPDataDecoder::ProcessAudioData Done ts(%.2f) totalLength(%d)", absoluteTimestamp, totalLength);
		if (!m_bGetAudioMeta)
		{
			BYTE * pTemp = pData;
			int nAudioChannels = 0;
			if (pTemp[pos] == 0xAF && pTemp[pos + 1] == 0x00)
			{
				if (pTemp[pos + 2] != 0x11)//ONLY 48000
				{
					WriteLogA(m_szLogFile, FatalLevel, "RTMPDataDecoder::ProcessAudioData ONLY support 48000HZ (%d).",
						pTemp[pos + 2]);
					return;
				}

				switch (pTemp[pos + 3])
				{
				case 0x90:nAudioChannels = 2; break;
				case 0xA0:nAudioChannels = 4; break;
				case 0xB0:nAudioChannels = 6; break;
				default:
					break;
				}
				m_pCallback->InitAACDecoder(nAudioChannels);
				m_bGetAudioMeta = true;
			}
			else
			{
				WriteLogA(m_szLogFile, FatalLevel, "RTMPDataDecoder::ProcessAudioData the first two bytes(%x,%x) are expected(0xAF,0x00).",
					pTemp[pos], pTemp[pos + 1]);
			}
			return;
		}

		memcpy_s(m_pbufAudioUnit + m_dwAudioUnitPos, TEMP_AUDIO_SIZE - m_dwAudioUnitPos, pData, dataLength);
		m_dwAudioUnitPos += dataLength;

		if (m_pbufAudioUnit[pos] != 0xAF || m_pbufAudioUnit[pos + 1] != 0x01)
		{
			WriteLogA(m_szLogFile, FatalLevel, "RTMPDataDecoder::ProcessAudioData header bytes(%x,%x) are not correct!!!!",
				m_pbufAudioUnit[pos], m_pbufAudioUnit[pos + 1]);
			m_dwAudioUnitPos = 0;
			return;
		}
		m_naluCbDataA.bAddHeader = true;
		m_naluCbDataA.type	= 0;
		m_naluCbDataA.size = totalLength - 2;
		m_naluCbDataA.data = m_pbufAudioUnit + pos + 2;
		m_pCallback->cb(m_naluCbDataA);
		m_dwAudioUnitPos = 0;
	}
	else
	{
		memcpy_s(m_pbufAudioUnit + m_dwAudioUnitPos, TEMP_AUDIO_SIZE - m_dwAudioUnitPos, pData, dataLength);
		m_dwAudioUnitPos += dataLength;
	}
}

void RTMPDataDecoder::ProcessVideoData(BYTE *pData, double absoluteTimestamp, uint32_t dataLength, uint32_t processedLength, uint32_t totalLength)
{
	int pos = 0;
	m_naluCbDataV.bAudio = false;
	m_naluCbDataV.pts = absoluteTimestamp;
	if ((processedLength + dataLength) == totalLength)
	{
		cpuTick tickCur;
		UINT64 diff = 0;
		getTickCount(tickCur);
		if (m_lastTick != 0)
		{
			diff = tickCur - m_lastTick;
			if (diff >= (3*m_tickPerFrame))
				WriteLogA(m_szLogFile, WarnLevel, "--->RTMPDataDecoder::ProcessVideoData GetFramesFromRTMP(%I64d) time(%.2f).", m_GotFrameID,double(diff) / (m_freq / 1000));
		}
		m_lastTick = tickCur;
		++m_GotFrameID;
// 		static int nTest = 0;
// 		FILE *fp1 = nullptr;
// 		char szName[MAX_PATH];
// 		sprintf_s(szName, "D:\\JKLTEST\\%d.264", nTest++);
//		WriteLogA(m_szLogFile, InfoLevel, "RTMPDataDecoder::ProcessVideoData Done ts(%.2f) totalLength(%d)", absoluteTimestamp, totalLength);
		if (!m_bGetVideoMeta)
		{
// 			fopen_s(&fp1, szName, "wb");
// 			if (fp != nullptr)
// 				fwrite(pData, totalLength, 1, fp1);
// 			fclose(fp1);
			BYTE * pTemp = pData;			
			if (pTemp[pos + 0] == 0x17 &&
				pTemp[pos + 1] == 0x00 &&
				pTemp[pos + 2] == 0x00 &&
				pTemp[pos + 3] == 0x00 &&
				pTemp[pos + 4] == 0x00)
			{
				//SPS
				pos += 11;

				WORD nSpsLen = pTemp[pos] << 8 & 0xff00 | pTemp[pos + 1];
				pos += sizeof(WORD);

				m_naluCbDataV.bAddHeader = true;
				m_naluCbDataV.type = 0;
				m_naluCbDataV.size = nSpsLen;
				m_naluCbDataV.data = pTemp + pos;
				m_pCallback->cb(m_naluCbDataV);

				pos += nSpsLen;

				//PPS
				pos += 1;
				WORD nPpsLen = pTemp[pos] << 8 & 0xff00 | pTemp[pos + 1];
				pos += sizeof(WORD);
				m_naluCbDataV.bAddHeader = true;
				m_naluCbDataV.type = 0;
				m_naluCbDataV.size = nPpsLen;
				m_naluCbDataV.data = pTemp + pos;
				m_pCallback->cb(m_naluCbDataV);
				m_bGetVideoMeta = true;
			}
			else
				WriteLogA(m_szLogFile, FatalLevel, "RTMPDataDecoder::ProcessVideoData get meta data error.");
			return;
		}

		memcpy_s(m_pbufVideoUnit + m_dwVideoUnitPos, TEMP_VIDEO_SIZE - m_dwVideoUnitPos, pData, dataLength);
		m_dwVideoUnitPos += dataLength;

// 		fopen_s(&fp1, szName, "wb");
// 		if (fp != nullptr)
// 			fwrite(m_pbufVideoUnit, m_dwVideoUnitPos, 1, fp1);
// 		fclose(fp1);
		if (totalLength >= 11)
		{
			int nTempPos = 0;
			if (m_pbufVideoUnit[pos + 5] == 0x00 &&
				m_pbufVideoUnit[pos + 6] == 0x00 && 
				m_pbufVideoUnit[pos + 7] == 0x00 && 
				m_pbufVideoUnit[pos + 8] == 0x02 ) //NALU_TYPE_AUD
			{
				pos += 5;// skip 17 01 00 00 28 
				int nNaluSize = 0;
				while (pos < (m_dwVideoUnitPos - 4))
				{
					int nNALUsize = m_pbufVideoUnit[pos] << 24 & 0xff000000 | m_pbufVideoUnit[pos + 1] << 16 & 0x00ff0000 |
						m_pbufVideoUnit[pos + 2] << 8 & 0x0000ff00 | m_pbufVideoUnit[pos + 3] & 0x000000ff;
					pos += sizeof(DWORD);

					memcpy(m_pbufVideo+ nTempPos, headerVideo, 4);
					nTempPos += 4;
					memcpy(m_pbufVideo + nTempPos, m_pbufVideoUnit + pos, nNALUsize);
					nTempPos += nNALUsize;

					pos += nNALUsize;
				}

				m_naluCbDataV.bAddHeader = false;
				m_naluCbDataV.type = 0;
				m_naluCbDataV.size = nTempPos;
				m_naluCbDataV.data = m_pbufVideo;
				m_pCallback->cb(m_naluCbDataV);
			}
			else
			{
				if ((m_pbufVideoUnit[pos] == 0x17 || m_pbufVideoUnit[pos] == 0x27) &&
					m_pbufVideoUnit[pos + 1] == 0x01 &&
					m_pbufVideoUnit[pos + 2] == 0x00 &&
					m_pbufVideoUnit[pos + 3] == 0x00)
				{
					pos += 5;
					int nNALUsize = m_pbufVideoUnit[pos] << 24 & 0xff000000 | m_pbufVideoUnit[pos + 1] << 16 & 0x00ff0000 |
						m_pbufVideoUnit[pos + 2] << 8 & 0x0000ff00 | m_pbufVideoUnit[pos + 3] & 0x000000ff;
					pos += sizeof(DWORD);

					m_naluCbDataV.bAddHeader = true;
					m_naluCbDataV.type = 0;
					m_naluCbDataV.size = nNALUsize;
					m_naluCbDataV.data = m_pbufVideoUnit + pos;
					m_pCallback->cb(m_naluCbDataV);
				}
				else
					WriteLogA(m_szLogFile, ErrorLevel, "RTMPDataDecoder::ProcessVideoData error.(%d-%d-%d-%d-%d).",
						m_pbufVideoUnit[pos], m_pbufVideoUnit[pos + 1], m_pbufVideoUnit[pos + 2], m_pbufVideoUnit[pos + 3], m_pbufVideoUnit[pos + 4]);
			}
			m_dwVideoUnitPos = 0;
		}
		else
			WriteLogA(m_szLogFile, ErrorLevel, "RTMPDataDecoder::ProcessVideoData error.totalLength is less than 11(%d).", totalLength);
	}
	else
	{
		memcpy_s(m_pbufVideoUnit + m_dwVideoUnitPos, TEMP_VIDEO_SIZE - m_dwVideoUnitPos, pData, dataLength);
		m_dwVideoUnitPos += dataLength;
	}
}

void RTMPDataDecoder::AddRawRTMPData(BYTE *pData, double absoluteTimestamp, uint32_t dataLength,
	uint32_t processedLength, uint32_t totalLength, bool bAudio)
{	
	if (bAudio)
		ProcessAudioData(pData, absoluteTimestamp, dataLength, processedLength, totalLength);
	else
		ProcessVideoData(pData, absoluteTimestamp, dataLength, processedLength, totalLength);
	/*
	if (bAudio)
	{
		if ((processedLength + dataLength) == totalLength && totalLength != 7)
		{
			LockHolder locker(m_lockerQueue);
			m_audioQueue.push_back(totalLength);
		}
		m_bufAudio.WriteBuffer(pData, dataLength);
	}
 	else
		m_bufVideo.WriteBuffer(pData, dataLength);
	*/
}

int RTMPDataDecoder::_fetchRawRTMPData(uint8_t* buf, int size, bool bAudio)
{
	if (bAudio)
		return 	m_bufAudio.ReadBuffer(buf, size);
	else
		return 	m_bufVideo.ReadBuffer(buf, size);
}
