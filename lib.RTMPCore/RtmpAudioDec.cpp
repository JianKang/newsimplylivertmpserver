#include "RtmpAudioDec.h"
#include "../Lib.Logger/LogWriter.h"

CRtmpAudioDec::CRtmpAudioDec()
{
	m_audioFrameList.initialize<AudioFrame>(300);
}

CRtmpAudioDec::~CRtmpAudioDec()
{
}

int CRtmpAudioDec::buildAudioDec(int _idx, AVCodecID id,int sample_rate, int channels, AVSampleFormat sample_fmt,TCHAR *pLogPath)
{
	m_streamIdx = _idx;
	m_szLogFile = pLogPath;
	avcodec_register_all();



	m_pAVCodec = avcodec_find_decoder(id);
	m_pDec_ctx = avcodec_alloc_context3(m_pAVCodec);
	m_pDec_ctx->sample_rate = sample_rate;
	m_pDec_ctx->channels = channels;
	m_pDec_ctx->frame_size = 1024;
	m_pDec_ctx->bits_per_coded_sample = 16;
	m_pDec_ctx->profile = 1;
	//m_pDec_ctx->channel_layout = 3;
	//m_pDec_ctx->bit_rate = 96000;
	int nret = avcodec_open2(m_pDec_ctx, m_pAVCodec, nullptr);
	if (nret != 0)
	{
		return -1;
	}
	m_nChannelCnt = channels;
	uint64_t in_channel_layout = av_get_default_channel_layout(channels);
	uint64_t out_channel_layout = in_channel_layout;

	m_out_sample_rate = 48000;
	m_in_sample_rate = sample_rate;
	m_out_nb_samples = 0;
	
	m_out_channels = av_get_channel_layout_nb_channels(out_channel_layout);
	if (out_channel_layout == 0)
		m_out_channels = channels;
	m_out_sample_fmt = AV_SAMPLE_FMT_S32;

	m_nBufferSize = 0;
	au_convert_ctx = swr_alloc();
	av_opt_set_int(au_convert_ctx, "ich", channels, 0);
	av_opt_set_int(au_convert_ctx, "in_sample_rate", sample_rate, 0);
	av_opt_set_sample_fmt(au_convert_ctx, "in_sample_fmt", sample_fmt, 0);


	av_opt_set_int(au_convert_ctx, "och",channels, 0);
	av_opt_set_int(au_convert_ctx, "out_sample_rate", m_out_sample_rate, 0);
	av_opt_set_sample_fmt(au_convert_ctx, "out_sample_fmt", m_out_sample_fmt, 0);

	swr_init(au_convert_ctx);

	m_pAVFrame = av_frame_alloc();
	//m_loopBuffer.InitBuffer(MAX_AUDIO_CNT * MAX_AUDIO_FRAME_SIZE);
	m_bStop = false;

	AudioFrame *pFrame = nullptr;
	while (m_audioFrameList.pop_front_data(pFrame))
	{
		m_audioFrameList.push_back_empty(pFrame);
	}

	m_hdecthread = async(launch::async, &CRtmpAudioDec::_decThread, this);
	return 0;
}
int CRtmpAudioDec::buildAudioDec(int _idx, AVCodecContext*_pCtx,TCHAR *pLogPath)
{
	m_streamIdx = _idx;
	m_szLogFile = pLogPath;
	m_pDec_ctx = _pCtx;

	m_pAVCodec = avcodec_find_decoder(_pCtx->codec_id);
	int nret = avcodec_open2(_pCtx, m_pAVCodec, nullptr);

	m_nChannelCnt = _pCtx->channels;
	uint64_t in_channel_layout = av_get_default_channel_layout(_pCtx->channels);
	uint64_t out_channel_layout = in_channel_layout;

	m_out_sample_rate = 48000;
	m_in_sample_rate = _pCtx->sample_rate;
	m_out_nb_samples = 0;
	
	m_out_channels = av_get_channel_layout_nb_channels(out_channel_layout);
	if (out_channel_layout == 0)m_out_channels = _pCtx->channels;
	m_out_sample_fmt = AV_SAMPLE_FMT_S32;

	m_nBufferSize = 0;
	au_convert_ctx = swr_alloc();
	av_opt_set_int(au_convert_ctx, "ich", _pCtx->channels, 0);
	av_opt_set_int(au_convert_ctx, "in_sample_rate", _pCtx->sample_rate, 0);
	av_opt_set_sample_fmt(au_convert_ctx, "in_sample_fmt", _pCtx->sample_fmt, 0);


	av_opt_set_int(au_convert_ctx, "och", _pCtx->channels, 0);
	av_opt_set_int(au_convert_ctx, "out_sample_rate", m_out_sample_rate, 0);
	av_opt_set_sample_fmt(au_convert_ctx, "out_sample_fmt", m_out_sample_fmt, 0);

	swr_init(au_convert_ctx);

	m_pAVFrame = av_frame_alloc();
	//m_loopBuffer.InitBuffer(MAX_AUDIO_CNT * MAX_AUDIO_FRAME_SIZE);
	m_bStop = false;

	AudioFrame *pFrame = nullptr;
	while (m_audioFrameList.pop_front_data(pFrame))
	{
		m_audioFrameList.push_back_empty(pFrame);
	}

	m_hdecthread = async(launch::async, &CRtmpAudioDec::_decThread, this);
	return 0;
}
int CRtmpAudioDec::ReadData_16chs_32bits(int *pDestBuffer, int nperChannelSample)
{
	unsigned char*pp = (unsigned char *)pDestBuffer;
	int *pDest = pDestBuffer;
	int nNeedAudioSize = m_nChannelCnt;
	for (int i = 0; i < nperChannelSample / 4; i++)
	{
		int nReadSize = 0;// m_loopBuffer.ReadBuffer((unsigned char *)pDest, nNeedAudioSize * 4);
		if (nReadSize != nNeedAudioSize * 4)
		{
			printf("readbuffer error");
		}
		pDest += 16;
	}
	m_nReadedSize += nperChannelSample*nNeedAudioSize;
	return nNeedAudioSize;
}

bool CRtmpAudioDec::IsDataEnough(int nperChannelSample)
{
	//if (m_nBufferSize <= 0)
		return false;
	//return ((nperChannelSample*m_nChannelCnt) <= m_loopBuffer.GetSizeOfDataList());
}

int CRtmpAudioDec::Decode_Audio(const AVPacket *pkt)
{
	int bytesRemaining = 0;
	int bytesDecoded = 0;
	int got_picture = 0;
	uint8_t  *rawData = 0;

	bytesRemaining = pkt->size;
	rawData = pkt->data;
	m_nPktCount++;	
	AudioFrame *pFrameInfo = nullptr;
	while (bytesRemaining > 0)
	{
		if (m_bStop)return 0;
		bytesDecoded = avcodec_decode_audio4(m_pDec_ctx, m_pAVFrame, &got_picture, pkt);
		if (bytesDecoded < 0)
			return -1;

		bytesRemaining -= bytesDecoded;
		rawData += bytesDecoded;
		if (got_picture)
		{
			//WriteLogA(m_szLogFile, InfoLevel, "audio pts(%I64d) dts(%I64d) size(%d)", pkt->pts, pkt->dts, pkt->size);
			if (!m_audioFrameList.pop_front_empty(pFrameInfo))
				return -2;
			int m_dst_lineSize = 0;
			if (m_nBufferSize <= 0)
			{
				m_out_nb_samples = av_rescale_rnd(swr_get_delay(au_convert_ctx, m_in_sample_rate) + m_pAVFrame->nb_samples, m_out_sample_rate, m_in_sample_rate, AV_ROUND_UP);
				
				av_samples_alloc(&m_pDestBuffer, &m_dst_lineSize, m_out_channels, m_out_nb_samples, m_out_sample_fmt,1);
			}
			int nbConvert = swr_convert(au_convert_ctx, &m_pDestBuffer, m_out_nb_samples, (const uint8_t **)m_pAVFrame->data, m_pAVFrame->nb_samples);
			if (m_nBufferSize <= 0)
			{
				m_nBufferSize = av_samples_get_buffer_size(&m_dst_lineSize, m_out_channels, nbConvert, m_out_sample_fmt, 1);
				//m_loopBuffer.InitBuffer(MAX_AUDIO_CNT * m_nBufferSize);
			}


			pFrameInfo->setPts(pkt->pts);
			pFrameInfo->setBufferSize(m_nBufferSize);
			memcpy(pFrameInfo->getRaw(), m_pDestBuffer, m_nBufferSize);
			m_audioFrameList.push_back_data(pFrameInfo);

			//if (-1 == m_loopBuffer.WriteBuffer(m_pDestBuffer, m_nBufferSize))
			//	return -2;
			nDecodedCnt++;
			return 1;
		}
	}

	return 0;
}

bool CRtmpAudioDec::Flush_data(const AVPacket *pkt)
{
	return true;
}

void CRtmpAudioDec::CloseDec()
{
	m_bStop = true;
	if (m_hdecthread.valid())
		m_hdecthread.wait();
	swr_free(&au_convert_ctx);
	av_frame_free(&m_pAVFrame);
	//avcodec_close(m_pDec_ctx);
	//m_loopBuffer.UnInitBuffer();
}

void CRtmpAudioDec::addPacket(AVPacket * pkt)
{
	LockHolder lock(locker);
	lst_audio_buffer_.push_back(pkt);
}

void CRtmpAudioDec::_decThread()
{
	AVPacket * pkt = nullptr;

	while (!m_bStop)
	{
		if (lst_audio_buffer_.empty())
		{
			Sleep(5);
			continue;
		}
		{
			LockHolder lock(locker);
			pkt = lst_audio_buffer_.front();
			lst_audio_buffer_.pop_front();
		}

		int nRet = Decode_Audio(pkt);
		av_free_packet(pkt);
		if (nRet < 0)
		{
			printf("---->Decode audio failed.\n");
			WriteLogA(m_szLogFile, ErrorLevel, "CRtmpAudioDec::_decThread Decode_Audio failed(%d).", nRet);
			break;
		}
	}
	while (!lst_audio_buffer_.empty())
	{

		pkt = lst_audio_buffer_.front();
		lst_audio_buffer_.pop_front();
		av_free_packet(pkt);
	}
	WriteLogA(m_szLogFile, WarnLevel, "CRtmpAudioDec::_decThread exit.");
}
