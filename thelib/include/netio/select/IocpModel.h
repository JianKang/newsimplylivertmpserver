#ifndef __IOCPMODEL_H
#define	__IOCPMODEL_H
#include <vector>
#include <mswsock.h>

#define MAX_BUFFER_LEN        8192 

typedef enum _OPERATION_TYPE
{
	ACCEPT_POSTED,    
	SEND_POSTED,      
	RECV_POSTED,      
	NULL_POSTED       

}OPERATION_TYPE;


typedef struct _PER_IO_CONTEXT
{
	OVERLAPPED     m_Overlapped;                                 
	SOCKET         m_sockAccept;                    
	WSABUF         m_wsaBuf;                        
	char           m_szBuffer[MAX_BUFFER_LEN];  
	void		   *m_pTcpCarrier;
	OPERATION_TYPE m_OpType;   					
	_PER_IO_CONTEXT()
	{
		ZeroMemory(&m_Overlapped, sizeof(m_Overlapped));
		ZeroMemory(m_szBuffer, MAX_BUFFER_LEN);
		m_sockAccept = INVALID_SOCKET;
		m_wsaBuf.buf = m_szBuffer;
		m_wsaBuf.len = MAX_BUFFER_LEN;
		m_OpType = NULL_POSTED;
		m_pTcpCarrier = nullptr;
	}
	~_PER_IO_CONTEXT()
	{
		if (m_sockAccept != INVALID_SOCKET)
		{
			closesocket(m_sockAccept);
			m_sockAccept = INVALID_SOCKET;
		}
	}
	void ResetBuffer()
	{
		m_wsaBuf.len = MAX_BUFFER_LEN;
		ZeroMemory(m_szBuffer, MAX_BUFFER_LEN);
	}

} PER_IO_CONTEXT, *PPER_IO_CONTEXT;


typedef struct _PER_SOCKET_CONTEXT
{
	SOCKET      m_Socket;                               
	SOCKADDR_IN m_ClientAddr;  	
	vector<_PER_IO_CONTEXT*> m_arrayIoContext;       
	_PER_SOCKET_CONTEXT()
	{
		m_Socket = INVALID_SOCKET;
		memset(&m_ClientAddr, 0, sizeof(m_ClientAddr));
	}

	~_PER_SOCKET_CONTEXT()
	{
		if (m_Socket != INVALID_SOCKET)
		{
			closesocket(m_Socket);
			m_Socket = INVALID_SOCKET;
		}
		for (int i = 0; i < m_arrayIoContext.size(); i++)
		{
			delete m_arrayIoContext[i];
		}
		m_arrayIoContext.clear();
	}

	_PER_IO_CONTEXT* GetNewIoContext()
	{
		_PER_IO_CONTEXT* p = new _PER_IO_CONTEXT;
		m_arrayIoContext.push_back(p);
		return p;
	}

	void RemoveContext(_PER_IO_CONTEXT* pContext)
	{
		for (int i = 0; i < m_arrayIoContext.size(); i++)
		{
			if (pContext == m_arrayIoContext[i])
			{
				delete pContext;
				pContext = NULL;
				m_arrayIoContext.erase(m_arrayIoContext.begin()+i);
				break;
			}
		}
	}

} PER_SOCKET_CONTEXT, *PPER_SOCKET_CONTEXT;


#define WORKER_THREADS_PER_PROCESSOR 1
#define MAX_POST_ACCEPT              6
#define EXIT_CODE                    NULL
#define RELEASE(x)                      {if(x != NULL ){delete x;x=NULL;}}
#define RELEASE_HANDLE(x)               {if(x != NULL && x!=INVALID_HANDLE_VALUE){ CloseHandle(x);x = NULL;}}
#define RELEASE_SOCKET(x)               {if(x !=INVALID_SOCKET) { closesocket(x);x=INVALID_SOCKET;}}

#endif	/* __IOCPMODEL_H */
