#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "DecodeH264.h"
#include <Windows.h>
#include <iostream>

using namespace std;

void fun()
{
     FILE*   fpIn = fopen("d:\\test.264","rb");
    if(fpIn == NULL)
        return ;

    FILE*   fpOut = fopen("D:\\test.yuv","wb");
    
    DecodeH264* pDecoder = new DecodeH264();
    if(pDecoder->Init(0,1920, 1088) < 0)
        return ;

    int nFrameLen = 0;
    char*   pFrame = new char[1920*1088*4];
    int nReadSize = 0;
    char*   pOut = new char[2048*1088*4];
    int     nOutLen = 0;

    bool    bFinish = false;

    DWORD dwCur = GetTickCount();
    int i = 0;
   
    while(true)
    {
       i++;
      // if(i == 100)
       //    bFinish = true;
       nReadSize = fread(&nFrameLen,1,4,fpIn);
       if(nReadSize != 4)
       {
           while(pDecoder->GetDecodedData((unsigned char*)pOut,nOutLen) == 0)
           {
               fwrite(pOut,1,nOutLen,fpOut);
           }
           break;
       }

        nReadSize = fread(pFrame,1,nFrameLen,fpIn);
        if(nReadSize != nFrameLen)
            break;

        if(pDecoder->DecodeVideoH264(pFrame,nFrameLen,bFinish) < 0)
            break;

        if(pDecoder->GetDecodedData((unsigned char*)pOut,nOutLen) == 0)
        {
            fwrite(pOut,1,nOutLen,fpOut);
        }
    }

     DWORD dwEnd = GetTickCount();
    cout<<dwEnd-dwCur<<endl;

    fclose(fpIn);
    fclose(fpOut);
}

void fun1()
{
    FILE*   fpIn1 = fopen("D:\\TestFile\\test720-5.h264","rb");
    if(fpIn1 == NULL)
        return ;

    FILE*   fpIn2 = fopen("D:\\TestFile\\test720-1.h264","rb");
    if(fpIn2 == NULL)
        return ;

    FILE*   fpOut = fopen("D:\\720-5.yuv","wb");
    FILE*   fpOut2 = fopen("D:\\720-1.yuv","wb");
    
    DecodeH264* pDecoder = new DecodeH264();
    if(pDecoder->Init(0, 1280,720) < 0)
        return ;

    delete pDecoder;

    //DecodeH264* pDecoder2 = new DecodeH264();
    //if(pDecoder2->Init(1280,720) < 0)
    //    return ;

    //int nFrameLen = 0;
    //char*   pFrame = new char[1920*1088*4];
    //int nReadSize = 0;
    //char*   pOut = new char[2048*1088*4];
    //int     nOutLen = 0;

    //bool    bFinish = false;

    //DWORD dwCur = GetTickCount();
    //int i = 0;
   
    //while(true)
    //{

    //    nReadSize = fread(&nFrameLen,1,4,fpIn1);

    //    nReadSize = fread(pFrame,1,nFrameLen,fpIn1);
    //    if(nReadSize != nFrameLen)
    //        break;

    //    if(pDecoder->DecodeVideoH264(pFrame,nFrameLen,bFinish) < 0)
    //        break;

    //    if(pDecoder->GetDecodedData((unsigned char*)pOut,nOutLen) == 0)
    //    {
    //        fwrite(pOut,1,nOutLen,fpOut);
    //    }
    //}

    //while(true)
    //{

    //    nReadSize = fread(&nFrameLen,1,4,fpIn2);

    //    nReadSize = fread(pFrame,1,nFrameLen,fpIn2);
    //    if(nReadSize != nFrameLen)
    //        break;

    //    if(pDecoder2->DecodeVideoH264(pFrame,nFrameLen,bFinish) < 0)
    //        break;

    //    if(pDecoder2->GetDecodedData((unsigned char*)pOut,nOutLen) == 0)
    //    {
    //        fwrite(pOut,1,nOutLen,fpOut2);
    //    }
    //}

    //DWORD dwEnd = GetTickCount();
    //cout<<dwEnd-dwCur<<endl;

    fclose(fpIn1);
    fclose(fpIn2);
    fclose(fpOut);
    fclose(fpOut2);
}

int main(int argc,char* argv[])
{
    fun();

    return 0;
}