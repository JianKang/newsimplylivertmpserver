#include "DecodeH264.h"
#include <Windows.h>
//#include <d3dx9.h>
//#include <cuda_runtime.h>
//#include <cudad3d9.h>
#include <inc/helper_functions.h>
#include <inc/helper_cuda_drvapi.h>

DecodeH264::DecodeH264(void)
{
    m_Context = 0;
    m_device = 0;
    m_pBuf = NULL;
    m_pParser = NULL;
    m_pDecoder= NULL;
    m_pFrameQue = NULL;
    m_bFrameData[0] = NULL;
    m_bFrameData[1] = NULL;

    m_pInteropFrame[0] = NULL;
    m_pInteropFrame[1] = NULL;
    m_bFirstFrame = true;
    m_ReadbackSID = 0;
    m_pDecodedList = new CFrameList();

	m_dwChannel = 0;
    m_nWidth = 0;
    m_nHeight = 0;
    m_pTemp = NULL;
}


DecodeH264::~DecodeH264(void)
{
    Release();
}

int DecodeH264::Init(DWORD dwChannel, int nWidth,int nHeight)
{
	m_dwChannel = dwChannel;
    m_nWidth = nWidth;
    m_nHeight = nHeight;
	CUdevice cuda_device = 0;
	CUresult oResult = CUDA_SUCCESS;

	//CUDADRIVER hHandleDriver = 0;
	oResult = cuInit(0, __CUDA_API_VERSION, nullptr);
	if (CUDA_SUCCESS != oResult)
		return -1;
	oResult = cuvidInit(0);
	if (CUDA_SUCCESS != oResult)
		return -1;


    cuda_device = gpuGetMaxGflopsDeviceIdDRV();
    oResult = cuDeviceGet(&m_device, cuda_device);
    if(CUDA_SUCCESS != oResult)
        return -1;

    int major = 0, minor = 0;
    size_t totalGlobalMem = 0;
    char deviceName[256];
    cuDeviceComputeCapability(&major, &minor, m_device);
    cuDeviceGetName(deviceName, 256, m_device);

    oResult = cuCtxCreate(&m_Context,CU_CTX_BLOCKING_SYNC,m_device);
    if(CUDA_SUCCESS != oResult)
        return -1;

    oResult = cuvidCtxLockCreate(&m_VidCtxLock,m_Context);
    if(CUDA_SUCCESS != oResult)
        return -1;

    m_VideoCreateFlags = cudaVideoCreate_PreferCUVID;
    memset(&m_VideoDecodeCreateInfo, 0, sizeof(CUVIDDECODECREATEINFO));

    m_VideoDecodeCreateInfo.CodecType           = cudaVideoCodec_H264;
    m_VideoDecodeCreateInfo.ulWidth             = nWidth;
    m_VideoDecodeCreateInfo.ulHeight            = nHeight;
    m_VideoDecodeCreateInfo.ulNumDecodeSurfaces = 20;

    // Limit decode memory to 24MB (16M pixels at 4:2:0 = 24M bytes)
    while (m_VideoDecodeCreateInfo.ulNumDecodeSurfaces * nWidth * nHeight > 16*1024*1024)
    {
        m_VideoDecodeCreateInfo.ulNumDecodeSurfaces--;
    }

    m_VideoDecodeCreateInfo.ChromaFormat            = cudaVideoChromaFormat_420;
    m_VideoDecodeCreateInfo.OutputFormat            = cudaVideoSurfaceFormat_NV12;
    m_VideoDecodeCreateInfo.DeinterlaceMode         = cudaVideoDeinterlaceMode_Adaptive;
    m_VideoDecodeCreateInfo.ulTargetWidth           = nWidth;
    m_VideoDecodeCreateInfo.ulTargetHeight          = nHeight;
    m_VideoDecodeCreateInfo.ulNumOutputSurfaces     = 2;
    m_VideoDecodeCreateInfo.ulCreationFlags         = m_VideoCreateFlags;
    m_VideoDecodeCreateInfo.vidLock                 = m_VidCtxLock;

    oResult = cuvidCreateDecoder(&m_Decoder, &m_VideoDecodeCreateInfo);
    if(CUDA_SUCCESS != oResult)
        return -1;

    m_pBuf = new unsigned char[nWidth*nHeight*4];
    if(m_pBuf == NULL)
        return -1;

    m_pFrameQue = new FrameQueue();
    m_pDecoder = new VideoDecoder(m_Context,m_VideoCreateFlags,m_VidCtxLock,nWidth,nHeight);
    m_pParser = new VideoParser(m_pDecoder,m_pFrameQue);

    cuMemAlloc(&m_pInteropFrame[0], m_pDecoder->targetWidth() * m_pDecoder->targetHeight() * 2);
    cuMemAlloc(&m_pInteropFrame[1], m_pDecoder->targetWidth() * m_pDecoder->targetHeight() * 2);

    oResult = cuStreamCreate(&m_ReadbackSID, 0);
    if(CUDA_SUCCESS != oResult)
        return -1;

    //if(m_ReadbackSID)
    //{
    //    cuStreamDestroy(m_ReadbackSID);
    //    m_ReadbackSID = NULL;
    //}

    oResult = cuCtxPopCurrent(&m_Context);

    if (oResult != CUDA_SUCCESS)
    {
        return -1;
    }

    m_pTemp = new char[nWidth*nHeight];
	m_sTempVideo = new unsigned char[nWidth * nHeight * 2];
    return 0;
}

int DecodeH264::DecodeVideoH264(char*   pH264,int nLen,double pts,bool bIsEnd)
{
    if(pH264 == NULL || nLen < 0)
        return -1;

    CUVIDSOURCEDATAPACKET packet;    
    std::memset(&packet, 0, sizeof(CUVIDSOURCEDATAPACKET));

    if(bIsEnd)
    {
        packet.flags |= CUVID_PKT_ENDOFSTREAM;
        m_pFrameQue->endDecode();
    }

    //memcpy(m_pBuf,pH264,nLen);
    packet.payload_size = static_cast<unsigned long>(nLen);
    packet.payload = (unsigned char*)pH264;

    if (cuvidParseVideoData(m_pParser->hParser_, &packet) != CUDA_SUCCESS)
        return -1;

    CUVIDPARSERDISPINFO oDisplayInfo;
    int bIsProgressive = 1;
    if (m_pFrameQue->dequeue(&oDisplayInfo))
    {
        CCtxAutoLock lck(m_VidCtxLock);
        // Push the current CUDA context (only if we are using CUDA decoding path)
        CUresult result = cuCtxPushCurrent(m_Context);

        CUdeviceptr  pDecodedFrame[2] = { 0, 0 };
        CUdeviceptr  pInteropFrame[2] = { 0, 0 };

        int num_fields = (oDisplayInfo.progressive_frame ? (1) : (2+oDisplayInfo.repeat_first_field));
        bIsProgressive = oDisplayInfo.progressive_frame;
		bool bGot = false;
        for (int active_field=0; active_field<num_fields; active_field++)
        {
            //nRepeats = oDisplayInfo.repeat_first_field;
            CUVIDPROCPARAMS oVideoProcessingParameters;
            memset(&oVideoProcessingParameters, 0, sizeof(CUVIDPROCPARAMS));

            oVideoProcessingParameters.progressive_frame = oDisplayInfo.progressive_frame;
            oVideoProcessingParameters.second_field      = active_field;
            oVideoProcessingParameters.top_field_first   = oDisplayInfo.top_field_first;
            oVideoProcessingParameters.unpaired_field    = (num_fields == 1);

            unsigned int nDecodedPitch = 0;
            unsigned int nWidth = 0;
            unsigned int nHeight = 0;

            // map decoded video frame to CUDA surfae
            m_pDecoder->mapFrame(oDisplayInfo.picture_index, &pDecodedFrame[active_field], &nDecodedPitch, &oVideoProcessingParameters);
            nWidth  = m_pDecoder->targetWidth();
            nHeight = m_pDecoder->targetHeight();
            // map DirectX texture to CUDA surface
            size_t nTexturePitch = 0;

            // If we are Encoding and this is the 1st Frame, we make sure we allocate system memory for readbacks
            if ( m_bFirstFrame && m_ReadbackSID)
            {
                CUresult result;
                result = cuMemAllocHost((void **)&m_bFrameData[0], (nDecodedPitch * nHeight * 3 / 2));
                result = cuMemAllocHost((void **)&m_bFrameData[1], (nDecodedPitch * nHeight * 3 / 2));
                m_bFirstFrame = false;

                if (result != CUDA_SUCCESS)
                {
                    return -2;
                }
            }

            // If streams are enabled, we can perform the readback to the host while the kernel is executing
            if (m_ReadbackSID && active_field == 0)
            {
                CUresult result = cuMemcpyDtoHAsync(m_bFrameData[active_field], pDecodedFrame[active_field], 
                    (nDecodedPitch * nHeight * 3 / 2), m_ReadbackSID);
                if (result != CUDA_SUCCESS)
                {
                    return -1;
                }

                while (CUDA_ERROR_NOT_READY == cuStreamQuery(m_ReadbackSID)) 
                {
                    Sleep(1);
                }		
                //m_pDecodedList->AddData(m_bFrameData[active_field],nDecodedPitch * nHeight * 3 / 2);
				int nOutLen = 0;
#ifdef USE_GPU_CONVERT
				m_cudaDec.DMA_CPU_GPU_NV12(0, 0, m_bFrameData[active_field], nDecodedPitch, nHeight);
				m_cudaDec.NV12_to_yuv422(0,0, 1);
				m_cudaDec.DMA_GPU_CPU_YUV422(0, 1, m_pBuf);
				m_cudaDec.waitEngine(0);
#else              
				ConvertNV12toUYVY((char*)m_bFrameData[active_field],nDecodedPitch,nHeight,(char*)m_pBuf,nOutLen);	
#endif			
				int nResHeight = m_nHeight;
				if (m_nHeight == 1088)
					nResHeight = 1080;
				nOutLen = nResHeight*m_nWidth * 2;
				m_pDecodedList->AddData(m_pBuf, nOutLen,pts);
				bGot = true;
            }

            m_pDecoder->unmapFrame(pDecodedFrame[active_field]);
            // release the frame, so it can be re-used in decoder
            m_pFrameQue->releaseFrame(&oDisplayInfo);
			if(bGot)
				break;
        }
    }

    return 0;
}

void DecodeH264::Release()
{
    if(m_ReadbackSID)
    {
        cuStreamDestroy(m_ReadbackSID);
        m_ReadbackSID = NULL;
    }

    cuCtxDestroy(m_Context);
    cuvidCtxLockDestroy(m_VidCtxLock);

    if(m_pBuf != NULL)
    {
        delete [] m_pBuf;
        m_pBuf = NULL;
    }

    if(m_pParser != NULL)
    {
        delete m_pParser;
        m_pParser = NULL;
    }

    if(m_pFrameQue != NULL)
    {
        delete m_pFrameQue;
        m_pFrameQue = NULL;
    }

    if(m_bFrameData[0] != NULL)
    {
        cuMemFreeHost((void *)m_bFrameData[0]);
        m_bFrameData[0] = 0;
    }

    if(m_bFrameData[1] != NULL)
    {
        cuMemFreeHost((void *)m_bFrameData[1]);
        m_bFrameData[1] = 0;
    }

    if(m_pInteropFrame[0] != NULL)
    {
        cuMemFree(m_pInteropFrame[0]);
        m_pInteropFrame[0] = NULL;
    }

    if(m_pInteropFrame[1] != NULL)
    {
        cuMemFree(m_pInteropFrame[1]);
        m_pInteropFrame[1] = NULL;
    }

    if(m_pDecodedList != NULL)
    {
        delete m_pDecodedList;
        m_pDecodedList = NULL;
    }

    if(m_pTemp != NULL)
    {
        delete [] m_pTemp;
		m_pTemp = nullptr;
    }
	if (m_sTempVideo != NULL)
	{
		delete[] m_sTempVideo;
		m_sTempVideo = nullptr;
	}
	
}

/* convert 4:2:0 to yuv 4:2:2 */
void yuv420p_to_UYVY(uint8_t * yuv420[3], uint8_t * dest, int width,int height)
{
	for (unsigned int y = 0; y < height; ++y)
	{
		uint8_t *Y = yuv420[0] + y * width;
		uint8_t *Cb = yuv420[1] + (y / 2) * (width / 2);
		uint8_t *Cr = yuv420[2] + (y / 2) * (width / 2);
		for (unsigned int x = 0; x < width; x += 2)
		{
			*(dest + 0) = Cb[0]; 
			*(dest + 1) = Y[0];
			*(dest + 2) = Cr[0]; 
			*(dest + 3) = Y[1];
			dest += 4;
			Y += 2;
			++Cb;
			++Cr;
		}
	}
}

void DecodeH264::ConvertNV12toUYVY(char* pNV12Src,int nNV12WidthSrc,int nNV12HeightSrc,char* pDstOut,int& nOutLen)
{
	{
		uint8_t * dest = (uint8_t *)pDstOut;
		for (unsigned int y = 0; y < m_nHeight; ++y)
		{
			uint8_t *Y = (uint8_t *)pNV12Src + y * nNV12WidthSrc;
			uint8_t *Cb = (uint8_t *)pNV12Src + nNV12WidthSrc*nNV12HeightSrc + (y / 2) * nNV12WidthSrc;
			uint8_t *Cr = (uint8_t *)pNV12Src + nNV12WidthSrc*nNV12HeightSrc + (y / 2) * nNV12WidthSrc + 1;

			for (unsigned int x = 0; x < m_nWidth; x += 2)
			{
				*(dest + 0) = Cb[0];
				*(dest + 1) = Y[0];
				*(dest + 2) = Cr[0];
				*(dest + 3) = Y[1];
				dest += 4;
				Y += 2;
				Cb += 2;
				Cr += 2;
			}
		}
		int nResHeight = m_nHeight;
		if (m_nHeight == 1088)
			nResHeight = 1080;
		nOutLen = m_nWidth*nResHeight * 2;		
	}
	return;
    //copy y fild
    int nIndexNV12 = 0;
    int nIndexYUV = 0;
    for(int i=0;i<m_nHeight;i++)
    {
        memcpy(m_sTempVideo + nIndexYUV,pNV12Src + nIndexNV12,m_nWidth);
        nIndexYUV += m_nWidth;
        nIndexNV12 += nNV12WidthSrc;
    }

    //copy uv filed
    int nIndexTemp = 0;
    nIndexNV12 = nNV12WidthSrc*nNV12HeightSrc;
    for(int i=0;i<m_nHeight/2;i++)
    {
        memcpy(m_pTemp + nIndexTemp,pNV12Src + nIndexNV12,m_nWidth);
        nIndexTemp += m_nWidth;
        nIndexNV12 += nNV12WidthSrc;
    }
  
    //convert NV12 to YUV420  (U V filed)
    for(int j=0;j<m_nWidth*m_nHeight/4;j++)
    {
        //u filed
        m_sTempVideo[m_nWidth*m_nHeight + j] = m_pTemp[j*2];
        //v filed
        m_sTempVideo[m_nWidth*m_nHeight + m_nWidth*m_nHeight/4 + j] = m_pTemp[j*2 + 1];
    }	

	//YUV420 ->UYVY
	uint8_t * dest = (uint8_t *)pDstOut;
	for (unsigned int y = 0; y < m_nHeight; ++y)
	{
		uint8_t *Y = m_sTempVideo + y * m_nWidth;
		uint8_t *Cb = m_sTempVideo + m_nWidth*m_nHeight + (y / 2) * (m_nWidth / 2);
		uint8_t *Cr = m_sTempVideo + m_nWidth*m_nHeight + m_nWidth*m_nHeight / 4 + (y / 2) * (m_nWidth / 2);
		for (unsigned int x = 0; x < m_nWidth; x += 2)
		{
			*(dest + 0) = Cb[0];
			*(dest + 1) = Y[0];
			*(dest + 2) = Cr[0];
			*(dest + 3) = Y[1];
			dest += 4;
			Y += 2;
			++Cb;
			++Cr;
		}
	}
	nOutLen = m_nWidth*m_nHeight * 2;
}