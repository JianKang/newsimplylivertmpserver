#pragma once

#include "../DecodeH264/inc/dynlink_cuda.h"
#include <../DecodeH264/inc/dynlink_cuda.h>
#include <../DecodeH264/inc/dynlink_cuviddec.h>
#include <../DecodeH264/inc/dynlink_nvcuvid.h>
#include "VideoParser.h"
#include "FrameQueue.h"
#include "VideoDecoder.h"
#include "FrameList.h"

class DecodeH264
{
public:
    DecodeH264(void);
    ~DecodeH264(void);

    int         Init(DWORD dwChannel, int nWidth,int nHeight);
    int         DecodeVideoH264(char*   pH264,int nLen,double pts, bool bIsEnd);
    void        Release();
    FrameQueue*     GetFrameQueue() {   return m_pFrameQue;}
    VideoParser*    GetParser() {   return m_pParser;}

    int            GetDecodedData(unsigned char* pOut,int& nOutLen, double &pts)
    {
        return m_pDecodedList->GetData(pOut,nOutLen,pts);
    }

    void            ConvertNV12toUYVY(char* pNV12,int nNV12Width,int nNV12Height, char* pYUV,int& nYUVLen);
private:
    CUcontext               m_Context;
    CUvideoctxlock          m_VidCtxLock;
    CUvideodecoder          m_Decoder;
    CUVIDDECODECREATEINFO   m_VideoDecodeCreateInfo;
    cudaVideoCreateFlags    m_VideoCreateFlags;
    CUdevice                m_device;

    unsigned char*          m_pBuf;
    VideoParser*            m_pParser;
    FrameQueue*             m_pFrameQue;
    VideoDecoder*           m_pDecoder;
    BYTE                    *m_bFrameData[2];
    CUdeviceptr             m_pInteropFrame[2];

    bool                    m_bFirstFrame;
    CUstream                m_ReadbackSID;
    CFrameList*             m_pDecodedList;              
    int                     m_nWidth;
    int                     m_nHeight;
	DWORD					m_dwChannel;
    char*                   m_pTemp;
	unsigned char *m_sTempVideo;

};

