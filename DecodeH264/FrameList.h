#pragma once
#include <queue>
//#include <boost/thread/mutex.hpp>

using namespace std;
//using namespace boost;

class Frame
{
public:
	Frame(int nLen);
	~Frame();

	int				Init(unsigned char*	pData,int nLen, double pts);

	unsigned char*	GetFrameBuf()	{	return m_pBuf;}
	int				GetFrameSize()	{	return m_nBufSize;}
	double			GetFramePts() { return m_pts; }
private:
	unsigned char*		m_pBuf;
	int					m_nBufSize;
	double				m_pts;
};

class CFrameList
{
public:
	CFrameList();
	~CFrameList();

	int		Init(int nWidth,int nHeith);

	int		AddData(unsigned char*	pData,int nLen,double pts);
	int		GetData(unsigned char*	pData,int& nLen, double &pts);
private:
	queue<Frame*>		m_FreeQue;
	queue<Frame*>		m_FrameQue;
	int					m_nMaxFrameLen;
	double				m_lastPts;
	//mutex				m_lock;
};