#include "FrameList.h"

Frame::Frame(int nLen)
{
	m_pBuf = new unsigned char[nLen];
}

Frame::~Frame()
{
	if(m_pBuf != NULL)
	{
		delete [] m_pBuf;
		m_pBuf = NULL;
	}
}

int Frame::Init( unsigned char* pData,int nLen, double pts)
{
	if(pData == NULL || nLen < 0)
		return -1;

	memcpy(m_pBuf,pData,nLen);
	m_nBufSize = nLen;
	m_pts = pts;
	return 0;
}


CFrameList::CFrameList()
{
	m_nMaxFrameLen = 1920*1080*4;
}

CFrameList::~CFrameList()
{
	int nFreeQueSize = m_FreeQue.size();
	for(int i=0;i<nFreeQueSize;i++)
	{
		Frame*	pFrame = m_FreeQue.front();
		delete pFrame;
		m_FreeQue.pop();
	}

	int nFrameQueSize = m_FrameQue.size();
	for(int j=0;j<nFrameQueSize;j++)
	{
		Frame*	pFrame = m_FrameQue.front();
		delete pFrame;
		m_FrameQue.pop();
	}
}

int CFrameList::Init( int nWidth,int nHeith )
{
	m_nMaxFrameLen = nWidth*nHeith*4;
	//test
	for(int i=0;i<100;i++)
	{
		Frame*	pFrame = new Frame(m_nMaxFrameLen);
		m_FreeQue.push(pFrame);
	}
	return 0;
}

int CFrameList::AddData( unsigned char* pData,int nLen, double pts)
{
	//多线程需要加锁
	if(pData == NULL || nLen < 0)
		return -1;
	if (pts != m_lastPts)
		m_lastPts = pts;
	else
		int a = 0;
	Frame*	pFrame = NULL;
	if(m_FreeQue.empty())
	{
		pFrame = new Frame(m_nMaxFrameLen);
	}
	else
	{
		pFrame = m_FreeQue.front();
		m_FreeQue.pop();
	}

	pFrame->Init(pData,nLen,pts);
	m_FrameQue.push(pFrame);
	
	return 0;
}

int CFrameList::GetData( unsigned char* pData,int& nLen, double &pts)
{
	if(m_FrameQue.empty())
		return -1;

	Frame*	pFrame = NULL;
	pFrame = m_FrameQue.front();
	m_FrameQue.pop();
	pts = pFrame->GetFramePts();
	nLen = pFrame->GetFrameSize();
	memcpy(pData,pFrame->GetFrameBuf(),nLen);
	m_FreeQue.push(pFrame);
	return 0;
}