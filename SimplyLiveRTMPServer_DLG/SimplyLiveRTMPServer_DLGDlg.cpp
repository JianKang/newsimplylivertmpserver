
// SimplyLiveRTMPServer_DLGDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SimplyLiveRTMPServer_DLG.h"
#include "SimplyLiveRTMPServer_DLGDlg.h"
#include "afxdialogex.h"
#include "../lib.Base/CapturePoolMgr.h"
#include "../lib.Base/TimeCount.h"
#include "../Lib.Logger/LogWriter.h"
#include "../FrameConsumer/Lib.FrameConsumerManager/IFrameConsumerManager.h"
#include "../lib.Base/VFrame_1280_720.h"
#include "../lib.Base/VFrame_1920_1080.h"
#include "../lib.Base/VFrame_3840_2160.h"
#include "../FrameConsumer/Lib.FrameConsumer/IFrameConsumer.h"

FILE _iob[] = { *stdin, *stdout, *stderr };
extern "C" FILE * __cdecl __iob_func(void) { return _iob; }

#pragma comment(lib,"SDL2.lib")
#pragma comment(lib,"SDL2main.lib")

#define VERSION_NUMVER_U	L"1.00.32.01"
#define VERSION_NUMVER_A	 "1.00.32.01"

#define SimplyLiveRTMPServerLog  L"C:\\Logs\\SimplyLiveRTMPServer\\SimplyLiveRTMPServer.log"

Uint32  CSimplyLiveRTMPServer_DLGDlg::audio_len = 0;
Uint8  *CSimplyLiveRTMPServer_DLGDlg::audio_chunk = nullptr;
Uint8  *CSimplyLiveRTMPServer_DLGDlg::audio_pos = nullptr;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CSimplyLiveRTMPServer_DLGDlg dialog



CSimplyLiveRTMPServer_DLGDlg::CSimplyLiveRTMPServer_DLGDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_SIMPLYLIVERTMPSERVER_DLG_DIALOG, pParent)
{
	//m_hTimerRender = nullptr;
	for (int _cnl = 0; _cnl < MAX_PAYER; ++_cnl)
	{
		m_novVideo[_cnl] = 0;
	}
	m_bDumpUYVY = false;
	m_szConvertAudio = new BYTE[100 * 1024];
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSimplyLiveRTMPServer_DLGDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO3, m_comRecorders);
}

BEGIN_MESSAGE_MAP(CSimplyLiveRTMPServer_DLGDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_CBN_SELCHANGE(IDC_COMBO3, &CSimplyLiveRTMPServer_DLGDlg::OnCbnSelchangeCombo3)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CSimplyLiveRTMPServer_DLGDlg message handlers

BOOL CSimplyLiveRTMPServer_DLGDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	CString strVerion;
	strVerion.Format(_T("SimplyLiveRTMPServer-%s"), VERSION_NUMVER_U);
	SetWindowText(strVerion);
	
	if (Config::getInstance()->isHD())
		m_videoframe = new VFrame_1920_1080();
	else if (Config::getInstance()->is4K())
		m_videoframe = new VFrame_3840_2160();
	else if (Config::getInstance()->is720p())
		m_videoframe = new VFrame_1280_720();
	m_audioframe =new AudioFrame;
	m_audioframe->SetSampleCnt(1920);

	CapturePoolMgr::GetInstance()->initialize(Config::getInstance()->getVideoFormat(), MAX_PAYER * 100);
	IFrameConsumerManager::getInstance()->initialize();

	ChannelMask nRecMask = Config::getInstance()->getRecChannelMask();
	string str;
	for (int _cnl = 0; _cnl < MAX_PAYER; ++_cnl)
	{
		str = Config::getInstance()->getInputAddress(_cnl);
		if (!str.empty() && nRecMask[_cnl])
			m_prvider[_cnl].addChannel(_cnl, str, this);
		USES_CONVERSION;
		m_comRecorders.InsertString(_cnl, A2W(str.c_str()));
	}
	m_loopBufferAudio.InitBuffer(MAX_AUDIO_SIZE);
	if (!InitSDL())
	{
		AfxMessageBox(_T("INIT SDL failed."));
		return FALSE;
	}
	m_dwSel = 0;
	m_comRecorders.SetCurSel(m_dwSel);
	m_pMasterClock = IMasterClock::CreateInstance(false);
	m_pMasterClock->start(Config::getInstance()->getFramesPerSec());
	SetTimer(0, 100, nullptr);
	startThread();
	m_curLoop = 0;
	m_bStopPreview = false;
	m_updateUIResult = async(launch::async, &CSimplyLiveRTMPServer_DLGDlg::_updateUIThread, this);
	WriteLogA(SimplyLiveRTMPServerLog, InfoLevel,"SimplyLiveRTMPServer start. version(%s)", VERSION_NUMVER_A);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSimplyLiveRTMPServer_DLGDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSimplyLiveRTMPServer_DLGDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSimplyLiveRTMPServer_DLGDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CSimplyLiveRTMPServer_DLGDlg::OnClose()
{
	m_bStopPreview = true;
	stopThread(2000);
	KillTimer(0);
	IFrameConsumerManager::releaseInstance();	
	if (m_updateUIResult.valid())
		m_updateUIResult.wait();
	m_pMasterClock->stop();
	m_pMasterClock->ReleaseInstance(m_pMasterClock);
	//if (m_hTimerRender != nullptr)
	//{
	//	DeleteTimerQueueTimer(nullptr, m_hTimerRender, INVALID_HANDLE_VALUE);
	//	m_hTimerRender = nullptr;
	//}

	SDL_CloseAudio();
	WriteLogA(SimplyLiveRTMPServerLog, InfoLevel, "SimplyLiveRTMPServer SDL_CloseAudio finished.");
	SDL_Quit();
	WriteLogA(SimplyLiveRTMPServerLog, InfoLevel, "SimplyLiveRTMPServer SDL_Quit finished.");
	// TODO: Add your message handler code here and/or call default
	for (int n = 0; n < MAX_PAYER; ++n)
	{
		m_prvider[n].removeChannel(n);
		WriteLogA(SimplyLiveRTMPServerLog, InfoLevel,"SimplyLiveRTMPServer removeChannel(%d) finished.");
	}
	CDialogEx::OnClose();
}


void CSimplyLiveRTMPServer_DLGDlg::_updateUIThread()
{
	CTimeCount tsTotal;
	float timeUpdateUI;
	while (!m_bStopPreview)
	{
		if (!m_updateUIEvent.waitEvent())
			continue;
		tsTotal.reset();

		AudioFrame::get2AudioCnl(m_audioframe->getSampleCount(), (long *)m_audioframe->getRaw(), (long *)m_szConvertAudio, 0, 1);
		AddAudio(m_szConvertAudio, m_audioframe->getDataSize() / 8);

		//SDL---------------------------  
		SDL_UpdateTexture(sdlTexture, NULL, m_videoframe->getRaw(), 2 * Config::getInstance()->getVideoWidth());
		SDL_RenderClear(sdlRenderer);
		//SDL_RenderCopy(sdlRenderer, sdlTexture, &sdlRect, &sdlRect);
		SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, NULL);
		SDL_RenderPresent(sdlRenderer);

		timeUpdateUI = tsTotal.getCountTime();

		if (timeUpdateUI >= Config::getInstance()->getInterval())
		{
			WriteLogA(SimplyLiveRTMPServerLog, WarnLevel, "CurLoop(%I64d) timeUpdateUI %.2f", m_curLoop,timeUpdateUI);
		}
	}
	WriteLogA(SimplyLiveRTMPServerLog, InfoLevel, "SimplyLiveRTMPServer _updateUIThread stopped.");

}

void CSimplyLiveRTMPServer_DLGDlg::excutePlayOut(uint32_t _cnl)
{
	static ChannelMask pgmCnlMask = Config::getInstance()->getPGMChannelMask();
	if (m_videoList[_cnl].empty())
	{
		++m_novVideo[_cnl];
		return;
	}
	VideoFrame* videoframe = m_videoList[_cnl].front();
	m_videoList[_cnl].pop_front();
	AudioFrame* audioframe = m_audioList[_cnl].front();
	m_audioList[_cnl].pop_front();
	if (pgmCnlMask[_cnl])
	{
		if (IFrameConsumer::getInstance()->OutputFrames(_cnl, videoframe, audioframe, nullptr) != 0)
			WriteLogW(SimplyLiveRTMPServerLog, ErrorLevel, _T("GestTrain::playoutToKeyPlay(%d) OutputFrames Error."), _cnl);
	}
	if (_cnl == m_dwSel)
	{
		memcpy(m_videoframe->getRaw(), videoframe->getRaw(), videoframe->getRawSize());

		m_audioframe->SetSampleCnt(audioframe->getSampleCount());
		memcpy(m_audioframe->getRaw(), audioframe->getRaw(), audioframe->getDataSize());
		m_updateUIEvent.raiseEvent();
	}

	CapturePoolMgr::GetInstance()->release(audioframe);
	CapturePoolMgr::GetInstance()->release(videoframe);
}

bool CSimplyLiveRTMPServer_DLGDlg::InitSDL()
{
	screen_w = Config::getInstance()->getVideoWidth() / 2;
	screen_h = Config::getInstance()->getVideoHeight() / 2;
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER))
	{
		WriteLogA(SimplyLiveRTMPServerLog, ErrorLevel,
			"Could not initialize SDL - %s.", SDL_GetError());
		return false;
	}
	//char szTitile[MAX_PATH];
	//sprintf_s(szTitile, "(V:%s)--%s", VERSION_NUMVER, url);
	//(void *)( GetDlgItem(IDC_STATIC1)-
	//screen = SDL_CreateWindow(szTitile, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,screen_w, screen_h, SDL_WINDOW_OPENGL);
	screen = SDL_CreateWindowFrom((void *)GetDlgItem(IDC_STATIC_1)->GetSafeHwnd());// (GetDlgItem(IDC_STATIC)->GetSafeHwnd()));
	//screen = SDL_CreateWindow("test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screen_w, screen_h, SDL_WINDOW_OPENGL);
	if (!screen)
	{
		WriteLogA(SimplyLiveRTMPServerLog, ErrorLevel,
			"SDL: could not create window - exiting:%s", SDL_GetError());
		return false;
	}
	sdlRenderer = SDL_CreateRenderer(screen, -1, 0);
	sdlTexture = SDL_CreateTexture(sdlRenderer, SDL_PIXELFORMAT_UYVY, SDL_TEXTUREACCESS_STREAMING, Config::getInstance()->getVideoWidth(),
		Config::getInstance()->getVideoHeight());
	sdlRect.x = 0;
	sdlRect.y = 0;
	sdlRect.w = screen_w;
	sdlRect.h = screen_h;

	//for  audio
	wanted_spec.freq = 48000;
	wanted_spec.format = AUDIO_S32LSB;
	wanted_spec.channels = 2;
	wanted_spec.silence = 0;
	wanted_spec.samples = 1920;
	wanted_spec.callback = fill_audio;
	wanted_spec.userdata = this;
	int nRes = SDL_OpenAudio(&wanted_spec, NULL);
	if (nRes < 0)
	{
		WriteLogA(SimplyLiveRTMPServerLog, ErrorLevel, "SimplyLiveRTMPServer SDL_OpenAudio failed.");
		return -1;
	}
	//m_nAudioWritePos = 0;
	//m_nAudioReadPos = 0;
	//m_nAudioReadTotal = 0;
	//m_nAudioWriteTotal = 0;

	SDL_PauseAudio(0);
	//m_audioPlayer.Start();	
	//video_tid = SDL_CreateThread(sfp_refresh_thread, NULL, this);
	//CreateTimerQueueTimer(&m_hTimerRender, NULL, SendVideoCB, (void*)this, 0, 40, WT_EXECUTEDEFAULT);
	return true;
}

void CSimplyLiveRTMPServer_DLGDlg::cb(DWORD _channelID, VideoFrame* pFrameVideo /*= nullptr*/, AudioFrame* pFrameAudio /*= nullptr*/)
{
	if (m_bDumpUYVY && _channelID == m_dwSel)
	{
		char path[MAX_PATH];
		sprintf_s(path, "C:\\Logs\\SimplyLiveRTMPServer\\Cam(%c)-%I64d-%dX%d.yuv", 'A' + m_dwSel, m_curLoop,
			Config::getInstance()->getVideoWidth(), Config::getInstance()->getVideoHeight());
		FILE*  f = nullptr;
		fopen_s(&f, path, "wb");
		if (f)
		{
			fwrite(pFrameVideo->getRaw(), 1, pFrameVideo->getRawSize(), f);
			fclose(f);
		}
		m_bDumpUYVY = false;
	}
	m_audioList[_channelID].push_back(pFrameAudio);
	m_videoList[_channelID].push_back(pFrameVideo);
}
/*
VOID CALLBACK CSimplyLiveRTMPServer_DLGDlg::SendVideoCB(
	_In_ PVOID lpParameter,
	_In_ BOOLEAN TimerOrWaitFired
)
{
	CSimplyLiveRTMPServer_DLGDlg* pthis = (CSimplyLiveRTMPServer_DLGDlg*)lpParameter;
	pthis->SendOneVideoFrm();
}
void CSimplyLiveRTMPServer_DLGDlg::SendOneVideoFrm()
{
	CTimeCount tsTotal, tsV, tsA;
	tsTotal.reset();
	float ftsTotal = 0, ftsV = 0, ftsA = 0;
	CString strSize;
	strSize.Format(_T("%d"), m_videoList.size());
	((CWnd *)GetDlgItem(IDC_EDIT_LISTSIZE))->SetWindowText(strSize);
	if (!m_videoList.empty())
	{
		
		VideoFrame* videoframe = m_videoList.front();
		m_videoList.pop_front();
		AudioFrame* audioframe = m_audioList.front();
		m_audioList.pop_front();
		tsA.reset();
		AudioFrame::get2AudioCnl(audioframe->getSampleCount(), (long *)audioframe->getRaw(), (long *)szAudioTemp, 0, 1);
		AddAudio(szAudioTemp, audioframe->getDataSize() / 8);
		CapturePoolMgr::GetInstance()->release(audioframe);
		ftsA = tsA.getCountTime();
		tsV.reset();
		//SDL---------------------------  
		SDL_UpdateTexture(sdlTexture, NULL, videoframe->getRaw(), 2 * Config::getInstance()->getVideoWidth());
		CapturePoolMgr::GetInstance()->release(videoframe);
		SDL_RenderClear(sdlRenderer);
		//SDL_RenderCopy(sdlRenderer, sdlTexture, &sdlRect, &sdlRect);
		SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, NULL);
		SDL_RenderPresent(sdlRenderer);
		ftsV = tsV.getCountTime();
	}
	ftsTotal = tsTotal.getCountTime();

	if (ftsTotal > 40)
	{
		WriteLogA(L"e:\\test.log",WarnLevel ,"ftsTotal %.2f,ftsV %.2f,ftsA %.2f", ftsTotal, ftsV, ftsA);
	}
}
*/

void CSimplyLiveRTMPServer_DLGDlg::callBack()
{
	bool bOutIsAja = Config::getInstance()->getOutTypeIsAja();
	bool bHasAjaOut = IFrameConsumerManager::getInstance()->hasPlayout();
	bool bUseAja = bOutIsAja && bHasAjaOut;
	CTimeCount timer;
	ChannelMask recMask = Config::getInstance()->getRecChannelMask();

	while (isRunning())
	{
		bUseAja ? IFrameConsumerManager::getInstance()->WaitGenlockInterrupt():m_pMasterClock->waitEventClock();
		for (int _cnl = 0; _cnl < MAX_PAYER; ++_cnl)
		{
			if (recMask[_cnl])
				m_prvider[_cnl].frameConsumed();
		}
		timer.reset();
		for (size_t _cnl = 0; _cnl < MAX_PAYER; _cnl++)
		{
			if (recMask[_cnl])
				excutePlayOut(_cnl);
		}
		++m_curLoop;
		float floopTotal = timer.getCountTime();
		if (floopTotal > Config::getInstance()->getInterval())
			WriteLogW(SimplyLiveRTMPServerLog, WarnLevel, L"_PlayOutThread (%I64d) cost %.2f", m_curLoop,floopTotal);
		
	}	
	WriteLogA(SimplyLiveRTMPServerLog, InfoLevel, "SimplyLiveRTMPServer callBack stopped.");
}



int CSimplyLiveRTMPServer_DLGDlg::sfp_refresh_thread(void *opaque)
{
	CSimplyLiveRTMPServer_DLGDlg *pThis = (CSimplyLiveRTMPServer_DLGDlg*)opaque;
	pThis->thread_exit = 0;
	pThis->thread_pause = 0;

	while (!pThis->thread_exit)
	{
		if (!pThis->thread_pause)
		{
			SDL_Event event;
			event.type = SFM_REFRESH_EVENT;
			SDL_PushEvent(&event);
		}
		SDL_Delay(40);
	}
	pThis->thread_exit = 0;
	pThis->thread_pause = 0;
	//Break  
	SDL_Event event;
	event.type = SFM_BREAK_EVENT;
	SDL_PushEvent(&event);

	return 0;
}

void CSimplyLiveRTMPServer_DLGDlg::fill_audio(void *udata, Uint8 *stream, int len)
{
	static uint8_t buftemp[30 * 1024];
	int nTrueSize = 0;
	SDL_memset(stream, 0, len);
	nTrueSize = ((CSimplyLiveRTMPServer_DLGDlg*)udata)->fillAudioData(buftemp, len);
	if (nTrueSize == 0)
		return;
	SDL_MixAudio(stream, buftemp, len, SDL_MIX_MAXVOLUME);
	/*
	//SDL 2.0
	SDL_memset(stream, 0, len);
	if (audio_len == 0)
	return;

	len = (len > audio_len ? audio_len : len);	//  Mix  as  much  data  as  possible

	SDL_MixAudio(stream, audio_pos, len, SDL_MIX_MAXVOLUME);
	audio_pos += len;
	audio_len -= len;
	*/
}

void CSimplyLiveRTMPServer_DLGDlg::AddAudio(BYTE *pAudio, int nSize)
{
	m_loopBufferAudio.WriteBuffer(pAudio, nSize);
}

int CSimplyLiveRTMPServer_DLGDlg::fillAudioData(uint8_t* buf, int size)
{
	return m_loopBufferAudio.ReadBuffer(buf, size);
}


void CSimplyLiveRTMPServer_DLGDlg::OnCbnSelchangeCombo3()
{
	// TODO: Add your control notification handler code here
  	while (!m_videoList[m_dwSel].empty())
  	{
  		VideoFrame* videoframe = m_videoList[m_dwSel].front();
  		m_videoList[m_dwSel].pop_front();
  		AudioFrame* audioframe = m_audioList[m_dwSel].front();
  		m_audioList[m_dwSel].pop_front();
  
  		CapturePoolMgr::GetInstance()->release(videoframe);
  		CapturePoolMgr::GetInstance()->release(audioframe);
  	}
	m_dwSel = m_comRecorders.GetCurSel();
}


void CSimplyLiveRTMPServer_DLGDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	CString strSize;
	for (int n=0; n < MAX_PAYER;++n)
	{
		strSize.Format(_T("%d"), m_videoList[n].size());
		((CWnd *)GetDlgItem(IDC_EDIT_LISTSIZE_0+n))->SetWindowText(strSize);

		strSize.Format(_T("%I64d"), m_novVideo[n]);
		((CWnd *)GetDlgItem(IDC_EDIT_NOVIDEO_0 + n))->SetWindowText(strSize);

	}	
	__super::OnTimer(nIDEvent);
}


BOOL CSimplyLiveRTMPServer_DLGDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_CONTROL:
			m_bDumpUYVY = true;
			break;
		}
	}
	return __super::PreTranslateMessage(pMsg);
}
