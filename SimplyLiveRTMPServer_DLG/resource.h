//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SimplyLiveRTMPServer_DLG.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SIMPLYLIVERTMPSERVER_DLG_DIALOG 102
#define IDR_MAINFRAME                   128
#define IDC_STATIC_1                    1000
#define IDC_STATIC_2                    1001
#define IDC_STATIC_3                    1002
#define IDC_STATIC_4                    1003
#define IDC_COMBO2                      1006
#define IDC_COMBO3                      1007
#define IDC_EDIT_LISTSIZE_0             1009
#define IDC_EDIT_LISTSIZE_2             1010
#define IDC_EDIT_LISTSIZE_1             1011
#define IDC_EDIT_LISTSIZE_3             1012
#define IDC_EDIT_LISTSIZE_4             1013
#define IDC_EDIT_LISTSIZE_5             1014
#define IDC_EDIT_LISTSIZE_6             1015
#define IDC_EDIT_LISTSIZE_7             1016

#define IDC_EDIT_NOVIDEO_0              1020
#define IDC_EDIT_NOVIDEO_1              1021
#define IDC_EDIT_NOVIDEO_2              1022
#define IDC_EDIT_NOVIDEO_3              1023
#define IDC_EDIT_NOVIDEO_4              1024
#define IDC_EDIT_NOVIDEO_5              1025
#define IDC_EDIT_NOVIDEO_6              1026
#define IDC_EDIT_NOVIDEO_7              1027

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
