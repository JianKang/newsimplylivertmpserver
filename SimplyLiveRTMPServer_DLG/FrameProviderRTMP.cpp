#include "stdafx.h"
#include "FrameProviderRTMP.h"
#include "../lib.RTMPCore/RTMPDataReceiver.h"
#include "../lib.RTMPCore/RTMPCore.h"

FrameProviderRTMP::FrameProviderRTMP()
{
	m_pMediaStream = nullptr;
	m_RTMPDataReceiver = nullptr;
}


FrameProviderRTMP::~FrameProviderRTMP()
{
	_stop();
}

int FrameProviderRTMP::addChannel(DWORD dwCnlID, const string &strUrl, IFPInputCallBack* _pGetFrameCB)
{
	swprintf_s(m_szLogFile, _T("C:\\Logs\\SimplyLiveRTMPServer\\FrameProviderRTMP_%d.Log"), dwCnlID);
	if (m_pMediaStream == nullptr)
		m_pMediaStream = new CMediaStream(dwCnlID, strUrl, _pGetFrameCB, m_szLogFile);// _pGetFrameCB);
	if (m_RTMPDataReceiver == nullptr)
	{
		m_RTMPDataReceiver = new CRTMPDataReceiver(m_pMediaStream, m_szLogFile);
		if (!m_RTMPDataReceiver->InitChannel(dwCnlID, strUrl))
		{
			return -1;
		}
	}
	CRTMPCore::getInstance()->AddChannel(dwCnlID, strUrl, m_RTMPDataReceiver);
	return 0;
}

int FrameProviderRTMP::removeChannel(DWORD dwCnlID)
{
	_stop();
	return 0;
}

void FrameProviderRTMP::frameConsumed()
{
	m_pMediaStream->frameConsumed();
}

void FrameProviderRTMP::_stop()
{
	CRTMPCore::getInstance()->releaseInstance();
	if (m_pMediaStream != nullptr)
	{
		delete m_pMediaStream;
		m_pMediaStream = nullptr;
	}
	if (m_RTMPDataReceiver != nullptr)
	{
		delete m_RTMPDataReceiver;
		m_RTMPDataReceiver = nullptr;
	}
}
