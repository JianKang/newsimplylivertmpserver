
// SimplyLiveRTMPServer_DLG.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CSimplyLiveRTMPServer_DLGApp:
// See SimplyLiveRTMPServer_DLG.cpp for the implementation of this class
//

class CSimplyLiveRTMPServer_DLGApp : public CWinApp
{
public:
	CSimplyLiveRTMPServer_DLGApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CSimplyLiveRTMPServer_DLGApp theApp;