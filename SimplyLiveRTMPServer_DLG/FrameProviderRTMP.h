#pragma once
#include <string>
using namespace std;
#include "../lib.RTMPCore/IFPInputCallBack.h"
#include "../lib.RTMPCore/MediaStream.h"
#include "../lib.RTMPCore/IRTMPDataReceiver.h"


class FrameProviderRTMP
{
public:
	FrameProviderRTMP();
	~FrameProviderRTMP();

	int		addChannel(DWORD dwCnlID, const string &strUrl, IFPInputCallBack* _pGetFrameCB);
	int		removeChannel(DWORD dwCnlID);
	void	frameConsumed();
private:
	IDecodedDataCallBack *  m_pMediaStream;
	IRTMPDataReceiver*		m_RTMPDataReceiver;
	TCHAR		m_szLogFile[MAX_PATH];

	void _stop();
};

