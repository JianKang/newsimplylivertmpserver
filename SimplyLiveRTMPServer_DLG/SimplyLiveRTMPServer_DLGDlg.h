
// SimplyLiveRTMPServer_DLGDlg.h : header file
//

#pragma once
#include "FrameProviderRTMP.h"
#include "../lib.RTMPCore/IFPInputCallBack.h"
#include "../Lib.Base/SyncList.h"
#include "../Lib.Base/EasyThread.h"
#include "../lib.Base/IMasterClock.h"
#include "../lib.Base/eventClock.h"

#define __STDC_CONSTANT_MACROS  

#ifdef _WIN32  
//Windows  
extern "C"
{
#include "libavcodec/avcodec.h"  
#include "libavformat/avformat.h"  
#include "libswscale/swscale.h"  
#include "libavutil/imgutils.h"  
#include "SDL2/SDL.h"  
};
#else  
//Linux...  
#ifdef __cplusplus  
extern "C"
{
#endif  
#include <libavcodec/avcodec.h>  
#include <libavformat/avformat.h>  
#include <libswscale/swscale.h>  
#include <libavutil/imgutils.h>  
#include <SDL2/SDL.h>  
#ifdef __cplusplus  
};
#endif  
#endif  
#include "afxwin.h"

//Refresh Event  
#define SFM_REFRESH_EVENT  (SDL_USEREVENT + 1)  
#define SFM_BREAK_EVENT  (SDL_USEREVENT + 2) 

// CSimplyLiveRTMPServer_DLGDlg dialog
class CSimplyLiveRTMPServer_DLGDlg : public CDialogEx, public IFPInputCallBack, public EasyThread
{
// Construction
public:
	CSimplyLiveRTMPServer_DLGDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SIMPLYLIVERTMPSERVER_DLG_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	void cb(DWORD _channelID, VideoFrame* pFrameVideo = nullptr, AudioFrame* pFrameAudio = nullptr) override;
	void callBack() override;
	//static VOID CALLBACK SendVideoCB(_In_ PVOID lpParameter, _In_ BOOLEAN TimerOrWaitFired);
	//void SendOneVideoFrm();
	//HANDLE                   m_hTimerRender;
// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
private:
	//Player  m_player[MAX_PAYER];
	FrameProviderRTMP m_prvider[MAX_PAYER];
	DWORD	m_dwSel = 0;
	UINT64	m_curLoop;
public:
	afx_msg void OnClose();

	SemaphoreClock	m_eventPlayout_PGM;
	EventClock m_updateUIEvent;
	bool m_bStopPreview;
	future<void> m_updateUIResult;
	void _updateUIThread();

	void excutePlayOut(uint32_t _cnl);

	IMasterClock*   m_pMasterClock;
	UINT64			m_novVideo[MAX_PAYER];
	SyncList<VideoFrame*>			m_videoList[MAX_PAYER];
	SyncList<AudioFrame*>			m_audioList[MAX_PAYER];

	VideoFrame* m_videoframe;
	AudioFrame* m_audioframe;

	bool InitSDL();
	//for video
	int screen_w, screen_h;
	SDL_Window *screen;
	SDL_Renderer* sdlRenderer;
	SDL_Texture* sdlTexture;
	SDL_Rect sdlRect;
	SDL_Thread *video_tid;
	SDL_Event event;
	static int sfp_refresh_thread(void *opaque);
	int thread_exit = 0;
	int thread_pause = 0;

	//AudioPlayer           m_audioPlayer;
	//for audio
	//Buffer:
	//|-----------|-------------|
	//chunk-------pos---len-----|
	static  Uint8  *audio_chunk;
	static  Uint32  audio_len;
	static  Uint8  *audio_pos;
	
	SDL_AudioSpec wanted_spec;
	static void  fill_audio(void *udata, Uint8 *stream, int len);
	bool	m_bDumpUYVY;
	//
	//BYTE *m_pbufAudio;
	BYTE *m_szConvertAudio;
	CLoopBuffer m_loopBufferAudio;
	const int MAX_AUDIO_SIZE = 4 * 1024 * 1024;
	//UINT64 m_nAudioWritePos = 0;
	//UINT64 m_nAudioReadPos = 0;
	//UINT64 m_nAudioReadTotal = 0;
	//UINT64 m_nAudioWriteTotal = 0;
	void AddAudio(BYTE *pAudio, int nSize);
	//Locker m_audiolock;
	int fillAudioData(uint8_t* buf, int size);
	CComboBox m_comRecorders;
	afx_msg void OnCbnSelchangeCombo3();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};