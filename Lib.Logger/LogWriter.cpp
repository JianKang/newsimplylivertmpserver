#include "stdafx.h"
#include "LogWriter.h"
#include "Shlwapi.h"
#include "../Lib.Base/locker.h"
#include <filesystem>
#include "log4cplus/logger.h"
#include "log4cplus/fileappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/layout.h"


using namespace log4cplus;
using namespace helpers;
using namespace spi;
using namespace std;
using namespace experimental::filesystem;
#define LOGSIZE         100*1024

static bool enableDebugLog = false;

void getLogConfig(long &nSize, int &nNum, bool &_enableDebugLog)
{
	nSize = (10*1024 * 1024);
	nNum = 5;
	_enableDebugLog = 0;
}

void WriteLogW(TCHAR * logPath, LOGLEVEL  lLogLevel, const WCHAR * fmt, ...)
{
	if (lLogLevel == DebugLevel)
	{
		if (!enableDebugLog)
		{
			return;
		}
	}

	static Locker m_mutex;
	static AppenderAttachableImpl _append_list;
	TCHAR szBuffer[LOGSIZE];

	SharedAppenderPtr appenderPtr;
	{
		LockHolder locker(m_mutex);
		appenderPtr = _append_list.getAppender(logPath);
		if (nullptr == appenderPtr)
		{
			path dir(logPath);
			dir.remove_filename();
			if (!exists(dir))
			{
				if (!create_directories(dir))
				{
					return;
				}
			}

			long  nSize = 0;
			int   nNum = 0;
			getLogConfig(nSize, nNum, enableDebugLog);

			appenderPtr = new RollingFileAppender(logPath, nSize, nNum);
			appenderPtr->setName(logPath);
			tstring pattern = LOG4CPLUS_TEXT("%D{%y/%m/%d %H:%M:%S.%q} [%-5t][%-5p]- %m%n");

			appenderPtr->setLayout(auto_ptr<Layout>(new PatternLayout(pattern)));
			_append_list.addAppender(appenderPtr);
		}
	}

	va_list marker;
	va_start(marker, fmt);
	_vsntprintf_s(szBuffer, size(szBuffer), _TRUNCATE, fmt, marker);
	va_end(marker);

	InternalLoggingEvent event(
		Logger::getInstance(LOG4CPLUS_TEXT("ID4TV")).getName(),
		lLogLevel, szBuffer, nullptr, 0);

	appenderPtr->doAppend(event);
}

void WriteLogA(IN TCHAR * logPath, IN LOGLEVEL lLogLevel, IN const CHAR * fmt, ...)
{
	if (lLogLevel == DebugLevel)
	{
		if (!enableDebugLog)
		{
			return;
		}
	}

	va_list marker;
	char szBuffer[LOGSIZE / 2] = { 0 };

	va_start(marker, fmt);
	_vsnprintf_s(szBuffer, size(szBuffer), _TRUNCATE, fmt, marker);
	va_end(marker);

	WCHAR wLogBuf[LOGSIZE];
	wmemset(wLogBuf, 0, LOGSIZE);
	size_t size = 0;
	if (mbstowcs_s(&size, wLogBuf, szBuffer, strlen(szBuffer)) == 0)
	{
		WriteLogW(logPath, lLogLevel, wLogBuf);
	}
}

string SafeA(CHAR * buf)
{
	if (buf == nullptr)
	{
		return "";
	}
	string s1(buf);
	string::size_type pos = 0;
	string toPlace("%%");
	while ((pos = s1.find('%', pos)) != string::npos)
	{
		s1.replace(pos, 1, toPlace);
		pos += 2;
	}
	return s1;
}

wstring SafeW(WCHAR * buf)
{
	if (buf == nullptr)
	{
		return L"";
	}
	wstring s1(buf);
	wstring::size_type pos = 0;
	wstring toPlace(L"%%");
	while ((pos = s1.find(L'%', pos)) != wstring::npos)
	{
		s1.replace(pos, 1, toPlace);
		pos += 2;
	}
	return s1;
}