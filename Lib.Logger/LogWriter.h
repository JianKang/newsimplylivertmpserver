#pragma once
#include <tchar.h>
#include <windows.h>
#include <string>
#include <wtypes.h>

#define SAFEA(str) (SafeA(str).c_str())
#define SAFEW(str) (SafeW(str).c_str())

enum LOGLEVEL
{
	DebugLevel = 10000,
	InfoLevel = 20000,
	WarnLevel = 30000,
	ErrorLevel = 40000,
	FatalLevel = 50000,
};

void WriteLogA(IN TCHAR * logPath, IN LOGLEVEL  lLogLevel, IN const CHAR * fmt, ...);
void WriteLogW(IN TCHAR * logPath, IN LOGLEVEL  lLogLevel, IN const WCHAR * fmt, ...);

std::string SafeA(CHAR * buf);
std::wstring SafeW(WCHAR * buf);
